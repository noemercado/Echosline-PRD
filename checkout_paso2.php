<?php
if(!isset($_COOKIE["ech_lg"]))
  header("Location: checkout_paso1.php");
require_once("server/api.php");

require __DIR__ . '/vendor/autoload.php';

require_once("server/ppp/functions.php");

function eliminarCuponesCookie1(){
  if(isset($_COOKIE["cupon"])){
    global $cookieExp;
    setcookie("cupon","",$cookieExp * -1,"/");
      unset($_COOKIE["cupon"]);
  }
}

function infoCupon1($id){
  try {
    $conexion_prosalon = PDOConnection::getConnection();
    //global $conexion_prosalon;
    //$conn =  PDOConnection2::getConnection();
    $sql = "SELECT * FROM cupones_full WHERE ID = :id AND Fecha_venc >= :fecha
              AND Fecha_inicio <= :fecha
              AND STATUS = 'Activo'";
    $stm = $conexion_prosalon->prepare($sql);
    $d = time();
    $fecha = date("Y-m-d", $d);
    $stm->bindParam(":id",$id);
    $stm->bindParam(":fecha",$fecha);
    $stm->execute();
    $num = $stm->rowCount();
    if($num > 0){
      $row = $stm->fetch(PDO::FETCH_ASSOC);
      /*extract($row);
      if($Tipo == "Promocion")*/
      return $row;
    }
  } catch (Exception $e) {
    echo $e->getMessage();
    return false;
  }
  $conexion_prosalon = null;
  return false;
}

function aplicarCupon1($idCupon){
  $cupon = infoCupon1($idCupon);
  global $cookieExp;
  if($cupon){
    $carrito = get_cart();
    $monto = $cupon["Monto"];
    $modo = $cupon["Modo"];
    $vencimiento = $cupon["Fecha_venc"];
    if($carrito["code"] == 400){
      $response["code"] = 400;
      $response["msg"] = "No hay productos en tu carrito";
    }else{
      $productos = $carrito["itemsCart"];
      $response["code"] = 200;
      $cupon = array("id"=>$idCupon,"monto"=>$monto,"modo"=>$modo,"vencimiento"=>$vencimiento,"gral"=>false);
      setcookie("cupon",json_encode($cupon),$cookieExp,"/");
      return true;
      
    }
  }else{
    $response["code"] = 400;
    $response["msg"] = "Cupón inválido";
    eliminarCuponesCookie1();
  }
  return false;
}


function conteoCuponGeneral1($id){
    try {
        $conexion_prosalon = PDOConnection::getConnection();
        //$conn =  PDOConnection2::getConnection();
        $sql = "SELECT COUNT(*) AS conteo FROM Ventas WHERE idCupon = :id";
        $stm = $conexion_prosalon->prepare($sql);
        $stm->bindParam(":id",$id);
        $stm->execute();
        $num = $stm->rowCount();
        if($num > 0){
            $row = $stm->fetch(PDO::FETCH_ASSOC);
            //return $row;
            extract($row);
            return $conteo;
            /*if($Tipo == "Promocion")
                return $row;*/
        }
    } catch (Exception $e) {
        return 0;
    }

    return 0;
}

function infoCuponGeneral1($id){
    try {
        $conexion_prosalon = PDOConnection::getConnection();
        //$conn =  PDOConnection2::getConnection();
        $sql = "SELECT * FROM cupones_full WHERE ID = :id AND Fecha_venc >= :fecha
                AND Fecha_inicio <= :fecha
                AND STATUS = 'Activo'
                AND idCliente_menudeo IS NULL";
        $stm = $conexion_prosalon->prepare($sql);
        $d = time();
        $fecha = date("Y-m-d", $d);
        $stm->bindParam(":id",$id);
        $stm->bindParam(":fecha",$fecha);
        $stm->execute();
        $num = $stm->rowCount();
        if($num > 0){
            $row = $stm->fetch(PDO::FETCH_ASSOC);
            return $row;
            //extract($row);
            /*if($Tipo == "Promocion")
                return $row;*/
        }
    } catch (Exception $e) {
        return false;
    }

    return false;
}

function validarCuponGeneral1($cup){
    $cupon = $cup;
    /*condicion 1: verificar la fecha de vigencia del cupon*/
    $info = infoCuponGeneral1($cupon); // contiene todo lo del cupon, la cantidad por ejemplo, para el siguiente paso
    if($info){
        /*condicion 2: contar las veces que se ha aplicado para saber si aún puede aplicarlo*/
        $conteo = conteoCuponGeneral1($cupon);
        if($conteo <= $info["Cantidad"]){
            if($cupon){
                $carrito = get_cart();
                if($carrito["code"] == 400){
                    $respuesta["code"] = 400;
                    $respuesta["msg"] = "No hay productos en tu carrito";
                }else{
                    $respuesta["code"] = 200;
                    global $cookieExp;
                    $cupon = array("id"=>$info["ID"],"monto"=>$info["Monto"],"modo"=>$info["Modo"],"vencimiento"=>$info["Fecha_venc"], "gral"=>true);
                    setcookie("cupon",json_encode($cupon),$cookieExp,"/");
                    return true;
                }
            }else{
                $respuesta["code"] = 400;
                $respuesta["msg"] = "Cupón inválido";
                eliminarCuponesCookie1();
            }
        }else{
            $respuesta["code"] = 400;
            $respuesta["msg"] = "No se puede aplicar el cupón.";
        }
    }else{
        $respuesta["code"] = 400;
        $respuesta["msg"] = "Cupón invalido.";
    }
    return false;
}



if(!isset($_COOKIE["ech_lg"]) && !isset($_COOKIE["ech_lg_tmp"])){
    header("Location: login.html?r=chk");
}
else{
  $base_url="http://".$_SERVER['SERVER_NAME'].dirname($_SERVER["REQUEST_URI"].'?').'/';
  $datos = get_datos_usuario2();
  $usuario = $datos["usuario"];
  //var_dump($usuario);
  $descuentoCliente = $datos["usuario"]["Descuento_%"];
  //echo $descuento;
  //var_dump($datos);

  $error_pago = isset($_GET["collection_status"]);


  MercadoPago\SDK::setAccessToken('APP_USR-4316132444735395-031822-161d41833e9a2aef5249acbfc39cc3d1-535572779');

  //MercadoPago\SDK::setAccessToken('TEST-4316132444735395-031822-e0f7eaa20611305e499b0eda039ee661-535572779');

  $preference = new MercadoPago\Preference();
  $lista = array();
  $itemsPPP = array();
  $item = array();
  $currency = "MXN";
  $resp = get_cart();
  //var_dump($resp);
  if($resp["code"] == 200){
      $subt = 0;
      if(isset($resp["itemsCart"])){
          $cart = $resp["itemsCart"];
          //var_dump($cart);
          foreach ($cart as $key => $value) {
              $item = new MercadoPago\Item();
              $item->id = $value["ID"];
              $item->title = $value["Producto"];
              $item->quantity = $value["qty"];
              $item->unit_price = $value["Precio_publico"];
              $item->description = $value["Descripcion"];
              $item->currency_id = "MXN";
              $itemP = array(
                "name"=>$value["Producto"],
                "description"=>$value["Descripcion"],
                "quantity"=>$value["qty"],
                "price"=>$value["Precio_publico"],
                "sku"=>$value["ID"],
                "currency"=>$currency
              );
              array_push($itemsPPP,$itemP);
              array_push($lista, $item);
              $subt += $value["qty"] * $value["Precio_publico"];
          }
      }

      if(isset($resp["itemsCart_oferta"])){
          $cart_oferta = $resp["itemsCart_oferta"];
          foreach ($cart_oferta as $key => $value) {
              $item = new MercadoPago\Item();
              $item->id = $value["ID"];
              $item->title = $value["Promocion"];
              $item->quantity = $value["qty"];
              $item->unit_price = $value["precio"];
              $item->description = $value["Descripcion"];
              $item->currency_id = "MXN";

              array_push($lista, $item);
              $subt += $value["qty"] * $value["precio"];
          }
      }

      if($subt >= 1000)
        $envio = 0;
      else{
        $envio = 100;
        $item2 = new MercadoPago\Item();
        $item2->id = "envio";
        $item2->title = "envío";
        $item2->quantity = 1;
        $item2->unit_price = $envio;
        $item2->description = "";
        $item2->currency_id = "MXN";

        $itemP = array(
          "name"=>"Envio",
          "description"=>"Envio",
          "quantity"=>1,
          "price"=>"".$envio,
          "sku"=>"env1",
          "currency"=>$currency
        );
        array_push($itemsPPP,$itemP);
        array_push($lista, $item2);
      }

      if($descuentoCliente > 0){
          $descuentoCliente = ($subt * $descuentoCliente)/100;
          $item3 = new MercadoPago\Item();
          $item3->id = "descuento";
          $item3->title = "descuento";
          $item3->quantity = 1;
          $item3->unit_price = ($descuentoCliente * -1);
          $item3->description = "";
          $item3->currency_id = "MXN";
          $itemP = array(
            "name"=>"Descuento Cliente",
            "description"=>"DescuentoCli",
            "quantity"=>1,
            "price"=>($descuentoCliente * -1),
            "sku"=>"desc_cli",
            "currency"=>$currency
          );
          array_push($itemsPPP,$itemP);
          array_push($lista, $item3);
      }
      
      //var_dump($cart);
      
      

      if($datos["code"] == 200){
        $payer = new MercadoPago\Payer();
        $payer->name = $usuario["Nombre"]." ".$usuario["Apellidos"];
        $payer->email = $usuario["Email"];
        $payer->phone = array(
            "area_code" => "",
            "number" => $usuario["Tel1"]
          );

        $calle_num = explode("#",$usuario["Calle_numero"]);
        $calle = $calle_num[0];
        $numeros = explode(" ", $calle_num[1]);
        $exterior = $numeros[0];
        //$interior = $numeros[1];

        $payer->address = array(
            "street_name" => $calle,
            "street_number" => $exterior,
            "zip_code" => $usuario["CP"]
          );

        $preference->payer = $payer;
        //echo "</br>".$datos["idV"];
        $preference->external_reference = $usuario["ID"];
      }

      
      /*$preference->back_urls = array(
          "success" => "http://echosline.mx/prueba/success.php",
          "failure" => "http://echosline.mx/prueba/checkout_paso2.php",
          "pending" => "http://echosline.mx/prueba/success.php"
      );
      $preference->back_urls = array(
          "success" => "http://localhost/sitio_echosline/prueba/success.php",
          "failure" => "http://localhost/sitio_echosline/prueba/checkout_paso2.php",
          "pending" => "http://localhost/sitio_echosline/prueba/success.php"
      );*/
      $preference->back_urls = array(
          "success" => $base_url."success.php",
          "failure" => $base_url."checkout_paso2.php",
          "pending" => $base_url."success.php"
      );

      $preference->auto_return = "all";
      
  }
  //var_dump($resp);
  $descuentoProf = number_format(0,2,'.',',');


  if(isset($_COOKIE["cupon"])){
    $cupon = json_decode($_COOKIE["cupon"],true);

  }

  if(isset($cupon)){
      if(!$cupon["gral"]){
          if(aplicarCupon1($cupon["id"])){
            if($cupon["modo"] == "Porcentaje"){
              $descuentoCupon = floatval(($subt-$descuentoCliente)*(floatval($cupon["monto"])/100));
            }else{
              $descuentoCupon = floatval($cupon["monto"]);
            }
          }else{
            $descuentoCupon = 0;
            eliminarCuponesCookie1();
          }
      }else{
          if(validarCuponGeneral1($cupon["id"])){
            if($cupon["modo"] == "Porcentaje"){
              $descuentoCupon = floatval(($subt-$descuentoCliente)*(floatval($cupon["monto"])/100));
            }else{
              $descuentoCupon = floatval($cupon["monto"]);
            }
          }else{
            $descuentoCupon = 0;
            eliminarCuponesCookie1();
          }
      }
      if($descuentoCupon > 0){
          $item4 = new MercadoPago\Item();
          $item4->id = "descuento";
          $item4->title = "descuento cupon";
          $item4->quantity = 1;
          $item4->unit_price = ($descuentoCupon * -1);
          $item4->description = "";
          $item4->currency_id = "MXN";

          $itemP = array(
            "name"=>"Descuento Cupón",
            "description"=>"DescuentoCupon",
            "quantity"=>1,
            "price"=>($descuentoCupon * -1),
            "sku"=>"desc_cupon",
            "currency"=>$currency
          );
          array_push($itemsPPP,$itemP);
          array_push($lista, $item4);
      }
    
  }else{
    $descuentoCupon = 0;
    eliminarCuponesCookie1();
  }



  $preference->items = $lista;
  $preference->save();

  $subtotal = $subt;

  $total = floatval(($subt) - ($descuentoCliente + $descuentoCupon));

  //$subtotal = floatval($total / 1.16);
  if($subtotal < 0)
    $subtotal = 0;
  //echo $total;


  //echo $subtotal;
  $impuestos = floatval($total - $subtotal);
  if($impuestos < 0)
    $impuestos = 0;


  //echo $impuestos;
  if($total < 0)
    $total = 0;
  //echo $envio;
  $total += $envio;
  $totalPaypal = $total;
  $totalPaypal2 = $total;
  //echo $totalPaypal;
  $total = number_format($total,2,'.',',');
  $subtotal = number_format($subtotal,2,'.',',');
  $impuestos = number_format($impuestos,2,'.',',');
  $descuentoCliente = number_format($descuentoCliente,2,'.',',');
  $descuentoCupon = number_format($descuentoCupon,2,'.',',');
  //echo $envio;
  //$envio = 0;
  $totalPaypal;


  /*PPP*/

  $payerEmail=$usuario['Email'];
  $payerPhone=$usuario['Tel1'];
  $payerFirstName=$usuario['Nombre'];
  $payerLastName=$usuario['Apellidos'];
  $shippingAddressRecipient=$usuario['Nombre']." ".$usuario['Apellidos'];
  $shippingAddressStreet1=$usuario['Calle_numero'];
  $shippingAddressStreet2=$usuario["Colonia"];
  $shippingAddressPostal=$usuario['CP'];
  $shippingAddressCity=$usuario['Municipio'];
  $shippingAddressCountry="MX";
  $shippingAddressState=$usuario['Estado'];
  $disallowRememberedCards = "false";
  //echo $disallowRememberedCards;
  $rememberedCards = FALSE;
  /*$paypalMode="sandbox";
  $clientId="AXX8ermyXZI2tzZFL4s0lDGUNOkq3Xf1Js5WRq3y8lF7K6_VCwV42MRMoggs6vxFs275ipjUoXCEK4gS";
  $secret="EKFGoYrtIRawG7HahI6wF3oDeKKVL-3Aklqicent2s_PHgqj2Fra0aYPuMjx3iqwzudIBoULt4i4kOmn";
  */
  $paypalMode="live";
  $clientId="AQJcoDrAZV_XrXWgjCrB3MbKLx6mImyK_YFMgguEXkcvV9QBDPgq4NUQc-A9K6sl0M7rcQckjN0lXSDx";
  $secret="EF1wskqFPhlt1v9-hC6_j2Jo1_3V_znlEO6BQk11ul0YN-Io_RX0ZEMIZTV7Jbus60esy-5l11Z6how7";

  $respuesta["paypalMode"]=$paypalMode;
  $respuesta["returnUrl"]="http://www.example.com";
  $respuesta["cancelUrl"]="http://www.example.com";

  $respuesta["ppplusJsLibraryLang"]="es_MX";
  $respuesta["currency"]="MXN";
  $respuesta["iframeHeight"]="";
  $respuesta["merchantInstallmentSelection"]="";
  $respuesta["merchantInstallmentSelectionOptional"]=true;


  $totalPaypal = number_format($totalPaypal,2);
  $respuesta["totalPaypal"] = $totalPaypal;

  $returnUrl="http://www.example.com";
  $cancelUrl="http://www.example.com";

  $ppplusJsLibraryLang="es_MX";
  $currency="MXN";
  $iframeHeight="";
  $merchantInstallmentSelection="";
  $merchantInstallmentSelectionOptional=true;

  //$totalPaypal = number_format($totalPaypal,2);

  if ($paypalMode=="sandbox") {
      $host = 'https://api.sandbox.paypal.com';
  }
  if ($paypalMode=="live") {
      $host = 'https://api.paypal.com';
  }
  #GET ACCESS TOKEN

  $url = $host.'/v1/oauth2/token'; 
  $postArgs = 'grant_type=client_credentials';
  $access_token= get_access_token($url,$postArgs);
  #IMPORTANT - Please implement a process to generate a new access token once every 6 hours
  #You must avoid generating a new access token for every transaction/API Call
  #Failing to do so could result in PayPal blocking your account without previous notice.

  #CREATE PAYMENT
  $url = $host.'/v1/payments/payment';
  $payment = '{
    "intent": "sale",
     "application_context": {
          "shipping_preference": "SET_PROVIDED_ADDRESS"
      },
    "payer": {
      "payment_method": "paypal"
    },
    "transactions": [
      {
          "amount": {
          "currency": "'.$currency.'",
          "total": "'.$totalPaypal.'",
          "details": {}
        },
        "description": "Pago de la venta: '.$datos['idV'].'",
        "custom": "ID_venta_'.$datos['idV'].'",
        "payment_options": {
          "allowed_payment_method": "IMMEDIATE_PAY"
        },
        "item_list": {
          "items": '.json_encode($itemsPPP).',
           "shipping_address": {
            "recipient_name": "'.$shippingAddressRecipient.'",
            "line1": "'.$shippingAddressStreet1.'",
            "line2": "'.$shippingAddressStreet2.'",
            "city": "'.$shippingAddressCity.'",
            "country_code": "'.$shippingAddressCountry.'",
            "postal_code": "'.$shippingAddressPostal.'",
            "state": "'.$shippingAddressState.'",
            "phone": "'.$payerPhone.'"
          }
        }
      }
    ],
    "redirect_urls": {
      "return_url": "'.$returnUrl.'",
      "cancel_url": "'.$cancelUrl.'"
    }
  }
  ';

  //var_dump ($payment);
  //die($payment);
  $json_resp = make_post_call($url, $payment);
  //var_dump($json_resp);

  #Get the approval URL for later use
  $approval_url = $json_resp['links']['1']['href'];

  #Get the token out of the approval URL
  $token = substr($approval_url,-20);

  #Get the PaymentID for later use
  $paymentID = ($json_resp['id']);

  $respuesta["json_resp"] = $json_resp;
  $respuesta["approval_url"] = $approval_url;
  $respuesta["token"] = $token;
  $respuesta["paymentID"] = $paymentID;
  #Put JSON in a nice readable format
  $json_resp = stripslashes(json_format($json_resp));

  /*END PPP*/

?>
<!doctype html>
<html class="no-js" lang="zxx">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="meta description">
    <meta name="keywords" content="cabello, decoloracion, alaciado, balayage, nanoplastia,, queratina, hialuronico, frizz, lacio, peinado, maquillaje, piel, seca, piel grasa, tratamiento cabello, cabello maltratado, piel antiedad, matizador, tinte cabello, corte cabello moderno, estilista">
    <!-- Favicons -->
    <link rel="shortcut icon" href="assets/img/favicon.jpg" type="image/x-icon">
    <link rel="apple-touch-icon" href="assets/img/icon.png">

    <!-- Title -->
    <title>ECHOSLINE</title>

    <!-- ************************* CSS Files ************************* -->

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="assets/css/bootstrap.min.css">

    <!-- Font Awesome CSS -->
    <link rel="stylesheet" href="assets/css/font-awesome.min.css">

    <!-- dl Icon CSS -->
    <link rel="stylesheet" href="assets/css/dl-icon.css">

    <!-- All Plugins CSS -->
    <link rel="stylesheet" href="assets/css/plugins.css">

    <!-- Revoulation Slider CSS  -->
    <link rel="stylesheet" href="assets/css/revoulation.css">

    <!-- style CSS -->
    <link rel="stylesheet" href="assets/css/style.css">
    <link rel="stylesheet" href="assets/css/main.css">
    

    <!-- modernizr JS
    ============================================ -->
    <script src="assets/js/vendor/modernizr-2.8.3.min.js"></script>
    <!--[if lt IE 9]>
    <script src="//oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="//oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    
    <!--FONT AWESOME-->
    <script src="https://kit.fontawesome.com/816afb7ec8.js"></script>
    <script src="server/controllers/jquery/dist/jquery.js"></script>
    <script src="server/controllers/header.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@9.3.16/dist/sweetalert2.all.min.js"></script>

    

    <style>
        .swal2-popup{
            font-size: 1.4rem;
        }
        #agregado{
            display: flex;
            justify-content: center;
            align-items: center;
            flex-flow: column;
        }
        #agregado img{
            max-width: 150px;
        }
    </style>

     <!--sandbox
      sb-6y9vd4169174@personal.example.com
      RNv8u3/P
     
    <script src="https://www.paypal.com/sdk/js?client-id=AY9DShNeB9F7ugIRNh5pITSlWke82pJhBRd0q8VpoLb-dKBiYUPgzGKOBaLR_38tzB6C6exkij070cAf&disable-funding=card&currency=MXN"></script>-->

    <!--live-->
    <script src="https://www.paypal.com/sdk/js?client-id=AQJcoDrAZV_XrXWgjCrB3MbKLx6mImyK_YFMgguEXkcvV9QBDPgq4NUQc-A9K6sl0M7rcQckjN0lXSDx&disable-funding=card&currency=MXN"></script>
    
</head>

<body>


    <div class="ai-preloader active">
        <div class="ai-preloader-inner h-100 d-flex align-items-center justify-content-center">
            <div class="ai-child ai-bounce1"></div>
            <div class="ai-child ai-bounce2"></div>
            <div class="ai-child ai-bounce3"></div>
        </div>
    </div>
  
    <!-- Main Wrapper Start -->
    <div class="wrapper">
        <!-- Header Area Start -->
        <header class="header header-fullwidth header-style-4">
            <div class="header-inner fixed-header">
                <div class="container-fluid">
                    <div class="row align-items-center">
                        <div class="col-lg-2 col-md-3 col-4 order-1">
                            <div class="header-left d-flex">
                                <!-- Logo Start Here -->
                                <a href="index.html" class="logo-box">
                                    <figure class="logo--normal"> 
                                        <img src="assets/img/logo/logo.png" alt="Logo"/>   
                                    </figure>
                                    <figure class="logo--transparency">
                                        <img src="assets/img/logo/logo.png" alt="Logo"/>  
                                    </figure>
                                </a>
                                <!-- Logo End Here -->
                            </div>
                        </div>

                        <div class="col-lg-8 order-3 order-lg-2">
                            <!-- Main Navigation Start Here -->
                            <nav class="main-navigation">
                                <ul class="mainmenu mainmenu--centered">
                                    <li class="mainmenu__item">
                                        <a href="index.html" class="mainmenu__link">
                                            <span class="mm-text">INICIO</span>
                                        </a>
                                    </li>
                                    <li class="mainmenu__item menu-item-has-children megamenu-holder">
                                        <a href="linea.html" class="mainmenu__link">
                                            <span class="mm-text">LÍNEAS</span>
                                        </a>
                                        <ul class="megamenu four-column">
                                            <li>
                                                <a class="megamenu-title" href="#">
                                                    <span class="mm-text">LÍNEAS DE PRODUCTO</span>
                                                </a>
                                                <ul>
                                                    <li>
                                                        <a href="linea.html">
                                                            <span class="mm-text">Karbon 9</span>
                                                        </a>
                                                    </li>
                                                    <li>
                                                        <a href="linea-kipower.html">
                                                            <span class="mm-text">Ki-Power</span>
                                                        </a>
                                                    </li>
                                                    <li>
                                                        <a href="linea-keratin.html">
                                                            <span class="mm-text">Seliar Keratin</span>
                                                        </a>
                                                    </li>
                                                    <li>
                                                        <a href="linea-luxury.html">
                                                            <span class="mm-text">Seliar Luxury</span>
                                                        </a>
                                                    </li>
                                                </ul>
                                            </li>
                                            <li>
                                                <a class="megamenu-title" href="#">
                                                    <span class="mm-text">INGREDIENTES</span>
                                                </a>
                                                <ul>
                                                    <li>
                                                        <a href="linea.html">
                                                            <span class="mm-text">Argan</span>
                                                        </a>
                                                    </li>
                                                    <li>
                                                        <a href="linea.html">
                                                            <span class="mm-text">Ácido Hialurónico</span>
                                                        </a>
                                                    </li>
                                                    <li>
                                                        <a href="linea.html">
                                                            <span class="mm-text">Aceites Botánicos</span>
                                                        </a>
                                                    </li>
                                                    <li>
                                                        <a href="linea.html">
                                                            <span class="mm-text">Carbón Activado</span>
                                                        </a>
                                                    </li>
                                                    <li>
                                                        <a href="linea.html">
                                                            <span class="mm-text">Keratina</span>
                                                        </a>
                                                    </li>
                                                </ul>
                                            </li>
                                            <li>
                                                <a class="megamenu-title" href="#">
                                                    <span class="mm-text">TIPO DE PRODUCTO</span>
                                                </a>
                                                <ul>
                                                    <li>
                                                        <a href="linea.html">
                                                            <span class="mm-text">Fluid</span>
                                                        </a>
                                                    </li>
                                                    <li>
                                                        <a href="linea.html">
                                                            <span class="mm-text">Kits</span>
                                                        </a>
                                                    </li>
                                                    <li>
                                                        <a href="linea.html">
                                                            <span class="mm-text">Lotion</span>
                                                        </a>
                                                    </li>
                                                    <li>
                                                        <a href="linea.html">
                                                            <span class="mm-text">Mask</span>
                                                        </a>
                                                    </li>
                                                    <li>
                                                        <a href="linea.html">
                                                            <span class="mm-text">Oil</span>
                                                        </a>
                                                    </li>
                                                    <li>
                                                        <a href="linea.html">
                                                            <span class="mm-text">Serum</span>
                                                        </a>
                                                    </li>
                                                    <li>
                                                        <a href="linea.html">
                                                            <span class="mm-text">Shampoo</span>
                                                        </a>
                                                    </li>
                                                    <li>
                                                        <a href="linea.html">
                                                            <span class="mm-text">Treatment</span>
                                                        </a>
                                                    </li>
                                                </ul>
                                            </li>
                                            <li class="d-none d-lg-block banner-holder">
                                                <div class="megamenu-banner">
                                                    <div class="megamenu-banner-image"></div>
                                                    <div class="megamenu-banner-info" id="contenedorBannerM">
                                                           <h3>Suscríbete<br> y recibe un<br> regalo en tus<br> pedidos<br> mensuales</h3>
                                                        <a class="banner-btn-2" href="shop-sidebar.html">
                                                            <span class="normal-view">SABER MÁS</span>
                                                        </a>
                                                    </div>
                                                    <a href="shop-sidebar.html" class="megamenu-banner-link"></a>
                                                </div>
                                            </li>   
                                        </ul>
                                    </li>
                                    <li class="mainmenu__item">
                                        <a href="catalogo.html" class="mainmenu__link">
                                            <span class="mm-text">TIENDA</span>
                                        </a>
                                    </li>
                                    <li class="mainmenu__item">
                                        <a href="nosotros.html" class="mainmenu__link">
                                            <span class="mm-text">ECHOSLINE</span>
                                        </a>
                                    </li>
                                    <li class="mainmenu__item">
                                        <a href="blog.html" class="menuActivo mainmenu__link">
                                            <span class="mm-text">BLOG</span>
                                        </a>
                                    </li>
                                    <li class="mainmenu__item">
                                        <a href="contacto.html" class="mainmenu__link">
                                            <span class="mm-text">CONTACTO</span>
                                        </a>
                                    </li>
                                </ul>
                            </nav>
                            <!-- Main Navigation End Here -->
                        </div>

                        <div class="col-lg-2 col-sm-4 col-md-9 col-8 order-2 order-lg-3 p-0">
                            <ul class="header-toolbar text-right">
                                <li class="header-toolbar__item">
                                    <div class="col-lg-12 col-md-12 col-12 order-12 p-0">
                                        <ul class="contenedorIdioma">
                                            <li class="es langAct"><a>ES</a></li>
                                            <li class="en"><a>EN</a></li>
                                        </ul>
                                    </div>
                                </li>
                                <li class="header-toolbar__item user-info-menu-btn">
                                    <a href="#">
                                        <i class="fas fa-user"></i>
                                    </a>
                                    <ul class="user-info-menu">
                                        <li>
                                            <a href="my-account.html">Mi cuenta</a>
                                        </li>
                                        <li>
                                            <a href="cart.html">Carrito de compras</a>
                                        </li>
                                        <li>
                                            <a href="checkout.html">Check Out</a>
                                        </li>
                                        <li>
                                            <a href="wishlist.html">Lista de deseos</a>
                                        </li>
                                        <li>
                                            <a href="order-tracking.html">Número de orden</a>
                                        </li>
                                        <li>
                                            <a href="#" id="cerrarSesion" style="display: none;">Cerrar sesión</a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="header-toolbar__item">
                                    <a href="#miniCart" class="mini-cart-btn toolbar-btn">
                                        <i class="fa fa-shopping-cart" aria-hidden="true"></i>
                                        <sup class="mini-cart-count">2</sup>
                                    </a>
                                </li>
                                <li class="header-toolbar__item">
                                    <a href="#searchForm" class="search-btn toolbar-btn">
                                        <i class="dl-icon-search1"></i>
                                    </a>
                                </li>
                                <li class="header-toolbar__item d-lg-none">
                                    <a href="#" class="menu-btn"></a>                 
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </header>
        <!-- Header Area End -->

        <!-- Mobile Header area Start -->
        <header class="header-mobile">
            <div class="container-fluid">
                <div class="row align-items-center">
                    <div class="col-4">
                        <a href="index.html" class="logo-box">
                            <figure class="logo--normal">
                                <img src="assets/img/logo/logo.png" alt="Logo">
                            </figure>
                        </a>
                    </div>
                    <div class="col-8">
                        <ul class="header-toolbar text-right">
                            <li class="header-toolbar__item user-info-menu-btn">
                                <a href="#">
                                    <i class="fas fa-user"></i>
                                </a>
                                <ul class="user-info-menu">
                                    <li>
                                        <a href="my-account.html">Mi cuenta</a>
                                    </li>
                                    <li>
                                        <a href="cart.html">Carrito de compras</a>
                                    </li>
                                    <li>
                                        <a href="checkout.html">Check Out</a>
                                    </li>
                                    <li>
                                        <a href="wishlist.html">Lista de deseos</a>
                                    </li>
                                    <li>
                                        <a href="order-tracking.html">Número de orden</a>
                                    </li>
                                    <li>
                                        <a href="#" id="cerrarSesionMovil" style="display: none;">Cerrar sesión</a>
                                    </li>
                                </ul>
                            </li>
                            <li class="header-toolbar__item">
                                <a href="#miniCart" class="mini-cart-btn toolbar-btn">
                                    <i class="fa fa-shopping-cart" aria-hidden="true"></i>
                                    <sup class="mini-cart-count">2</sup>
                                </a>
                            </li>
                            <li class="header-toolbar__item">
                                <a href="#searchForm" class="search-btn toolbar-btn">
                                    <i class="dl-icon-search1"></i>
                                </a>
                            </li>
                            <li class="header-toolbar__item d-lg-none">
                                <a href="#" class="menu-btn"></a>                 
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="row">
                    <div class="col-12">
                        <!-- Mobile Navigation Start Here -->
                        <div class="mobile-navigation dl-menuwrapper" id="dl-menu">
                            <button class="dl-trigger">Abrir Menú</button>
                            <ul class="dl-menu">
                                <li>
                                    <a href="index.html">
                                        INICIO
                                    </a>
                                </li>
                                <li>
                                    <a href="linea.html">
                                        LÍNEAS
                                    </a>
                                    <ul class="dl-submenu">
                                        <li>
                                            <a class="megamenu-title" href="#">
                                                LÍNEAS DE PRODUCTO
                                            </a>
                                            <ul class="dl-submenu">
                                                <li>
                                                    <a href="linea.html">
                                                        Karbon 9
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="linea-kipower.html">
                                                        KI-Power
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="linea-keratin.html">
                                                        Seliar Keratin
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="linea-luxury.html">
                                                        Seliar Luxury
                                                    </a>
                                                </li>
                                            </ul>
                                        </li>
                                        <li>
                                            <a class="megamenu-title" href="#">
                                                Ingredientes
                                            </a>
                                            <ul class="dl-submenu">
                                                <li>
                                                    <a href="linea.html">
                                                        Argán
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="linea.html">
                                                        Ácido Hialurónico
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="linea.html">
                                                        Aceites Botánicos
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="linea.html">
                                                        Carbón Activado
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="linea.html">
                                                        Keratina
                                                    </a>
                                                </li>
                                            </ul>
                                        </li>
                                        <li>
                                            <a class="megamenu-title" href="#">
                                                TIPO DE PRODUCTO
                                            </a>
                                            <ul class="dl-submenu">
                                                <li>
                                                    <a href="linea.html">
                                                        Fluid
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="linea.html">
                                                        Kits
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="linea.html">
                                                        Lotion
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="linea.html">
                                                        Mask
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="linea.html">
                                                        Oil
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="linea.html">
                                                        Serum
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="linea.html">
                                                        Shampoo
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="linea.html">
                                                        Treatment
                                                    </a>
                                                </li>
                                            </ul>
                                        </li>
                                    </ul>
                                </li>
                                <li>
                                    <a href="catalogo.html">
                                        TIENDA
                                    </a>
                                </li>
                                <li>
                                    <a href="nosotros.html">
                                        ECHOSLINE
                                    </a>
                                </li>
                                <li>
                                    <a href="blog.html">
                                        BLOG
                                    </a>
                                </li>
                                <li>
                                    <a href="contacto.html">
                                        CONTACTO
                                    </a>
                                </li>
                            </ul>
                        </div>
                        <!-- Mobile Navigation End Here -->
                    </div>
                </div>
            </div>
        </header>
        <!-- Mobile Header area End -->

        <!-- Main Content Wrapper Start -->
        <div id="content" class="main-content-wrapper">
            <div class="page-content-inner">
                <div class="container">
                    <div class="row pt--80 pt-md--60 pt-sm--40">
                        <div class="col-12">
                            <h1 class="heading-tertiary heading-color mb--15 averta-bold" id="pedidoGenerado" style="display: none;">Pedido generado con éxito</h1>
                        </div>
                    </div> 
                    <div class="row pb--80 pb-md--60 pb-sm--40">
                        <!-- Checkout Area Start -->  
                        <div class="col-lg-6">
                          <h2 id="metodosPago">Métodos de pago</h2>
                          <!--nuevo pago paypal-->
                          <div class="instrucciones" style="display: none !important;">
                            <h3>Pagar con tarjeta de crédito / débito</h3>
                            <div class="form-group" id="psp-group">
                                <div class="panel">
                                    <div class="panel-body">
                                        <div id="pppDiv"> <!-- the div which id the merchant reaches into the clientlib configuration -->
                                            <script type="text/javascript">
                                                document.write("iframe is loading...");
                                            </script>
                                            <noscript> <!-- in case the shop works without javascript and the user has really disabled it and gets to the merchant's checkout page -->
                                                <iframe src="https://www.paypalobjects.com/webstatic/ppplusbr/ppplusbr.min.js/public/pages/pt_BR/nojserror.html" style="height: 400px; width: 100%; border: none;"></iframe>
                                            </noscript>
                                        </div>
                                    </div>
                                </div>
                                <button
                                  type="submit"
                                  id="continueButton"
                                  class="btn btn-lg btn-primary btn-block infamous-continue-button"
                                  style="background-color:#C21E29;"
                                  onclick="ppp.doContinue(); return false;">
                                    Validar Tarjeta
                                </button>
                                <a id="payNowButton" class="btn btn-lg btn-primary btn-block infamous-continue-button hidden" href="?action=commit">Pagar Ahora</a>
                            </div>
                          </div>
                          <!--fin nuevo pago paypal-->

                          <!-- transferencia-->
                          <div id="transfer-button-container" class="instrucciones">
                            <button class="btn btn-fullwidth" id="pagarTransferencia">Transferencia Electrónica</button>
                            <div id="instrucciones" style="display: none;">
                              <br>
                              <p><strong>CUIDADO DIARIO BASICO S.A DE C.V</strong></p>
                              <p><strong>RFC: </strong>CDB200922U22</p>
                              <p><strong>Banco: </strong>Banregio</p>
                              <p><strong>Cuenta: </strong>139007710010</p>
                              <p><strong>Clabe : </strong>058320000005225486</p>
                              <!--p>Puedes realizar transferencia electrónica a la cuenta anterior, o realizar depósito a esa cuenta en los siguientes establecimientos...</p>
                              <p>
                                <img src="assets/img/lugaresPagos.jpeg" id="lugaresPagos">
                              </p-->
                              <br/>
                              <p>Posteriormente envía tu comprobante de pago por whatsapp al número: +523316706832</p>
                              <p>Después de enviar tu comprobante tu pedido será procesado y enviado, este proceso puede tardar hasta <span style="color: red;">2 días hábiles</span>...</p>
                              <br/>
                              <p><strong>Para continuar, da click en Finalizar Compra...</strong></p>
                              <br>
                              <button id="finalizarTransfer" class="btn btn-fullwidth">FINALIZAR COMPRA</button>
                            </div>
                          </div>
                          <!--fin transferencia-->

                          <!--pago paypal normal -->
                          <div class="instrucciones">
                            <!--h3>Pagar con PayPal</h3-->
                            <div id="paypal-button-container"></div>
                            <script>
                                // Render the PayPal button into #paypal-button-container
                                paypal.Buttons({

                                    // Set up the transaction
                                    createOrder: function(data, actions) {
                                        return actions.order.create({
                                            purchase_units: [{
                                                amount: {
                                                    value: '<?php echo $totalPaypal2;?>'
                                                }
                                            }]
                                        });
                                    },

                                    // Finalize the transaction
                                    onApprove: function(data, actions) {
                                        return actions.order.capture().then(function(details) {
                                            // Show a success message to the buyer
                                            //alert('Transaction completed by ' + details.payer.name.given_name + '!');
                                            console.log("Enviando");
                                            /*var formD = new FormData(document.getElementById("formDatos"));
                                            guardarDatosEnvio(formD);*/
                                            /*openModalTicket(idVentaMenudeo);
                                            limpiar_carrito();*/
                                            idPago = details.purchase_units[0].payments.captures[0].id;
                                            idOrden = details.id;
                                            status = details.status;
                                            tipo = "Paypal";
                                            //idTransaccion = 
                                            //actualizar_status_venta(status);
                                            actualizar_status_venta("Pagado");
                                        });
                                    }


                                }).render('#paypal-button-container');
                                
                            </script>
                          </div>
                          <!-- fin pago paypal normal -->

                          <!-- mercado pago -->
                          <div class="checkout-payment instrucciones">
                              
                              <div id="mercadopago-button-container">
                                  <?php
                                      echo "<a href='$preference->init_point' class='btn btn-fullwidth btn-style-5' id='botonMP'>  </a>";
                                  ?>
                                  <!--<form method="POST" id="mercadopagoForm" action="javascript:void(0);">
                                    <script
                                     src="https://www.mercadopago.com.mx/integrations/v1/web-payment-checkout.js"
                                     data-preference-id="<?php //echo $preference->id; ?>" data-button-label="Pagar con MercadoPago">
                                    </script>
                                  </form>-->
                              </div>
                          </div>
                          <!-- fin mercado pago -->


                        </div>
                        <div class="col-xl-5 offset-xl-1 col-lg-6 mt-md--40">
                            <div class="order-details">
                                <div class="checkout-title mt--10">
                                    <h2>Resumen de tu pedido</h2>
                                </div>
                                <div class="table-content table-responsive mb--30">
                                    <table class="tabla-pedido table order-table order-table-2">
                                        <thead>
                                            <tr>
                                                <?php
                                                $countProd = 0;
                                                //$envio = number_format(0,2,'.',',');
                                                if($resp["code"] == 200){
                                                    if(isset($resp["itemsCart"])){
                                                        foreach ($cart as $key => $value) {
                                                            $countProd += $value["qty"];
                                                        }
                                                    }
                                                    if(isset($resp["itemsCart_oferta"])){
                                                        foreach ($cart_oferta as $key => $value) {
                                                            $countProd += $value["qty"];
                                                        }
                                                    }
                                                    
                                                }
                                                ?>
                                                <th class="averta-bold p-0"><?php echo $countProd;?> Productos</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php
                                                if($resp["code"] == 200){
                                                    if(isset($resp["itemsCart_oferta"])){
                                                        foreach ($cart_oferta as $key => $value) {
                                                        //$countProd += $value["qty"];
                                            ?>
                                            <tr>
                                                <th><?php echo $value["Promocion"];?></th>
                                                <!--<th>$<?php //echo $value["Precio_publico"];?></th>-->
                                                <th><strong style="color:#000">x</strong> <?php echo $value["qty"];?></th>
                                            </tr>
                                            <?php
                                                        }
                                                    }
                                                }
                                            ?>
                                            <?php
                                                if($resp["code"] == 200){
                                                    if(isset($resp["itemsCart"])){
                                                        foreach ($cart as $key => $value) {
                                                        //$countProd += $value["qty"];
                                            ?>
                                            <tr>
                                                <th><?php echo $value["Producto"];?></th>
                                                <!--<th>$<?php //echo $value["Precio_publico"];?></th>-->
                                                <th><strong style="color:#000">x</strong> <?php echo $value["qty"];?></th>
                                            </tr>
                                            <?php
                                                        }
                                                    }
                                                }
                                            ?>
                                            <!--<tr>
                                                <th>Aliquam lobortis est 
                                                </th>
                                            </tr>
                                            <tr>
                                                <th>Aliquam lobortis est 
                                                </th>
                                            </tr>
                                            <tr>
                                                <th>Auctor gravida enim 
                                                </th>
                                            </tr>-->
                                        </tbody>
                                        <tfoot class="tfootResumen">
                                            <?php
                                            if(isset($cupon)){
                                              ?>
                                              <tr class="shipping">
                                                  <th class="averta-bold pb--20">Cupón</th>
                                                  <td class="text-right p-0 pb--20">
                                                      <span class="order-total-ammount">   -$<?php 
                                                        if($resp["code"] == 200 && count($lista) > 0){
                                                          echo $descuentoCupon;
                                                        }else{
                                                            echo "0.00";
                                                        }
                                                      ?>           
                                                      </span>
                                                  </td>
                                              </tr>
                                              <?php
                                            }
                                            ?>
                                            <tr class="cart-subtotal">
                                                <th class="averta-bold">Subtotal</th>
                                                <td class="text-right p-0">
                                                    <span class="order-total-ammount">$<?php
                                                        if($resp["code"] == 200 && count($lista) > 0){
                                                            /*if($resp["subtotal"] >= 1000){
                                                              $envio = 0;
                                                            }else{
                                                              $envio = 100;
                                                            }

                                                            //$sub = floatval($resp["subtotal"] / 1.16);
                                                            $sub = floatval($resp["subtotal"]);
                                                            //echo $sub;
                                                            echo number_format($sub,2,'.',',');*/
                                                            echo $subtotal;
                                                        }
                                                        else
                                                            echo "0.00";
                                                    ?>
                                                        
                                                    </span>
                                                </td>
                                            </tr>
                                            <!--<tr class="cart-subtotal">
                                                <th class="averta-bold">Descuento profesional</th>
                                                <td class="text-right p-0">
                                                    <span class="order-total-disccount">-$<?php //echo $descuentoProf;?>
                                                        
                                                    </span></td>
                                            </tr>-->
                                            <tr class="shipping">
                                                <th class="averta-bold pb--20">Envío</th>
                                                <td class="text-right p-0 pb--20">
                                                    <span class="order-total-ammount">$<?php 
                                                        if($resp["code"] == 200 && count($lista) > 0){
                                                            
                                                              echo $envio;
                                                            
                                                            
                                                        }else
                                                          echo "0.00";
                                                    ?>
                                                        
                                                    </span>
                                                </td>
                                            </tr>
                                            <tr class="shipping">
                                                <th class="averta-bold pb--20">Descuento</th>
                                                <td class="text-right p-0 pb--20">
                                                    <span class="order-total-ammount">   $<?php
                                                    if($resp["code"] == 200 && count($lista) > 0){
                                                      echo $descuentoCliente;
                                                    }else{
                                                        echo "0.00";
                                                    }
                                                    ?>             
                                                    </span>
                                                </td>
                                            </tr>
                                            <!--tr class="cart-subtotal">
                                                <th class="averta-bold">Impuestos</th>
                                                <td class="text-right p-0">
                                                    <span class="order-total-ammount">$<?php
                                                        /*if($resp["code"] == 200 && count($lista) > 0){
                                                          echo $impuestos;
                                                        }else{
                                                            echo "0.00";
                                                        }*/
                                                    ?>
                                                        
                                                    </span>
                                                </td>
                                            </tr-->
                                            <tr class="order-total b-3">
                                                <th class="averta-bold pt--20 pb--20">Total</th>
                                                <td class="text-right pt--20 pb--20 pl-0 pr-0">
                                                    <span class="order-total-ammount">$<?php 
                                                    if($resp["code"] == 200){
                                                        //$total = floatval((($resp["subtotal"] + $envio)-$descuentoProf)-$desc); 
                                                        //echo number_format($total,2,'.',',');
                                                        echo $total;
                                                    }else{
                                                        echo "0.00";
                                                    }
                                                    ?>
                                                    </span>
                                                </td>
                                            </tr>
                                        </tfoot>
                                    </table>
                                </div>
                                
                                <button class="btn btn-fullwidth btn-style-1" id=""><a href="catalogo.html">SEGUIR COMPRANDO</a></button>
                            </div>
                        </div>
                        <!-- Checkout Area End -->
                    </div>
                </div>
            </div>
        </div>
        <!-- Main Content Wrapper Start -->


        <!-- Footer Start -->
        <footer class="footer footer-3 bg--gris border-top">
            <div class="container">
                <div class="row pt--40 pt-md--30 mb--40 mb-sm--30">
                    <div class="col-12 text-md-center">
                        <div class="footer-widget">
                            <div class="textwidget">
                                <a href="index.html" class="footer-logo">
                                    <img src="assets/img/logo/logo-footer.png" alt="Logo">
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row mb--15 mb-sm--20">
                    <div class="col-xl-3 col-md-3 mb-lg--30">
                        <div class="footer-widget">
                            <h3 class="widget-title widget-title--2">Atención al cliente</h3>
                            <ul class="widget-menu widget-menu--2">
                                <li>Contáctanos</li>
                                <li><a href="tel:+523316706832">+52 33 1670 6832</a></li>
                                <li><a href="faq.html">Preguntas frecuentes</a></li>
                                <li><a href="politicas.html">Políticas de Devoluciones</a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-xl-3 col-md-3 mb-lg--30">
                        <div class="footer-widget">
                            <h3 class="averta-bold widget-title widget-title--2" id="tituloCuenta">Tu cuenta</h3>
                            <ul class="widget-menu widget-menu--2">
                                <li><a href="mi-cuenta.html" id="footer_registro">Registrarme</a></li>
                                <li><a href="clientes.html" id="footer_clientes">Clientes</a></li>
                                <li><a href="pedidos.html" id="footer_pedidos">Mis pedidos</a></li>
                                <li><a href="favoritos.html" id="footer_favoritos">Mis favoritos</a></li>
                                <li><a href="salones.html" id="footer_salones">Salones y mayoristas</a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-xl-3 col-md-3 mb-lg--30">
                        <div class="footer-widget">
                            <h3 class="widget-title widget-title--2">Mapa de sitio</h3>
                            <ul class="widget-menu widget-menu--2">
                                <li><a href="index.html">Inicio</a></li>
                                <li><a href="catalogo.html">Tienda</a></li>
                                
                                <li><a href="nosotros.html">Echosline</a></li>
                                <li><a href="contacto.html">Contacto</a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-xl-3 col-md-3 mb-lg--30">
                        <div class="row">
                            <div class="col-xl-12 col-md-12 mb-lg--30">
                                <div class="footer-widget">
                                    <h3 class="widget-title widget-title--2">Síguenos</h3>
                                    <ul class="widget-menu widget-menu--2">
                                        <li>
                                            <ul class="contenedorSocials">
                                                <li><a href="https://facebook.com/echoslinemex"><i class="fab fa-facebook-f"></i></a></li>
                                                <li><a href="https://instagram.com/echoslinemex/?hl=es-la"><i class="fab fa-instagram"></i></a></li>
                                            </ul>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                            <div class="col-xl-12 col-md-12 mb-lg--30">
                                <div class="footer-widget">
                                    <h3 class="widget-title widget-title--2">Métodos de pago</h3>
                                    <ul class="widget-menu widget-menu--2">
                                        <li>
                                            <ul class="contenedorPagos">
                                                <li><a href="#"><img class="meotodoPago" src="assets/img/icons/mercado-pago.png"></a></li>
                                                <li><a href="#"><img class="paypal" src="assets/img/icons/paypal.png"></a></li>
                                            </ul>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row align-items-center pt--10 pb--30">
                    <div class="col-md-4">
                    </div>
                    <div class="col-md-4 text-md-center">
                        <p class="copyright-text">© Copyright Echosline 2019</p>
                    </div>
                </div>
            </div>
        </footer>
        <!-- Footer End -->


        <!-- Search from Start --> 
        <div class="searchform__popup" id="searchForm">
            <a href="#" class="btn-close"><i class="dl-icon-close"></i></a>
            <div class="searchform__body">
                <p>Start typing and press Enter to search</p>
                <form class="searchform">
                    <input type="text" name="search" id="search" class="searchform__input" placeholder="Search Entire Store...">
                    <button type="submit" class="searchform__submit"><i class="dl-icon-search10"></i></button>
                </form>
            </div>
        </div>
        <!-- Search from End --> 
        
        <!-- Side Navigation Start -->
        <aside class="side-navigation" id="sideNav">
            <div class="side-navigation-wrapper">
                <a href="" class="btn-close"><i class="dl-icon-close"></i></a>
                <div class="side-navigation-inner">
                    <div class="widget">
                        <ul class="sidenav-menu">
                            <li><a href="about-us.html">About Airi Shop</a></li>
                            <li><a href="about-us.html">Help Center</a></li>
                            <li><a href="catalogo.html">Portfolio</a></li>
                            <li><a href="blog.html">Blog</a></li>
                            <li><a href="unete.html">New Look</a></li>
                        </ul>
                    </div>
                    <div class="widget pt--30 pr--20">
                        <div class="text-widget">
                            <p>
                                <img src="assets/img/others/payments.png" alt="payment">
                            </p>
                            <p>Pellentesque mollis nec orci id tincidunt. Sed mollis risus eu nisi aliquet, sit amet fermentum justo dapibus.</p>
                        </div>
                    </div>
                    <div class="widget">
                        <div class="text-widget">
                            <p>
                                <a href="#">(+612) 2531 5600</a>
                                <a href="mailto:info@la-studioweb.com">info@la-studioweb.com</a>
                                PO Box 1622 Colins Street West
                            </p>
                        </div>
                    </div>
                    <div class="widget">
                        <div class="text-widget google-map-link">
                            <p>
                                <a href="https://www.google.com/maps" target="_blank">Google Maps</a>
                            </p>
                        </div>
                    </div>
                    <div class="widget">
                        <div class="text-widget">
                            <!-- Social Icons Start Here -->
                            <ul class="social social-small">
                                <li class="social__item">
                                    <a href="https://twitter.com" class="social__link">
                                        <i class="fa fa-twitter"></i>
                                    </a>
                                </li>
                                <li class="social__item">
                                    <a href="https://plus.google.com" class="social__link">
                                        <i class="fa fa-google-plus"></i>
                                    </a>
                                </li>
                                <li class="social__item">
                                    <a href="https://facebook.com" class="social__link">
                                        <i class="fa fa-facebook"></i>
                                    </a>
                                </li>
                                <li class="social__item">
                                    <a href="https://youtube.com" class="social__link">
                                        <i class="fa fa-youtube"></i>
                                    </a>
                                </li>
                                <li class="social__item">
                                    <a href="https://instagram.com" class="social__link">
                                        <i class="fa fa-instagram"></i>
                                    </a>
                                </li>
                            </ul>
                            <!-- Social Icons End Here -->
                        </div>
                    </div>
                    <div class="widget">
                        <div class="text-widget">
                            <p class="copyright-text">&copy; 2018 Airi All rights reserved</p>
                        </div>
                    </div>
                </div>
            </div>
        </aside>
        <!-- Side Navigation End -->

        <!-- Mini Cart Start -->
        <aside class="mini-cart" id="miniCart">
            <div class="mini-cart-wrapper">
                <a href="" class="btn-close"><i class="dl-icon-close"></i></a>
                <div class="mini-cart-inner">
                    <h5 class="mini-cart__heading mb--40 mb-lg--30">Shopping Cart</h5>
                    <div class="mini-cart__content">
                        <ul class="mini-cart__list">
                            <li class="mini-cart__product">
                                <a href="#" class="remove-from-cart remove">
                                    <i class="dl-icon-close"></i>
                                </a>
                                <div class="mini-cart__product__image">
                                    <img src="assets/img/products/prod-17-1-70x91.jpg" alt="products">
                                </div>
                                <div class="mini-cart__product__content">
                                    <a class="mini-cart__product__title" href="detalle-producto.html">Chain print bermuda shorts  </a>
                                    <span class="mini-cart__product__quantity">1 x $49.00</span>
                                </div>
                            </li>
                            <li class="mini-cart__product">
                                <a href="#" class="remove-from-cart remove">
                                    <i class="dl-icon-close"></i>
                                </a>
                                <div class="mini-cart__product__image">
                                    <img src="assets/img/products/prod-18-1-70x91.jpg" alt="products">
                                </div>
                                <div class="mini-cart__product__content">
                                    <a class="mini-cart__product__title" href="detalle-producto.html">Waxed-effect pleated skirt</a>
                                    <span class="mini-cart__product__quantity">1 x $49.00</span>
                                </div>
                            </li>
                            <li class="mini-cart__product">
                                <a href="#" class="remove-from-cart remove">
                                    <i class="dl-icon-close"></i>
                                </a>
                                <div class="mini-cart__product__image">
                                    <img src="assets/img/products/prod-19-1-70x91.jpg" alt="products">
                                </div>
                                <div class="mini-cart__product__content">
                                    <a class="mini-cart__product__title" href="detalle-producto.html">Waxed-effect pleated skirt</a>
                                    <span class="mini-cart__product__quantity">1 x $49.00</span>
                                </div>
                            </li>
                            <li class="mini-cart__product">
                                <a href="#" class="remove-from-cart remove">
                                    <i class="dl-icon-close"></i>
                                </a>
                                <div class="mini-cart__product__image">
                                    <img src="assets/img/products/prod-2-1-70x91.jpg" alt="products">
                                </div>
                                <div class="mini-cart__product__content">
                                    <a class="mini-cart__product__title" href="detalle-producto.html">Waxed-effect pleated skirt</a>
                                    <span class="mini-cart__product__quantity">1 x $49.00</span>
                                </div>
                            </li>
                        </ul>
                        <div class="mini-cart__total">
                            <span>Subtotal</span>
                            <span class="ammount">$98.00</span>
                        </div>
                        <div class="mini-cart__buttons">
                            <a href="cart.html" class="btn btn-fullwidth btn-style-1">Ver Carrito</a>
                            <a href="checkout.html" class="btn btn-fullwidth btn-style-1">Checkout</a>
                        </div>
                    </div>
                </div>
            </div>
        </aside>
        <!-- Mini Cart End -->



    </div>
    <!-- Main Wrapper End -->


    <!-- ************************* JS Files ************************* -->

    <!-- jQuery JS -->
    <script src="assets/js/vendor/jquery.min.js"></script>

    <!-- Bootstrap and Popper Bundle JS -->
    <script src="assets/js/bootstrap.bundle.min.js"></script>

    <!-- All Plugins Js -->
    <script src="assets/js/plugins.js"></script>

    <!-- Ajax Mail Js -->
    <script src="assets/js/ajax-mail.js"></script>

    <!-- Main JS -->
    <script src="assets/js/main.js"></script>

    <!-- REVOLUTION JS FILES -->
    <script src="assets/js/revoulation/jquery.themepunch.tools.min.js"></script>
    <script src="assets/js/revoulation/jquery.themepunch.revolution.min.js"></script>    

    <!-- SLIDER REVOLUTION 5.0 EXTENSIONS  (Load Extensions only on Local File Systems !  The following part can be removed on Server for On Demand Loading) -->
    <script src="assets/js/revoulation/extensions/revolution.extension.actions.min.js"></script>
    <script src="assets/js/revoulation/extensions/revolution.extension.carousel.min.js"></script>
    <script src="assets/js/revoulation/extensions/revolution.extension.kenburn.min.js"></script>
    <script src="assets/js/revoulation/extensions/revolution.extension.layeranimation.min.js"></script>
    <script src="assets/js/revoulation/extensions/revolution.extension.migration.min.js"></script>
    <script src="assets/js/revoulation/extensions/revolution.extension.navigation.min.js"></script>
    <script src="assets/js/revoulation/extensions/revolution.extension.parallax.min.js"></script>
    <script src="assets/js/revoulation/extensions/revolution.extension.slideanims.min.js"></script>
    <script src="assets/js/revoulation/extensions/revolution.extension.video.min.js"></script>

    <!-- REVOLUTION ACTIVE JS FILES -->
    <script src="assets/js/revoulation.js"></script>
    <script src="server/controllers/ajax_cart.js"></script>
    <script src="https://secure.mlstatic.com/sdk/javascript/v1/mercadopago.js"></script>
    <script src="server/controllers/cupones.js"></script>
    <script src="server/controllers/checkout_pasos.js"></script>

    <!--Start of Tawk.to Script-->
    <script type="text/javascript">
    var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
    (function(){
    var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
    s1.async=true;
    s1.src='https://embed.tawk.to/5e7cc9c469e9320caabd4dcf/default';
    s1.charset='UTF-8';
    s1.setAttribute('crossorigin','*');
    s0.parentNode.insertBefore(s1,s0);
    })();
    </script>
    <!--End of Tawk.to Script-->

      <form action="success.php" style="display: none;" id="paypalForm" method="post">
        <input type="text" name="payment_id" hidden>
        <input type="text" name="payment_status" hidden>
        <input type="text" name="merchant_order_id" hidden>
        <input type="text" name="tipo" value="paypal" hidden>
        <input type="submit" hidden>
      </form>

    <script>
      $(document).ready(function(){
        var error = "<?php echo $error_pago;?>";
        if(error)
          Swal.fire({
            icon: 'error',
            title: 'Oops...',
            text: 'Error al procesar el pago, por favor intente nuevamente.'
          })
      })
    </script>

    <script src="https://www.paypalobjects.com/webstatic/ppplusdcc/ppplusdcc.min.js?ver=3.1.2"></script>


    <script>

        var ppp = PAYPAL.apps.PPP({

            approvalUrl: "<?php echo $approval_url;?>",

            buttonLocation: "outside",
            preselection: "none",
            surcharging: false,
            hideAmount: false,
            placeholder: "pppDiv",

            disableContinue: "continueButton",
            enableContinue: "continueButton",

            // merchant integration note:
            // this is executed when the iframe posts the "checkout" action to the library
            // the merchant can do an ajax call to his shop backend to save the remembered cards token
            onContinue: function (rememberedCards, payerId, token, term) {
                console.log(term);
                // TODO: remove payNowButton
                $('#payNowButton').removeClass('hidden');
                $('#continueButton').addClass('hidden');
                var paymentID = "<?php echo $paymentID; ?>";
                var paypalMode = "<?php echo $paypalMode; ?>";
                var payURL = "payment.php?payerId=" + payerId + "&paymentID=" + paymentID + "&paypalMode=" + paypalMode;
                $('#payNowButton').prop('href', payURL);
                
                document.getElementById("installmentsJson").innerHTML = (term ? "<p><strong><code id='installmentsText'>"+ JSON.stringify(term) +"</code></strong></p>" : "No installments option selected");
               
            document.getElementById("responseJson").innerHTML = JSON.stringify('Success');
                if(rememberedCards) {
                
          document.getElementById("responseDiv").innerHTML = "<p><strong><code>Transaction has been approved</code></strong></p>"+
                    "<p><strong><code id='rememberedCardsText'>user token (remembered cards) = "+ rememberedCards +"</code></strong></p>";

                } else {
                    //document.getElementById("responseDiv").innerHTML = "<p><strong><code>Transaction has been approved</code></strong></p>";
                }

                // TODO: use this instead of payNowButton to go directly to the Execute Payment Page (and finalize the payment)
                /*var url = "ExecutePayment.php?access_token=" + access_token + "&payerId=" + payerId + "&paymentID=" + paymentID + "&paypalMode=" + paypalMode;
                $('#payNowButton').prop('href', payURL);
                window.top.location = url;*/
                
            },

            onError: function (err) {
                var msg = jQuery("#responseOnError").html()  + "<BR />" + JSON.stringify(err);
                jQuery("#responseOnError").html(msg);
            },

            language: "<?php echo $ppplusJsLibraryLang; ?>",
            country: "<?php echo $shippingAddressCountry; ?>",
            disallowRememberedCards: "<?php echo $disallowRememberedCards; ?>",
            rememberedCards: "<?php echo $rememberedCards; ?>",
            mode: "<?php echo $paypalMode; ?>",
            useraction: "continue",
            payerEmail: "<?php echo $payerEmail; ?>",
            payerPhone: "<?php echo $payerPhone; ?>",
            payerFirstName: "<?php echo $payerFirstName; ?>",
            payerLastName: "<?php echo $payerLastName; ?>",
            payerTaxId: "",
            payerTaxIdType: "",
            merchantInstallmentSelection: "<?php echo $merchantInstallmentSelection; ?>",
            merchantInstallmentSelectionOptional:"<?php echo $merchantInstallmentSelectionOptional; ?>",
            hideMxDebitCards: false,
            iframeHeight: "<?php echo $iframeHeight; ?>"
            
        });
    </script>
   

    <style type="text/css">
        .btnCupon{
          border: none;
          padding: 3px 20px;
          background: red;
          color: #fff;
          border-radius: 100px;
        }
        .aplicado{
          pointer-events: none;
        }
        .enUso{

        }
        .deleteCupon{
          cursor: pointer;
        }
        td{
            color:#000;
        }
        .instrucciones{
          margin: 10px 0px;
          border: 2px solid lightgray;
          padding: 10px;
        }
        .hidden{
          display: none;
        }
        #pagarTransferencia{
          background: #fe9c38;
        }
        #botonMP{
          background-image: url(assets/img/MP.jfif);
          background-size: contain;
          background-repeat: no-repeat;
          background-position: center;
          background-color: transparent;
        }
    </style>


</body>

</html>
<?php
require_once("ticket.php");
}
?>