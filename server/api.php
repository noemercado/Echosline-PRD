<?php
date_default_timezone_set('America/Mexico_City');
require "conexion.php";

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

require "Exception.php";
require "PHPMailer.php";
require "SMTP.php";

$secreto = 'echoslineNefas1809';

$idSucursal = 31;
$cookieExp = time()+3600 * 24 * 365;
$loginExp = time() + 3600;
$idCliente = 100007;
$random_por_linea = 4;

if(isset($_POST["opcion"])){
	switch ($_POST["opcion"]) {
		case 1:
			getCatalogo();
			break;
		case 2:
			getDetallesProducto();
		/*funciones del carrito*/
		case 3:
			add_to_cart();
			break;
		case 4:
			cart_data_ajax();
			break;
		case 5:
			del_item_cart_ajax();
			break;
		case 6:
			get_cart();
			break;
		case 7:
			update_cart();
			break;
		/*functiones checkout*/
		case 8:
			guardar_datos_envio();
			break;
		case 9:
			get_catalogo_linea();
			break;
		case 10:
			getDetalleProducto_ajax();
			break;
		case 11:
			send_mail_ticket();
			break;
		case 12:
			get_productos_home();
			break;
		case 13:
			get_lineas_sublineas();
			break;
		case 14:
			get_catalogo_linea_sublinea();
			break;
		case 15:
			get_random_por_linea();
			break;
		case 16:
			get_promociones();
			break;
		case 17:
			add_to_cart_oferta();
			break;
		case 18:
			getDetallesProducto_oferta();
			break;
		case 19:
			del_item_cart_ajax_oferta();
			break;
		case 20:
			login();
			break;
		case 21:
			get_datos_usuario();
			break;
		case 22:
			registrar();
			break;
		case 23:
			revisar_sesion_activa();
			break;
		case 24:
			recuperar_pass();
			break;
		case 25:
			buscar();
			break;
		case 26:
			limpiar_carrito();
			break;
		case 27:
			borrar_venta();
			break;
		case 28:
			actualizar_status_venta();
			break;
		case 29:
			ordenar_productos();
			break;
		case 30:
			get_catalogo_tipoCabello();
			break;
		case 31:
			get_banners_home();
			break;
		case 33:
			get_productos_by_division();
			break;
		case 34:
			get_productos_by_linea();
			break;
		case 35:
			get_productos_by_subLinea();
			break;
		default:
			break;

	}
}else{
	$respuesta["code"] = 400;
	$respuesta["msg"] = "No se recibió opción";
	//responder($respuesta);
}

function revisar_sesion_activa(){
	if(isset($_COOKIE["ech_lg"]) || isset($_COOKIE["ech_lg_tmp"]))
		$respuesta["code"] = 200;
	else
		$respuesta["code"] = 400;
	responder($respuesta);
}

function recuperar_pass(){
	try {
		$conn = PDOConnection::getConnection();
		$sql = "SELECT Password FROM Clientes_menudeo WHERE Email = :correo LIMIT 1";
		global $secreto;
		$stm = $conn->prepare($sql);
		$stm->bindParam(":correo",$_POST["email"]);
		$stm->execute();
		$num = $stm->rowCount();
		if($num > 0){
			$row = $stm->fetch(PDO::FETCH_ASSOC);
			$plain = openssl_decrypt($row["Password"], "AES-128-ECB", $secreto);
			$respuesta["pass"] = $plain;
			$respuesta["code"] = 200;
			/*enviar por correo*/
				$mail = new PHPMailer(TRUE);

				/* Open the try/catch block. */
				try {
				   //$mail->setFrom("noe@integratto.com.mx", 'Echosline');
				   $mail->setFrom('het@echosline.mx', 'EchosLine');
				   $mail->addAddress($_POST["email"], 'Echosline');
			       
				   //$mail->addAddress('nefas1809@gmail.com', 'Echosline');
				   //$mail->addAddress('administracion@blueskeet.com', 'Blueskeet');

				   /* Set the subject. */
				   $mail->Subject = 'Contraseña Echosline';

				   $mail->isHTML(TRUE);

				   $mensaje = "<html><body style='width: 100%; margin:0;'>
					<h2>Has solicitado tu contraseña para acceder al sitio EchosLine. Si no has sido tú, ignora este mensaje.</h2>";
					$mensaje .= "<h2>Contraseña: <span style='color:blue;'>".$plain."</span></h2>";
					$mensaje .= "</body><html>";
				   /* Set the mail message body. */
				   $mail->Body = $mensaje;
				   $mail->CharSet = 'UTF-8';

				   /* Finally send the mail. */
				   if($mail->send())
				   	$resp["code"] = 200;
				   else
				   	$resp["code"] = 400;
				}
				catch (Exception $e)
				{
				   /* PHPMailer exception. */
				   $resp["code"] = 400;
				   $resp["msg"] = $e->errorMessage();
				}
				catch (\Exception $e)
				{
				   /* PHP exception (note the backslash to select the global namespace Exception class). */
				   $resp["code"] = 400;
				   $resp["msg"] = $e->getMessage();
				}
		}else{
			$respuesta["code"] = 400;			
			$respuesta["msg"] = "Este correo no ha sido registrado";
		}
	} catch (Exception $e) {
		$respuesta["code"] = 500;
		$respuesta["msg"] = "Error en el servidor: \n".$e->getMessage();
	}

	responder($respuesta);
	
}

function getIdPadre($conn){
	try {
		$sql = 'SELECT * FROM Clientes_menudeo WHERE Nombre LIKE "%Dirección%" LIMIT 0,1';
		$stm = $conn->prepare($sql);
		$result = $stm->execute();
		if($result){
			return $stm->fetch(PDO::FETCH_ASSOC);
		}
	} catch (Exception $e) {
		return false;
	}
	return false;
}

function registrar(){
	try {
		global $idCliente;
		global $secreto;
		global $loginExp;
		$conn = PDOConnection::getConnection();

		/*validar el id multinivel*/
		//$idPadre = $_POST["idPadre"];
		$idPadre = getIdPadre($conn);
		//var_dump($idPadre);

		/*if($idPadre != null && $idPadre != ""){
			$sql = "SELECT * FROM Clientes_menudeo WHERE ID = ".$idPadre;
			$stm = $conn->prepare($sql);
			$stm->execute();
			$num = $stm->rowCount();
		}*/
		if($idPadre){
			$num = 1;
		}
		else{
			$idPadre = 100019;
			$num = 1;
		}
		
		if($num > 0){
			$sql = "INSERT INTO Clientes_menudeo SET Nombre = :nombre, Apellidos = :apellidos,Empresa = 'NA', Cargo = '', Tel1 = '', Email = :email, Clientes_menudeo.`Descuento_%` = 0, idCliente = :idCliente, Clientes_menudeo.`Status` = 'Activo', Nivel = '', Dia_visita = 0, Asignado = 0, Password = :pass, idPadre = :idPadre";
			$stm = $conn->prepare($sql);
			$stm->bindParam(":email",$_POST["email"]);
			$stm->bindParam(":nombre",$_POST["nombre"]);
			$stm->bindParam(":apellidos",$_POST["apellidos"]);
			$passEnc = openssl_encrypt($_POST["password"], "AES-128-ECB", $secreto);
			$stm->bindParam(":pass",$passEnc);
			$stm->bindParam(":idCliente",$idCliente);
			$stm->bindParam(":idPadre",$idPadre["ID"]);
			$respuesta["sql"] = $stm;
			$result = false;
			$result = $stm->execute();
			if($result){
				$respuesta["code"] = 200;
				$tmp = array(
					"u"=>$_POST["email"],
					"id"=> $conn->lastInsertId()
				);
				//setcookie("ech_lg_tmp",json_encode($temp),$loginExp,"/");
				setcookie("ech_lg",json_encode($tmp),$loginExp,"/");
			}else{
				$respuesta["code"] = 400;			
				$respuesta["msg"] = "Error al guardar al cliente";
			}
		}else{
			$respuesta["code"] = 400;
			$respuesta["msg"] = "No se encontró el ID del Vendedor multinivel.";
			$respuesta["idPadre"] = $idPadre;
		}

		
	} catch (Exception $e) {
		$respuesta["code"] = 500;
		$respuesta["msg"] = "Error en el servidor: \n".$e->getMessage();
	}
	responder($respuesta);
}

function get_datos_tmp(){
	$tmp = json_decode( $_COOKIE["ech_lg_tmp"], true );
}

function login(){
	global $loginExp;
	if(isset($_POST["password"],$_POST["username_email"])){
		try {
			$conn = PDOConnection::getConnection();
			$sql = "SELECT * FROM Clientes_menudeo WHERE Email = :correo AND Password = :pass LIMIT 1";
			//global $secreto;
			$stm = $conn->prepare($sql);
			global $secreto;
			$passEnc = openssl_encrypt($_POST["password"], "AES-128-ECB", $secreto);
			$stm->bindParam(":correo",$_POST["username_email"]);
			$stm->bindParam(":pass",$passEnc);
			$response["pass"] = $passEnc;
			$stm->execute();
			$num = $stm->rowCount();
			if($num > 0){// ya existe ese ticket
				$row = $stm->fetch(PDO::FETCH_ASSOC);
				extract($row);
				$response["code"]=200;
				$response["pass"] = $passEnc;
				$d = time();
				$fecha = date("Y-m-d", $d);
				$tmp = array(
					"u"=>$row["Email"],
					"id"=>$row["ID"]
				);
				setcookie("ech_lg",json_encode($tmp),$loginExp,"/");
			}else{
				$response["code"]=400;
				$response["msg"]="Usuario y contraseña incorrectos";
			}
		} catch (Exception $e) {
			$response["code"]=500;
			$response["msg"]="Error en el servidor: ".$e->getMessage();
		}
	}else{
		$response["code"]=400;
		$response["msg"]="No se enviaron datos";
	}
	responder($response);
}

function get_datos_usuario(){
	global $loginExp;
	if(isset($_COOKIE["ech_lg"])){
		$tmp = json_decode( $_COOKIE["ech_lg"], true );
		$email = $tmp["u"];
		try {
			$conn = PDOConnection::getConnection();
			$sql = "SELECT * FROM Clientes_menudeo WHERE Email = :correo LIMIT 1";
			//global $secreto;
			$stm = $conn->prepare($sql);
			$stm->bindParam(":correo",$email);
			$stm->execute();
			$num = $stm->rowCount();
			if($num > 0){
				$row = $stm->fetch(PDO::FETCH_ASSOC);
				extract($row);
				$response["code"]=200;
				$response["usuario"] = $row;
				$tmp = array(
					"u"=>$row["Email"],
					"id"=>$row["ID"]
				);
				setcookie("ech_lg",json_encode($tmp),$loginExp,"/");
			}
		} catch (Exception $e) {
			$response["code"]=500;
			$response["msg"]="Error en el servidor: ".$e->getMessage();
		}
	}else if(isset($_COOKIE["ech_lg_tmp"])){
		$response["usuario"] = json_decode( $_COOKIE["ech_lg_tmp"], true );
		$response["tmp"] = true;
		$response["code"]=200;
	}else{
		$response["code"]=400;
		$response["msg"]="No se enviaron datos";
	}
	responder($response);
}

function get_datos_usuario2(){
  global $loginExp;
  if(isset($_COOKIE["ech_lg"])){
    $tmp = json_decode( $_COOKIE["ech_lg"], true );
    $email = $tmp["u"];
    try {
      $conn = PDOConnection::getConnection();
      $sql = "SELECT * FROM Clientes_menudeo WHERE Email = :correo LIMIT 1";
      //global $secreto;
      $stm = $conn->prepare($sql);
      $stm->bindParam(":correo",$email);
      $stm->execute();
      $num = $stm->rowCount();
      if($num > 0){
        $row = $stm->fetch(PDO::FETCH_ASSOC);
        extract($row);
        $response["code"]=200;
        $response["usuario"] = $row;
        $tmp = array(
          "u"=>$row["Email"],
          "id"=>$row["ID"]
        );
        setcookie("ech_lg",json_encode($tmp),$loginExp,"/");
      }
    } catch (Exception $e) {
      $response["code"]=500;
      $response["msg"]="Error en el servidor: ".$e->getMessage();
    }
  }else if(isset($_COOKIE["ech_lg_tmp"])){
    $response["usuario"] = json_decode( $_COOKIE["ech_lg_tmp"], true );
    $response["tmp"] = true;
    $response["code"]=200;
  }else{
    $response["code"]=400;
    $response["msg"]="No se enviaron datos";
  }
  if(isset($_COOKIE["idV"]))
    $response["idV"] = $_COOKIE["idV"];
  else
    $response["idV"] = "";
  if(isset($_COOKIE["idVM"]))
    $response["idVM"] = $_COOKIE["idVM"];
  else
    $response["idVM"] = "";
  $conn = null;
  return($response);
}

function get_promociones(){
	try {
		$d = time();
		$fecha = date("Y-m-d", $d);
		$dt = strtotime ($fecha);
		$inicio = date ('Y-m-d', strtotime ('first day of this month', $dt));
		$final = date ('Y-m-d', strtotime ('last day of this month', $dt));
		$respuesta["inicio"] = $inicio;
		$respuesta["final"] = $final;
		global $idSucursal;
		$conn = PDOConnection::getConnection();
		$sql = "SELECT Promociones.ID,Promociones.Promocion, Promociones.Url_imagen, Promociones.Descripcion, /*ROUND(*/Asignacion_promo.precio /*/ 2.0,2)*/ AS precio, Asignacion_promo.ahorro
		FROM Promociones
		INNER JOIN Asignacion_promo ON Promociones.ID = Asignacion_promo.idPromocion
		WHERE Asignacion_promo.idSucursal = $idSucursal
		AND Promociones.Vigencia_inicial >= :inicio
		AND Promociones.Vigencia_final <= :final";
		$stm = $conn->prepare($sql);
		$stm->bindParam(":inicio",$inicio);
		$stm->bindParam(":final",$final);
		$stm->execute();
		$respuesta["productos"] = $stm->fetchAll();
		$respuesta["code"] = 200;
	} catch (Exception $e) {
		$respuesta["code"] = 500;
		$respuesta["msg"] = "Error en el servidor: \n".$e->getMessage();
	}
	$conn = null;
	responder($respuesta);
}

function get_random_por_linea(){
	global $random_por_linea;
	try {
		global $idSucursal;
		$conn = PDOConnection::getConnection();
		$sql = "SELECT Catalogo.*, Inventario.ID as ID_inventario, /*ROUND(*/Inventario.Precio_publico /*/ 2.0,2)*/ AS Precio_publico
		FROM Catalogo
		INNER JOIN Inventario
		ON Inventario.idCatalogo = Catalogo.ID
		WHERE Inventario.idSucursal = $idSucursal
		AND Catalogo.Linea = :linea
		AND Catalogo.Status = 'Activo'
		ORDER BY RAND()
		LIMIT $random_por_linea";
		$stm = $conn->prepare($sql);
		$stm->bindParam(":linea",$_POST["linea"]);
		$stm->execute();
		$respuesta["productos"] = $stm->fetchAll();

		/*$sql = "SELECT * FROM Lineas";
		$stm = $conn->prepare($sql);
		$stm->execute();*/
		//$respuesta["lineas"] = get_lineas($conn);
		$respuesta["code"] = 200;
	} catch (Exception $e) {
		$respuesta["code"] = 500;
		$respuesta["msg"] = "Error en el servidor: \n".$e->getMessage();
	}
	$conn = null;
	responder($respuesta);
}

function responder($respuesta){
	/*if(isset($_COOKIE["tokLog"]))
		setcookie("tokLog",$_COOKIE["tokLog"],time() + 60 * 60,"/");
	if(isset($_COOKIE["tokUSID"]))
		setcookie("tokUSID",$_COOKIE["tokUSID"],time() + 60 * 60,"/");
	if(isset($_COOKIE["tokUS"]))
		setcookie("tokUS",$_COOKIE["tokUS"],time() + 60 * 60,"/");*/
	header('Content-type: application/json');
	echo json_encode($respuesta);
}

function send_mail_ticket(){
	$ticket = $_POST["urlTicket"];
	$email = $_POST["email"];

    $from = 'ventas@echosline.mx';
    $to = $email;
    $subject = 'Ticket de compra';
    $message = 'Descarga o imprime tu ticket desde el siguiente link: '.$ticket;
    $headers = "From:" . $from;
    $send = mail($to,$subject,$message, $headers);
    if($send){
    	print_r(1);
    }else {
    	print_r(0);
    }
}

function get_lineas_sublineas(){
	try {
		$conn = PDOConnection::getConnection();
		$respuesta["divisiones"] = getDivisiones($conn);
		$respuesta["lineas"] = get_lineas($conn);
		$respuesta["sublineas"] = get_sublineas($conn);
		$respuesta["code"] = 200;
	} catch (Exception $e) {
		$respuesta["code"] = 500;
		$respuesta["msg"] = "Error en el servidor: \n".$e->getMessage();
	}
	$conn = null;
	responder($respuesta);
}




function ordenar_productos(){
	try {
		global $idSucursal;
		$metodo = $_POST["metodo"];
		$linea = $_POST["linea"];
		$tipoCabello = $_POST["tipoCabello"];
		$sql = "SELECT Catalogo.*, Inventario.ID as ID_inventario, Inventario.Precio_publico AS Precio_publico, Inventario.Minimo_compra, Lineas.Linea as Nombre_linea
		FROM Catalogo
		INNER JOIN Inventario
		ON Inventario.idCatalogo = Catalogo.ID
		INNER JOIN Lineas
		ON Catalogo.Linea = Lineas.ID
		WHERE Inventario.idSucursal = $idSucursal AND Catalogo.Status = 'Activo'";

		if($linea != "")
			$sql .= " AND Catalogo.Linea = $linea";
		if($tipoCabello != "")
			$sql .= " AND Catalogo.idTipo_cabello = $tipoCabello";
		if($metodo == "menorMayor"){
			$sql .= "\nORDER BY Inventario.Precio_publico ASC";
		}else if($metodo == "mayorMenor"){
			$sql .= "\nORDER BY Inventario.Precio_publico DESC";
		}else{
			if($linea != "")
				$sql .= "\nORDER BY Catalogo.Linea ASC";
			if($tipoCabello != "")
				$sql .= "\nORDER BY Catalogo.idTipo_cabello ASC";
		}
		$conn = PDOConnection::getConnection();
		
		$stm = $conn->prepare($sql);
		$stm->execute();
		$respuesta["productos"] = $stm->fetchAll();
		$respuesta["divisiones"] = getDivisiones($conn);
		$respuesta["lineas"] = get_lineas($conn);
		$respuesta["tiposCabello"] = get_TiposCabello($conn);
		$respuesta["code"] = 200;
	} catch (Exception $e) {
		$respuesta["code"] = 500;
		$respuesta["msg"] = "Error en el servidor: \n".$e->getMessage();
	}
	$conn = null;
	responder($respuesta);
}

function buscar(){
	try {
		global $idSucursal;
		$termino = $_POST["termino"];
		$conn = PDOConnection::getConnection();
		$sql = "SELECT Catalogo.*, Inventario.ID as ID_inventario, Inventario.Precio_publico AS Precio_publico, Lineas.Linea as Nombre_linea
		FROM Catalogo
		INNER JOIN Inventario
		ON Inventario.idCatalogo = Catalogo.ID
		INNER JOIN Lineas
		ON Catalogo.Linea = Lineas.ID
		WHERE Inventario.idSucursal = $idSucursal
		AND Catalogo.Status = 'Activo'
		AND Catalogo.Producto LIKE '%$termino%'";
		$stm = $conn->prepare($sql);
		$stm->execute();
		$respuesta["productos"] = $stm->fetchAll();
		$respuesta["divisiones"] = getDivisiones($conn);
		$respuesta["lineas"] = get_lineas($conn);
		$respuesta["tiposCabello"] = get_TiposCabello($conn);

		$respuesta["code"] = 200;
	} catch (Exception $e) {
		$respuesta["code"] = 500;
		$respuesta["msg"] = "Error en el servidor: \n".$e->getMessage();
	}
	$conn = null;
	responder($respuesta);
}

function get_productos_home(){
	try {
		global $idSucursal;
		$conn = PDOConnection::getConnection();
		$sql = "SELECT Catalogo.*, Inventario.ID as ID_inventario, Inventario.Precio_publico AS Precio_publico, Lineas.Linea as Nombre_linea
		FROM Catalogo
		INNER JOIN Inventario
		ON Inventario.idCatalogo = Catalogo.ID
		INNER JOIN Lineas
		ON Catalogo.Linea = Lineas.ID
		WHERE Inventario.idSucursal = $idSucursal
		AND Catalogo.Status = 'Activo'
		ORDER BY RAND()
		LIMIT 4";
		$stm = $conn->prepare($sql);
		$stm->execute();
		$respuesta["productos"] = $stm->fetchAll();

		/*$sql = "SELECT * FROM Lineas";
		$stm = $conn->prepare($sql);
		$stm->execute();*/
		$respuesta["lineas"] = get_lineas($conn);
		$respuesta["divisiones"] = getDivisiones($conn);
		$respuesta["tiposCabello"] = get_TiposCabello($conn);
		$respuesta["code"] = 200;
	} catch (Exception $e) {
		$respuesta["code"] = 500;
		$respuesta["msg"] = "Error en el servidor: \n".$e->getMessage();
	}
	$conn = null;
	responder($respuesta);
}

function get_lineas($conn){
	try {
		$sql = "SELECT * FROM Lineas WHERE Status = 'Activo'";
		$stm = $conn->prepare($sql);
		$stm->execute();
		return $stm->fetchAll();
	} catch (Exception $e) {
		return false;
	}
	return false;
}


function getCatalogo(){
	try {
		global $idSucursal;
		$conn = PDOConnection::getConnection();
		$sql = "SELECT Catalogo.*, Inventario.ID as ID_inventario, Inventario.Precio_publico  AS Precio_publico, Inventario.Minimo_compra, Lineas.Linea as Nombre_linea
		FROM Catalogo
		INNER JOIN Inventario
		ON Inventario.idCatalogo = Catalogo.ID
		INNER JOIN Lineas
		ON Catalogo.Linea = Lineas.ID
		WHERE Inventario.idSucursal = $idSucursal AND Catalogo.Status = 'Activo'
		ORDER BY Catalogo.Linea";
		$stm = $conn->prepare($sql);
		$stm->execute();
		$respuesta["productos"] = $stm->fetchAll();
		$respuesta["divisiones"] = getDivisiones($conn);
		/*$respuesta["lineas"] = get_lineas($conn);
		$respuesta["tiposCabello"] = get_TiposCabello($conn);*/
		$respuesta["code"] = 200;
	} catch (Exception $e) {
		$respuesta["code"] = 500;
		$respuesta["msg"] = "Error en el servidor: \n".$e->getMessage();
	}
	$conn = null;
	responder($respuesta);
}

function getDivisiones($conn){
	try {
		$sql = "SELECT * FROM Divisiones";
		$stm = $conn->prepare($sql);
		$stm->execute();
		return $stm->fetchAll();
	} catch (Exception $e) {
		return false;
	}
	return false;
}

function get_productos_by_division(){
	try {
		global $idSucursal;
		$conn = PDOConnection::getConnection();
		$sql = "SELECT Catalogo.*, Inventario.ID as ID_inventario, Inventario.Precio_publico  AS Precio_publico, Inventario.Minimo_compra, Lineas.Linea as Nombre_linea
		FROM Catalogo
		INNER JOIN Inventario
		ON Inventario.idCatalogo = Catalogo.ID
		INNER JOIN Lineas
		ON Catalogo.Linea = Lineas.ID
		WHERE Inventario.idSucursal = $idSucursal AND Catalogo.Status = 'Activo' AND Catalogo.Division = :idD";
		$stm = $conn->prepare($sql);
		$stm->bindParam(":idD",$_POST["idD"]);
		$stm->execute();
		$respuesta["productos"] = $stm->fetchAll();
		//$respuesta["divisiones"] = getDivisiones($conn);
		$respuesta["lineas"] = get_lineas_by_division($conn);
		//$respuesta["tiposCabello"] = get_TiposCabello($conn);
		$respuesta["code"] = 200;
	} catch (Exception $e) {
		$respuesta["code"] = 500;
		$respuesta["msg"] = "Error en el servidor: \n".$e->getMessage();
	}
	$conn = null;
	responder($respuesta);
}

function get_productos_by_linea(){
	try {
		global $idSucursal;
		$conn = PDOConnection::getConnection();
		$sql = "SELECT Catalogo.*, Inventario.ID as ID_inventario, Inventario.Precio_publico  AS Precio_publico, Inventario.Minimo_compra, Lineas.Linea as Nombre_linea
		FROM Catalogo
		INNER JOIN Inventario
		ON Inventario.idCatalogo = Catalogo.ID
		INNER JOIN Lineas
		ON Catalogo.Linea = Lineas.ID
		WHERE Inventario.idSucursal = $idSucursal AND Catalogo.Status = 'Activo' AND Catalogo.Division = :idD AND Catalogo.Linea = :idL";
		$stm = $conn->prepare($sql);
		$stm->bindParam(":idD",$_POST["idD"]);
		$stm->bindParam(":idL",$_POST["idL"]);
		$stm->execute();
		$respuesta["productos"] = $stm->fetchAll();
		//$respuesta["divisiones"] = getDivisiones($conn);
		$respuesta["subLineas"] = get_sub_lineas_by_linea($conn);
		$respuesta["code"] = 200;
	} catch (Exception $e) {
		$respuesta["code"] = 500;
		$respuesta["msg"] = "Error en el servidor: \n".$e->getMessage();
	}
	$conn = null;
	responder($respuesta);
}

function get_productos_by_subLinea(){
	try {
		global $idSucursal;
		$conn = PDOConnection::getConnection();
		$sql = "SELECT Catalogo.*, Inventario.ID as ID_inventario, Inventario.Precio_publico  AS Precio_publico, Inventario.Minimo_compra, Lineas.Linea as Nombre_linea
		FROM Catalogo
		INNER JOIN Inventario
		ON Inventario.idCatalogo = Catalogo.ID
		INNER JOIN Lineas
		ON Catalogo.Linea = Lineas.ID
		WHERE Inventario.idSucursal = $idSucursal AND Catalogo.Status = 'Activo' AND Catalogo.Division = :idD AND Catalogo.Linea = :idL AND Catalogo.Sublinea = :idS";
		$stm = $conn->prepare($sql);
		$stm->bindParam(":idD",$_POST["idD"]);
		$stm->bindParam(":idL",$_POST["idL"]);
		$stm->bindParam(":idS",$_POST["idS"]);
		$stm->execute();
		$respuesta["productos"] = $stm->fetchAll();
		//$respuesta["divisiones"] = getDivisiones($conn);
		//$respuesta["subLineas"] = get_sub_lineas_by_linea($conn);
		$respuesta["code"] = 200;
	} catch (Exception $e) {
		$respuesta["code"] = 500;
		$respuesta["msg"] = "Error en el servidor: \n".$e->getMessage();
	}
	$conn = null;
	responder($respuesta);
}

function get_lineas_by_division($conn){
	try {
		$sql = "SELECT * FROM Lineas WHERE idDivision = :idD AND Status = 'Activo'";
		$stm = $conn->prepare($sql);
		$stm->bindParam(":idD",$_POST["idD"]);
		$stm->execute();
		return $stm->fetchAll();
	} catch (Exception $e) {
		return false;
	}
	return false;
}

function get_sub_lineas_by_linea($conn){
	try {
		$sql = "SELECT * FROM Sublineas WHERE idLinea = :idL AND Status = 'Activo'";
		$stm = $conn->prepare($sql);
		$stm->bindParam(":idL",$_POST["idL"]);
		$stm->execute();
		return $stm->fetchAll();
	} catch (Exception $e) {
		return false;
	}
	return false;
}

function get_TiposCabello($conn){
	try {
		$sql = "SELECT * FROM Tipo_de_cabello";
		$stm = $conn->prepare($sql);
		$stm->execute();
		return $stm->fetchAll();
	} catch (Exception $e) {
		return false;
	}
	return false;
}



function get_sublineas($conn){
	try {
		$sql = "SELECT * FROM Sublineas";
		$stm = $conn->prepare($sql);
		$stm->execute();
		return $stm->fetchAll();
	} catch (Exception $e) {
		return false;
	}
	return false;
}

function get_catalogo_linea(){
	try {
		global $idSucursal;
		$conn = PDOConnection::getConnection();
		$sql = "SELECT Catalogo.*, Inventario.ID as ID_inventario, Inventario.Precio_publico AS Precio_publico, Lineas.Linea as Nombre_linea
		FROM Catalogo
		INNER JOIN Inventario
		ON Inventario.idCatalogo = Catalogo.ID
		INNER JOIN Lineas
		ON Catalogo.Linea = Lineas.ID
		WHERE Inventario.idSucursal = $idSucursal
		AND Catalogo.Status = 'Activo'
		AND Catalogo.Linea = :linea";
		$stm = $conn->prepare($sql);
		$stm->bindParam(":linea",$_POST["linea"]);
		$stm->execute();
		$respuesta["productos"] = $stm->fetchAll();
		$respuesta["divisiones"] = getDivisiones($conn);
		$respuesta["lineas"] = get_lineas($conn);
		$respuesta["tiposCabello"] = get_TiposCabello($conn);
		$sql = "SELECT idDivision FROM Lineas WHERE ID = '".$_POST["linea"]."'";
		$stm = $conn->prepare($sql);
		$stm->execute();
		$respuesta["idDivision"] = $stm->fetchAll();
		$respuesta["code"] = 200;
	} catch (Exception $e) {
		$respuesta["code"] = 500;
		$respuesta["msg"] = "Error en el servidor: \n".$e->getMessage();
	}
	$conn = null;
	responder($respuesta);
}

function get_catalogo_linea_sublinea(){
	try {
		global $idSucursal;
		$conn = PDOConnection::getConnection();
		$sql = "SELECT Catalogo.*, Inventario.ID as ID_inventario, /*ROUND(*/Inventario.Precio_publico /*/ 2.0,2)*/ AS Precio_publico
		FROM Catalogo
		INNER JOIN Inventario
		ON Inventario.idCatalogo = Catalogo.ID
		WHERE Inventario.idSucursal = $idSucursal
		AND Catalogo.Linea = :linea
		AND Catalogo.Status = 'Activo'
		AND Catalogo.Sublinea = :sublinea";
		$stm = $conn->prepare($sql);
		$stm->bindParam(":linea",$_POST["linea"]);
		$stm->bindParam(":sublinea",$_POST["sublinea"]);
		$stm->execute();
		$respuesta["productos"] = $stm->fetchAll();
		$respuesta["divisiones"] = getDivisiones($conn);
		$respuesta["lineas"] = get_lineas($conn);
		$respuesta["tiposCabello"] = get_TiposCabello($conn);
		
		$respuesta["code"] = 200;
	} catch (Exception $e) {
		$respuesta["code"] = 500;
		$respuesta["msg"] = "Error en el servidor: \n".$e->getMessage();
	}
	$conn = null;
	responder($respuesta);
}

function get_catalogo_tipoCabello(){
	try {
		global $idSucursal;
		$conn = PDOConnection::getConnection();
		$sql = "SELECT Catalogo.*, Inventario.ID as ID_inventario, Inventario.Precio_publico AS Precio_publico, Lineas.Linea as Nombre_linea
		FROM Catalogo
		INNER JOIN Inventario
		ON Inventario.idCatalogo = Catalogo.ID
		INNER JOIN Lineas
		ON Catalogo.Linea = Lineas.ID
		WHERE Inventario.idSucursal = $idSucursal
		AND Catalogo.Status = 'Activo'
		AND Catalogo.idTipo_cabello = :tipo";
		$stm = $conn->prepare($sql);
		$stm->bindParam(":tipo",$_POST["tipo"]);
		$stm->execute();
		$respuesta["productos"] = $stm->fetchAll();
		$respuesta["divisiones"] = getDivisiones($conn);
		$respuesta["lineas"] = get_lineas($conn);
		$respuesta["tiposCabello"] = get_TiposCabello($conn);
		
		$respuesta["code"] = 200;
	} catch (Exception $e) {
		$respuesta["code"] = 500;
		$respuesta["msg"] = "Error en el servidor: \n".$e->getMessage();
	}
	$conn = null;
	responder($respuesta);
}

function getDetallesProducto($idP, $idInv){
	try {
		$conn = PDOConnection::getConnection();
		$sql = "SELECT Catalogo.*, /*ROUND(*/Inventario.Precio_publico/* / 2.0,2)*/ AS Precio_publico, Inventario.Min, Lineas.Linea, Lineas.ID as idLinea, Inventario.ID as ID_inventario, Inventario.Minimo_compra
		FROM Catalogo
		INNER JOIN Inventario
		ON Inventario.idCatalogo = Catalogo.ID
		INNER JOIN Lineas
		on Lineas.ID = Catalogo.Linea
		WHERE Catalogo.ID = :id
		AND Catalogo.Status = 'Activo'
		AND Inventario.ID = :idInv";
		$stm = $conn->prepare($sql);
		$stm->bindParam("id",$idP);
		$stm->bindParam("idInv",$idInv);
		$stm->execute();
		$respuesta["producto"] = $stm->fetchAll();
		$respuesta["code"] = 200;
	} catch (Exception $e) {
		$respuesta["code"] = 500;
		$respuesta["msg"] = "Error en el servidor: \n".$e->getMessage();
	}
	$conn = null;
	return $respuesta;
}

function getDetalleProducto_ajax(){
	try {
		$conn = PDOConnection::getConnection();
		$sql = "SELECT Catalogo.*, /*ROUND(*/Inventario.Precio_publico /*/ 2.0,2)*/ AS Precio_publico, Inventario.Min, Lineas.Linea as NombreLinea,Inventario.ID as ID_inventario
		FROM Catalogo
		INNER JOIN Inventario
		ON Inventario.idCatalogo = Catalogo.ID
		INNER JOIN Lineas
		on Lineas.ID = Catalogo.Linea
		WHERE Catalogo.ID = :id
		AND Catalogo.Status = 'Activo'
		AND Inventario.ID = :idInv";
		$stm = $conn->prepare($sql);
		$stm->bindParam("id",$_POST["idP"]);
		$stm->bindParam("idInv",$_POST["idInv"]);
		$stm->execute();
		$respuesta["producto"] = $stm->fetchAll();
		$respuesta["code"] = 200;
	} catch (Exception $e) {
		$respuesta["code"] = 500;
		$respuesta["msg"] = "Error en el servidor: \n".$e->getMessage();
	}
	$conn = null;
	responder($respuesta);
}

function add_to_cart(){
	global $cookieExp;
	if(isset($_POST["idP"],$_POST["idInv"],$_POST["qty"])){
		if(isset($_COOKIE["ech_cart"])){
			/*ya hay un carrito, hay que hacer algo para actualizar productos, etc*/
			$carrito = json_decode( $_COOKIE["ech_cart"], true );
			//$respuesta["decode"] = $carrito;
			$esNuevo = true;
			foreach ($carrito as $key => $value) {
				//$respuesta["pruenba"] = $value;
				if(false !== $k = array_search($_POST["idP"], $value)){
					if(false !== $k = array_search($_POST["idInv"], $value)){
						/*existe este producto en el carrito, hay que actualizar las cantidades*/
						$qty = $value["qty"];
						$qtyT = $_POST["qty"] + $qty;
						$temp = array("p"=>$_POST["idP"],"inv"=>$_POST["idInv"],"qty"=>$qtyT);
						$carrito[$key] = $temp;
						$esNuevo = false;
					}
				}
			}
			if($esNuevo){
				$temp = array("p"=>$_POST["idP"],"inv"=>$_POST["idInv"],"qty"=>$_POST["qty"]);
				array_push($carrito, $temp);
			}
			
			
		}else{
			$carrito = array();
			$temp = array("p"=>$_POST["idP"],"inv"=>$_POST["idInv"],"qty"=>$_POST["qty"]);
			array_push($carrito, $temp);
			/*setcookie("ech_cart",json_encode($carrito),time() + 60 * 60,"/");
			$respuesta["cart"] = $carrito;*/
		}

		//$respuesta["itemsCart"] = cart_data();
	}else{

	}
	setcookie("ech_cart",json_encode($carrito),$cookieExp,"/");
	$respuesta["cart"] = $carrito;
	$respuesta["producto"] = findProd($_POST["idP"],$_POST["idInv"],null);
	$respuesta["qty"] = $_POST["qty"];
	responder($respuesta);
}

function add_to_cart_oferta(){
	global $cookieExp;
	if(isset($_POST["idP"],$_POST["qty"])){
		if(isset($_COOKIE["ech_cart_oferta"])){
			/*ya hay un carrito, hay que hacer algo para actualizar productos, etc*/
			$carrito = json_decode( $_COOKIE["ech_cart_oferta"], true );
			//$respuesta["decode"] = $carrito;
			$esNuevo = true;
			foreach ($carrito as $key => $value) {
				//$respuesta["pruenba"] = $value;
				if(false !== $k = array_search($_POST["idP"], $value)){
					//if(false !== $k = array_search($_POST["idInv"], $value)){
						/*existe este producto en el carrito, hay que actualizar las cantidades*/
						
					//}
					$qty = $value["qty"];
					$qtyT = $_POST["qty"] + $qty;
					$temp = array("p"=>$_POST["idP"],"qty"=>$qtyT);
					$carrito[$key] = $temp;
					$esNuevo = false;
				}
			}
			if($esNuevo){
				$temp = array("p"=>$_POST["idP"],"qty"=>$_POST["qty"]);
				array_push($carrito, $temp);
			}
			
			
		}else{
			$carrito = array();
			$temp = array("p"=>$_POST["idP"],"qty"=>$_POST["qty"]);
			array_push($carrito, $temp);
			/*setcookie("ech_cart",json_encode($carrito),time() + 60 * 60,"/");
			$respuesta["cart"] = $carrito;*/
		}

		//$respuesta["itemsCart"] = cart_data();
	}else{

	}
	setcookie("ech_cart_oferta",json_encode($carrito),$cookieExp,"/");
	$respuesta["cart_oferta"] = $carrito;
	$respuesta["producto"] = findProd_oferta($_POST["idP"],null);
	$respuesta["qty"] = $_POST["qty"];
	responder($respuesta);
}

function cart_data_ajax(){
	$respuesta = array();
	$productos = array();
	$subtotal = 0.0;
	$algunError = false;

	$ech_cart = isset($_COOKIE["ech_cart"]);
	$ech_cart_oferta = isset($_COOKIE["ech_cart_oferta"]);

	if($ech_cart){
		$carrito = json_decode( $_COOKIE["ech_cart"], true );
		foreach ($carrito as $k => $v) {
			$resp = findProd($v["p"],$v["inv"],null);
			if($resp){
				$resp["qty"] = $v["qty"];
				$resp["total"] = $v["qty"] * $resp["Precio_publico"];
				$subtotal += floatval($v["qty"] * $resp["Precio_publico"]);
				
				if($v["qty"] < $resp["Minimo_compra"]){
					$resp["error"] = true;
					$resp["detalle_error"] = "Este producto requiere un mínimo de compra de ".$resp["Minimo_compra"]." unidades.";
					$algunError = true;
				}
				array_push($productos, $resp);
			}
		}
		/*foreach ($carrito as $k => $v) {
			$resp = findProd($v["p"],$v["inv"],$conn);
			if($resp){
				$resp["qty"] = $v["qty"];
				if($v["qty"] >= $resp["Minimo_compra"]){
					$resp["total"] = $v["qty"] * $resp["Precio_publico"];
					$subtotal += floatval($v["qty"] * $resp["Precio_publico"]);
					array_push($productos, $resp);
				}else{
					$respuesta["code"] = 400;
					$respuesta["msg"] = "El producto ".$resp["Producto"]." requiere un mínimo de compra de ".$resp["Minimo_compra"]." unidades.";
					return $respuesta;
					array_push($errores, array())
					break;
					
				}
				
			}
		}*/
		$respuesta["code"] = 200;
		$respuesta["itemsCart"] = $productos;
		$respuesta["algunError"] = $algunError;
		//$subtotal = floatval($subtotal/1.16);
		//$respuesta["subtotal"] = number_format($subtotal,2,'.',',');
	}/*else{
		$algun_carrito = false;
		//$respuesta["code"] = 400;
	}*/
	$productos = array();
	if($ech_cart_oferta){
		$carrito = json_decode( $_COOKIE["ech_cart_oferta"], true );
		foreach ($carrito as $k => $v) {
			$resp = findProd_oferta($v["p"],null);
			if($resp){
				$resp["qty"] = $v["qty"];
				$resp["total"] = $v["qty"] * $resp["precio"];
				$subtotal += floatval($v["qty"] * $resp["precio"]);
				array_push($productos, $resp);
			}
		}
		$respuesta["code"] = 200;
		$respuesta["itemsCart_oferta"] = $productos;
		//$subtotal = floatval($subtotal/1.16);
		//$respuesta["subtotal_oferta"] = number_format($subtotal,2,'.',',');
	}
	if($ech_cart || $ech_cart_oferta){
		$respuesta["code"] = 200;
		/*$sub = $ech_cart ? $respuesta["subtotal"] : 0.0;
		$sub2 = $ech_cart_oferta == true? floatval($respuesta["subtotal_oferta"]) : 0.0;
		$suma = $sub+$sub2;
		$respuesta["sub"] = $sub;
		$respuesta["sub2"] = $sub2;
		$respuesta["suma"] = $suma;
		$subtotal = floatval($suma/1.16);*/
		//$subtotal = floatval($subtotal/1.16);
		$respuesta["subtotal"] = number_format($subtotal,2,'.',',');
	}
	else
		$respuesta["code"] = 400;
	responder($respuesta);
	
}

function del_item_cart_ajax(){
	$respuesta = array();
	global $cookieExp;
	if(isset($_COOKIE["ech_cart"])){
		if(isset($_POST["idP"],$_POST["idInv"])){
			$carrito = json_decode( $_COOKIE["ech_cart"], true );
			foreach ($carrito as $key => $v) {
				if(false !== $k = array_search($_POST["idP"], $v)){
					if(false !== $k = array_search($_POST["idInv"], $v)){
						/*existe este producto en el carrito, hay que actualizar las cantidades*/
						unset($carrito[$key]);
						$respuesta["del"] = $key;
					}
				}
			}
			setcookie("ech_cart",json_encode($carrito),$cookieExp,"/");
			$respuesta["cart"] = $carrito;
		}else{
			$respuesta["mal"] = "NO HAY NADA";
		}
	}else{
		$respuesta["mal2"] = "NO HAY NADA2";
	}
	responder($respuesta);
}

function del_item_cart_ajax_oferta(){
	$respuesta = array();
	global $cookieExp;
	if(isset($_COOKIE["ech_cart_oferta"])){
		if(isset($_POST["idP"])){
			$carrito = json_decode( $_COOKIE["ech_cart_oferta"], true );
			foreach ($carrito as $key => $v) {
				if(false !== $k = array_search($_POST["idP"], $v)){
					unset($carrito[$key]);
					$respuesta["del"] = $key;
				}
			}
			setcookie("ech_cart_oferta",json_encode($carrito),$cookieExp,"/");
			$respuesta["cart_oferta"] = $carrito;
		}else{
			$respuesta["mal"] = "NO HAY NADA";
		}
	}else{
		$respuesta["mal2"] = "NO HAY NADA2";
	}
	responder($respuesta);
}

function findProd($idP,$idInv, $conn){
	$data = false;
	try {
		if($conn == null)
			$conn = PDOConnection::getConnection();
		$sql = "SELECT Catalogo.ID, Catalogo.Producto, Catalogo.Descripcion, Catalogo.Url_imagen, /*ROUND(*/Inventario.Precio_publico/* / 2.0,2)*/ AS Precio_publico, Inventario.Min, Inventario.ID as ID_inventario, Inventario.Minimo_compra
		FROM Catalogo
		INNER JOIN Inventario
		ON Inventario.idCatalogo = Catalogo.ID
		WHERE Catalogo.ID = :id
		AND Catalogo.Status = 'Activo'
		AND Inventario.ID = :idInv";
		$stm = $conn->prepare($sql);
		$stm->bindParam("id",$idP);
		$stm->bindParam("idInv",$idInv);
		$stm->execute();
		$data = $stm->fetch(PDO::FETCH_ASSOC);
	} catch (Exception $e) {
		$data = false;
	}
	if($conn != null)
		$conn = null;
	return $data;

}

function findProd_oferta($idP, $conn){
	$data = false;
	global $idSucursal;
	try {
		if($conn == null)
			$conn = PDOConnection::getConnection();
		$sql = "SELECT Promociones.ID,Promociones.Promocion, Promociones.Url_imagen, Promociones.Descripcion, /*ROUND(*/Asignacion_promo.precio /*/ 2.0,2)*/ AS precio, Asignacion_promo.ahorro, Lineas.Linea
		FROM Promociones
		INNER JOIN Asignacion_promo ON Promociones.ID = Asignacion_promo.idPromocion
		INNER JOIN Lineas ON Promociones.idLinea = Lineas.ID
		WHERE Asignacion_promo.idSucursal = $idSucursal
		AND Promociones.ID = :idP";
		$stm = $conn->prepare($sql);
		$stm->bindParam(":idP",$idP);
		//$stm->bindParam("idInv",$idInv);
		$stm->execute();
		$data = $stm->fetch(PDO::FETCH_ASSOC);
	} catch (Exception $e) {
		$data = false;
	}
	if($conn != null)
		$conn = null;
	return $data;

}

function get_cart(){
	$respuesta = array();
	$productos = array();
	$algunError = false;
	$subtotal = 0.0;
	$ech_cart = isset($_COOKIE["ech_cart"]);
	$ech_cart_oferta = isset($_COOKIE["ech_cart_oferta"]);
	if($ech_cart){
		$carrito = json_decode( $_COOKIE["ech_cart"], true );
		foreach ($carrito as $k => $v) {
			$resp = findProd($v["p"],$v["inv"],null);
			if($resp){
				$resp["qty"] = $v["qty"];
				$resp["total"] = $v["qty"] * $resp["Precio_publico"];
				$subtotal += floatval($v["qty"] * $resp["Precio_publico"]);
				if($v["qty"] < $resp["Minimo_compra"]){
					$resp["error"] = true;
					$resp["detalle_error"] = "Este producto requiere un mínimo de compra de ".$resp["Minimo_compra"]." unidades.";
					$algunError = true;
				}
				array_push($productos, $resp);
			}
		}
		$respuesta["code"] = 200;
		$respuesta["itemsCart"] = $productos;
		$respuesta["algunError"] = $algunError;
		//$subtotal = floatval($subtotal/1.16);
		//$respuesta["subtotal"] = number_format($subtotal,2,'.',',');
	}/*else{
		$algun_carrito = false;
		//$respuesta["code"] = 400;
	}*/
	$productos = array();
	if($ech_cart_oferta){
		$carrito = json_decode( $_COOKIE["ech_cart_oferta"], true );
		foreach ($carrito as $k => $v) {
			$resp = findProd_oferta($v["p"],null);
			if($resp){
				$resp["qty"] = $v["qty"];
				$resp["total"] = $v["qty"] * $resp["precio"];
				$subtotal += floatval($v["qty"] * $resp["precio"]);
				array_push($productos, $resp);
			}
		}
		$respuesta["code"] = 200;
		$respuesta["itemsCart_oferta"] = $productos;
		//$subtotal = floatval($subtotal/1.16);
		//$respuesta["subtotal_oferta"] = number_format($subtotal,2,'.',',');
	}
	if($ech_cart || $ech_cart_oferta){
		$respuesta["code"] = 200;
		/*$sub = $ech_cart ? $respuesta["subtotal"] : 0.0;
		$sub2 = $ech_cart_oferta == true? floatval($respuesta["subtotal_oferta"]) : 0.0;
		$suma = $sub+$sub2;
		$respuesta["sub"] = $sub;
		$respuesta["sub2"] = $sub2;
		$respuesta["suma"] = $suma;
		$subtotal = floatval($suma/1.16);*/
		//$subtotal = floatval($subtotal/1.16);
		$respuesta["subtotal"] = /*number_format($subtotal,2,'.',',')*/(float)$subtotal;
	}
	else
		$respuesta["code"] = 400;
	//responder($respuesta);


	/*$respuesta = array();
	$productos = array();
	$subtotal = 0.0;
	if(isset($_COOKIE["ech_cart"])){
		$carrito = json_decode( $_COOKIE["ech_cart"], true );
		foreach ($carrito as $k => $v) {
			$resp = findProd($v["p"],$v["inv"],null);
			if($resp){
				$resp["qty"] = $v["qty"];
				$resp["total"] = $v["qty"] * $resp["Precio_publico"];
				$subtotal += floatval($v["qty"] * $resp["Precio_publico"]);
				array_push($productos, $resp);
			}
		}
		$respuesta["code"] = 200;
		$respuesta["itemsCart"] = $productos;
		$respuesta["subtotal"] = number_format($subtotal,2,'.',',');
	}else{
		$respuesta["code"] = 400;
	}*/
	 
	return $respuesta;
}

/*case 7*/
function update_cart(){
	global $cookieExp;
	$respuesta["code"] = 400;
	if(isset($_COOKIE["ech_cart"])){
		if(isset($_POST["carrito"])){
			$carrito = json_decode($_POST["carrito"],true);
			$carritoCookie = json_decode( $_COOKIE["ech_cart"], true );
			foreach ($carritoCookie as $key => $value) {
				foreach ($carrito as $key2 => $value2) {
					$respuesta["keyCar2"] = $key2;
					if(false !== $k = array_search($value2["p"], $value)){
						if(false !== $k = array_search($value2["inv"], $value)){
							if($value2["eliminar"]){
								/*hay que borrarlo de las cookies, porque lo borró en la pagina cart.php*/
								unset($carritoCookie[$key]);
							}else{
								/*hacer update de cantidades*/
								$carritoCookie[$key] = $carrito[$key2];
							}
						}
					}
				}
				
			}
			setcookie("ech_cart",json_encode($carritoCookie),$cookieExp,"/");
			$respuesta["code"] = 200;
		}
	}

	if(isset($_COOKIE["ech_cart_oferta"])){
		if(isset($_POST["carritoOferta"])){
			$carrito = json_decode($_POST["carritoOferta"],true);
			$carritoCookie = json_decode( $_COOKIE["ech_cart_oferta"], true );
			foreach ($carritoCookie as $key => $value) {
				foreach ($carrito as $key2 => $value2) {
					$respuesta["keyCar2"] = $key2;
					if(false !== $k = array_search($value2["p"], $value)){
						if($value2["eliminar"]){
							/*hay que borrarlo de las cookies, porque lo borró en la pagina cart.php*/
							unset($carritoCookie[$key]);
						}else{
							/*hacer update de cantidades*/
							$carritoCookie[$key] = $carrito[$key2];
						}
						/*if(false !== $k = array_search($value2["inv"], $value)){
							
						}*/
					}
				}
				
			}
			setcookie("ech_cart_oferta",json_encode($carritoCookie),$cookieExp,"/");
			$respuesta["code"] = 200;
		}
	}
	responder ($respuesta);
}

/*case 8*/
function guardar_datos_envio(){
	global $cookieExp;
	global $loginExp;
	global $idCliente;
	$descuento = 0;
	$usuario = get_datos_usuario2();
	if($usuario["code"] == 200)
	    $descuento = $usuario["usuario"]["Descuento_%"];
	if(!isset($_COOKIE["ech_lg"])){
		if(isset($_COOKIE["ech_lg_tmp"])){
			try {
				//global $idCliente;
				$descuento = 0;
				$tmp = json_decode( $_COOKIE["ech_lg_tmp"], true );
				$conn = PDOConnection::getConnection();
				$sql = "INSERT INTO Clientes_menudeo SET Nombre = :nombre, Apellidos = '',Empresa = :empresa, Cargo = '', Calle_numero = :calle_numero, Colonia = :colonia, Ciudad = :ciudad, Municipio = :municipio, Estado = :estado, Pais = 'Mexico', CP = :cp, RFC = '', Tel1 = :tel, Tel2 = '', Email = :email, Clientes_menudeo.`Descuento_%` = :descuento, idCliente = :idCliente, Clientes_menudeo.`Status` = 'Activo', Nivel = '', Dia_visita = 0, Asignado = 0, Nacimiento = :nacimiento, Password = :pass";
				$calle_num = $_POST["billing_calle"]." #".$_POST["billing_exterior"];
				$stm = $conn->prepare($sql);
				$stm->bindParam(":nombre",$_POST["billing_fname"]);
				$stm->bindParam(":empresa",$_POST["shipping_company_factura"] != "" ? $_POST["shipping_company_factura"] : "NA");
				$stm->bindParam(":calle_numero",$calle_num);
				$stm->bindParam(":colonia",$_POST["billing_colonia"]);
				$stm->bindParam(":ciudad",$_POST["billing_municipio"]);
				$stm->bindParam(":municipio",$_POST["billing_municipio"]);
				$stm->bindParam(":estado",$_POST["billing_estado"]);
				$stm->bindParam(":cp",$_POST["billing_cp"]);
				$stm->bindParam(":tel",$_POST["tel"]);
				$stm->bindParam(":email",$_POST["billing_email"]);
				$stm->bindParam(":idCliente",$idCliente);
				$stm->bindParam(":descuento",$descuento);
				$nac = $_POST["billing_year"]."-".$_POST["billing_mes"]."-".$_POST["billing_dia"];
				$stm->bindParam(":nacimiento",$nac);
				$stm->bindParam(":pass",$tmp["p"]);
				/*$stm->bindParam(":des",$descuento);*/
				$respuesta["sql"] = $stm;
				$result = false;
				$result = $stm->execute();
				if($result){

					$idClienteMen = $conn->lastInsertId();
					$tmp = array(
						"u"=>$_POST["billing_email"],
						"id"=>$idClienteMen
					);
					setcookie("ech_lg",json_encode($tmp),$loginExp,"/");
					setcookie("ech_lg_tmp","",$loginExp * -1,"/");
				  	unset($_COOKIE["ech_lg_tmp"]);
					$respuesta["idCliente"] = $idClienteMen;
					//$respuesta["ventas"]  = guardar_venta($idClienteMen, $conn);
					$respuesta["code"] = 200;

				}else{
					$respuesta["code"] = 400;			
					$respuesta["msg"] = "Error al guardar al cliente";
				}
			} catch (Exception $e) {
				$respuesta["code"] = 500;
				$respuesta["msg"] = "Error en el servidor: \n".$e->getMessage();
			}
			$conn = null;
		}
		
	}else{
		$conn = PDOConnection::getConnection();
		$sql = "UPDATE Clientes_menudeo SET Nombre = :nombre, Apellidos = :apellidos, Empresa = :empresa, Calle_numero = :calle_numero, Colonia = :colonia, Ciudad = :ciudad, Municipio = :municipio, Estado = :estado, CP = :cp, Tel1 = :tel, Email = :email, Nacimiento = :nacimiento
			WHERE ID = :id";
		$calle_num = $_POST["billing_calle"]." #".$_POST["billing_exterior"];
		$stm = $conn->prepare($sql);
		$stm->bindParam(":nombre",$_POST["billing_fname"]);
		$stm->bindParam(":apellidos",$_POST["apellidos"]);
		$empresa = $_POST["shipping_company_factura"] != "" ? $_POST["shipping_company_factura"] : "NA";
		$stm->bindParam(":empresa",$empresa);
		$stm->bindParam(":calle_numero",$calle_num);
		$stm->bindParam(":colonia",$_POST["billing_colonia"]);
		$stm->bindParam(":ciudad",$_POST["billing_municipio"]);
		$stm->bindParam(":municipio",$_POST["billing_municipio"]);
		$stm->bindParam(":estado",$_POST["billing_estado"]);
		$stm->bindParam(":cp",$_POST["billing_cp"]);
		$stm->bindParam(":tel",$_POST["tel"]);
		$stm->bindParam(":email",$_POST["billing_email"]);
		//$stm->bindParam(":idCliente",$idCliente);
		//$stm->bindParam(":descuento",$descuento);
		$stm->bindParam(":id",$_POST["id"]);
		$nac = $_POST["billing_year"]."-".$_POST["billing_mes"]."-".$_POST["billing_dia"];
		//echo $nac;
		if($nac == "--")
			$nac = NULL;
		$stm->bindParam(":nacimiento",$nac);
		/*$stm->bindParam(":des",$descuento);*/
		$respuesta["sql"] = $stm;
		$result = false;
		$result = $stm->execute();
		$respuesta["idCliente"] = $_POST["id"];
		//$respuesta["ventas"]  = guardar_venta($_POST["id"], $conn);
		$respuesta["code"] = 200;
		
	}
	
	responder($respuesta);
}

function guardar_venta_original($idC, $conn){

	try {
		global $idCliente;
		global $cookieExp;
		$productos = array();
		$productosOferta = array();
		$usuario = get_datos_usuario2();

		if(isset($_COOKIE["ech_cart"]))
			$carrito = json_decode( $_COOKIE["ech_cart"], true );
		else
			$carrito = array();
		if(isset($_COOKIE["ech_cart_oferta"]))
			$carritoOferta = json_decode( $_COOKIE["ech_cart_oferta"], true );
		else
			$carritoOferta = array();
		$subtotal = 0;
		//$idCliente = $idC;
		foreach ($carrito as $k => $v) {
			$resp = findProd($v["p"],$v["inv"],$conn);
			if($resp){
				$resp["qty"] = $v["qty"];
				if($v["qty"] >= $resp["Minimo_compra"]){
					$resp["total"] = $v["qty"] * $resp["Precio_publico"];
					$subtotal += floatval($v["qty"] * $resp["Precio_publico"]);
					array_push($productos, $resp);
				}else{
					$respuesta["code"] = 400;
					$respuesta["msg"] = "El producto ".$resp["Producto"]." requiere un mínimo de compra de ".$resp["Minimo_compra"]." unidades.";
					return $respuesta;
					break;
					
				}
				
			}
		}
		foreach ($carritoOferta as $k => $v) {
			$resp = findProd_oferta($v["p"],$conn);
			if($resp){
				$resp["qty"] = $v["qty"];
				$resp["total"] = $v["qty"] * $resp["precio"];
				$subtotal += floatval($v["qty"] * $resp["precio"]);
				array_push($productosOferta, $resp);
			}
		}
		$pedidos = 1;
		$extraido = 0;
		$empaquetado = 0;

		if($usuario["code"] = 200){
			$desc = $usuario["usuario"]["Descuento_%"];
			$descuentoCliente = floatval(($subtotal * $desc)/100);
			$idCliente = $usuario["usuario"]["idCliente"];
		}else{
			$descuentoCliente = 0;
		}
		if(isset($_COOKIE["cupon"])){
		  $cupon = json_decode($_COOKIE["cupon"],true);
		  
		  if($cupon["modo"] == "Porcentaje"){
		    $descuentoCupon = floatval(($subtotal-$descuentoCliente)*(floatval($cupon["monto"])/100));
		    $respuesta["operacion"] = "((".$subtotal." - ".$descuentoCliente.")*(".$cupon["monto"].")/100)";
		  }else{
		    $descuentoCupon = floatval($cupon["monto"]);
		    if($descuentoCupon >= $subtotal)
		    	$descuentoCupon = $subtotal;
		  }
		}else{
			$descuentoCupon = 0;
			$cupon = false;
		}
		
		$impuestos = 0;
		$envio = 0.0;
		$respuesta["subtotal"] = $subtotal;
		//$descuento_adicional = floatval(($descuentoCupon/($subtotal - $descuentoCliente))*100);
		$descuento_adicional = floatval(($descuentoCupon/$subtotal)*100);
		$respuesta["descuentoCupon"] = $descuentoCupon;
		if($subtotal >= 1000)
			$envio = 0;
		else
			$envio = 100;
		/*if(isset($_COOKIE["ech_lg"])){
		  $tmp = json_decode( $_COOKIE["ech_lg"], true );
		  $email = $tmp["u"];
		  $idCl = $tmp["idCl"];
		  //if($idCl != 1967)
		  	//$envio = 100;
		}*/
		$totalSinDescuento = $subtotal;
		$respuesta["descCli"] = $descuentoCliente;
		$total = floatval($subtotal - ($descuentoCliente + $descuentoCupon));
		$respuesta["TOTAL"] = $total;
		if($total < 0)
		  $total = 0;
		$subtotal = floatval($total / 1.16);
		$respuesta["otro_subt"] = $subtotal;
		$total += $envio;
		if($subtotal < 0)
		  $subtotal = 0;
		$impuestos = floatval($total - $subtotal);
		if($impuestos < 0)
		  $impuestos = 0;

		$total = number_format($total,2,'.','');
		$subtotal = number_format($subtotal,2,'.','');
		$impuestos = number_format($impuestos,2,'.','');
		$descuentoCliente = number_format($descuentoCliente,2,'.','');
		//$descuentoCupon = number_format($descuentoCupon,2,'.','');
		$totalSinDescuento = number_format($totalSinDescuento,2,'.','');

		//$conn = PDOConnection::getConnection();
		//if(isset($_POST["idVenta"]))
			//$sql = "UPDATE Ventas SET Fecha_venta = :fecha, idCliente = :idCliente, Descuento = :descuento, Subtotal = :subtotal, Impuestos = :impuestos, Total = :total, Adeudo = :adeudo, Status = 'Pendiente', Pedidos = :pedidos, Extraido = :extraido, Empaquetado = :empaquetado WHERE ID = :idVenta";
		//else
			//$sql = "INSERT INTO Ventas SET Fecha_venta = :fecha, idCliente = :idCliente, Descuento = :descuento, Subtotal = :subtotal, Impuestos = :impuestos, Total = :total, Adeudo = :adeudo, Status = 'Pendiente', Pedidos = :pedidos, Extraido = :extraido, Empaquetado = :empaquetado";
		$sql = "INSERT INTO Ventas SET Fecha_venta = :fecha, idCliente = :idCliente, Descuento = :descuento, Subtotal = :subtotal, Impuestos = :impuestos, Total = :total, Adeudo = :adeudo, Status = 'Pendiente', Pedidos = :pedidos, Extraido = :extraido, Empaquetado = :empaquetado, Ventas_Directas = 1, descuento_adicional = :cupon";

		$conn->beginTransaction();
		$d = time();
		$fecha = date("Y-m-d H:i:s", $d);
		/*$total = $subtotal;
		if($total >= 1000)
			$envio = 0;
		else
			$envio = 100;
		$totalVM = ($total - $descuentoVM) + $envio;
		$total = $total - $descuento;
		
		$total += $envio;*/
		
		//$subtotal = floatval($total/1.16);
		//$impuestos = floatval($total - $subtotal);
		$stm = $conn->prepare($sql);
		$stm->bindParam(":fecha",$fecha);
		$idCli = $idC;
		$stm->bindParam(":idCliente",$idCliente);
		$stm->bindParam(":descuento",$descuentoCliente);
		//$subtotal = number_format($subtotal,2,'.','');
		$stm->bindParam(":subtotal",$subtotal);
		//$impuestos = number_format($impuestos,2,'.','');
		$stm->bindParam(":impuestos",$impuestos);
		//$total = number_format($total,2,'.','');
		$stm->bindParam(":total",$total);
		$stm->bindParam(":adeudo",$total);
		$stm->bindParam(":pedidos",$pedidos);
		$stm->bindParam(":extraido",$extraido);
		$stm->bindParam(":empaquetado",$empaquetado);
		$stm->bindParam(":cupon",$descuento_adicional);
		//if(isset($_POST["idVenta"]))
			//$stm->bindParam(":idVenta",$_POST["idVenta"]);
		$result = $stm->execute();
		if($result){
			/*if(isset($_POST["idVenta"])){
				$respuesta["idVenta"] = $_POST["idVenta"];
				setcookie("idV",$_POST["idVenta"],$cookieExp,"/");
			}*/
			//else{
				$idVenta = $conn->lastInsertId();
				$respuesta["idVenta"] = $idVenta;
				setcookie("idV",$idVenta,$cookieExp,"/");
			//}
			
			$conn->commit();

			//if(isset($_POST["idVenta"]))
				//$sql = "UPDATE Ventas_menudeo SET Fecha_venta = :fecha, idCliente_menudeo = :idCliente, Descuento = :descuento, Subtotal = :subtotal, Impuestos = :impuestos, Total = :total, Total_desc = :totalDesc, Adeudo = :adeudo, Status = 'Pendiente', Extraido = :extraido, Empaquetado = :empaquetado, Compras_puntos = 0, Tipo_app = 'Web_service' WHERE idVenta = :idVenta";
			//else
				$sql = "INSERT INTO Ventas_menudeo SET idVenta = :idVenta, Fecha_venta = :fecha, idCliente_menudeo = :idCliente, Descuento = :descuento, Subtotal = :subtotal, Impuestos = :impuestos, Total = :total, Total_desc = :totalDesc, Adeudo = :adeudo, Status = 'Pendiente', Extraido = :extraido, Empaquetado = :empaquetado, Compras_puntos = 0, Tipo_app = 'Web_service'";
			$conn->beginTransaction();
			$stm = $conn->prepare($sql);
			$stm->bindParam(":idVenta",$idVenta);
			$stm->bindParam(":fecha",$fecha);
			$stm->bindParam(":idCliente",$idCli);
			$stm->bindParam(":descuento",$descuentoCliente);
			//$subtotal = number_format($subtotal,2,'.','');
			$stm->bindParam(":subtotal",$subtotal);
			//$impuestos = number_format($impuestos,2,'.','');
			$stm->bindParam(":impuestos",$impuestos);
			//$total = floatval(($subtotal+$impuestos+$envio)-$descuento);
			//$total = number_format($total,2,'.','');
			$stm->bindParam(":total",$totalSinDescuento);
			$stm->bindParam(":totalDesc",$total);
			$stm->bindParam(":adeudo",$total);
			$stm->bindParam(":extraido",$extraido);
			$stm->bindParam(":empaquetado",$empaquetado);
			//if(isset($_POST["idVenta"]))
				//$stm->bindParam(":empaquetado",$_POST["idVenta"]);
			$result2 = $stm->execute();
			if($result2){
				/*if(isset($_POST["idVentaMenudeo"])){
					$respuesta["idVentaMen"] = $_POST["idVentaMenudeo"];
					setcookie("idVM",$_POST["idVentaMenudeo"],$cookieExp,"/");
				}*/
				//else{
					$idVentaMen = $conn->lastInsertId();
					$respuesta["idVentaMen"] = $idVentaMen;
					setcookie("idVM",$idVentaMen,$cookieExp,"/");
				//}				
				$conn->commit();
				
				//$conn->beginTransaction();
				//if(isset($_POST["idVentaMenudeo"]))
					//$sql = "UPDATE Detalle_venta_menudeo SET idCatalogo = :idCatalogo, idPromocion = :idPromo, Cantidad = :qty, Precio_unitario = :precio, Importe = :importe, Tipo = 'Producto' WHERE idVenta_menudeo = :idVentaMenudeo";
				//else
					$sql = "INSERT INTO Detalle_venta_menudeo SET idVenta_menudeo = :idVenta, idCatalogo = :idCatalogo, idPromocion = :idPromo, Cantidad = :qty, Precio_unitario = :precio, Importe = :importe, Tipo = 'Producto'";
				$idPromo = NULL;
				foreach ($productos as $key => $value) {
					$stm1 = $conn->prepare($sql);
					/*if(isset($_POST["idVentaMenudeo"]))
						$stm1->bindParam(":idVentaMenudeo",$_POST["idVentaMenudeo"]);
					else*/
						$stm1->bindParam(":idVenta",$idVentaMen);
					$stm1->bindParam(":idCatalogo",$value["ID"]);
					$stm1->bindParam(":idPromo",$idPromo);
					$stm1->bindParam(":qty",$value["qty"]);
					$stm1->bindParam(":precio",$value["Precio_publico"]);
					$stm1->bindParam(":importe",$value["total"]);
					$result3 = $stm1->execute();
					$respuesta["foreach"][$key] = $conn->lastInsertId();

				}
				$idCatalogo = NULL;
				foreach ($productosOferta as $key => $value) {
					$stm1 = $conn->prepare($sql);
					//if(isset($_POST["idVentaMenudeo"]))
						//$stm1->bindParam(":idVentaMenudeo",$_POST["idVentaMenudeo"]);
					//else
						$stm1->bindParam(":idVenta",$idVentaMen);
					$stm1->bindParam(":idCatalogo",$idCatalogo);
					$stm1->bindParam(":idPromo",$value["ID"]);
					$stm1->bindParam(":qty",$value["qty"]);
					$stm1->bindParam(":precio",$value["precio"]);
					$stm1->bindParam(":importe",$value["total"]);
					$result3 = $stm1->execute();
					$respuesta["foreach2"][$key] = $conn->lastInsertId();

				}
				$respuesta["agregados"] = $productos;
				$respuesta["agregados_oferta"] = $productosOferta;
				//$conn->commit();
			}
			$respuesta["code"] = 200;
			//limpiar_carrito();
		}else{
			$respuesta["code"] = 400;
			$respuesta["msg"] = "Error al guardar los datos de la venta";
		}
		
	} catch (Exception $e) {
		$conn->rollback();
		$respuesta["code"] = 500;
		$respuesta["msg"] = $e->getMessage();
	}
	$conn = null;
	
	return $respuesta;
}

function guardar_venta(){
	try {
		global $idCliente;
		global $cookieExp;
		$conn = PDOConnection::getConnection();
		$productos = array();
		$productosOferta = array();
		$usuario = get_datos_usuario2();

		if(isset($_COOKIE["ech_cart"]))
			$carrito = json_decode( $_COOKIE["ech_cart"], true );
		else
			$carrito = array();
		if(isset($_COOKIE["ech_cart_oferta"]))
			$carritoOferta = json_decode( $_COOKIE["ech_cart_oferta"], true );
		else
			$carritoOferta = array();
		$subtotal = 0;
		//$idCliente = $idC;
		foreach ($carrito as $k => $v) {
			$resp = findProd($v["p"],$v["inv"],$conn);
			if($resp){
				$resp["qty"] = $v["qty"];
				if($v["qty"] >= $resp["Minimo_compra"]){
					$resp["total"] = $v["qty"] * $resp["Precio_publico"];
					$subtotal += floatval($v["qty"] * $resp["Precio_publico"]);
					array_push($productos, $resp);
				}else{
					$respuesta["code"] = 400;
					$respuesta["msg"] = "El producto ".$resp["Producto"]." requiere un mínimo de compra de ".$resp["Minimo_compra"]." unidades.";
					return $respuesta;
					break;
					
				}
				
			}
		}
		foreach ($carritoOferta as $k => $v) {
			$resp = findProd_oferta($v["p"],$conn);
			if($resp){
				$resp["qty"] = $v["qty"];
				$resp["total"] = $v["qty"] * $resp["precio"];
				$subtotal += floatval($v["qty"] * $resp["precio"]);
				array_push($productosOferta, $resp);
			}
		}
		$pedidos = 1;
		$extraido = 0;
		$empaquetado = 0;

		if($usuario["code"] = 200){
			$desc = $usuario["usuario"]["Descuento_%"];
			$descuentoCliente = floatval(($subtotal * $desc)/100);
			$idCliente = $usuario["usuario"]["idCliente"];
		}else{
			$descuentoCliente = 0;
		}
		if(isset($_COOKIE["cupon"])){
		  $cupon = json_decode($_COOKIE["cupon"],true);
		  
		  if($cupon["modo"] == "Porcentaje"){
		    $descuentoCupon = floatval(($subtotal-$descuentoCliente)*(floatval($cupon["monto"])/100));
		    $respuesta["operacion"] = "((".$subtotal." - ".$descuentoCliente.")*(".$cupon["monto"].")/100)";
		  }else{
		    $descuentoCupon = floatval($cupon["monto"]);
		    if($descuentoCupon >= $subtotal)
		    	$descuentoCupon = $subtotal;
		  }
		}else{
			$descuentoCupon = 0;
			$cupon = false;
		}
		
		$impuestos = 0;
		$envio = 0.0;
		$respuesta["subtotal"] = $subtotal;
		//$descuento_adicional = floatval(($descuentoCupon/($subtotal - $descuentoCliente))*100);
		$descuento_adicional = floatval(($descuentoCupon/$subtotal)*100);
		$respuesta["descuentoCupon"] = $descuentoCupon;
		if($subtotal >= 1000)
			$envio = 0;
		else
			$envio = 100;
		$totalSinDescuento = $subtotal;
		$respuesta["descCli"] = $descuentoCliente;
		$total = floatval($subtotal - ($descuentoCliente + $descuentoCupon));
		$respuesta["TOTAL"] = $total;
		if($total < 0)
		  $total = 0;
		$subtotal = floatval($total / 1.16);
		$impuestos = floatval($total - $subtotal);
		$respuesta["otro_subt"] = $subtotal;
		$total += $envio;
		if($subtotal < 0)
		  $subtotal = 0;
		
		if($impuestos < 0)
		  $impuestos = 0;

		$total = number_format($total,2,'.','');
		$subtotal = number_format($subtotal,2,'.','');
		$impuestos = number_format($impuestos,2,'.','');
		$descuentoCliente = number_format($descuentoCliente,2,'.','');
		//$descuentoCupon = number_format($descuentoCupon,2,'.','');
		$totalSinDescuento = number_format($totalSinDescuento,2,'.','');

		$sql = "INSERT INTO Ventas SET Fecha_venta = :fecha, idCliente = :idCliente, Descuento = :descuento, Subtotal = :subtotal, Impuestos = :impuestos, Total = :total, Adeudo = :adeudo, Status = 'Pendiente', Pedidos = :pedidos, Extraido = :extraido, Empaquetado = :empaquetado, Ventas_Directas = 1, descuento_adicional = :cupon";

		$conn->beginTransaction();
		$d = time();
		$fecha = date("Y-m-d H:i:s", $d);
		$stm = $conn->prepare($sql);
		$stm->bindParam(":fecha",$fecha);
		$idCli = $usuario["usuario"]["ID"];
		$stm->bindParam(":idCliente",$idCliente);
		$stm->bindParam(":descuento",$descuentoCliente);
		//$subtotal = number_format($subtotal,2,'.','');
		$stm->bindParam(":subtotal",$subtotal);
		//$impuestos = number_format($impuestos,2,'.','');
		$stm->bindParam(":impuestos",$impuestos);
		//$total = number_format($total,2,'.','');
		$stm->bindParam(":total",$total);
		$stm->bindParam(":adeudo",$total);
		$stm->bindParam(":pedidos",$pedidos);
		$stm->bindParam(":extraido",$extraido);
		$stm->bindParam(":empaquetado",$empaquetado);
		$stm->bindParam(":cupon",$descuento_adicional);
		$result = $stm->execute();
		if($result){
			$idVenta = $conn->lastInsertId();
			$respuesta["idVenta"] = $idVenta;
			setcookie("idV",$idVenta,$cookieExp,"/");
			$conn->commit();
			$sql = "INSERT INTO Ventas_menudeo SET idVenta = :idVenta, Fecha_venta = :fecha, idCliente_menudeo = :idCliente, Descuento = :descuento, Subtotal = :subtotal, Impuestos = :impuestos, Total = :total, Total_desc = :totalDesc, Adeudo = :adeudo, Status = 'Pendiente', Extraido = :extraido, Empaquetado = :empaquetado, Compras_puntos = 0, Tipo_app = 'Web_service'";
			$conn->beginTransaction();
			$stm = $conn->prepare($sql);
			$stm->bindParam(":idVenta",$idVenta);
			$stm->bindParam(":fecha",$fecha);
			$stm->bindParam(":idCliente",$idCli);
			$stm->bindParam(":descuento",$descuentoCliente);
			//$subtotal = number_format($subtotal,2,'.','');
			$stm->bindParam(":subtotal",$subtotal);
			//$impuestos = number_format($impuestos,2,'.','');
			$stm->bindParam(":impuestos",$impuestos);
			//$total = floatval(($subtotal+$impuestos+$envio)-$descuento);
			//$total = number_format($total,2,'.','');
			$stm->bindParam(":total",$totalSinDescuento);
			$stm->bindParam(":totalDesc",$total);
			$stm->bindParam(":adeudo",$total);
			$stm->bindParam(":extraido",$extraido);
			$stm->bindParam(":empaquetado",$empaquetado);
			//if(isset($_POST["idVenta"]))
				//$stm->bindParam(":empaquetado",$_POST["idVenta"]);
			$result2 = $stm->execute();
			if($result2){
				$idVentaMen = $conn->lastInsertId();
				$respuesta["idVentaMen"] = $idVentaMen;
				setcookie("idVM",$idVentaMen,$cookieExp,"/");
							
				$conn->commit();
					$sql = "INSERT INTO Detalle_venta_menudeo SET idVenta_menudeo = :idVenta, idCatalogo = :idCatalogo, idPromocion = :idPromo, Cantidad = :qty, Precio_unitario = :precio, Importe = :importe, Tipo = 'Producto'";
				$idPromo = NULL;
				foreach ($productos as $key => $value) {
					$stm1 = $conn->prepare($sql);
					$stm1->bindParam(":idVenta",$idVentaMen);
					$stm1->bindParam(":idCatalogo",$value["ID"]);
					$stm1->bindParam(":idPromo",$idPromo);
					$stm1->bindParam(":qty",$value["qty"]);
					$stm1->bindParam(":precio",$value["Precio_publico"]);
					$stm1->bindParam(":importe",$value["total"]);
					$result3 = $stm1->execute();
					$respuesta["foreach"][$key] = $conn->lastInsertId();

				}
				$idCatalogo = NULL;
				foreach ($productosOferta as $key => $value) {
					$stm1 = $conn->prepare($sql);
					$stm1->bindParam(":idVenta",$idVentaMen);
					$stm1->bindParam(":idCatalogo",$idCatalogo);
					$stm1->bindParam(":idPromo",$value["ID"]);
					$stm1->bindParam(":qty",$value["qty"]);
					$stm1->bindParam(":precio",$value["precio"]);
					$stm1->bindParam(":importe",$value["total"]);
					$result3 = $stm1->execute();
					$respuesta["foreach2"][$key] = $conn->lastInsertId();

				}
				$respuesta["agregados"] = $productos;
				$respuesta["agregados_oferta"] = $productosOferta;
				//$conn->commit();
			}
			$respuesta["code"] = 200;
			//limpiar_carrito();
		}else{
			$respuesta["code"] = 400;
			$respuesta["msg"] = "Error al guardar los datos de la venta";
		}
		
	} catch (Exception $e) {
		$conn->rollback();
		$respuesta["code"] = 500;
		$respuesta["msg"] = $e->getMessage();
	}
	$conn = null;
	
	return $respuesta;
}

function limpiar_carrito(){
	global $cookieExp;
	if(isset($_COOKIE["ech_cart"])){
		setcookie("ech_cart","",$cookieExp * -1,"/");
	  	unset($_COOKIE["ech_cart"]);
	}
	if(isset($_COOKIE["ech_cart_oferta"])){
		setcookie("ech_cart_oferta","",$cookieExp * -1,"/");
	  	unset($_COOKIE["ech_cart_oferta"]);
	}
	if(isset($_COOKIE["idV"])){
		setcookie("idV","",$cookieExp * -1,"/");
	  	unset($_COOKIE["idV"]);
	}
	if(isset($_COOKIE["idVM"])){
		setcookie("idVM","",$cookieExp * -1,"/");
	  	unset($_COOKIE["idVM"]);
	}
	$respuesta["code"] = 200;
	responder($respuesta);
}

function limpiar_carrito_return(){
	global $cookieExp;
	if(isset($_COOKIE["ech_cart"])){
		setcookie("ech_cart","",$cookieExp * -1,"/");
	  	unset($_COOKIE["ech_cart"]);
	}
	if(isset($_COOKIE["ech_cart_oferta"])){
		setcookie("ech_cart_oferta","",$cookieExp * -1,"/");
	  	unset($_COOKIE["ech_cart_oferta"]);
	}
	if(isset($_COOKIE["idV"])){
		setcookie("idV","",$cookieExp * -1,"/");
	  	unset($_COOKIE["idV"]);
	}
	if(isset($_COOKIE["idVM"])){
		setcookie("idVM","",$cookieExp * -1,"/");
	  	unset($_COOKIE["idVM"]);
	}
	$respuesta["code"] = 200;
	return($respuesta);
}

function borrar_venta(){
	try {
		$conn = PDOConnection::getConnection();
		$sql = "DELETE FROM Detalle_venta_menudeo WHERE idVenta_menudeo = :idVentaMenudeo";
		$stm = $conn->prepare($sql);
		$stm->bindParam(":idVentaMenudeo",$_POST["idVentaMenudeo"]);
		$result = false;
		$result = $stm->execute();	
		$sql = "DELETE FROM Ventas_menudeo WHERE idVenta = :idVenta";
		$stm = $conn->prepare($sql);
		$stm->bindParam(":idVenta",$_POST["idVenta"]);
		$stm->execute();
		$sql = "DELETE FROM Ventas WHERE ID = :idVenta";
		$stm = $conn->prepare($sql);
		$stm->bindParam(":idVenta",$_POST["idVenta"]);
		$stm->execute();
		$respuesta["code"] = 200;
	} catch (Exception $e) {
		$respuesta["code"] = 500;
		$respuesta["msg"] = "Error en el servidor: \n".$e->getMessage();
	}
	responder($respuesta);
}

/*function actualizar_status_venta(){
	try {
		$conn = PDOConnection::getConnection();
		if($_POST["status"] == "Pagado")
			$sql = "UPDATE Ventas_menudeo SET Status = :status, Adeudo = 0.00 WHERE ID = :idVentaMenudeo";
		else
			$sql = "UPDATE Ventas_menudeo SET Status = :status WHERE ID = :idVentaMenudeo";
		$stm = $conn->prepare($sql);
		$stm->bindParam(":status",$_POST["status"]);
		$stm->bindParam(":idVentaMenudeo",$_POST["idVentaMenudeo"]);
		$result = false;
		$result = $stm->execute();
		if($_POST["status"] == "Pagado")	
			$sql = "UPDATE Ventas SET Status = :status, Adeudo = 0.00 WHERE ID = :idVenta";
		else
			$sql = "UPDATE Ventas SET Status = :status WHERE ID = :idVenta";
		$stm = $conn->prepare($sql);
		$stm->bindParam(":idVenta",$_POST["idVenta"]);
		$stm->bindParam(":status",$_POST["status"]);
		$stm->execute();
		$respuesta["code"] = 200;
	} catch (Exception $e) {
		$respuesta["code"] = 500;
		$respuesta["msg"] = "Error en el servidor: \n".$e->getMessage();
	}
	responder($respuesta);
}*/

function actualizar_status_venta(){
	$res = guardar_venta();
	//var_dump($res);
	if($res["code"] == 200){
		$eleccion = '';
		$sql = "UPDATE Ventas SET Status = :status, Adeudo = 0.00, idPago = :idPago, idOrden = :idOrden WHERE ID = :idVenta";
		if(isset($_COOKIE["cupon"])){
			$cupon = json_decode($_COOKIE["cupon"],true);
			if($cupon["gral"]){
				$sql = "UPDATE Ventas SET Status = :status, Adeudo = 0.00, idPago = :idPago, idOrden = :idOrden, idCupon = ".$cupon['id']." WHERE ID = :idVenta";
			}
		}
		//$sql = "UPDATE Ventas SET Status = :status, Adeudo = 0.00, idPago = :idPago, idOrden = :idOrden WHERE ID = :idVenta";

		if (isset($_POST["tipo"]) && $_POST["tipo"] == "Paypal") {
			$id_pago = $_POST['payment_id'];
			$status_pago = $_POST['payment_status'];
			$id_orden = $_POST['merchant_order_id'];
			$status = 'Pagado';
			$eleccion = 'PayPal';
		}else if(isset($_POST["tipo"]) && $_POST["tipo"] == "transferencia"){
			$sql = "UPDATE Ventas SET Status = :status, idPago = :idPago, idOrden = :idOrden WHERE ID = :idVenta";
			if(isset($_COOKIE["cupon"])){
				$cupon = json_decode($_COOKIE["cupon"],true);
				if($cupon["gral"]){
					$sql = "UPDATE Ventas SET Status = :status, idPago = :idPago, idOrden = :idOrden, idCupon = ".$cupon['id']." WHERE ID = :idVenta";
				}
			}
			$id_pago = "";
			$status_pago = "Adeudo";
			$id_orden = "";
			$status = 'Adeudo';
			$eleccion = 'Transferencia';
		}
		else{
			$id = $_POST["payment_id"];
			$status = $_POST["payment_status"];
			$cupon = false;
			if(isset($_COOKIE["cupon"])){
				$cupon = json_decode($_COOKIE["cupon"],true);
				if($cupon["gral"]){
					$sql .= " AND idCupon = ".$cupon['id'];
				}
			}

			if($status == 'success'){
				$status = 'Aprobado';
				if($cupon && $cupon["gral"])
					$sql = "UPDATE Ventas SET Status = :status, Adeudo = 0.00, idPago = :idPago, idOrden = :idOrden, idCupon = ".$cupon['id']." WHERE ID = :idVenta";
				else
					$sql = "UPDATE Ventas SET Status = :status, Adeudo = 0.00, idPago = :idPago, idOrden = :idOrden WHERE ID = :idVenta";
			}else
			if($status == 'failure'){
				$status = 'Fallido';

			}else
			if($status == 'pending'){
				$status = 'Adeudo';
				if($cupon && $cupon["gral"])
					$sql = "UPDATE Ventas SET Status = :status, idPago = :idPago, idOrden = :idOrden, idCupon = ".$cupon['id']." WHERE ID = :idVenta";
				else
					$sql = "UPDATE Ventas SET Status = :status, idPago = :idPago, idOrden = :idOrden WHERE ID = :idVenta";
			}else
			if($status == 'approved'){
				$status = 'Aprobado';
				if($cupon && $cupon["gral"])
					$sql = "UPDATE Ventas SET Status = :status, Adeudo = 0.00, idPago = :idPago, idOrden = :idOrden, idCupon = ".$cupon['id']." WHERE ID = :idVenta";
				else
					$sql = "UPDATE Ventas SET Status = :status, Adeudo = 0.00, idPago = :idPago, idOrden = :idOrden WHERE ID = :idVenta";
			}else
			if($status == 'in_process'){
				$status = 'Adeudo';
				if($cupon && $cupon["gral"])
					$sql = "UPDATE Ventas SET Status = :status, idPago = :idPago, idOrden = :idOrden, idCupon = ".$cupon['id']." WHERE ID = :idVenta";
				else
					$sql = "UPDATE Ventas SET Status = :status, idPago = :idPago, idOrden = :idOrden WHERE ID = :idVenta";
			}

			$id_pago = $_POST['payment_id'];
			$status_pago = $_POST['payment_status'];
			$id_orden = $_POST['merchant_order_id'];		
			$eleccion = 'MercadoPago';

			
			//echo $id_pago;
		}
		if(isset($res["idVenta"])){
			$idV = $res["idVenta"];
			//echo "<h1>".$idV."</h1>";
			try {
				$conn = PDOConnection::getConnection();
				$stm = $conn->prepare($sql);
				$stm->bindParam(":idVenta",$idV);
				$stm->bindParam(":status",$status);
				$stm->bindParam(":idPago",$id_pago);
				$stm->bindParam(":idOrden",$id_orden);
				if($stm->execute()){
					$respuesta["code"] = 200;
					$respuesta["idVenta"] = $idV;

					$fecha = date('d-m-Y');
					$nuevafecha = strtotime ( '+18 day' , strtotime ( $fecha ) ) ;
					$nuevafecha = date( 'd-m-Y' , $nuevafecha );
					//$respuesta["envio"] = fechaEsp($nuevafecha);
					if(isset($_COOKIE["cupon"])){
						$cupon = json_decode($_COOKIE["cupon"],true);
						if(!$cupon["gral"]){
							$sql = "UPDATE cupones_full SET Status = 'Asignado', idVenta = :idVenta WHERE ID = :idCupon";
							$stm = $conn->prepare($sql);
							$stm->bindParam(":idCupon",$cupon["id"]);
							$stm->bindParam(":idVenta",$idV);
							$stm->execute();
						}
						
						global $cookieExp;
						setcookie("cupon","",$cookieExp * -1,"/");
					  	unset($_COOKIE["cupon"]);
					}
				}else{
					$respuesta["code"] = 400;
					$respuesta["msg"] = "Error al guardar";
				}
				//$respuesta["msg"] = "Error en el servidor: \n".$e->getMessage();
			} catch (Exception $e) {
				$conn = null;
				$respuesta["code"] = 500;
				$respuesta["msg"] = "Error en el servidor: \n".$e->getMessage();
			}
		}else{
	    	$idV = "";
	  	}

		if(isset($res["idVentaMen"])){
			$idVM = $res["idVentaMen"];
			$respuesta["idVentaMen"] = $idVM;
		}else{
			$idVM = "";
		}

		
		/*try {
			$conn = PDOConnection2::getConnection();
			if($_POST["status"] == "Pagado")
				$sql = "UPDATE Ventas_menudeo SET Status = :status, Adeudo = 0.00 WHERE ID = :idVentaMenudeo";
			else
				$sql = "UPDATE Ventas_menudeo SET Status = :status WHERE ID = :idVentaMenudeo";
			$stm = $conn->prepare($sql);
			$stm->bindParam(":status",$_POST["status"]);
			$stm->bindParam(":idVentaMenudeo",$_POST["idVentaMenudeo"]);
			$result = false;
			$result = $stm->execute();
			if($_POST["status"] == "Pagado")	
				$sql = "UPDATE Ventas SET Status = :status, Adeudo = 0.00 WHERE ID = :idVenta";
			else
				$sql = "UPDATE Ventas SET Status = :status WHERE ID = :idVenta";
			$stm = $conn->prepare($sql);
			$stm->bindParam(":idVenta",$_POST["idVenta"]);
			$stm->bindParam(":status",$_POST["status"]);
			$stm->execute();
			$respuesta["code"] = 200;
		} catch (Exception $e) {
			$respuesta["code"] = 500;
			$respuesta["msg"] = "Error en el servidor: \n".$e->getMessage();
		}*/
	}else{
		$respuesta["code"] = $res["code"];
		$respuesta["msg"] = $res["meg"];
	}
	responder($respuesta);
}

function get_banners_home(){
	try {
		$conn = PDOConnection::getConnection();
		$sql = "SELECT * FROM Banners_web WHERE Status = 'Activo' AND Tipo = 'Banner' ORDER BY Posicion";
		$stm = $conn->prepare($sql);
		$stm->execute();
		$respuesta["banners"] = $stm->fetchAll();
		$sql = "SELECT * FROM Lineas WHERE Status = 'Activo'";
		$stm = $conn->prepare($sql);
		$stm->execute();
		$respuesta["lineas"] = $stm->fetchAll();
		if(count($respuesta["banners"]) > 0 || count($respuesta["lineas"]) > 0)
			$respuesta["code"] = 200;
		else
			$respuesta["code"] = 400;
	} catch (Exception $e) {
		$respuesta["code"] = 500;
	}
	responder($respuesta);
}



function url_imagenes(){
	return "http://prosalon4810.cloudapp.net:500/Echosline_Desk/assets/img/products/Web/";
}



?>