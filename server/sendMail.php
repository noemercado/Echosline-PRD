<?php
date_default_timezone_set('America/Mexico_City');
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

require "Exception.php";
require "PHPMailer.php";
require "SMTP.php";
$mailFrom = "het@echosline.mx";

if(isset($_POST["enviar"])){
	sendMail();
}

if(isset($_POST["opcion"])){
	switch ($_POST["opcion"]) {
		case 1:
			unete();
			break;
		case 2:
			contacto();
			break;

	}
}


function subirArchivos($name){
	if(isset($_FILES[$name])){
		$ruta = "../uploads/";
		$ext = pathinfo($_FILES[$name]['name'], PATHINFO_EXTENSION);
		//$uuid = md5(uniqid(rand(), true));
		$ruta = $ruta . $_FILES[$name]['name'];
		//$rutaImg = $rutaImg . ($_FILES["foto"]["name"]);
		if(move_uploaded_file($_FILES[$name]["tmp_name"], $ruta)){
			return $ruta;
		}
	}
	return false;
}

/*function unete_mail(){
	global $mailFrom;
	$mail = "nefas1809@gmial.com";
	$mensaje = "<html><body style='width: 100%; margin:0;'>";
	$mensaje .= "<h2>Solicitud: <span style='color:blue;'>".$$_POST['']."</span></h2>";
	;
	//$email_rem = $_POST["email"];
	$email_rem = $mailFrom;
	$headers = 'From: $email_rem'."\r\n";
	 
	//'Reply-To: '.$email_rem."\r\n";
	$headers .= "Content-type:text/html;charset=UTF-8";
	$email_subject = "Prueba correo";
	$headers  = 'MIME-Version: 1.0' . "\r\n";
	$headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
	$headers .= "From: $email_rem\r\n"."X-Mailer: php";
	 
	$success = mail($mail, $email_subject, $mensaje, $headers);
	$response_array["stat"] = $success;
	if (!$success) {
	    //$errorMessage = error_get_last()['message'];
	    while((!$success) && ($intentos<5)){
	    	sleep(3);
	    	$success = mail($mail, $email_subject, $mensaje, $headers);
	    	$intentos = $intentos-1;
	    }
	    
	}
	//if(!$success){
		$response["enviarCorreo"] = $success;
		responder($response);
}*/

function unete(){
	global $mailFrom;	
	$mail = new PHPMailer(TRUE);

	/* Open the try/catch block. */
	try {
	   /* Set the mail sender. */
	   //$mail->setFrom($_POST["email"], 'Aplicacion vacante');
	   $mail->setFrom($_POST["email"], 'Solicitud para unirse');

	   /* Add a recipient. */
	   //$mail->addAddress('nefas1809@gmail.com', 'EchosLine');
	   $mail->addAddress('noe@integratto.com.mx', 'EchosLine');
	   //$mail->addAddress('aldom@integratto.com.mx', 'EchosLine');
	   $mail->addAddress('het@echosline.mx', 'EchosLine');
	   //$mail->addAddress('aldoer@gmail.com', 'EchosLine');
	   /*$mail->addAddress('manuel@blueskeet.com', 'Blueskeet');
	   $mail->addAddress('nefas1809@gmail.com', 'Blueskeet');
	   $mail->addAddress('administracion@blueskeet.com', 'Blueskeet');*/
	   

	   /* Set the subject. */
	   $mail->Subject = $_POST["nombre"] . ' solicitó unirse a EchosLine';

	   $mail->isHTML(TRUE);

	   $mensaje = "<html>";
	   $mensaje .= "<h2>Opción para unirse: ";
	   switch ($_POST["opcion_radio"]) {
	   	case 'uso_y_venta':
	   		$mensaje .= "<span style='color:blue;'>Quiero Echosline para uso y venta en mi salón</span>";
	   		break;
	   	case 'peluqueria':
	   		$mensaje .= "<span style='color:blue;'>Soy una academia de estilismo o peluquería</span>";
	   		break;
	   	case 'distribuir':
	   		$mensaje .= "<span style='color:blue;'>Quiero distribuir el producto</span>";
	   		break;
	   	default:
	   		# code...
	   		break;
	   }
	   $mensaje .= "</h2>";
	   $mensaje .= "<h2>Nombre: <span style='color:blue;'>".$_POST['nombre']."</span></h2>";
	  //$mensaje .= "<p>".$_POST['nombre']."</p>";
	   $mensaje .= "<h2>Fecha de nacimiento: <span style='color:blue;'>".$_POST['combo_dia']."-".$_POST['combo_mes']."-".$_POST['combo_anio']."</span></h2>";
	   //$mensaje .= "<p>".$_POST['combo_dia']."-".$_POST['combo_mes']."-".$_POST['combo_anio']."</p>";
	   $mensaje .= "<h2>Teléfono: <span style='color:blue;'> ".$_POST['telefono']."</h2>";
	   //$mensaje .= "<p>".$_POST['telefono']."</p>";
	   $mensaje .= "<h2>Correo: <span style='color:blue;'> ".$_POST['email']."</h2>";
	   //$mensaje .= "<p>".$_POST['email']."</p>";
	   $mensaje .= "<h2>Dirección: <span style='color:blue;'> ".$_POST['direccion']."</h2>";
	   //$mensaje .= "<p>".$_POST['direccion']."</p>";
	   $mensaje .= "<h2>Código Postal: <span style='color:blue;'> ".$_POST['cp']."</h2>";
	   //$mensaje .= "<p>".$_POST['cp']."</p>";
	   $mensaje .= "<h2>Estado: <span style='color:blue;'> ".$_POST['estado']."</h2>";
	   //$mensaje .= "<p>".$_POST['estado']."</p>";
	   $mensaje .= "<h2>Municipio: <span style='color:blue;'> ".$_POST['municipio']."</h2>";
	   //$mensaje .= "<p>".$_POST['municipio']."</p>";
	   $mensaje .= "<h2>Colonia: <span style='color:blue;'> ".$_POST['colonia']."</h2>";
	   //$mensaje .= "<p>".$_POST['colonia']."</p>";
	   $mensaje .= "</html>";
	   /* Set the mail message body. */
	   $mail->Body = $mensaje;
	   $mail->CharSet = 'UTF-8';

	   /* Finally send the mail. */
	   if($mail->send())
	   	$resp["code"] = 200;
	   else
	   	$resp["code"] = 400;
	   /*if($cv)
	   	unlink($cv);
	   if($cover)
	   	unlink($cover);*/
	}
	catch (Exception $e)
	{
	   /* PHPMailer exception. */
	   $resp["code"] = 400;
	   $resp["msg"] = $e->errorMessage();
	}
	catch (\Exception $e)
	{
	   /* PHP exception (note the backslash to select the global namespace Exception class). */
	   $resp["code"] = 400;
	   $resp["msg"] = $e->getMessage();
	}

	responder($resp);
}

function responder($respuesta){
	header('Content-type: application/json');
	echo json_encode($respuesta);
}

function contacto(){
	if(isset($_POST["email"])){
		//$mail = $_POST["email"];
		$mail = new PHPMailer(TRUE);

		/* Open the try/catch block. */
		try {
		   /* Set the mail sender. */
		   //$mail->setFrom($_POST["email"], 'Contacto');
		   $mail->setFrom($_POST["email"], 'Contacto EchosLine');
		   /* Add a recipient. */
		   //$mail->addAddress('aldom@integratto.com.mx', 'Blueskeet');
	       //$mail->addAddress('manuelbnda@gmail.com', 'Blueskeet');
	       //$mail->addAddress('manuel@blueskeet.com', 'Blueskeet');
		   //$mail->addAddress('nefas1809@gmail.com', 'EchosLine');
		   $mail->addAddress('noe@integratto.com.mx', 'EchosLine');
		   $mail->addAddress('hmejia201908@gmail.com', 'EchosLine');
		   
		   //$mail->addAddress('aldom@integratto.com.mx', 'EchosLine');
		   //$mail->addAddress('het@echosline.mx', 'EchosLine');

		   /* Set the subject. */
		   $mail->Subject = 'Mensaje de contacto EchosLine';

		   $mail->isHTML(TRUE);

		   $mensaje = "<html><body style='width: 100%; margin:0;'>
			<h2>Nombre: <span style='color:blue;'>".$_POST["nombre"]."</span></h2>";
			$tipo = "";
			switch ($_POST["ayuda"]) {
				case 'info':
					$tipo = "Información sobre productos";
					break;
				case 'ventas':
					$tipo = "Quiero vender productos";
					# code...
					break;
				case 'otro':
					$tipo = "Otro";
					break;
				default:
					# code...
					break;
			}
			$mensaje .= "<h2>Área de ayuda: <span style='color:blue;'>".$tipo."</span></h2>";
			$mensaje .= "<h2>Correo: <span style='color:blue;'>".$_POST["email"]."</span></h2>";
			$mensaje .= "<h2>Teléfono: <span style='color:blue;'>".$_POST["telefono"]."</span></h2>";
			$mensaje .= "<br><br><h2>Mensaje:</h2>";
			$mensaje .= "<p style='font-size:20px;'>".$_POST["mensaje"]."</p>";
			$mensaje .= "</body><html>";
		   /* Set the mail message body. */
		   $mail->Body = $mensaje;
		   $mail->CharSet = 'UTF-8';
		   /* Finally send the mail. */
		   if($mail->send()){
		   	$resp["code"] = 200;
		   	$resp["msg"] = "Todo bien";
		   }
		   else
		   	$resp["code"] = 400;
		}
		catch (Exception $e)
		{
		   /* PHPMailer exception. */
		   $resp["code"] = 400;
		   $resp["msg"] = $e->errorMessage();
		}
		catch (\Exception $e)
		{
		   /* PHP exception (note the backslash to select the global namespace Exception class). */
		   $resp["code"] = 400;
		   $resp["msg"] = $e->getMessage();
		}
		/*try {
			$intentos = 1;
			$mensaje = "<html><body style='width: 100%; margin:0;'>
			<h2>Nombre: <span style='color:blue;'>".$_POST["nombre"]."</span></h2>";
			$mensaje .= "<br><br><h2>Comentarios:</h2>";
			$mensaje .= "<p style='font-size:20px;'>".$_POST["comentarios"]."</p>";
			$mensaje .= "</body><html>";
			$email_rem = "administracion@blueskeet.com,nefas1809@gmail.com";
			$headers = 'From: $email_rem'."\r\n".
			 
			'Reply-To: '.$email_rem."\r\n";
			$headers .= "Content-type:text/html;charset=UTF-8";
			$email_subject = "Contacto Blueskeet";
			$headers  = 'MIME-Version: 1.0' . "\r\n";
			$headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
			$headers .= "From: $email_rem\r\n"."X-Mailer: php";
			 
			$success = mail($mail, $email_subject, $mensaje, $headers);
			$response_array["stat"] = $success;
			if (!$success) {
				$response["code"]=400;
			    //$errorMessage = error_get_last()['message'];
			    while((!$success) && ($intentos<5)){
			    	sleep(3);
			    	$success = mail($mail, $email_subject, $mensaje, $headers);
			    	$intentos++;;
			    }
			    
			}else{
				$response["code"]=200;
			}
		} catch (Exception $e) {
			$response["code"]=500;
			$response["msg"]="Error en el servidor: ".$e->getMessage();
		}*/
		
	}else{
		$resp["code"]=400;
		$resp["msg"] = "No se enviaron los datos solicitados";
	}
	responder($resp);
}

function referencia(){
	if(isset($_POST["email"])){
		$mail = new PHPMailer(TRUE);
		try {
		   /* Set the mail sender. */
		   //$mail->setFrom($_POST["email"], 'Contacto con referencia');
		   $mail->setFrom("administracion@blueskeet.com", 'Contacto con referencia');
		   /* Add a recipient. */
		   $mail->addAddress('aldom@integratto.com.mx', 'Blueskeet');
	       $mail->addAddress('manuelbnda@gmail.com', 'Blueskeet');
	       $mail->addAddress('manuel@blueskeet.com', 'Blueskeet');
		   $mail->addAddress('nefas1809@gmail.com', 'Blueskeet');
		   $mail->addAddress('administracion@blueskeet.com', 'Blueskeet');

		   /* Set the subject. */
		   $mail->Subject = 'Mensaje de contacto Blueskeet';

		   $mail->isHTML(TRUE);

		   $mensaje = "<html><body style='width: 100%; margin:0;'>";
			$mensaje .= "<h1>Tus datos</h1>";
			$mensaje .= "<h2>Nombre: <span style='color:blue;'>".$_POST["nombre"]."</span></h2>";
			$mensaje .= "<h2>Correo: <span style='color:blue;'>".$_POST["email"]."</span></h2>";
			$mensaje .= "<h2>Teléfono: <span style='color:blue;'>".$_POST["telefono"]."</span></h2>";
			$mensaje .= "<h2>Relación: <span style='color:blue;'>".$_POST["relacion"]."</span></h2>";
			$mensaje .= "<h2>Comentarios: <span style='color:blue;'>".$_POST["comentarios"]."</span></h2>";
			$mensaje .= "<br><br><br>";
			$mensaje .= "<h1>Datos del referido</h1><br>";
			$mensaje .= "<h2>Nombre: <span style='color:blue;'>".$_POST["nombre1"]."</span></h2>";
			$mensaje .= "<h2>Correo: <span style='color:blue;'>".$_POST["email1"]."</span></h2>";
			$mensaje .= "<h2>Relación: <span style='color:blue;'>".$_POST["relacion1"]."</span></h2>";
			$mensaje .= "<h2>Empresa: <span style='color:blue;'>".$_POST["empresa"]."</span></h2>";
			$mensaje .= "<h2>Puesto: <span style='color:blue;'>".$_POST["puesto"]."</span></h2>";
			/*$mensaje .= "<br><br><h2>Comentarios:</h2>";
			$mensaje .= "<p style='font-size:20px;'>".$_POST["comentarios"]."</p>";*/
			$mensaje .= "</body><html>";
		   /* Set the mail message body. */
		   $mail->Body = $mensaje;

		   /* Finally send the mail. */
		   if($mail->send())
		   	$resp["code"] = 200;
		   else
		   	$resp["code"] = 400;
		}
		catch (Exception $e)
		{
		   /* PHPMailer exception. */
		   $resp["code"] = 400;
		   $resp["msg"] = $e->errorMessage();
		}
		catch (\Exception $e)
		{
		   /* PHP exception (note the backslash to select the global namespace Exception class). */
		   $resp["code"] = 400;
		   $resp["msg"] = $e->getMessage();
		}
		/*try {
			$intentos = 1;
			$mensaje = "<html><body style='width: 100%; margin:0;'>";
			$mensaje .= "<h1>Tus datos</h1>";
			$mensaje .= "<h2>Nombre: <span style='color:blue;'>".$_POST["nombre"]."</span></h2>";
			$mensaje .= "<h2>Correo: <span style='color:blue;'>".$_POST["email"]."</span></h2>";
			$mensaje .= "<h2>Teléfono: <span style='color:blue;'>".$_POST["telefono"]."</span></h2>";
			$mensaje .= "<h2>Relación: <span style='color:blue;'>".$_POST["relacion"]."</span></h2>";
			$mensaje .= "<h2>Comentarios: <span style='color:blue;'>".$_POST["comentarios"]."</span></h2>";
			$mensaje .= "<br><br><br>";
			$mensaje .= "<h1>Datos del referido</h1><br>";
			$mensaje .= "<h2>Nombre: <span style='color:blue;'>".$_POST["nombre1"]."</span></h2>";
			$mensaje .= "<h2>Correo: <span style='color:blue;'>".$_POST["email1"]."</span></h2>";
			$mensaje .= "<h2>Relación: <span style='color:blue;'>".$_POST["relacion1"]."</span></h2>";
			$mensaje .= "<h2>Empresa: <span style='color:blue;'>".$_POST["empresa"]."</span></h2>";
			$mensaje .= "<h2>Puesto: <span style='color:blue;'>".$_POST["puesto"]."</span></h2>";
			$mensaje .= "</body><html>";
			$email_rem = "administracion@blueskeet.com,nefas1809@gmail.com";
			$headers = 'From: $email_rem'."\r\n".
			 
			'Reply-To: '.$email_rem."\r\n";
			$headers .= "Content-type:text/html;charset=UTF-8";
			$email_subject = "Contacto Blueskeet";
			$headers  = 'MIME-Version: 1.0' . "\r\n";
			$headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
			$headers .= "From: $email_rem\r\n"."X-Mailer: php";
			 
			$success = mail($mail, $email_subject, $mensaje, $headers);
			$response_array["stat"] = $success;
			if (!$success) {
			    //$errorMessage = error_get_last()['message'];
			    while((!$success) && ($intentos<5)){
			    	sleep(3);
			    	$success = mail($mail, $email_subject, $mensaje, $headers);
			    	$intentos++;;
			    }
			    
			}else{
				$response["code"]=200;
			}
		} catch (Exception $e) {
			$response["code"]=500;
			$response["msg"]="Error en el servidor: ".$e->getMessage();
		}*/
		
	}else{
		$resp["code"]=400;
		$resp["msg"] = "No se enviaron los datos solicitados";
	}
	responder($resp);
}


?>