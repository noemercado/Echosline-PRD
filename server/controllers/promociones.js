$(document).ready(function(){
	getPromociones();

	$(document).on("click",".btnTienda",function(e){
		e.preventDefault();
		var idP = $(this).data("idp");
		//var idInv = $(this).data("idinv");
		console.log(idP);
		add_to_cart(idP,1);
	});


})

function getPromociones(){
	$.ajax({
	  url:urlApi,
	  data:{"opcion":16},
	  type:"POST",
	  beforeSend:function(){
	    $(".ai-preloader").addClass("active");
	  },
	  success:function(data){
	    console.log(data);
	    renderPromociones(data.productos);
	    //render_lineas(data.lineas);
	  },
	  complete:function(){
	    $(".ai-preloader").removeClass("active");
	    //getParams();
	  }
	});
}

function renderPromociones(productos){
	var item = "";
	var producto;
	$("#productos").html("");
	for(var i in productos){
		producto = productos[i];
		item = "";
		item += '<div class="col-lg-4 col-sm-6 mb--40 mb-md--30">';
		item += '<div class="airi-product">';
		item += '<div class="product-inner">';
		item += '<figure class="product-image">';
		item += '<div class="product-image--holder">';
		item += '<a href="detalle-producto-oferta.php?p='+producto.ID+'">';
		item += '<img src="'+urlImagenes+producto.Url_imagen+'" alt="Product Image" class="primary-image">';
		//item += '<img src="assets/img/products/producto1.jpg" alt="Product Image" class="secondary-image">';
		item += '</a>';
		item += '</div>';
		item += '<div class="airi-product-action">';
		item += '<div class="product-action">';
		item += '<a class="quickview-btn action-btn" href="#" data-idp="'+producto.ID+'" data-toggle="tooltip" data-placement="top" title="Compra Rápida">';
		item += '<span data-toggle="modal">';
		item += '<i class="dl-icon-view"></i>';
		item += '</span>';
		item += '</a>';
		item += '<a class="add_to_cart_btn action-btn" href="#" data-idP="'+producto.ID+'" data-toggle="tooltip" data-placement="top" title="Añadir al carrito">';
		item += '<i class="dl-icon-cart29"></i>';
		item += '</a>';
		item += '<a class="add_wishlist action-btn" href="wishlist.html" data-toggle="tooltip" data-placement="top" title="Añadir a favoritos">';
		item += '<i class="dl-icon-heart4"></i>';
		item += '</a>';
		item += '</div>';
		item += '</div>';
		item += '</figure>';
		item += '<div class="product-info text-center">';
		item += '<h3 class="product-title">';
		item += '<a href="detalle-producto-oferta.php?p='+producto.ID+'">'+producto.Promocion+'</a>';
		item += '</h3>';
		item += '<span class="product-price-wrapper">';
		item += '<span class="money">$'+producto.precio+'</span>';
		item += '</span>';
		item += '</div>';
		item += '<div class="product-overlay"></div>';
		item += '</div>';
		item += '</div>';
		item += '<div class="post-content textBlack mb--5 mt--5 text-center">';
		item += '<p>'+producto.Descripcion+'</p>';
		item += '</div>';
		item += '<a href="#" class="btn btnTienda mt--15" data-idp="'+producto.ID+'">AGREGAR AL CARRITO</a>'
		item += '</div>';
		$("#productos").append(item);
	}
}

function add_to_cart(idP,qty){
	//var qty = $("#qty").val();
	$.ajax({
	  url:urlApi,
	  data:{"opcion":17,"idP":idP,"qty":qty},
	  type:"POST",
	  beforeSend:function(){
	    $(".ai-preloader").addClass("active");
	  },
	  success:function(data){
	    console.log(data);
	    Swal.fire({
	      html:'<div id="agregado"><h3>Producto agregado al carrito!</h3><img src="'+urlImagenes+data.producto.Url_imagen+'" alt="" /><strong>'+data.producto.Promocion+'</strong><br /><strong>Cantidad: '+data.qty+'</strong></div>',
	      timer: 5000
	    })
	    
	  },
	  complete:function(){
	    $(".ai-preloader").removeClass("active");
	    update_cart_ajax();
	    //getParams();
	  }
	});
}