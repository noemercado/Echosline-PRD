var idVenta = "";
var idVentaMenudeo = "";
var codigoSalon = "";
var idUsario;

var statusPago = "";
var idPago = "";
var idOrden = "";
var idVM = "";
var idV = "";
var tipo = "";
var idTransaccion = "";

var transferencia = false;

$(document).ready(function(){
	window.dir = window.location.protocol+"//"+window.location.host+"/echosline_actual_27_03_2020/";
	//window.dir = window.location.protocol+"//"+window.location.host+"/prueba/";
	$("#mercadopagoForm").attr("action",dir+"success.php");
	/*
	sandbox paypal:
	sb-43nadj663855@personal.example.com
	96S^i>pR
	*/
	setInterval(function(){
		//$("#paypal-button-container").hide();
		//$("#mercadopago-button-container").hide();
	},600000);
	get_datos_usuario();
	//1 = 1000


	//window.Mercadopago.setPublishableKey("TEST-c5f0554c-a062-442d-9422-6be720de2dac");
	//window.Mercadopago.setPublishableKey("APP_USR-1ca46ead-ddfe-4f76-876c-bf41aa6eef43");

	/*window.addEventListener("message", (event) => {
	   if (event.origin !== 'https://www.mercadopago.com.mx' || ! event.data.type) {
	     return;
	   }

	   var dataType = event.data.type; // "close", "submit", etc...
	   console.log(dataType);
	   if (dataType === 'submit') {
	      // Tu funcionalidad a ejecutar...
	      const paymentData = event.data.value; // Es un arreglo con la respuesta del pago procesado.
	      console.log(paymentData[4].value);
	      switch(paymentData[4].value){
	      	case "pending":
	      		actualizar_status_venta("Adeudo");
	      		break;
	      	case "approved":
	      		actualizar_status_venta("Pagado");
	      		break;
	      }
	      $(".mp-mercadopago-checkout-wrapper").fadeOut("slow");
	      $("body").css({"overflow-y":"auto", "padding-right":"0px !important"});
	      
	      //return;
	   }else if(dataType === "approved"){
	   	  actualizar_status_venta("Pagado");
	   }
	   else if(dataType == "close"){
	   	  borrar_venta();
	   }
	});*/

	var meses = ["Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"];
	var valido = false;
	
	//$("#paypal-button-container").hide();
	for(var i = 1; i <= 31; i++){
		if(i < 10)
			$("#billing_dia").append("<option value='0"+i+"'>0"+i+"</option>");
		else
			$("#billing_dia").append("<option value='"+i+"'>"+i+"</option>");
	}
	for(var i in meses){
		console.log(parseInt(i)+1);
		if(i < 9)
			$("#billing_mes").append("<option value='0"+(parseInt(i)+1)+"'>"+meses[i]+"</option>");
		else
			$("#billing_mes").append("<option value='"+(parseInt(i)+1)+"'>"+meses[i]+"</option>");
	}
	for(var i = 2001; i >= 1960; i--)
		$("#billing_year").append("<option value='"+i+"'>"+i+"</option>");

	$("#shipdifferetads").on("change",function(){
		if(!$(this).prop("checked"))
			facturacion(true);
		else
			facturacion(false);
	});

	$("#btn_pagar").on("click",function(e){
		e.preventDefault();
		e.preventDefault();
		console.log("algo")
		if(!$("#acepto").prop("checked"))
			Swal.fire({
			  icon: 'error',
			  title: 'Oops...',
			  text: 'Debes aceptar los Términos y condiciones y el Aviso de Privacidad'
			})
		else{
			$("#btn_enviar_datos").click();
		}
	});
	
	$("#formDatos").on("submit",function(){
		sesion_activa(validar_botones_pago);
	});

	$("#paypal-button-container button").on("click",function(e){
		options = options || {};
		console.log("paypal")
		
		if(!options.paypal){
			e.preventDefault();
			sesion_activa(activar_paypal);
		}
	});

	function activar_paypal(r){
		console.log(r);
		if(r)
			$("#paypal-button-container button").trigger("click",{'paypal':true});
		else{
			alert("Tu sesion ha expirado, ingresa nuevamente");
			$("#paypal-button-container button").off("click");
			$(".mp-mercadopago-checkout-wrapper").hide();
			window.location.href = "login.html?r=chk";
		}
	}

	function validar_botones_pago(r){
		if(r){
			var formD = new FormData(document.getElementById("formDatos"));
			formD.append("id",idUsario);
			guardarDatosEnvio(formD);
		}else{
			alert("Tu sesion ha expirado, ingresa nuevamente");
			window.location.href = "login.html?r=chk";
		}
	}
})

var idUsario;

function get_datos_usuario(){
	$.ajax({
	  url:urlApi,
	  data:{"opcion":21},
	  type:"POST",
	  beforeSend:function(){
	     $(".ai-preloader").addClass("active");
	  },
	  success:function(data){
	  	console.log(data)
	  	if(data.code == 200)
	  		if(!data.tmp){
	  			idUsario = data.usuario.ID;
	  			render_datos_usuario(data.usuario);
	  		}
	  		else
	  			$("#billing_email").val(data.usuario.u);
	  },
	  complete:function(){
	     $(".ai-preloader").removeClass("active");
	  }
	});
}

function limpiar_carrito(){
	$.ajax({
	  url:urlApi,
	  data:{"opcion":26},
	  type:"POST",
	  beforeSend:function(){
	    $(".ai-preloader").addClass("active");
	  },
	  success:function(data){
	  	if(data.code == 200){
	  		$(".payment-group,#mercadopago-button-container, .instrucciones, #metodosPago").hide();
	  		update_cart_ajax();
    		Swal.fire({
            	title: 'Pedido generado',
            	text: 'Tu pedido ha sido generado con éxito',
            	icon: "success",
            	buttons: true,
            	dangerMode: false,
              })
              .then((ok) => {
                if (ok) {
                	//openModalTicket(data.ventas.idVentaMen)
                }
            })
	  	}
	  	$("#pedidoGenerado").show();
	    console.log(data);
	  },
	  complete:function(){
	    $(".ai-preloader").removeClass("active");
	    //getParams();
	  }
	});
}

function render_datos_usuario(u){
	$("#billing_fname").val(u.Nombre);
	var calle = "";
	var num = "";
	if(u.Calle_numero){
		if(u.Calle_numero.includes("#")){
			var sp = u.Calle_numero.split("#");
			calle = sp[0];
			num = sp[1];
		}
		console.log(num)
	}
	
	$("#billing_calle").val(calle);
	$("#billing_exterior").val(parseInt(num));
	$("#billing_cp").val(u.CP);
	$("#billing_estado").val(u.Estado).change();
	$("#billing_municipio").val(u.Ciudad).change();
	$("#billing_colonia").val(u.Colonia).change();
	$('.nice-select').niceSelect('update');
	$("#apellidos").val(u.Apellidos)
	$("#billing_email").val(u.Email);
	if(u.Nacimiento != ""){
		var da = u.Nacimiento.split("-");
		$("#billing_dia").val(da[2]).change();
		$("#billing_mes").val(da[1]).change();
		$("#billing_year").val(da[0]).change();
	}
	$("#telefono").val(u.Tel1);
}

function openModalTicket(id){
	$("#idTicket").val(id);
	$("#urlTicket").val("http://integrattodev.cloudapp.net/echosline/server/Imprimir_Ticket.php?IdVenta="+id);
	$(".sidebar-mini").removeClass("sidebar-open")
    $(".sidebar-mini").addClass("sidebar-collapse")

    let div = document.getElementById('ticket');
    div.innerHTML = '<iframe style="width:100%;height:100%;" frameborder="0" src="server/Imprimir_Ticket.php?IdVenta='+ id + '" />';
    setTimeout(function(){
    	if($("#idTicket").val() !== ''){
    		$("#modal_ticket").modal('show');
    	}        
    },100)
}

function closeModalTicket(){
	$("#modal_ticket").modal('hide');
	$("#idTicket").val('');
	$("#urlTicket").val('');
	$("#emailTicket").val('');
}

function sendEmailTicket(){
	console.log($("#emailTicket").val())
    if($("#emailTicket").val() == '' || $("#emailTicket").val() == null){
        Swal.fire({
		  icon: 'error',
		  title: 'Oops...',
		  text: 'Por favor ingrese un correo electrónico.'
		})
        return false
    }
    var formData = new FormData();
    formData.append("opcion",11);
	formData.append("id",  $("#idTicket").val());
	formData.append("urlTicket", $("#urlTicket").val());
	formData.append("email", $("#emailTicket").val());
    
	$.ajax({
		url:"server/api.php",
		type: 'POST',
		processData: false,
		contentType: false,
		timeout: 800000,
		data: formData,
		beforeSend: function () {
			$('#loading_send_mail').css('display', '');
		},
		success: function (response) {
			console.log(response)
			if(response == 0){
				Swal.fire({
				  icon: 'error',
				  title: 'Oops...',
				  text: 'Ocurrio un error al enviar correo electrónico, por favor intente de nuevo.'
				})
		    }
		    else {
		    	Swal.fire({
        		  icon: 'success',
        		  title: 'Ticket enviado con éxito.',
        		})
		    }
		}
	})
	.done(function () {
		
	})
	.fail(function (jqXHR, textStatus, errorThrown) {
		
	})
	.always(function () {
		
	});
}

var idVenta = "";
var idVentaMenudeo = "";

function guardarDatosEnvio(formD){
	formD.append("opcion",8);
	if(idVenta != "")
		formD.append("idVenta",idVenta);
	if(idVentaMenudeo != "")
		formD.append("idVentaMenudeo",idVentaMenudeo);
	$.ajax({
	    url:"server/api.php",
	    type:"post",
	    data:formD,
	    cache: false,
	    contentType: false,
	    processData: false,
	    beforeSend:function(){
	        $(".ai-preloader").addClass("active");
	    },
	    success:function(data){
	        console.log(data);
	        if(data.code == 200){
	        	window.location.href = "checkout_paso2.php";
	        	/*if(data.ventas.code == 200){
	        		window.location.href = "checkout_paso2.php";
	        		idVenta = data.ventas.idVenta;
	        		idVentaMenudeo = data.ventas.idVentaMen;
	        		//$("#paypal-button-container").show();
	        		//$("#mercadopago-button-container").show();
	        		
	        		//update_cart_ajax();
	        	}else{
	        		Swal.fire({
	        		  icon: 'error',
	        		  title: 'Oops...',
	        		  text: data.ventas.msg
	        		})
	        	}*/
	        }else{
	        	Swal.fire({
	        	  icon: 'error',
	        	  title: 'Oops...',
	        	  text: data.msg
	        	})
	        }
	        
	    },
	    complete:function(){
	        $(".ai-preloader").removeClass("active");

	    },
	    error:function(x,e){
	      if (x.status==0) {
	            alert('You are offline!!\n Please Check Your Network.');
	        } else if(x.status==404) {
	            alert('Requested URL not found.');
	        } else if(x.status==500) {
	            alert('Internel Server Error.');
	        } else if(e=='parsererror') {
	            alert('Error.\nParsing JSON Request failed.');
	        } else if(e=='timeout'){
	            alert('Request Time out.');
	        } else {
	            alert('Unknow Error.\n'+x.responseText);
	        }
	      //console.log("ERROR: "+data);
	      //$("#alertaError").show();
	      $(".ai-preloader").removeClass("active");
	    }
	});
}

function facturacion(required){
	$("#billing_fname_factura,#billing_exterior_factura,#billing_cp_factura,#billing_estado_factura,#billing_municipio_factura,#billing_colonia_factura").prop("required",required);
}

$("#billing_estado").on("change",function(){
	console.log($(this).val());
});

function borrar_venta(){
	$.ajax({
	  url:urlApi,
	  data:{"opcion":27,"idVenta":idVenta,"idVentaMenudeo":idVentaMenudeo},
	  type:"POST",
	  beforeSend:function(){
	    $(".ai-preloader").addClass("active");
	  },
	  success:function(data){
	  	if(data.code == 200){
	  	}else{
	  		alert(data.msg);
	  	}
	    console.log(data);
	  },
	  complete:function(){
	    $(".ai-preloader").removeClass("active");
	    //getParams();
	  }
	});
}

function actualizar_status_venta(status){
	$.ajax({
	  url:urlApi,
	  data:{"opcion":28,/*"idVenta":idVenta,"idVentaMenudeo":idVentaMenudeo,*/ "payment_status":status, "tipo":tipo, "payment_id":idPago, "merchant_order_id":idOrden},
	  type:"POST",
	  beforeSend:function(){
	    $(".ai-preloader").addClass("active");
	  },
	  success:function(data){
	  	if(data.code == 200){
  			openModalTicket(data.idVentaMen);
  			enviar_correo_pedido(data.idVenta);
  			limpiar_carrito();	  		
	  	}else{
	  		alert(data.msg);
	  	}
	    console.log(data);
	  },
	  complete:function(){
	    $(".ai-preloader").removeClass("active");
	    //getParams();
	  }
	});
}

function enviar_correo_pedido(idVenta){
	
	$.ajax({
	  //url:"https://avynacos.mx/WebServiceSendMail/SendGenerateOrder.php?idVenta="+idVenta+"&transferencia="+transferencia,
	  url:"WebServiceSendMail/SendGenerateOrder.php?idVenta="+idVenta+"&transferencia="+transferencia,
	  type:"POST",
	  beforeSend:function(){
	    $(".ai-preloader").addClass("active");
	  },
	  success:function(data){
	  	console.log(data);
	  },
	  complete:function(){
	  }
	});
}

$("#pagarTransferencia").on("click",function(e){
	e.preventDefault();
	$("#instrucciones").show();
});

$("#finalizarTransfer").on("click",function(e){
	e.preventDefault();
	transferencia = true;
	tipo = "transferencia";
	idPago = "";
	idOrden = "";
	actualizar_status_venta("Adeudo");
});
