


$("#form-login").on("submit",function(){
	console.log("algo")
	var formD = new FormData(document.getElementById("form-login"));
	/*var email = $("#username_email").val();
	var readableString = $("#password-login").val();
	var nonceValue = '<?php echo $nonceValue; ?>';
	var encryptedString = '<?php echo $encryptedString; ?>';

	var encryption = new Encryption();
	var encrypted = encryption.encrypt(readableString, nonceValue);
	console.log(encrypted);
	var decrypted = encryption.decrypt(encrypted, nonceValue);
	console.log(decrypted);
	//var decryptedOldString = encryption.decrypt(encryptedString, nonceValue);
	formD.append("password",encrypted);
	formD.append("username_email",email);*/
	formD.append("opcion",20);
	iniciar(formD);
});

$("#form-registro").on("submit",function(){
	console.log("algo2")
	var formD = new FormData(document.getElementById("form-registro"));
	formD.append("opcion",22);
	if($("#idPadre").val() == ""){
		Swal.fire({
		  icon: 'warning',
		  title: 'Advertencia',
		  text: "No ingresaste un ID Vendedor multinivel, si continuas se te asignará uno automáticamente. ¿Deseas continuar?",
		  confirmButtonText: 'Si, continuar',
		  showCancelButton: true,
		  cancelButtonText: "No, cancelar"
		}).then((result) => {
		  if (result.value) {
		  	formD.append("idPadre",100019);
		    registrar(formD);
		  }
		})
	}else{
		registrar(formD);
	}
	
});

$("#recuperar_pass").on("click",function(e){
	e.preventDefault();
	var correo = $("#username_email").val();
	if(correo != "")
		recuperar(correo);
	else
		Swal.fire({
		  icon: 'error',
		  title: 'Oops...',
		  text: "Debes ingresar tu email."
		});
})

function iniciar(datos){
	$.ajax({
	  url:urlApi,
	  data:datos,
	  cache: false,
	  contentType: false,
	  processData: false,
	  type:"POST",
	  beforeSend:function(){
	     $(".ai-preloader").addClass("active");
	  },
	  success:function(data){
	  	if(data.code == 200){
	  		var params = GET(window.location.href);
	  		/*console.log(params2)
	  		var params = params2.replace("#","");*/
	  		console.log(params);
	  		if(params){
	  			if(params.r.replace("#","") == "chk"){
	  				window.location.href = "checkout_paso1.php";
	  			}else if(params.r.replace("#","") == "acc"){
	  				window.location.href = "mi-cuenta.php";
	  			}
	  		}else{
	  			window.location.href = "catalogo.html";
	  		}
	    	
	  	}
	    else
	    	Swal.fire({
	    	  icon: 'error',
	    	  title: 'Oops...',
	    	  text: data.msg
	    	});
	    
	  },
	  complete:function(){
	     $(".ai-preloader").removeClass("active");
	    //getParams();
	  }
	});
}

function registrar(datos){
	$.ajax({
	  url:urlApi,
	  data:datos,
	  cache: false,
	  contentType: false,
	  processData: false,
	  type:"POST",
	  beforeSend:function(){
	     $(".ai-preloader").addClass("active");
	  },
	  success:function(data){
	  	if(data.code == 200){
	  		var params = GET(window.location.href);
	  		if(params){
	  			if(params.r == "chk"){
	  				window.location.href = "checkout_paso1.php";
	  			}else if(params.r.replace("#","") == "acc"){
	  				window.location.href = "mi-cuenta.php";
	  			}else{
	  				window.location.href = "mi-cuenta.php";
	  			}
	  		}else{
	  			window.location.href = "mi-cuenta.php";
	  		}
	    	
	  	}
	    else
	    	Swal.fire({
	    	  icon: 'error',
	    	  title: 'Oops...',
	    	  text: data.msg
	    	});
	    
	  },
	  complete:function(){
	     $(".ai-preloader").removeClass("active");
	    //getParams();
	  }
	});
}

function recuperar(datos){
	$.ajax({
	  url:urlApi,
	  data:{"opcion":24,"email":datos},
	  type:"POST",
	  beforeSend:function(){
	     $(".ai-preloader").addClass("active");
	  },
	  success:function(data){
	  	if(data.code == 200){
	  		Swal.fire({
	    	  icon: 'success',
	    	  title: 'Éxito',
	    	  text: "Tu contraseña ha sido enviada a tu correo, revisa tu bandeja e ingresa a nuestro sitio"
	    	});
	    	
	  	}
	    else
	    	Swal.fire({
	    	  icon: 'error',
	    	  title: 'Oops...',
	    	  text: data.msg
	    	});
	    
	  },
	  complete:function(){
	     $(".ai-preloader").removeClass("active");
	    //getParams();
	  }
	});
}