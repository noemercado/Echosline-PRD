$(document).ready(function(){
	//get_cart();
	$("#btn_actualizar_carrito").on("click",function(e){
		e.preventDefault();
		actualizarCarrito();
	})

	$(document).on("click",".cartDel,.cartDelOferta",function(e){
		e.preventDefault();
		$(this).closest("tr").fadeOut("slow");
		setTimeout(function(){
			actualizarCarrito();
		},1000)
	});

	$(document).on("click",".dec,.inc",function(){
		setTimeout(function(){
			actualizarCarrito();
		},500)
	})
});


function actualizarCarrito(){
	var carrito = [];
	var carritoOferta = [];
	$("#tablaProductosCarrito tbody tr").each(function(){
		//console.log($(this).data("idp") + " -- Visible: "+$(this).is(":visible"));
		if(!$(this).hasClass("oferta")){
			carrito.push({
				p:$(this).data("idp"),
				inv:$(this).data("idinv"),
				qty:$(this).find(".quantity-input").val(),
				eliminar:!$(this).is(":visible")
			});
		}else{
			carritoOferta.push({
				p:$(this).data("idp"),
				qty:$(this).find(".quantity-input").val(),
				eliminar:!$(this).is(":visible")
			});
		}
		
	});
	console.log(carrito);
	console.log(carritoOferta);
	$.ajax({
	  url:urlApi,
	  data:{"opcion":7,"carrito":JSON.stringify(carrito),"carritoOferta":JSON.stringify(carritoOferta)},
	  type:"POST",
	  beforeSend:function(){
	    $("#preloader").show();
	  },
	  success:function(data){
	  	
	    console.log(data);
	    update_cart_ajax();
	  },
	  complete:function(){
	    $("#preloader").fadeOut();
	    window.location.reload(true);
	    //getParams();
	  }
	});
}



function get_cart(){
	$.ajax({
	  url:urlApi,
	  data:{"opcion":4},
	  type:"POST",
	  beforeSend:function(){
	    $("#preloader").show();
	  },
	  success:function(data){
	  	if(data.code == 200){
	  		render_cart(data.itemsCart);
	  		$(".mini-cart__total .ammount").text("$"+data.subtotal);
	  	}else if(data.code == 400){

	  	}
	    console.log(data);
	    
	  },
	  complete:function(){
	    $("#preloader").fadeOut();
	    //getParams();
	  }
	});
}

function render_cart(items){
	$("#tablaProductosCarrito tbody").html("");
	for(var i in items){
		var producto = items[i];
		var elem = '<tr id="'+producto.ID+'">';
		elem += '<td class="product-remove text-left"><a href=""><i class="dl-icon-close" data-idp='+producto.ID+' data-idinv='+producto.ID_inventario+'></i></a></td>';
		elem += '<td class="product-thumbnail text-left">';
		elem += '<img src="'+urlImagenes+producto.Url_imagen+'" alt="products">';
		elem += '</td>';
		elem += '<td class="product-name text-left wide-column">';
		elem += '<h3>';
		elem += '<a class="text-black" href="detalle-producto.php?p='+producto.ID+'&inv='+producto.ID_inventario+'"> '+producto.Producto+'</a>';
		elem += '</h3>';
		elem += '</td>';
		elem += '<td class="product-price">';
		elem += '<span class="product-price-wrapper">';
		elem += '<span class="money">$'+producto.Precio_publico+'</span>';
		elem += '</span>';
		elem += '</td>';
		elem += '<td class="product-quantity">';
		elem += '<div class="quantity">';
		elem += '<input type="number" class="quantity-input" name="qty" id="qty-'+producto.ID+'" value="'+producto.qty+'" min="1">';
		elem += '<div class="dec qtybutton">-</div><div class="inc qtybutton">+</div></div>';
		elem += '</td>';
		elem += '<td class="product-total-price">';
		elem += '<span class="product-price-wrapper">';
		elem += '<span class="money"><strong>$'+producto.total+'</strong></span>';
		elem += '</span>';
		elem += '</td>';
		elem += '</tr>';
		$("#tablaProductosCarrito tbody").append(elem);
	} 
}





