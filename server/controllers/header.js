var urlApi = "server/api.php";
var urlApiAccount = "server/api-mi-cuenta.php";
//var urlImagenes = "assets/img/products/";
var urlImagenes = "http://prosalon4810.cloudapp.net:500/Echosline_Desk/assets/img/products/Web/";

function sesion_activa(callback){
	var href = window.location.href.split(".");
	console.log(href);
	var result = false;
	$.ajax({
	  url:urlApi,
	  data:{"opcion":23},
	  async:false,
	  type:"POST",
	  beforeSend:function(){
	    $(".ai-preloader").addClass("active");
	  },
	  success:function(data){
	  	//alert(data.code);
	  	if(data.code == 200){
	  		result = true;
	  		$(".user-info-menu-btn a").off("click");
	  		$("#cerrarSesion,#cerrarSesionMovil").show();
	  	}
	  	else{
	  		result = false;
	  	}
	  	//console.log(result);
	    
	  },
	  complete:function(){
	    $(".ai-preloader").removeClass("active");
	    //getParams();
	    callback(result);
	  }
	});
}

$(document).ready(function(){
	$(".searchform").prop("action","catalogo.html");
	sesion_activa(footer);

	$(".mainmenu__link").each(function(){
		if($(this).attr("href") == "unete.html" || $(this).attr("href") == "ofertas.html")
			$(this).closest("li.mainmenu__item").hide();
	})



});

function footer(activo){
	console.log(activo)
	$(".megamenu").remove();
	$(".mainmenu__item").each(function(){
		if($(this).find("a").attr("href") == "linea.html"){
			$(this).find("a").attr("href","home.html")
			//alert("Hay LINEAS")
		}
	})
	if(activo){
		$("#tituloCuenta").text("Mi cuenta");
		$("#footer_registro").text("Mis datos").attr("href","mi-cuenta.php");
		$("#footer_clientes,#footer_favoritos,#footer_salones").hide();
		$("#footer_pedidos").attr("href","mi-cuenta.php?section=orders");
	}else{
		$("#tituloCuenta").text("Mi cuenta");
		$("#footer_registro").text("Registrarme");
		$("#footer_favoritos,#footer_pedidos,#footer_clientes,#footer_salones").hide();
	}
}

$(document).on("click","#cerrarSesion,#cerrarSesionMovil",function(){
	$.ajax({
	  url:urlApiAccount,
	  data:{"opcion":3},
	  type:"POST",
	  beforeSend:function(){
	     $(".ai-preloader").addClass("active");
	  },
	  success:function(data){
	  	console.log(data)
	  	if(data.code == 200)
	  		window.location.href = "catalogo.html";
	  },
	  complete:function(){
	     $(".ai-preloader").removeClass("active");
	  }
	});
})
