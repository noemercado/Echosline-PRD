$(document).ready(function(){
	set_combos_fechas();
	var opcion_radio = "";
	$("#btn_unete").on("click",function(e){
		e.preventDefault();
		console.log($("input[name='opcion_radio']").val())
		if(!$("#acepto").prop("checked"))
			Swal.fire({
			  icon: 'error',
			  title: 'Oops...',
			  text: 'Debes aceptar los Términos y condiciones y el Aviso de Privacidad'
			})
		else if(opcion_radio == "")
			Swal.fire({
			  icon: 'error',
			  title: 'Oops...',
			  text: 'Selecciona una opcion para unirte'
			})
		else
			$("#enviar_unete").click();
	});

	$("input[name='opcion_radio']").on("change",function(){
		opcion_radio = $(this).val();
	})

	$("#formUnete").on("submit",function(){
		console.log("Enviando")
		var formD = new FormData(document.getElementById("formUnete"));
		enviar_mensaje(formD,1);
	});

	$("#btn_enviar").on("click",function(e){
		e.preventDefault();
		if(!$("#acepto").prop("checked"))
			Swal.fire({
			  icon: 'error',
			  title: 'Oops...',
			  text: 'Debes aceptar los Términos y condiciones y el Aviso de Privacidad'
			});
		else
			$("#enviar_contacto").click();
	});

	$("#contact-form").on("submit",function(){
		var formD = new FormData(document.getElementById("contact-form"));
		enviar_mensaje(formD,2);
	})
})

function set_combos_fechas(combo_dia,combo_mes,combo_anio){
	var meses = ["Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"];
	for(var i = 1; i <= 31; i++){
		$("#combo_dia").append("<option value='"+i+"'>"+i+"</option>");
	}
	for(var i in meses)
		$("#combo_mes").append("<option value='"+meses[i]+"'>"+meses[i]+"</option>");
	for(var i = 2001; i >= 1960; i--)
		$("#combo_anio").append("<option value='"+i+"'>"+i+"</option>");
}

function enviar_mensaje(formD,opcion){
		formD.append("opcion",opcion);
		$.ajax({
		    url:"server/sendMail.php",
		    type:"post",
		    data:formD,
		    cache: false,
		    contentType: false,
		    processData: false,
		    beforeSend:function(){
		        $(".ai-preloader").addClass("active");
		    },
		    success:function(data){
		        console.log(data);
		        if(data.code == 200)
			    	Swal.fire({
	        		  icon: 'success',
	        		  title: 'Solicitud enviada',
	        		  text: 'Nos pondremos en contacto contigo a la brevedad.'
	        		})
			    else
			    	Swal.fire({
			    	  icon: 'error',
			    	  title: 'Oops...',
			    	  text: data.msg
			    	})
		    },
		    complete:function(){
		        $(".ai-preloader").removeClass("active");

		    },
		    error:function(x,e){
		      if (x.status==0) {
		            alert('You are offline!!\n Please Check Your Network.');
		        } else if(x.status==404) {
		            alert('Requested URL not found.');
		        } else if(x.status==500) {
		            alert('Internel Server Error.');
		        } else if(e=='parsererror') {
		            alert('Error.\nParsing JSON Request failed.');
		        } else if(e=='timeout'){
		            alert('Request Time out.');
		        } else {
		            alert('Unknow Error.\n'+x.responseText);
		        }
		      $(".ai-preloader").removeClass("active");
		    }
		});
}