$(document).ready(function(){
	get_productos_home();
	getBanners();
	$("#btn-login").on("click",function(e){
		e.preventDefault();
		console.log("enviando");
		var email = $("#email_login").val();
		var pass = $("#pass_login").val();
		if(email != "" && pass != "")
			$("#iniciar-popup").submit();
		else
	        Swal.fire({
			  icon: 'error',
			  title: 'Oops...',
			  text: 'Por favor ingrese se correo y contraseña'
			})
	})

	$("#btn-registrar").on("click",function(e){
		e.preventDefault();
		console.log("enviando");
		var email = $("#email_registro").val();
		var pass = $("#pass_registro").val();
		if(email != "" && pass != ""){
			if($("#aceptar").prop("checked"))
				$("#registrar-popup").submit();
			else
		        Swal.fire({
				  icon: 'error',
				  title: 'Oops...',
				  text: 'Debes aceptar los términos y condiciones'
				})
		}
		else
	        Swal.fire({
			  icon: 'error',
			  title: 'Oops...',
			  text: 'Por favor ingrese se correo y contraseña'
			})
	})

	$("#iniciar-popup").on("submit",function(){
		console.log("peticion")
		var formD = new FormData(document.getElementById("iniciar-popup"));
		formD.append("opcion",20);
		var email = $("#email_login").val();
		var pass = $("#pass_login").val();
		formD.append("password",pass);
		formD.append("username_email",email);
		iniciar(formD);
	});
//nefas1809@gmail.com en Sus. Localiz. Movil • Suscripcion localizacion movil


	$("#registrar-popup").on("submit",function(){
		console.log("algo2")
		var formD = new FormData(document.getElementById("registrar-popup"));
		formD.append("opcion",22);
		var email = $("#email_registro").val();
		var pass = $("#pass_registro").val();
		formD.append("password",pass);
		formD.append("email",email);
		registrar(formD);
	});
})

function get_productos_home(){
	$.ajax({
	  url:urlApi,
	  data:{"opcion":12},
	  type:"POST",
	  beforeSend:function(){
	    $(".ai-preloader").addClass("active");
	  },
	  success:function(data){
	    console.log(data);
	    render_productos_home(data.productos);
	  },
	  complete:function(){
	    $(".ai-preloader").removeClass("active");
	    //getParams();
	  }
	});
}

function render_productos_home(productos){
	var item = "";
	var producto;
	$("#nav-all .row").html("");
	for(var i in productos){
		producto = productos[i];
		item = "";
		item += '<div class="col-xl-3 col-lg-4 col-sm-6 mb--40 mb-md--30">';
		item += '<div class="airi-product">';
		item += '<div class="product-inner">';
		item += '<figure class="product-image">';
		item += '<div class="product-image--holder">';
		item += '<a href="detalle-producto.php?p='+producto.ID+'&inv='+producto.ID_inventario+'">';
		item += '<img src="'+urlImagenes+producto.Url_imagen+'" alt="Product Image" class="primary-image">';
		//item += '<img src="assets/img/products/producto1.jpg" alt="Product Image" class="secondary-image">';
		item += '</a>';
		item += '</div>';
		/*item += '<div class="airi-product-action">';
		item += '<div class="product-action">';
		item += '<a class="quickview-btn action-btn" href="#" data-idp="'+producto.ID+'" data-idinv="'+producto.ID_inventario+'" data-toggle="tooltip" data-placement="top" title="Compra Rápida">';
		item += '<span data-toggle="modal">';
		item += '<i class="dl-icon-view"></i>';
		item += '</span>';
		item += '</a>';
		item += '<a class="add_to_cart_btn action-btn" href="#" data-idP="'+producto.ID+'" data-idInv="'+producto.ID_inventario+'" data-toggle="tooltip" data-placement="top" title="Añadir al carrito">';
		item += '<i class="dl-icon-cart29"></i>';
		item += '</a>';
		item += '<a class="add_wishlist action-btn" href="wishlist.html" data-toggle="tooltip" data-placement="top" title="Añadir a favoritos">';
		item += '<i class="dl-icon-heart4"></i>';
		item += '</a>';
		item += '</div>';
		item += '</div>';*/
		item += '</figure>';
		item += '<div class="product-info text-center">';
		item += '<h3 class="product-title">';
		item += '<a href="detalle-producto.php?p='+producto.ID+'&inv='+producto.ID_inventario+'">'+producto.Producto+'</a>';
		item += '</h3>';
		item += '<span class="product-price-wrapper">';
		item += '<span class="money">$'+producto.Precio_publico+'</span>';
		item += '</span>';
		item += '</div>';
		item += '<div class="product-overlay"></div>';
		item += '</div>';
		item += '</div>';
		item += '</div>';
		$("#nav-all .row").append(item);
	}
}

function iniciar(datos){
	$.ajax({
	  url:urlApi,
	  data:datos,
	  cache: false,
	  contentType: false,
	  processData: false,
	  type:"POST",
	  beforeSend:function(){
	     $(".ai-preloader").addClass("active");
	  },
	  success:function(data){
	  	if(data.code == 200){
	  		window.location.href = "mi-cuenta.php";
	    	
	  	}
	    else
	    	Swal.fire({
	    	  icon: 'error',
	    	  title: 'Oops...',
	    	  text: data.msg
	    	});
	    
	  },
	  complete:function(){
	     $(".ai-preloader").removeClass("active");
	    //getParams();
	  }
	});
}

function registrar(datos){
	$.ajax({
	  url:urlApi,
	  data:datos,
	  cache: false,
	  contentType: false,
	  processData: false,
	  type:"POST",
	  beforeSend:function(){
	     $(".ai-preloader").addClass("active");
	  },
	  success:function(data){
	  	if(data.code == 200){
	    	window.location.href = "catalogo.html";	
	  	}
	    else
	    	Swal.fire({
	    	  icon: 'error',
	    	  title: 'Oops...',
	    	  text: data.msg
	    	});
	  },
	  complete:function(){
	     $(".ai-preloader").removeClass("active");
	    //getParams();
	  }
	});
}


/*para los banners y galeria de lineas*/
function getBanners(){
	$.ajax({
	  url:urlApi,
	  data:{"opcion":31},
	  type:"POST",
	  beforeSend:function(){
	    $(".ai-preloader").addClass("active");
	  },
	  success:function(data){
	  	if(data.code == 200){
	  		if(data.banners.length > 0)
	  			renderBanners(data.banners);
	  		if(data.lineas.length > 0)
	  			renderLineas(data.lineas);
	  	}
	    console.log(data);
	    //render_productos_home(data.productos);
	  },
	  complete:function(){
	    $(".ai-preloader").removeClass("active");
	    //getParams();
	  }
	});
}

function renderBanners(banners){
	var item,banner,index;
	for(var i in banners){
		banner = banners[i];
		index = parseInt(i)+1;
		item = '<li data-index="rs-'+i+'" data-transition="random-premium" data-slotamount="default" data-hideafterloop="0" data-hideslideonmobile="off"  data-easein="default" data-easeout="default" data-masterspeed="default"  data-thumb="assets/img/slider/home-02/100x50_m2-s1-1.jpg"  data-rotate="0"  data-saveperformance="off"  data-title="0'+index+'" data-param1="" data-param2="" data-param3="" data-param4="" data-param5="" data-param6="" data-param7="" data-param8="" data-param9="" data-param10="" data-description="">'
	        item += '<!-- MAIN IMAGE -->';
	        item += '<img src="assets/banner/'+banner.Url_imagen+'"  alt=""  data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat" data-bgparallax="2" class="rev-slidebg" data-no-retina>'
	        item += '<!-- LAYERS -->';

	        item += '<!-- LAYER NR. 1 -->';
	        item += '<div class="tp-caption rev_group" ';
	            item += 'id="slide-4-layer-'+i+'" '
	            item += 'data-x="[\'left\',\'left\',\'left\',\'left\']" data-hoffset="[\'100\',\'100\',\'50\',\'50\']" ';
	            item += 'data-y="[\'middle\',\'middle\',\'middle\',\'middle\']" data-voffset="[\'-160\',\'-160\',\'-20\',\'-20\']" ';
	                        item += 'data-width="[\'520\',\'520\',\'520\',\'364\']"';
	            item += 'data-height="[\'280\',\'280\',\'241\',\'205\']"';
	            item += 'data-whitespace="nowrap"';

	            item += 'data-type="group" ';
	            item += 'data-responsive_offset="on" ';

	            item += 'data-frames=\'[{"delay":10,"speed":300,"frame":"0","from":"opacity:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"}]\'';
	            item += 'data-margintop="[0,0,0,0]"';
	            item += 'data-marginright="[0,0,0,0]"';
	            item += 'data-marginbottom="[0,0,0,0]"';
	            item += 'data-marginleft="[0,0,0,0]"';
	            item += 'data-textAlign="[\'inherit\',\'inherit\',\'inherit\',\'inherit\']"';
	            item += 'data-paddingtop="[0,0,0,0]"';
	            item += 'data-paddingright="[0,0,0,0]"';
	            item += 'data-paddingbottom="[0,0,0,0]"';
	            item += 'data-paddingleft="[0,0,0,0]"';

	            item += 'style="z-index: 5; min-width: 740px; max-width: 740px; max-width: 380px; max-width: 380px; white-space: nowrap; font-size: 20px; line-height: 22px; font-weight: 400; color: #ffffff; letter-spacing: 0px;">';

	        item += '<!-- LAYER NR. 3 -->';
	        item += '<div class="tp-caption tp-resizeme averta-bold" ';
	            item += 'id="slide-4-layer-'+(i+1)+'" ';
	            item += 'data-x="[\'left\',\'left\',\'left\',\'left\']" data-hoffset="[\'0\',\'0\',\'0\',\'0\']" ';
	            item += 'data-y="[\'top\',\'top\',\'top\',\'top\']" data-voffset="[\'20\',\'20\',\'42\',\'7\']" ';
	                        item += 'data-fontsize="[\'64\',\'64\',\'50\',\'36\']"';
	            item += 'data-lineheight="[\'85\',\'85\',\'50\',\'40\']"';
	            item += 'data-width="none"';
	            item += 'data-height="none"';
	            item += 'data-whitespace="nowrap"';

	            item += 'data-type="text" ';
	            item += 'data-responsive_offset="on" ';

	            item += 'data-frames=\'[{"delay":"+990","speed":1500,"frame":"0","from":"x:[-100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;","mask":"x:0px;y:0px;s:inherit;e:inherit;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"auto:auto;","ease":"Power3.easeInOut"}]\'';
	            item += 'data-margintop="[0,0,0,0]"';
	            item += 'data-marginright="[0,0,0,0]"';
	            item += 'data-marginbottom="[0,0,0,0]"';
	            item += 'data-marginleft="[0,0,0,0]"';
	            item += 'data-textAlign="[\'inherit\',\'inherit\',\'inherit\',\'inherit\']"';
	            item += 'data-paddingtop="[,,,]"';
	            item += 'data-paddingright="[,,,]"';
	            item += 'data-paddingbottom="[,,,]"';
	            item += 'data-paddingleft="[,,,]"';

	            item += 'style="z-index: 7; white-space: nowrap; font-size: 64px; line-height: 85px; font-weight: 400; color: white; letter-spacing: 0px;">'+banner.Texto_descripcion;
	        item += '</div>';


	        item += '<!-- LAYER NR. 6 -->';
	        item += '<a class="tp-caption LA_white_btn rev-btn " href="'+banner.Link_boton+'" ';
	            item += 'id="slide-4-layer-'+(i+2)+'" ';
	            item += 'data-x="[\'left\',\'left\',\'left\',\'left\']" data-hoffset="[\'0\',\'0\',\'0\',\'0\']" ';
	            item += 'data-y="[\'top\',\'top\',\'top\',\'top\']" data-voffset="[\'340\',\'340\',\'220\',\'140\']" ';
	                        item += 'data-width="none"';
	            item += 'data-height="none"';
	            item += 'data-whitespace="nowrap"';

	            item += 'data-type="button" ';
	            item += 'data-responsive_offset="on" ';
	            item += 'data-responsive="off"';
	            item += 'data-frames=\'[{"delay":"+1790","speed":2000,"frame":"0","from":"y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;","to":"o:1;","ease":"Power4.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"auto:auto;","ease":"Power3.easeInOut"},{"frame":"hover","speed":"300","ease":"Linear.easeNone","to":"o:1;rX:0;rY:0;rZ:0;z:0;","style":"c:rgb(255,255,255);bg:transparent;bs:solid;bw:0 0 0 0;"}]\'';
	            item += 'data-margintop="[0,0,0,0]"';
	            item += 'data-marginright="[0,0,0,0]"';
	            item += 'data-marginbottom="[0,0,0,0]"';
	            item += 'data-marginleft="[0,0,0,0]"';
	            item += 'data-textAlign="[\'inherit\',\'inherit\',\'inherit\',\'inherit\']"';
	            item += 'data-paddingtop="[15,15,15,12]"';
	            item += 'data-paddingright="[45,45,45,35]"';
	            item += 'data-paddingbottom="[15,15,15,12]"';
	            item += 'data-paddingleft="[45,45,45,35]"';

	            item += 'style="z-index: 10; white-space: nowrap; border-color:rgba(0,0,0,1);outline:none;box-shadow:none;box-sizing:border-box;-moz-box-sizing:border-box;-webkit-box-sizing:border-box;cursor:pointer;">'+banner.Texto_boton+' </a>';
	        item += '</div>';
	    item += '</li>';
	    $("#rev_slider_2_1 ul").append(item);
	}
		$("#rev_slider_2_1").show().revolution({
			sliderType:"standard",
			sliderLayout:"fullwidth",
			dottedOverlay:"none",
			delay:9000,
			navigation: {
				keyboardNavigation:"off",
				keyboard_direction: "horizontal",
				mouseScrollNavigation:"off",
	             mouseScrollReverse:"default",
				onHoverStop:"off",
				bullets: {
					enable:true,
					hide_onmobile:false,
					style:"larev-dot2",
					hide_onleave:false,
					direction:"vertical",
					h_align:"right",
					v_align:"center",
					h_offset:30,
					v_offset:0,
	                space:5,
					tmp:'<span class="tp-bullet-title">{{title}}</span><span class="tp-bullet-wrap"></span>'
				}
			},
			responsiveLevels:[1240,1240,778,480],
			visibilityLevels:[1240,1240,778,480],
			gridwidth:[1920,1920,778,480],
			gridheight:[650,650,600,500],
			lazyType:"none",
			parallax: {
				type:"mouse+scroll",
				origo:"enterpoint",
				speed:400,
	          speedbg:0,
	          speedls:0,
				levels:[5,10,15,20,25,30,35,40,45,46,47,48,49,50,51,55],
			},
			shadow:0,
			spinner:"spinner0",
			stopLoop:"off",
			stopAfterLoops:-1,
			stopAtSlide:-1,
			shuffle:"off",
			autoHeight:"off",
			disableProgressBar:"on",
			hideThumbsOnMobile:"off",
			hideSliderAtLimit:0,
			hideCaptionAtLimit:0,
			hideAllCaptionAtLilmit:0,
			debugMode:false,
			fallbacks: {
				simplifyAll:"off",
				nextSlideOnWindowFocus:"off",
				disableFocusListener:false,
			}
		});
	
}

function renderLineas(lineas){
	var item;
	for(var i in lineas){
		item = '<div class="">';
		item += '<div class="banner-box banner-type-2 banner-hover-2">';
		item += '<div class="banner-inner">';
		item += '<div class="banner-image">';
		item += '<img src="assets/banner/'+lineas[i].Url_imagen+'" alt="Banner">'
		item += '</div>';
		item += '<div class="banner-info">';
		item += '<a class="banner-btn-2" href="catalogo.html?linea='+lineas[i].ID+'">';
		item += '<span class="normal-view">Línea '+lineas[i].Linea+'</span>';
		item += '<span class="hover-view">VER LÍNEA</span>';
		item += '</a>';
		item += '</div>';
		item += '<a class="banner-link banner-overlay" href="catalogo.html?linea='+lineas[i].ID+'">Ver línea</a>';
		item += '</div>';
		item += '</div>';
		item += '</div>';
		$("#galeriaLineas").append(item);
	}
	$('#galeriaLineas').owlCarousel({
	    slidesToShow: 4,
	    slidesToScroll: 1,
	    autoplay: true,
	    margin:0,
	    autoplaySpeed: 500,
	    arrows: false,
	    dots: false,
	    pauseOnHover: false,
	    responsiveClass:true,
	    responsive:{
	        0:{
	            items:2,
	            nav: false,
	            autoplay: true
	            
	        },
	        600:{
	            items:3,
	            nav: false,
	            autoplay: true
	        },
	        1000:{
	            items:4,
	            loop:true
	        }
	    }
	})
	/*item = '<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">';
	item += '<div class="banner-box banner-type-2 banner-hover-2">';
	item += '<div class="banner-inner">';
	item += '<div class="banner-image">';
	item += '<img src="assets/banner/'+lineas[0].Url_imagen+'" alt="Banner">'
	item += '</div>';
	item += '<div class="banner-info">';
	item += '<a class="banner-btn-2" href="'+lineas[0].Link_boton+'">';
	item += '<span class="normal-view">'+lineas[0].Texto_boton+'</span>';
	item += '<span class="hover-view">VER LÍNEA</span>';
	item += '</a>';
	item += '</div>';
	item += '<a class="banner-link banner-overlay" href="'+lineas[0].Link_boton+'">Ver línea</a>';
	item += '</div>';
	item += '</div>';
	item += '</div>';
	$("#galeriaLineas").append(item);

	item = '<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">';

	item += '<div class="banner-box banner-type-2 banner-hover-2">';
	item += '<div class="banner-inner">';
	item += '<div class="banner-image">';
	item += '<img src="assets/banner/'+lineas[1].Url_imagen+'" alt="Banner">'
	item += '</div>';
	item += '<div class="banner-info">';
	item += '<a class="banner-btn-2" href="'+lineas[1].Link_boton+'">';
	item += '<span class="normal-view">'+lineas[1].Texto_boton+'</span>';
	item += '<span class="hover-view">VER LÍNEA</span>';
	item += '</a>';
	item += '</div>';
	item += '<a class="banner-link banner-overlay" href="'+lineas[1].Link_boton+'">Ver línea</a>';
	item += '</div>';
	item += '</div>';
	item += '<div class="banner-box banner-type-2 banner-hover-2">';
	item += '<div class="banner-inner">';
	item += '<div class="banner-image">';
	item += '<img src="assets/banner/'+lineas[2].Url_imagen+'" alt="Banner">'
	item += '</div>';
	item += '<div class="banner-info">';
	item += '<a class="banner-btn-2" href="'+lineas[2].Link_boton+'">';
	item += '<span class="normal-view">'+lineas[2].Texto_boton+'</span>';
	item += '<span class="hover-view">VER LÍNEA</span>';
	item += '</a>';
	item += '</div>';
	item += '<a class="banner-link banner-overlay" href="'+lineas[2].Link_boton+'">Ver línea</a>';
	item += '</div>';
	item += '</div>';

	item += '</div>';
	$("#galeriaLineas").append(item);

	item = '<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">';
	item += '<div class="banner-box banner-type-2 banner-hover-2">';
	item += '<div class="banner-inner">';
	item += '<div class="banner-image">';
	item += '<img src="assets/banner/'+lineas[3].Url_imagen+'" alt="Banner">'
	item += '</div>';
	item += '<div class="banner-info">';
	item += '<a class="banner-btn-2" href="'+lineas[3].Link_boton+'">';
	item += '<span class="normal-view">'+lineas[3].Texto_boton+'</span>';
	item += '<span class="hover-view">VER LÍNEA</span>';
	item += '</a>';
	item += '</div>';
	item += '<a class="banner-link banner-overlay" href="'+lineas[3].Link_boton+'">Ver línea</a>';
	item += '</div>';
	item += '</div>';
	item += '</div>';
	$("#galeriaLineas").append(item);*/
}
