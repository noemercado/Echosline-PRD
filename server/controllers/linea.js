$(document).ready(function(){

});


function get_productos_linea(idLinea){
	$("#random_products .btnTienda").attr("href","catalogo.html?linea="+idLinea);
	$.ajax({
	  url:urlApi,
	  data:{"opcion":15,"linea":idLinea},
	  type:"POST",
	  beforeSend:function(){
	    $(".ai-preloader").addClass("active");
	  },
	  success:function(data){
	    console.log(data);
	    render_productos_linea(data.productos);
	  },
	  complete:function(){
	    $(".ai-preloader").removeClass("active");
	    //getParams();
	  }
	});
}

function render_productos_linea(productos){
	var item = "";
	var producto;
	//$("#random_products").html("");
	for(var i in productos){
		producto = productos[i];
		item = "";
		item += '<div class="col-xl-3 col-lg-4 col-sm-6 mb--40 mb-md--30">';
		item += '<div class="airi-product">';
		item += '<div class="product-inner">';
		item += '<figure class="product-image">';
		item += '<div class="product-image--holder">';
		item += '<a href="detalle-producto.php?p='+producto.ID+'&inv='+producto.ID_inventario+'">';
		item += '<img src="'+urlImagenes+producto.Url_imagen+'" alt="Product Image" class="primary-image">';
		//item += '<img src="assets/img/products/producto1.jpg" alt="Product Image" class="secondary-image">';
		item += '</a>';
		item += '</div>';
		/*item += '<div class="airi-product-action">';
		item += '<div class="product-action">';
		item += '<a class="quickview-btn action-btn" href="#" data-idp="'+producto.ID+'" data-idinv="'+producto.ID_inventario+'" data-toggle="tooltip" data-placement="top" title="Compra Rápida">';
		item += '<span data-toggle="modal">';
		item += '<i class="dl-icon-view"></i>';
		item += '</span>';
		item += '</a>';
		item += '<a class="add_to_cart_btn action-btn" href="#" data-idP="'+producto.ID+'" data-idInv="'+producto.ID_inventario+'" data-toggle="tooltip" data-placement="top" title="Añadir al carrito">';
		item += '<i class="dl-icon-cart29"></i>';
		item += '</a>';
		item += '<a class="add_wishlist action-btn" href="wishlist.html" data-toggle="tooltip" data-placement="top" title="Añadir a favoritos">';
		item += '<i class="dl-icon-heart4"></i>';
		item += '</a>';
		item += '</div>';
		item += '</div>';*/
		item += '</figure>';
		item += '<div class="product-info text-center">';
		item += '<h3 class="product-title">';
		item += '<a href="detalle-producto.php?p='+producto.ID+'&inv='+producto.ID_inventario+'">'+producto.Producto+'</a>';
		item += '</h3>';
		item += '<span class="product-price-wrapper">';
		item += '<span class="money">$'+producto.Precio_publico+'</span>';
		item += '</span>';
		item += '</div>';
		item += '<div class="product-overlay"></div>';
		item += '</div>';
		item += '</div>';
		item += '</div>';
		$("#random_products").prepend(item);
	}
}