/*var idProducto;
var idInventario;*/
//var producto = [];
$(document).ready(function(){
	/*var temp = getIdProducto();
	idProducto = temp["idP"];
	idInventario = temp["idInv"];*/
	//getDetalleProducto();

	getCatalogo();

	console.log(productoID+" "+inventarioID);
	$(".add-to-cart").on("click",function(){
		add_to_cart();
	});
	//Swal.fire('Any fool can use a computer')

	Promise.all(Array.from(document.images).filter(img => !img.complete).map(img => new Promise(resolve => { img.onload = img.onerror = resolve; }))).then(() => {
	    console.log('images finished loading');
	    //$(".sld").slick();
	    //elementCarousel();
	    
	});


});

function getDetalleProducto(){
	$.ajax({
	  url:urlApi,
	  data:{"opcion":2,"idP":idProducto,"idInv":idInventario},
	  type:"POST",
	  beforeSend:function(){
	    $("#preloader").show();
	  },
	  success:function(data){
	  	if(data.code == 200){
	  		producto = data.producto;
	  		renderDetalle();
	  	}else if(data.code == 400){

	  	}
	    console.log(data);
	    
	  },
	  complete:function(){
	    $("#preloader").fadeOut();
	    //getParams();
	  }
	});
}

function add_to_cart(){
	var qty = $("#qty").val();
	$.ajax({
	  url:urlApi,
	  data:{"opcion":3,"idP":productoID,"idInv":inventarioID, "qty":qty},
	  type:"POST",
	  beforeSend:function(){
	    $("#preloader").show();
	  },
	  success:function(data){
	  	/*if(data.code == 200){
	  		producto = data.producto;
	  		//renderDetalle();
	  	}else if(data.code == 400){

	  	}*/
	    console.log(data);
	    Swal.fire({
	      html:'<div id="agregado"><h3>Producto agregado al carrito!</h3><img src="'+urlImagenes+data.producto.Url_imagen+'" alt="" /><strong>'+data.producto.Producto+'</strong><br /><strong>Cantidad: '+data.qty+'</strong></div>',
	      timer: 5000,
	      onClose: () => {
	        window.location.href = "catalogo.html";
	      }
	    })
	    
	  },
	  complete:function(){
	    $("#preloader").fadeOut();
	    update_cart_ajax();
	    //getParams();
	  }
	});
}

/*functiones para renderizar*/
function renderDetalle(){
	var item = '<div class="col-md-6 col-lg-6 product-main-image">'+
	'<div class="product-image">'+
	'<div class="product-gallery vertical-slide-nav">'+
	'<div class="product-gallery__large-image">'+
	'<div class="gallery-with-thumbs">'+
	'<div class="product-gallery__wrapper">'+
	'<div class="main-slider product-gallery__full-image image-popup slick-initialized slick-slider">'+
	'<div class="slick-list draggable"><div class="slick-track" style="opacity: 1; width: 370px;"><figure class="product-gallery__image zoom slick-slide slick-current slick-active" style="position: relative; overflow: hidden; width: 370px; left: 0px; top: 0px; z-index: 999; opacity: 1;" data-slick-index="0" aria-hidden="false" tabindex="0">'+
	'<img src="assets/img/products/bigProduct.png" alt="Product">'+
	'<img role="presentation" alt="" src="http://localhost/echosline/assets/img/products/bigProduct.png" class="zoomImg" style="position: absolute; top: -0.95679px; left: -139.926px; opacity: 0; width: 547px; height: 579px; border: none; max-width: none; max-height: none;"></figure></div></div>'+
	'</div>'+
	'</div>'+
	'</div>'+
	'</div>'+
	'</div>'+
	'<!--<span class="product-badge new">New</span>-->'+
	'</div>'+
	'</div>';
	//$("#contenedorProducto").append(item);



	item = '<div class="row justify-content-center pt--45 pt-lg--50 pt-md--55 pt-sm--35">'
	+'<div class="col-12">'
	+'<div class="product-data-tab tab-style-1">'
	+'<div class="nav nav-tabs product-data-tab__head mb--5 mb-md--5 tablaInformacion" id="product-tab" role="tablist">'
	+'<a class="product-data-tab__link nav-link active" id="nav-description-tab" data-toggle="tab" href="#nav-description" role="tab" aria-selected="true"> '
	+'<span>Descripción</span>'
	+'</a>'
	+'<a class="product-data-tab__link nav-link" id="nav-uso-tab" data-toggle="tab" href="#nav-uso" role="tab" aria-selected="true">'
	+'<span>Cómo usar</span>'
	+'</a>'
	+'<a class="product-data-tab__link nav-link" id="nav-acerca-tab" data-toggle="tab" href="#nav-acerca" role="tab" aria-selected="true">'
	+'<span>Sobre Karbon</span>'
	+'</a>'
	+'</div>'
	+'<div class="tab-content product-data-tab__content" id="product-tabContent">'
	+'<div class="tab-pane fade show active" id="nav-description" role="tabpanel" aria-labelledby="nav-description-tab">'
	+'<div class="product-description textBlack">'
	+'<p>Producto vegano enriquecido con Carbón Activado y 9 extractos botánicos. Desenreda, acondiciona y nutre el cabello, dejándolo suave, sano y revitalizado. Regenera la fibra capilar, combate los efectos del estrés que el estilo de vida metropolitano puede tener en el cabello.</p>'
	+'</div>'
	+'</div>'
	+'<div class="tab-pane fade" id="nav-uso" role="tabpanel" aria-labelledby="nav-reviews-tab">'
	+'<div class="product-description textBlack">'
	+'<p>Lorem ipsum afat</p>'
	+'</div>'
	+'</div>'
	+'<div class="tab-pane fade" id="nav-acerca" role="tabpanel" aria-labelledby="nav-reviews-tab">'
	+'<div class="product-description textBlack">'
	+'<p>Lorem ipsum</p>'
	+'</div>'
	+'</div>'
	+'</div>'
	+'</div>'
	+'</div>'
	+'</div>';
	//$("#contenedorProducto").append(item);
}

var listaProductos = [];
var sig;
var ant;
function getCatalogo(){
	$.ajax({
	  url:"server/api.php",
	  data:{"opcion":1},
	  type:"POST",
	  beforeSend:function(){
	    $(".ai-preloader").addClass("active");
	  },
	  success:function(data){
	    //console.log(data);
	    listaProductos = data.productos;
	    //alert(productoID)
	    var index = 0;
	    for(var i in listaProductos){
	    	if(listaProductos[i].ID == productoID){
	    		//alert(index)
	    		if(listaProductos[index+1]){
	    			sig = "detalle-producto.php?p="+listaProductos[index+1].ID+"&inv="+listaProductos[index+1].ID_inventario
	    		}
	    		if(listaProductos[index-1]){
	    			ant = "detalle-producto.php?p="+listaProductos[index-1].ID+"&inv="+listaProductos[index-1].ID_inventario
	    		}
	    		break;
	    	}
	    	index++;
	    }
	    if(!sig)
	    	$(".product-navigation .next").hide();
	    if(!ant)
	    	$(".product-navigation .prev").hide();
	  },
	  complete:function(){
	    $(".ai-preloader").removeClass("active");
	    //getParams();
	  }
	});
}

$(".product-navigation .prev").on("click",function(){
	window.location.href = ant;
})
$(".product-navigation .next").on("click",function(){
	window.location.href = sig;
})