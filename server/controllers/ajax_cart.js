$(document).ready(function(){
	update_cart_ajax();
	getLineas_subLineas();
	$(document).on("click",".ajaxCartDel",function(e){
		e.preventDefault();
		var idP = $(this).data("idp");
		if(!$(this).hasClass("oferta")){
			var idInv = $(this).data("idinv");
			console.log(idP+" / "+idInv);
			del_item_cart_ajax(idP,idInv,$(this).closest("li"));
		}else{
			del_item_cart_ajax_oferta(idP,$(this).closest("li"));
		}
		
	});

	$("ul.megamenu.four-column li:nth-child(2)").hide();

	$(".user-info-menu-btn a").attr("href","mi-cuenta.php");
	$(".user-info-menu-btn a").each(function(){
		if($(this).text() == "Cerrar sesión")
			$(this).attr("href","#");
	})

});

function update_cart_ajax(){
	$.ajax({
	  url:urlApi,
	  data:{"opcion":4},
	  type:"POST",
	  beforeSend:function(){
	    $(".ai-preloader").addClass("active");
	  },
	  success:function(data){
	  	if(data.code == 200){
	  		//var subtotal = 
	  		$(".mini-cart__total .ammount").text("$"+data.subtotal);
	  	}else if(data.code == 400){
	  		$(".mini-cart__total .ammount").text("$0.00");
	  	}
	  	cantidad = 0;
	  	render_cart_ajax(data.itemsCart_oferta,true);
	  	render_cart_ajax(data.itemsCart,false);
	    console.log(data);
	    
	  },
	  complete:function(){
	    $(".ai-preloader").removeClass("active");
	    //getParams();
	  }
	});
}

function del_item_cart_ajax(idP,idInv,elem){
	$.ajax({
	  url:urlApi,
	  data:{"opcion":5,"idP":idP,"idInv":idInv},
	  type:"POST",
	  beforeSend:function(){
	    $(".ai-preloader").addClass("active");
	  },
	  success:function(data){
	    
	  },
	  complete:function(){
	    $(".ai-preloader").removeClass("active");
	    //getParams();
	    update_cart_ajax();
	  }
	});
}
function del_item_cart_ajax_oferta(idP,elem){
	$.ajax({
	  url:urlApi,
	  data:{"opcion":19,"idP":idP},
	  type:"POST",
	  beforeSend:function(){
	    $(".ai-preloader").addClass("active");
	  },
	  success:function(data){
	    
	  },
	  complete:function(){
	    $(".ai-preloader").removeClass("active");
	    //getParams();
	    update_cart_ajax();
	  }
	});
}

var cantidad = 0;
function render_cart_ajax(productos,oferta){
	if(oferta)
		$("ul.mini-cart__list").html("");
	//var cantidad = 0;
	for(var i in productos){
		var producto = productos[i];
		console.log(producto)
		var elem = '<li class="mini-cart__product">';
		elem += '<a href="#" class="remove-from-cart remove">';
		if(!oferta)
			elem += '<i class="dl-icon-close ajaxCartDel" data-idP='+producto.ID+' data-idInv='+producto.ID_inventario+'></i>';
		else
			elem += '<i class="dl-icon-close ajaxCartDel oferta" data-idP='+producto.ID+'></i>';
		elem += '</a>';
		elem += '<div class="mini-cart__product__image">';
		elem += '<img src="'+urlImagenes+producto.Url_imagen+'" alt="products">';
		elem += '</div>';
		elem += '<div class="mini-cart__product__content">';
		if(oferta){
			elem += '<a class="mini-cart__product__title" href="detalle-producto-oferta.php?p='+producto.ID+'">'+producto.Promocion+'  </a>';
			elem += '<span class="mini-cart__product__quantity">'+producto.qty+' x $'+producto.precio+'</span>';
		}else{
			elem += '<a class="mini-cart__product__title" href="detalle-producto.php?p='+producto.ID+'&inv='+producto.ID_inventario+'">'+producto.Producto+'  </a>';
			elem += '<span class="mini-cart__product__quantity">'+producto.qty+' x $'+producto.Precio_publico+'</span>';
		}
		
		elem += '</div>';
		elem += '</li>';
		$("ul.mini-cart__list").append(elem);
		cantidad += parseInt(producto.qty);
	}
	$(".mini-cart-count").text(cantidad);                          
}

/*

funciones para el menu, lo pondría en header.js pero me da hueva modificar todas las páginas

*/
var sublineas;
function getLineas_subLineas(){
	$.ajax({
	  url:urlApi,
	  data:{"opcion":13},
	  type:"POST",
	  beforeSend:function(){
	    $(".ai-preloader").addClass("active");
	  },
	  success:function(data){
	  	if(data.code == 200){
	  		render_lineas_sublineas(data.lineas,data.sublineas);
	  		sublineas = data.sublineas;
	  	}
	  	
	    console.log(data);
	    
	  },
	  complete:function(){
	    $(".ai-preloader").removeClass("active");
	    //getParams();
	  }
	});
}

function render_lineas_sublineas(lineas,sublineas){
	var linea;
	var sublinea;
	var elem;
	$("ul.megamenu.four-column li:nth-child(1) ul").html("");
	$("ul.megamenu.four-column li:nth-child(3) ul").html("");
	var urlLinea = "";
	for(var i in lineas){
		linea = lineas[i];
		elem = "<li data-id='"+linea.ID+"'>";
		switch(linea.Linea){
			case "Karbon 9":
				urlLinea = "linea.html";
				break;
			case "Keratin":
				urlLinea = "linea-keratin.html";
				break;
			case "Luxury":
				urlLinea = "linea-luxury.html";
				break;
			case "KI-Power":
				urlLinea = "linea-kipower.html";
				break;
			default:
				break;
		}
		//elem += '<a href="catalogo.html?linea='+linea.ID+'">';
		elem += '<a href="'+urlLinea+'">';
        elem += '<span class="mm-text">'+linea.Linea+'</span>';
        elem += '</a>';
        elem += '</li>';
        $("ul.megamenu.four-column li:nth-child(1) ul").append(elem);
	}

	/*for(var i in sublineas){
		sublinea = sublineas[i];
		elem = "<li>";
		elem += '<a href="catalogo.html?linea='+sublinea.idLinea+'&sublinea='+sublinea.ID+'">';
        elem += '<span class="mm-text">'+sublinea.Sublinea+'</span>';
        elem += '</a>';
        elem += '</li>';
        $("ul.megamenu.four-column li:nth-child(3) ul").append(elem);
	}*/
}

$(document).on("mouseenter","ul.megamenu.four-column li:nth-child(1) ul li a",function(){
	var id = $(this).parent().data("id");
	$("ul.megamenu.four-column li:nth-child(3) ul").html("");
	for(var i in sublineas){
		var sublinea = sublineas[i];
		if(sublinea.idLinea == id){
			elem = "<li>";
			elem += '<a href="catalogo.html?linea='+sublinea.idLinea+'&sublinea='+sublinea.ID+'">';
	        elem += '<span class="mm-text">'+sublinea.Sublinea+'</span>';
	        elem += '</a>';
	        elem += '</li>';
	        $("ul.megamenu.four-column li:nth-child(3) ul").append(elem);
		}

	}
});

