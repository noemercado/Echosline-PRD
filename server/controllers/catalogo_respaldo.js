$(document).ready(function(){

	getParams();
	//getCatalogo();
	$(document).on("click",".add_to_cart_btn",function(e){
		e.preventDefault();
		var idP = $(this).data("idp");
		var idInv = $(this).data("idinv");
		console.log(idP+" "+idInv);
		add_to_cart(idP,idInv,1);
	});

	$(document).on("click",".quickview-btn",function(e){
		e.preventDefault();
		var idP = $(this).data("idp");
		var idInv = $(this).data("idinv");
		console.log(idP+" "+idInv);
		get_detalle_producto(idP,idInv);
		//productModal
		//add_to_cart(idP,idInv);
	});

	$(document).on("click","#modal_add_to_cart",function(e){
		e.preventDefault();
		var idP = $(this).data("idp");
		var idInv = $(this).data("idinv");
		var qty = $("#quick-qty").val();
		add_to_cart(idP,idInv,qty);
	});
});
/*funceiones para obtener datos*/

function getParams(){
  var params = GET(window.location.href);
  if(params){
    if(params.linea){//buscó por linea
    	if(params.sublinea){
    		get_catalogo_linea_sublinea(params.linea.replace("#",""),params.sublinea.replace("#",""))
    	}else{
    		/*hacer llamada a filtro por linea*/
    		get_catalogo_linea(params.linea.replace("#",""));
    	}
      //renderMigas("funcion",params.d.replace("#",""),params.f.replace("#",""));
    }else if(params.p){//para detalle del producto
      getDetalleProducto(params.p.replace("#",""));
    }else if(params.search){
      buscarProductos(params.search.replace("#",""));
    }else{
    	/*solo*/
    }
  }else if (window.location.href.indexOf("catalogo") > -1) {
    console.log("es catalogo");
    getCatalogo(); 
  }else{
    console.log("no hay parametros y no es catalogo");
    //getProductosHome();
  }

}

function getCatalogo(){
	$.ajax({
	  url:urlApi,
	  data:{"opcion":1},
	  type:"POST",
	  beforeSend:function(){
	    $(".ai-preloader").addClass("active");
	  },
	  success:function(data){
	    console.log(data);
	    renderCatalogo(data.productos);
	    render_lineas(data.lineas);
	    console.log("total: "+data.productos.length);
	    $(".product-pages").text("Mostrando "+data.productos.length+" de "+data.productos.length+" productos");
	  },
	  complete:function(){
	    $(".ai-preloader").removeClass("active");
	    //getParams();
	  }
	});
}

function get_catalogo_linea(linea){
	$.ajax({
	  url:urlApi,
	  data:{"opcion":9,"linea":linea},
	  type:"POST",
	  beforeSend:function(){
	    $(".ai-preloader").addClass("active");
	  },
	  success:function(data){
	    console.log(data);
	    renderCatalogo(data.productos);
	    render_lineas(data.lineas);
	    console.log("total: "+data.productos.length);
	    $(".product-pages").text("Mostrando "+data.productos.length+" de "+data.productos.length+" productos");
	  },
	  complete:function(){
	    $(".ai-preloader").removeClass("active");
	    //getParams();
	  }
	});
}

function get_catalogo_linea_sublinea(linea,sublinea){
	$.ajax({
	  url:urlApi,
	  data:{"opcion":14,"linea":linea,"sublinea":sublinea},
	  type:"POST",
	  beforeSend:function(){
	    $(".ai-preloader").addClass("active");
	  },
	  success:function(data){
	    console.log(data);
	    renderCatalogo(data.productos);
	    render_lineas(data.lineas);
	  },
	  complete:function(){
	    $(".ai-preloader").removeClass("active");
	    //getParams();
	  }
	});
}

function add_to_cart(idP,idInv,qty){
	//var qty = $("#qty").val();
	$.ajax({
	  url:urlApi,
	  data:{"opcion":3,"idP":idP,"idInv":idInv, "qty":qty},
	  type:"POST",
	  beforeSend:function(){
	    $(".ai-preloader").addClass("active");
	  },
	  success:function(data){
	    console.log(data);
	    Swal.fire({
	      html:'<div id="agregado"><h3>Producto agregado al carrito!</h3><img src="'+urlImagenes+data.producto.Url_imagen+'" alt="" /><strong>'+data.producto.Producto+'</strong><br /><strong>Cantidad: '+data.qty+'</strong></div>',
	      timer: 5000
	    })
	    
	  },
	  complete:function(){
	    $(".ai-preloader").removeClass("active");
	    update_cart_ajax();
	    //getParams();
	  }
	});
}

function get_detalle_producto(idP,idInv){
	$.ajax({
	  url:urlApi,
	  data:{"opcion":10,"idP":idP,"idInv":idInv},
	  type:"POST",
	  beforeSend:function(){
	    $(".ai-preloader").addClass("active");
	  },
	  success:function(data){
	    console.log(data);
	    if(data.code == 200)
	    	render_modal(data.producto[0])
	    else
	    	Swal.fire({
	    	  icon: 'error',
	    	  title: 'Oops...',
	    	  text: data.msg
	    	})
	  },
	  complete:function(){
	    $(".ai-preloader").removeClass("active");
	    //getParams();
	  }
	});
}

function buscarProductos(termino){
	$.ajax({
	  url:urlApi,
	  data:{"opcion":25,"termino":termino},
	  type:"POST",
	  beforeSend:function(){
	    $(".ai-preloader").addClass("active");
	  },
	  success:function(data){
	    console.log(data);
	    renderCatalogo(data.productos);
	    render_lineas(data.lineas);
	    if(data.productos.length <= 0)
	    	$("#productos .row").append("<h1>Sin resultados para la búsqueda: <span>"+termino+"</span></h1>")
	  },
	  complete:function(){
	    $(".ai-preloader").removeClass("active");
	    //getParams();
	  }
	});
}





/*functiones para renderizar*/
function renderCatalogo(productos){
	var item = "";
	var producto;
	$("#productos .row").html("");
	for(var i in productos){
		producto = productos[i];
		item = "";
		item += '<div class="col-lg-4 col-sm-6 mb--40 mb-md--30">';
		item += '<div class="airi-product">';
		item += '<div class="product-inner">';
		item += '<figure class="product-image">';
		item += '<div class="product-image--holder">';
		item += '<a href="detalle-producto.php?p='+producto.ID+'&inv='+producto.ID_inventario+'">';
		item += '<img src="'+urlImagenes+producto.Url_imagen+'" alt="Product Image" class="primary-image">';
		//item += '<img src="assets/img/products/producto1.jpg" alt="Product Image" class="secondary-image">';
		item += '</a>';
		item += '</div>';
		item += '<div class="airi-product-action">';
		item += '<div class="product-action">';
		item += '<a class="quickview-btn action-btn" href="#" data-idp="'+producto.ID+'" data-idinv="'+producto.ID_inventario+'" data-toggle="tooltip" data-placement="top" title="Compra Rápida">';
		item += '<span data-toggle="modal">';
		item += '<i class="dl-icon-view"></i>';
		item += '</span>';
		item += '</a>';
		item += '<a class="add_to_cart_btn action-btn" href="#" data-idP="'+producto.ID+'" data-idInv="'+producto.ID_inventario+'" data-toggle="tooltip" data-placement="top" title="Añadir al carrito">';
		item += '<i class="dl-icon-cart29"></i>';
		item += '</a>';
		item += '<a class="add_wishlist action-btn" href="wishlist.html" data-toggle="tooltip" data-placement="top" title="Añadir a favoritos">';
		item += '<i class="dl-icon-heart4"></i>';
		item += '</a>';
		item += '</div>';
		item += '</div>';
		item += '</figure>';
		item += '<div class="product-info text-center">';
		item += '<h3 class="product-title">';
		item += '<a href="detalle-producto.php?p='+producto.ID+'&inv='+producto.ID_inventario+'">'+producto.Producto+'</a>';
		item += '</h3>';
		item += '<span class="product-price-wrapper">';
		item += '<span class="money">$'+producto.Precio_publico+'</span>';
		item += '</span>';
		item += '</div>';
		item += '<div class="product-overlay"></div>';
		item += '</div>';
		item += '</div>';
		item += '</div>';
		$("#productos .row").append(item);
	}
}

function render_lineas(lineas){
	var linea = "";
	var item = "";
	for(var i in lineas){
		linea = lineas[i];
		item = '<li><a href="catalogo.html?linea='+linea["ID"]+'">'+linea["Linea"]+'</a><span class="count">(0)</span></li>';
		$("#lineasList").append(item);
	}

}

function render_modal(producto){
	$("#href_modal_detalle").attr("href","detalle-producto.php?p="+producto.ID+"&inv="+producto.ID_inventario);
	$("#modal_imagen").attr("src",urlImagenes+producto.Url_imagen);
	$("#modal_titulo").text(producto.Producto);
	$("#modal_precio").text(producto.Precio_publico);
	$("#modal_descripcion").text(producto.Descripcion);
	$("#modal_add_to_cart").data("idp",producto.ID);
	$("#modal_add_to_cart").data("idinv",producto.ID_inventario);
	$("#productModal").modal("show");
}