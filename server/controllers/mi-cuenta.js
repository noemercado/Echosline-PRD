$(document).ready(function(){

	var params = GET(window.location.href);
	if(params){
		if(params.section){
			if(params.section == "orders")
				$("#tabPedidos").click();
		}
	}
	

	get_datos_usuario();
	var meses = ["Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"];
	for(var i = 1; i <= 31; i++){
		if(i < 10)
			$("#dia").append("<option value='0"+i+"'>0"+i+"</option>");
		else
			$("#dia").append("<option value='"+i+"'>"+i+"</option>");
	}
	for(var i in meses){
		console.log(parseInt(i)+1);
		if(i < 9)
			$("#mes").append("<option value='0"+(parseInt(i)+1)+"'>"+meses[i]+"</option>");
		else
			$("#mes").append("<option value='"+(parseInt(i)+1)+"'>"+meses[i]+"</option>");
	}
	for(var i = 2001; i >= 1960; i--)
		$("#anio").append("<option value='"+i+"'>"+i+"</option>");
})

$(".nav-link").on("click",function(e){
	e.preventDefault();
	var seccion = $(this).attr("href");
	switch (seccion){
		case "#accountdetails":
			render_detalles_cuenta();
			break;
		case "#orders":
			render_pedidos();
			break;
		case "login-register.html":
			salir();
			break;
		case "#addresses":
			render_domicilios();
			break;
		default:
			break;
	}
	
});

$("#form-cuenta").on("submit",function(){
	var formD = new FormData(document.getElementById("form-cuenta"));
	formD.append("opcion",2);
	formD.append("id",usuario.ID);
	var currpass = $("#cur_pass").val();
	formD.append("cur_pass",currpass);
	if($("#new_pass").val() != "" || $("#conf_new_pass").val() != ""){
		if($("#new_pass").val() == $("#conf_new_pass").val()){
			formD.append("nueva_pass",$("#new_pass").val());
			actualizar_usuario(formD);
		}else{
			Swal.fire({
			  icon: 'error',
			  title: 'Oops...',
			  text: 'Las contraseñas no coinciden'
			})
		}
	}else{
		actualizar_usuario(formD);
	}

});

var usuario;
var pedidos;


function get_datos_usuario(){
	$.ajax({
	  url:urlApiAccount,
	  data:{"opcion":1},
	  type:"POST",
	  beforeSend:function(){
	     $(".ai-preloader").addClass("active");
	  },
	  success:function(data){
	  	console.log(data)
	  	if(data.code == 200)
	  		usuario = data.usuario;
	    	pedidos = data.pedidos;
	    	$("#nombre-usuario").text("Hola "+usuario.Nombre+"!");
	  },
	  complete:function(){
	     $(".ai-preloader").removeClass("active");
	    //getParams();
	  }
	});
}

function actualizar_usuario(formD){
	$.ajax({
	    url:urlApiAccount,
	    type:"post",
	    data:formD,
	    cache: false,
	    contentType: false,
	    processData: false,
	    beforeSend:function(){
	        $(".ai-preloader").addClass("active");
	    },
	    success:function(data){
	        console.log(data);
	        if(data.code == 200){
	        	usuario = data.usuario;
	        	Swal.fire({
	        	  icon: 'success',
	        	  title: 'Éxito',
	        	  text: 'Tus datos has sido actualizados'
	        	});
	        	render_detalles_cuenta();
	        }else{
	        	Swal.fire({
	        	  icon: 'error',
	        	  title: 'Oops...',
	        	  text: data.msg
	        	})
	        }
	        
	    },
	    complete:function(){
	        $(".ai-preloader").removeClass("active");

	    },
	    error:function(x,e){
	      if (x.status==0) {
	            alert('You are offline!!\n Please Check Your Network.');
	        } else if(x.status==404) {
	            alert('Requested URL not found.');
	        } else if(x.status==500) {
	            alert('Internel Server Error.');
	        } else if(e=='parsererror') {
	            alert('Error.\nParsing JSON Request failed.');
	        } else if(e=='timeout'){
	            alert('Request Time out.');
	        } else {
	            alert('Unknow Error.\n'+x.responseText);
	        }
	      //console.log("ERROR: "+data);
	      //$("#alertaError").show();
	      $(".ai-preloader").removeClass("active");
	    }
	});
}

function render_detalles_cuenta(){
	$("#nombre").val(usuario.Nombre);
	$("#telefono").val(usuario.Tel1);
	$("#email").val(usuario.Email);
	$("#cur_pass").val(usuario.Password);
	var fecha = usuario.Nacimiento;
	//$("#cur_pass").prop("enabled",false);
	var spl = fecha.split("-");
	$("#dia").val(spl[2]);
	$("#mes").val(spl[1]);
	$("#anio").val(spl[0]);
}

function render_pedidos(){
	var elem = "";
	var pedido;
	var fecha;
	var spl;
	$("#tabla-pedidos tbody").html("");

	for(var i in pedidos){
		pedido = pedidos[i];
		elem = '<tr class="">';
		elem += '<td class="text-underline"><a href="#" data-index="'+i+'" class="detalle-pedido">'+pedido.ID+'</a></td>';
		fecha = pedido.Fecha_venta.replace(" ","-").split("-");
		console.log(fecha);
		elem += '<td class="wide-column">'+fecha[2]+'/'+fecha[1]+'/'+fecha[0]+'</td>';
		elem += '<td>'+pedido.Status+'</td>';
		elem += '<td class="wide-column">$'+pedido.Total+'</td>';
		elem += '<td>';
		elem += '<a href="#" class="btn-recibo" data-index="'+i+'">';
		elem += '<i class="fas fa-receipt"></i>';
		elem += '</a>';
		elem += '</td>';
		elem += '</tr>';
		$("#tabla-pedidos tbody").append(elem);
	}
}

$(document).on("click",".detalle-pedido",function(){
	var index = $(this).data("index");
	console.log(pedidos[index])
	get_detalle_pedido(pedidos[index].idVentaMenudeo);
});

$(document).on("click",".btn-recibo",function(){
	var index = $(this).data("index");
	openModalTicket(pedidos[index].ID);
});

function get_detalle_pedido(id){
	$.ajax({
	    url:urlApiAccount,
	    type:"post",
	    data:{"id":id,"opcion":5},
	    beforeSend:function(){
	        $(".ai-preloader").addClass("active");
	    },
	    success:function(data){
	        console.log(data);
	        if(data.code == 200){
	        	$("#titulo-pedido-modal").text("Pedido #"+id);
	        	render_detalle_pedido(data.detalles);
	        }else{
	        	Swal.fire({
	        	  icon: 'error',
	        	  title: 'Oops...',
	        	  text: data.msg
	        	})
	        }
	        
	    },
	    complete:function(){
	        $(".ai-preloader").removeClass("active");

	    },
	    error:function(x,e){
	      if (x.status==0) {
	            alert('You are offline!!\n Please Check Your Network.');
	        } else if(x.status==404) {
	            alert('Requested URL not found.');
	        } else if(x.status==500) {
	            alert('Internel Server Error.');
	        } else if(e=='parsererror') {
	            alert('Error.\nParsing JSON Request failed.');
	        } else if(e=='timeout'){
	            alert('Request Time out.');
	        } else {
	            alert('Unknow Error.\n'+x.responseText);
	        }
	      //console.log("ERROR: "+data);
	      //$("#alertaError").show();
	      $(".ai-preloader").removeClass("active");
	    }
	});
}

function render_detalle_pedido(items){
	$("#tabla-detalle-pedido tbody").html("");
	var elem = "";
	var item;
	for(var i in items){
		item = items[i];
		elem = "<tr>";
		if(item.idPromocion != null)
			elem += '<td><a href="detalle-producto-oferta.php?p='+item.idPromocion+'">'+item.Producto+'</a></td>';
		else
			elem += '<td><a href="detalle-producto.php?p='+item.idCatalogo+'&inv='+item.idInventario+'">'+item.Producto+'</a></td>';
		elem += '<td>'+item.Cantidad+'</td>';
		elem += '<td>$'+item.Precio_unitario+'</td>';
		elem += '<td>$'+item.Importe+'</td>';
		$("#tabla-detalle-pedido tbody").append(elem);
	}
	$("#detalle-pedido-modal").modal("show");
}

function salir(){
	$.ajax({
	  url:urlApiAccount,
	  data:{"opcion":3},
	  type:"POST",
	  beforeSend:function(){
	     $(".ai-preloader").addClass("active");
	  },
	  success:function(data){
	  	console.log(data)
	  	if(data.code == 200)
	  		window.location.href = "catalogo.html";
	  },
	  complete:function(){
	     $(".ai-preloader").removeClass("active");
	    //getParams();
	  }
	});
}

/*por ahora solo el domicilio general de la tabla Clientes_menudeo*/
function render_domicilios(){
	$("#lista-domicilios").html("");
	var elem = '<div class="col-md-6 mb-sm--20 text-black">';
	elem += '<p class="mb--20">Casa</p>';
	elem += '<p class="m-0 ln-0">'+usuario.Calle_numero+'</p>';
	elem += '<p class="m-0 ln-0">'+usuario.Colonia+' '+usuario.CP+'</p>';
	elem += '<p class="m-0 ln-0">'+usuario.Ciudad+', '+usuario.Estado+', '+usuario.Pais+'</p>';
	elem += '<p class="m-0 ln-0 averta-bold">Teléfono: <span class="averta-regular">'+usuario.Tel1+'</span></p>';
	elem += '<div class="text-block mt--20 mb--20">';
	elem += '<a class="averta-bold" href="#" id="editar-direccion">Editar dirección</a> <!--| <a class="averta-bold" href="#" id="eliminar">Eliminar dirección</a>-->';
	elem += '</div>';
	elem += '<div class="custom-checkbox mb--10 bb2 pb--20">';
	elem += '<label class="contenedor">';
	elem += '<input type="checkbox" class="form__checkbox">';
	elem += '<span class="checkmark"></span>';
	elem += '</label>';
	elem += '<label for="" class="inline-block pl-0 form__label form__label--2 shipping-label mb-0 ">Dirección principal</label>';
	elem += '</div>';
	elem += '</div>';
    $("#lista-domicilios").append(elem);
    var callenum = usuario.Calle_numero.split("#");
    console.log(callenum)
    $("#calle").val(callenum[0]);
    $("#exterior_dom").val(parseInt(callenum[1]));
    $("#cp").val(usuario.CP);
    $("#estado").val(usuario.Estado);
    $("#municipio").val(usuario.Ciudad);
    $("#colonia").val(usuario.Colonia);
    $("#telefono_dom").val(usuario.Tel1);
}

$(document).on("click","#editar-direccion",function(){
	$("#editar-direccion-modal").modal("show");
});

$(document).on("click","#guardar-domicilio",function(){
	console.log("clik en guardar")
	$("#enviar-form").click();
});

/*$("#enviar-form").on("click",function(){
	console.log("algo mas")
})*/

$("#form-editar-direccion").on("submit",function(){
	var formD = new FormData(document.getElementById("form-editar-direccion"));
	formD.append("opcion",4);
	formD.append("id",usuario.ID);
	formD.append("email",usuario.Email);
	actualizar_domicilio(formD);
});

function actualizar_domicilio(formD){
	$.ajax({
	    url:urlApiAccount,
	    type:"post",
	    data:formD,
	    cache: false,
	    contentType: false,
	    processData: false,
	    beforeSend:function(){
	        $(".ai-preloader").addClass("active");
	    },
	    success:function(data){
	        console.log(data);
	        if(data.code == 200){
	        	$("#editar-direccion-modal").modal("hide");
	        	Swal.fire({
	        	  icon: 'success',
	        	  title: 'Éxito',
	        	  text: 'Tu domicilio ha sido actualizado'
	        	});
	        	usuario = data.usuario;
	        	render_domicilios();
	        }else{
	        	Swal.fire({
	        	  icon: 'error',
	        	  title: 'Oops...',
	        	  text: data.msg
	        	})
	        }
	        
	    },
	    complete:function(){
	        $(".ai-preloader").removeClass("active");

	    },
	    error:function(x,e){
	      if (x.status==0) {
	            alert('You are offline!!\n Please Check Your Network.');
	        } else if(x.status==404) {
	            alert('Requested URL not found.');
	        } else if(x.status==500) {
	            alert('Internel Server Error.');
	        } else if(e=='parsererror') {
	            alert('Error.\nParsing JSON Request failed.');
	        } else if(e=='timeout'){
	            alert('Request Time out.');
	        } else {
	            alert('Unknow Error.\n'+x.responseText);
	        }
	      //console.log("ERROR: "+data);
	      //$("#alertaError").show();
	      $(".ai-preloader").removeClass("active");
	    }
	});
}


function openModalTicket(id){
	$("#idTicket").val(id);
	$("#urlTicket").val("http://integrattodev.cloudapp.net/echosline/server/Imprimir_Ticket.php?IdVenta="+id);
	$(".sidebar-mini").removeClass("sidebar-open")
    $(".sidebar-mini").addClass("sidebar-collapse")

    let div = document.getElementById('ticket');
    div.innerHTML = '<iframe style="width:100%;height:100%;" frameborder="0" src="server/Imprimir_Ticket.php?IdVenta='+ id + '" />';
    setTimeout(function(){
    	if($("#idTicket").val() !== ''){
    		$("#modal_ticket").modal('show');
    	}        
    },100)
}

function closeModalTicket(){
	$("#modal_ticket").modal('hide');
	$("#idTicket").val('');
	$("#urlTicket").val('');
	$("#emailTicket").val('');
}

function sendEmailTicket(){
	console.log($("#emailTicket").val())
    if($("#emailTicket").val() == '' || $("#emailTicket").val() == null){
        Swal.fire({
		  icon: 'error',
		  title: 'Oops...',
		  text: 'Por favor ingrese un correo electrónico.'
		})
        return false
    }
    var formData = new FormData();
    formData.append("opcion",11);
	formData.append("id",  $("#idTicket").val());
	formData.append("urlTicket", $("#urlTicket").val());
	formData.append("email", $("#emailTicket").val());
    
	$.ajax({
		url:"server/api.php",
		type: 'POST',
		processData: false,
		contentType: false,
		timeout: 800000,
		data: formData,
		beforeSend: function () {
			$('#loading_send_mail').css('display', '');
		},
		success: function (response) {
			console.log(response)
			if(response == 0){
				Swal.fire({
				  icon: 'error',
				  title: 'Oops...',
				  text: 'Ocurrio un error al enviar correo electrónico, por favor intente de nuevo.'
				})
		    }
		    else {
		    	Swal.fire({
        		  icon: 'success',
        		  title: 'Ticket enviado con éxito.',
        		})
		    }
		}
	})
	.done(function () {
		
	})
	.fail(function (jqXHR, textStatus, errorThrown) {
		
	})
	.always(function () {
		
	});
}