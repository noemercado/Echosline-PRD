var urlCupones = "server/cupones.php";
var cuponesList;
var cuponEnUso;
getCupones();
function getCupones(){
	$.ajax({
	  url:urlCupones,
	  type:"POST",
	  data:{"opcion":"getCupones"},
	  beforeSend:function(){
	    $("#preloader").show();
	  },
	  success:function(data){
	    console.log(data);
	    if(data.code == 200){
	    	cuponesList = data.cupones;
	    	if(data.enUso){
	    		cuponEnUso = data.enUso;
	    		console.log(cuponEnUso)
	    	}
	    	renderCupones();
	    }
	  },
	  complete:function(){
	    $("#preloader").fadeOut();

	  }
	});
}

function renderCupones(){
	var elem,cupon;
	$("#tablaCupones tbody").html("");
	for(var i in cuponesList){
		cupon = cuponesList[i];
		if(cuponEnUso == cupon.ID)
			elem = '<tr id="cupon_'+cupon.ID+'" class="enUso">';
		else
			elem = '<tr id="cupon_'+cupon.ID+'">';
		elem += '<td>'+cupon.Cupon+'</td>';
		if(cupon.Modo == "Monto")
			elem += '<td>$'+cupon.Monto+'</td>';
		else
			elem += '<td>%'+cupon.Monto+'</td>';
		if(cuponEnUso == cupon.ID){
			elem += '<td><button class="btnCupon aplicado" data-cuponid="'+cupon.ID+'">Aplicado</button></td>';
			elem += '<td><span data-cuponid="'+cupon.ID+'" class="deleteCupon"><i class="fa fa-trash" aria-hidden="true"></i></span></td>';
		}
		else
			elem += '<td><button class="btnCupon" data-cuponid="'+cupon.ID+'">Aplicar</button></td>';
		elem += '</tr>';
		$("#tablaCupones tbody").append(elem);
	}
	/*if(cuponEnUso){
		$(document).find("#cupon_"+cuponEnUso).hide();
	}*/
}

$(document).on("click",".btnCupon",function(){
	var idCupon = $(this).data("cuponid");
	aplicarCupon(idCupon);
})

function aplicarCupon(idCupon){
	$.ajax({
	  url:urlCupones,
	  type:"POST",
	  data:{"opcion":"aplicarCupon","cupon":idCupon},
	  beforeSend:function(){
	    $("#preloader").show();
	  },
	  success:function(data){
	    console.log(data);
	    if(data.code == 200)
	    	location.reload();
	    else{
	    	Swal.fire({
	    	  icon: 'error',
	    	  title: 'Oops...',
	    	  text: data.msg
	    	})
	    }
	  },
	  complete:function(){
	    $("#preloader").fadeOut();

	  }
	});
}

$(document).on("click",".deleteCupon",function(){
	var idCupon = $(this).data("cuponid");
	borrarCupon(idCupon);
})

function borrarCupon(idCupon){
	$.ajax({
	  url:urlCupones,
	  type:"POST",
	  data:{"opcion":"quitarCupon","cupon":idCupon},
	  beforeSend:function(){
	    $("#preloader").show();
	  },
	  success:function(data){
	    console.log(data);
	    if(data.code == 200)
	    	Swal.fire({
	    	  icon: 'success',
	    	  title: 'Éxito',
	    	  text: data.msg,
	    	  onClose: () => {
                  location.reload();
                }
	    	})
	    	
	    else{
	    	Swal.fire({
	    	  icon: 'error',
	    	  title: 'Oops...',
	    	  text: data.msg
	    	})
	    }
	  },
	  complete:function(){
	    $("#preloader").fadeOut();

	  }
	});
}

$("#formCupon").on("submit",function(){
	var cupon = $("#coupon").val();
	$.ajax({
	  url:urlCupones,
	  type:"POST",
	  data:{"opcion":"validarCuponGeneral","cupon":cupon},
	  beforeSend:function(){
	    $("#preloader").show();
	  },
	  success:function(data){
	    console.log(data);
	    if(data.code == 200)
	    	Swal.fire({
	    	  icon: 'success',
	    	  title: 'Éxito',
	    	  text: data.msg,
	    	  onClose: () => {
                  location.reload();
                }
	    	})
	    	
	    else{
	    	Swal.fire({
	    	  icon: 'error',
	    	  title: 'Oops...',
	    	  text: data.msg
	    	})
	    }
	  },
	  complete:function(){
	    $("#preloader").fadeOut();

	  }
	});
});