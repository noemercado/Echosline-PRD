$(document).ready(function(){
	var listaProductos;
	var indexQuickView;
	var totalProductos;
	getParams();
	//getCatalogo();
	$(document).on("click",".add_to_cart_btn",function(e){
		e.preventDefault();
		var idP = $(this).data("idp");
		var idInv = $(this).data("idinv");
		console.log(idP+" "+idInv);
		var cantidad = prompt("Cantidad:", 1);
		if(parseInt(cantidad)){
			add_to_cart(idP,idInv,cantidad);
		}		
		
	});

	$(document).on("click","#modal_add_to_cart",function(e){
		e.preventDefault();
		var idP = $(this).data("idp");
		var idInv = $(this).data("idinv");
		var qty = $("#quick-qty").val();
		add_to_cart(idP,idInv,qty);
	});
	
});

/*funceiones para obtener datos*/
function getParams(){
  var params = GET(window.location.href);
  if(params){
    if(params.linea){//buscó por linea
    	if(params.sublinea){
    		get_catalogo_linea_sublinea(params.linea.replace("#",""),params.sublinea.replace("#",""))
    	}else{
    		/*hacer llamada a filtro por linea*/
    		get_catalogo_linea(params.linea.replace("#",""));
    	}
      //renderMigas("funcion",params.d.replace("#",""),params.f.replace("#",""));
    }else if(params.p){//para detalle del producto
      getDetalleProducto(params.p.replace("#",""));
    }else if(params.search){
      buscarProductos(params.search.replace("#",""));
    }else if(params.tipoCabello){
    	get_catalogo_tipoCabello(params.tipoCabello.replace("#",""));
    	/*solo*/
    }
  }else if (window.location.href.indexOf("catalogo") > -1) {
    console.log("es catalogo");
    getCatalogo(); 
  }else{
    console.log("no hay parametros y no es catalogo");
    getCatalogo(); 
    //getProductosHome();
  }

}

function getCatalogo(){
	$.ajax({
	  url:urlApi,
	  data:{"opcion":1},
	  type:"POST",
	  beforeSend:function(){
	    $(".ai-preloader").addClass("active");
	  },
	  success:function(data){
	    console.log(data);
	    listaProductos = data.productos;
	    console.log(listaProductos)
	    renderCatalogo(data.productos);
	    render_divisiones(data.divisiones);
	    /*render_lineas(data.lineas);
	    render_tiposCabello(data.tiposCabello);*/
	    console.log("total: "+data.productos.length);
	    totalProductos = data.productos.length;
	    $(".product-pages").text("Mostrando "+data.productos.length+" de "+data.productos.length+" productos");
	  },
	  complete:function(){
	    $(".ai-preloader").removeClass("active");
	    //getParams();
	  }
	});
}

$(document).on("click",".quickview-btn",function(e){
	e.preventDefault();
	var idP = $(this).data("idp");
	var idInv = $(this).data("idinv");
	console.log(idP+" "+idInv);
	$("#quick-qty").val("1");
	get_detalle_producto(idP,idInv);
	indexQuickView = $(this).data("indexp");
	//productModal
	//add_to_cart(idP,idInv);
});

$(".dl-icon-left").on("click",function(){
	if(indexQuickView > 0){
		indexQuickView--;
		console.log(listaProductos[indexQuickView])
		get_detalle_producto(listaProductos[indexQuickView]["ID"],listaProductos[indexQuickView]["ID_inventario"]);
	}
	
})
$(".dl-icon-right").on("click",function(){
	if(indexQuickView < totalProductos){
		indexQuickView++;
		console.log(listaProductos[indexQuickView])
		get_detalle_producto(listaProductos[indexQuickView]["ID"],listaProductos[indexQuickView]["ID_inventario"]);
	}
})

function get_catalogo_linea(linea){
	$.ajax({
	  url:urlApi,
	  data:{"opcion":9,"linea":linea},
	  type:"POST",
	  beforeSend:function(){
	    $(".ai-preloader").addClass("active");
	  },
	  success:function(data){
	    console.log(data);
	    renderCatalogo(data.productos);
	    render_divisiones(data.divisiones,data.idDivision);
	    render_lineas(data.lineas,linea);
	    render_tiposCabello(data.tiposCabello);
	    console.log("total: "+data.productos.length);
	    $(".product-pages").text("Mostrando "+data.productos.length+" de "+data.productos.length+" productos");

	  },
	  complete:function(){
	    $(".ai-preloader").removeClass("active");
	    //getParams();
	  }
	});
}

function get_catalogo_tipoCabello(tipo){
	$.ajax({
	  url:urlApi,
	  data:{"opcion":30,"tipo":tipo},
	  type:"POST",
	  beforeSend:function(){
	    $(".ai-preloader").addClass("active");
	  },
	  success:function(data){
	    console.log(data);
	    renderCatalogo(data.productos);
	    render_lineas(data.lineas);
	    render_tiposCabello(data.tiposCabello);
	    console.log("total: "+data.productos.length);
	    $(".product-pages").text("Mostrando "+data.productos.length+" de "+data.productos.length+" productos");
	  },
	  complete:function(){
	    $(".ai-preloader").removeClass("active");
	    //getParams();
	  }
	});
}

function get_catalogo_linea_sublinea(linea,sublinea){
	$.ajax({
	  url:urlApi,
	  data:{"opcion":14,"linea":linea,"sublinea":sublinea},
	  type:"POST",
	  beforeSend:function(){
	    $(".ai-preloader").addClass("active");
	  },
	  success:function(data){
	    console.log(data);
	    renderCatalogo(data.productos);
	    render_divisiones(data.divisiones);
	    render_lineas(data.lineas);
	  },
	  complete:function(){
	    $(".ai-preloader").removeClass("active");
	    //getParams();
	  }
	});
}

function add_to_cart(idP,idInv,qty){
	//var qty = $("#qty").val();
	$.ajax({
	  url:urlApi,
	  data:{"opcion":3,"idP":idP,"idInv":idInv, "qty":qty},
	  type:"POST",
	  beforeSend:function(){
	    $(".ai-preloader").addClass("active");
	  },
	  success:function(data){
	    console.log(data);
	    $("#palomita").show();
	    setTimeout(function(){
	    	$("#palomita").fadeOut("slow");
	    },1000)
	    
	  },
	  complete:function(){
	    $(".ai-preloader").removeClass("active");
	    update_cart_ajax();
	    //getParams();
	  }
	});
}

function get_detalle_producto(idP,idInv){
	$.ajax({
	  url:urlApi,
	  data:{"opcion":10,"idP":idP,"idInv":idInv},
	  type:"POST",
	  beforeSend:function(){
	    $(".ai-preloader").addClass("active");
	  },
	  success:function(data){
	    console.log(data);
	    if(data.code == 200)
	    	render_modal(data.producto[0])
	    else
	    	Swal.fire({
	    	  icon: 'error',
	    	  title: 'Oops...',
	    	  text: data.msg
	    	})
	  },
	  complete:function(){
	    $(".ai-preloader").removeClass("active");
	    //getParams();
	  }
	});
}

function buscarProductos(termino){
	$.ajax({
	  url:urlApi,
	  data:{"opcion":25,"termino":termino},
	  type:"POST",
	  beforeSend:function(){
	    $(".ai-preloader").addClass("active");
	  },
	  success:function(data){
	    console.log(data);
	    renderCatalogo(data.productos);
	    render_lineas(data.lineas);
	    if(data.productos.length <= 0)
	    	$("#productos .row").append("<h1>Sin resultados para la búsqueda: <span>"+termino+"</span></h1>")
	  },
	  complete:function(){
	    $(".ai-preloader").removeClass("active");
	    //getParams();
	  }
	});
}

/*functiones para renderizar*/
function renderCatalogo(productos){
	var item = "";
	var producto;
	var desc;
	$("#productos .row").html("");
	for(var i in productos){
		producto = productos[i];
		item = "";
		item += '<div class="col-lg-4 col-sm-6 mb--40 mb-md--30 prodElem">';
		item += '<div class="airi-product">';
		item += '<div class="product-inner">';
		/*agregar el nombre de la linea a cada producto*/
		item += '<h3 class="product-title text-center">'+producto.Nombre_linea+'</h3>';
		item += '<figure class="product-image">';
		item += '<div class="product-image--holder">';
		item += '<a href="detalle-producto.php?p='+producto.ID+'&inv='+producto.ID_inventario+'">';
		item += '<img src="'+urlImagenes+producto.Url_imagen+'" alt="Product Image" class="primary-image">';
		//item += '<img src="assets/img/products/producto1.jpg" alt="Product Image" class="secondary-image">';
		item += '</a>';
		item += '</div>';
		item += '<div class="airi-product-action">';
		item += '<div class="product-action">';
		item += '<a class="quickview-btn action-btn" href="#" data-idp="'+producto.ID+'" data-idinv="'+producto.ID_inventario+'" data-indexp="'+i+'" data-toggle="tooltip" data-placement="top" title="Compra Rápida">';
		item += '<span data-toggle="modal">';
		item += '<i class="fa fa-shopping-cart"></i>';
		item += '</span>';
		item += '</a>';
		/*item += '<a class="add_to_cart_btn action-btn" href="#" data-idP="'+producto.ID+'" data-idInv="'+producto.ID_inventario+'" data-toggle="tooltip" data-placement="top" title="Añadir al carrito">';
		item += '<i class="dl-icon-cart29"></i>';
		item += '</a>';*/
		/*item += '<a class="add_wishlist action-btn" href="wishlist.html" data-toggle="tooltip" data-placement="top" title="Añadir a favoritos">';
		item += '<i class="dl-icon-heart4"></i>';
		item += '</a>';*/
		item += '</div>';
		item += '</div>';
		item += '</figure>';
		item += '<div class="product-info text-center">';
		item += '<h3 class="product-title">';
		item += '<a href="detalle-producto.php?p='+producto.ID+'&inv='+producto.ID_inventario+'">'+producto.Producto+'</a>';
		item += '</h3>';
		/*una breve descripción*/
		desc = producto.Descripcion.substring(0,150);
		item += '<p class="txt-desc-p">'+desc+'...</p>';
		if(producto.Minimo_compra > 1){
			item += '<p><span class="minimo">Mínimo de compra: '+producto.Minimo_compra+"</span></p>";
		}
		item += '<span class="product-price-wrapper">';
		item += '<span class="money">$'+producto.Precio_publico+'</span>';
		item += '</span>';
		item += '</div>';
		item += '<div class="product-overlay"></div>';
		item += '</div>';
		item += '</div>';
		item += '</div>';
		if(producto.Status == "Activo")
			$("#productos .row").append(item);

	}

	Promise.all(Array.from(document.images).filter(img => !img.complete).map(img => new Promise(resolve => { img.onload = img.onerror = resolve; }))).then(() => {
	    console.log('images finished loading');
	    setTimeout(function(){
	      sameSize("figure.product-image");
	      sameSize(".txt-desc-p");
	      sameSize(".product-title");
	    },100)
	});

	$(".product-pages").text("Mostrando "+productos.length+" de "+productos.length+" productos");
}

function sameSize(yourelemselector){
  var maxHeight = 0;

  $(yourelemselector).each(function(){
     var thisH = $(this).height();
     if (thisH > maxHeight) {
     	maxHeight = thisH;
     	console.log($(this));
     }
  });
  //alert(maxHeight)

  $(yourelemselector).height(maxHeight);
  console.log(maxHeight)
}

function render_divisiones(divisiones){
	var division = "";
	var item = "";
	$("#divisionesList").html("");
	$("#lineasList").html("");
	$("#subLineasList").html("");
	var idDivision = "";
	if(arguments[1])
		idDivision = arguments[1][0]["idDivision"];
	for(var i in divisiones){
		division = divisiones[i];
		if(idDivision == division.ID)
			item = '<li class="sideActivo"><a href="#" data-id="'+division.ID+'" class="division">'+division["Division"]+'</a><span class="count"></span></li>';
		else
			item = '<li><a href="#" data-id="'+division.ID+'" class="division">'+division["Division"]+'</a><span class="count"></span></li>';
		$("#divisionesList").append(item);
	}
	$("#sublineasDiv, #lineasDiv").hide();
}

var idDivision, idLinea, idSublinea;

$(document).on("click",".division",function(){
	$(".division").closest("li").removeClass("sideActivo");
	$(this).closest("li").addClass("sideActivo");
	idDivision = $(this).data("id");
	$("#sublineasDiv, #lineasDiv").hide();
	get_productos_by_division();
	//get_lineas_by_division(idD);
})

$(document).on("click",".linea",function(){
	$(".linea").closest("li").removeClass("sideActivo");
	$(this).closest("li").addClass("sideActivo");
	idLinea = $(this).data("id");
	get_productos_by_linea();
	//get_lineas_by_division(idD);
})

$(document).on("click",".subLinea",function(){
	$(".subLinea").closest("li").removeClass("sideActivo");
	$(this).closest("li").addClass("sideActivo");
	idSublinea = $(this).data("id");
	get_productos_by_subLinea();
	//get_lineas_by_division(idD);
})

function get_productos_by_division(){
	$.ajax({
	  url:urlApi,
	  data:{"opcion":33, "idD":idDivision},
	  type:"POST",
	  beforeSend:function(){
	    $(".ai-preloader").addClass("active");
	  },
	  success:function(data){
	    console.log(data);
	    if(data.code == 200){
	    	renderCatalogo(data.productos);
	    	render_lineas(data.lineas);
	    }
	  },
	  complete:function(){
	    $(".ai-preloader").removeClass("active");
	  }
	});
}

function get_productos_by_linea(){
	$.ajax({
	  url:urlApi,
	  data:{"opcion":34, "idD":idDivision, "idL":idLinea},
	  type:"POST",
	  beforeSend:function(){
	    $(".ai-preloader").addClass("active");
	  },
	  success:function(data){
	    console.log(data);
	    if(data.code == 200){
	    	renderCatalogo(data.productos);
	    	render_sub_lineas(data.subLineas);
	    }
	  },
	  complete:function(){
	    $(".ai-preloader").removeClass("active");
	  }
	});
}

function get_productos_by_subLinea(){
	$.ajax({
	  url:urlApi,
	  data:{"opcion":35, "idD":idDivision, "idL":idLinea, "idS":idSublinea},
	  type:"POST",
	  beforeSend:function(){
	    $(".ai-preloader").addClass("active");
	  },
	  success:function(data){
	    console.log(data);
	    if(data.code == 200){
	    	renderCatalogo(data.productos);
	    	//render_sub_lineas(data.subLineas);
	    }
	  },
	  complete:function(){
	    $(".ai-preloader").removeClass("active");
	  }
	});
}

function render_lineas(lineas){
	var linea = "";
	var item = "";
	var lineaID = "";
	$("#lineasList").html("");
	$("#subLineasList").html("");
	if(arguments[1]){//linea
		lineaID = arguments[1];
	}
	for(var i in lineas){
		linea = lineas[i];
		if(lineaID == linea.ID)
			item = '<li class="sideActivo"><a href="#" data-id="'+linea.ID+'" class="linea">'+linea["Linea"]+'</a><span class="count"></span></li>';
		else
			item = '<li><a href="#" data-id="'+linea.ID+'" class="linea">'+linea["Linea"]+'</a><span class="count"></span></li>';
		$("#lineasList").append(item);
	}
	$("#lineasDiv").show();
}

function render_sub_lineas(lineas){
	var linea = "";
	var item = "";
	$("#subLineasList").html("");
	for(var i in lineas){
		linea = lineas[i];
		item = '<li><a href="#" data-id="'+linea.ID+'" class="subLinea">'+linea["Sublinea"]+'</a><span class="count"></span></li>';
		$("#subLineasList").append(item);
	}
	$("#sublineasDiv").show();
}

function render_tiposCabello(tiposCabello){
	console.log(tiposCabello)
	var linea = "";
	var item = "";
	for(var i in tiposCabello){
		linea = tiposCabello[i];
		item = '<li><a href="catalogo.html?tipoCabello='+linea["ID"]+'">'+linea["Tipo_de_cabello"]+'</a><span class="count">(0)</span></li>';
		$("#tiposCabelloList").append(item);
	}
}

function render_modal(producto){
	$("#href_modal_detalle").attr("href","detalle-producto.php?p="+producto.ID+"&inv="+producto.ID_inventario);
	$("#modal_imagen").attr("src",urlImagenes+producto.Url_imagen);
	$("#modal_titulo").text(producto.Producto);
	$("#modal_precio").text(producto.Precio_publico);
	$("#modal_descripcion").text(producto.Descripcion);
	$("#modal_add_to_cart").data("idp",producto.ID);
	$("#modal_add_to_cart").data("idinv",producto.ID_inventario);
	$("#productModal").modal("show");
	$("#linea_producto").attr("href",'catalogo.html?linea='+producto.Linea);
	$("#linea_producto").text(producto.NombreLinea)
}


/*ordenar*/
$(".product-ordering__list li a").on("click",function(){
	console.log($(this).attr("id"));
	ordenar($(this).attr("id"))
})

function ordenar(metodo){
	var params = GET(window.location.href);
	var linea = "";
	var tipoCabello = "";
	if(params){
		if(params.linea){
			linea = params.linea.replace("#","");
		}
		if(params.tipoCabello){
			tipoCabello = params.tipoCabello.replace("#","");
		}
	}
	$.ajax({
	  url:urlApi,
	  data:{"opcion":29,"metodo":metodo, "linea":linea, "tipoCabello":tipoCabello},
	  type:"POST",
	  beforeSend:function(){
	    $(".ai-preloader").addClass("active");
	  },
	  success:function(data){
	    console.log(data);
	    renderCatalogo(data.productos);
	    //render_lineas(data.lineas);
	    if(data.productos.length <= 0)
	    	$("#productos .row").append("<h1>Sin resultados para la búsqueda: <span>"+termino+"</span></h1>")
	  },
	  complete:function(){
	    $(".ai-preloader").removeClass("active");
	    //getParams();
	  }
	});
}


