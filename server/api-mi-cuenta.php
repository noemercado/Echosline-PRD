<?php
date_default_timezone_set('America/Mexico_City');
require "conexion.php";
$loginExp = time() + 3600;
$secreto = 'echoslineNefas1809';

if(isset($_POST["opcion"])){
	switch ($_POST["opcion"]) {
		case 1:
			get_datos_usuario();
			break;
		case 2:
			actualizar_datos_usuario();
			break;
		case 3:
			salir();
			break;
		case 4:
			actualizar_domicilio();
			break;
		case 5:
			get_detalle_pedido();
			break;
		default:
			break;
	}
}else{
	$respuesta["code"] = 400;
	$respuesta["msg"] = "No se recibió opción";
	//responder($respuesta);
}


function responder($respuesta){
	/*if(isset($_COOKIE["tokLog"]))
		setcookie("tokLog",$_COOKIE["tokLog"],time() + 60 * 60,"/");
	if(isset($_COOKIE["tokUSID"]))
		setcookie("tokUSID",$_COOKIE["tokUSID"],time() + 60 * 60,"/");
	if(isset($_COOKIE["tokUS"]))
		setcookie("tokUS",$_COOKIE["tokUS"],time() + 60 * 60,"/");*/
	header('Content-type: application/json');
	echo json_encode($respuesta);
}

/*function get_datos_usuario(){
	$respuesta["usuario"] = info_usuario()
}
*/

function info_usuario($email){
	$usuario = array();
	global $loginExp;
	try {
		$conn = PDOConnection::getConnection();
		$sql = "SELECT * FROM Clientes_menudeo WHERE Email = :correo LIMIT 1";
		global $secreto;
		$stm = $conn->prepare($sql);
		$stm->bindParam(":correo",$email);
		$stm->execute();
		$num = $stm->rowCount();
		if($num > 0){
			$row = $stm->fetch(PDO::FETCH_ASSOC);
			extract($row);
			$plain = openssl_decrypt($row["Password"], "AES-128-ECB", $secreto);
			$row["Password"] = $plain;
			$usuario = $row;
			$tmp = array(
				"u"=>$row["Email"],
				"id"=>$row["ID"]
			);
			setcookie("ech_lg",json_encode($tmp),$loginExp,"/");
		}
	} catch (Exception $e) {
	}
	return $usuario;
}

function get_pedidos($idUsuario){
	$pedidos = array();
	global $loginExp;
	try {
		$conn = PDOConnection::getConnection();
		$sql = "SELECT Ventas.*, Ventas_menudeo.ID AS idVentaMenudeo FROM Ventas_menudeo
INNER JOIN Ventas ON Ventas_menudeo.idVenta = Ventas.ID
WHERE idCliente_menudeo = :id";
		global $secreto;
		$stm = $conn->prepare($sql);
		$stm->bindParam(":id",$idUsuario);
		$stm->execute();
		$num = $stm->rowCount();
		if($num > 0){
			$pedidos = $stm->fetchAll();
		}
	} catch (Exception $e) {
	}
	return $pedidos;
}

function borrar_pedidos_pendientes($idClienteMenudeo){
	try {
		$respuesta = array();
		$conn = PDOConnection::getConnection();
		$sql = "SELECT ID,idVenta  FROM Ventas_menudeo WHERE idCliente_menudeo = :idCliente AND Status = 'Pendiente'";
		$stm = $conn->prepare($sql);
		$idCliente_menudeo = $idClienteMenudeo;
		$stm->bindParam(":idCliente",$idCliente_menudeo);
		$stm->execute();
		$num = $stm->rowCount();
		if($num > 0){
			while ($row = $stm->fetch(PDO::FETCH_ASSOC)) {
			  extract($row);
			  $idVenta = $row["idVenta"];
			  $idVentaMenudeo = $row["ID"];
			  $sql = "DELETE FROM Detalle_venta_menudeo WHERE idVenta_menudeo = :idVentaMenudeo";
			  $stm1 = $conn->prepare($sql);
			  $stm1->bindParam(":idVentaMenudeo",$idVentaMenudeo);
			  $result = false;
			  $result = $stm1->execute();
			  if($result){
			  	$sql2 = "DELETE FROM Ventas_menudeo WHERE idVenta = :idVenta";
			  	$stm2 = $conn->prepare($sql2);
			  	$stm2->bindParam(":idVenta",$idVenta);
			  	$result2 = $stm2->execute();
			  	if($result2){
			  		$sql3 = "DELETE FROM Ventas WHERE ID = :idVenta";
			  		$stm3 = $conn->prepare($sql3);
			  		$stm3->bindParam(":idVenta",$idVenta);
			  		$stm3->execute();
			  	}
			  }	
			  
			  
			  $temp = array(
			  	"idVenta" => $idVenta,
			  	"idMenudeo" =>$idVentaMenudeo
			  );
			  array_push($respuesta, $temp);
			  /*$admin_item = array(
			      "idAdmin" => $idAdmin,
			      "nombre" => $name,
			      "password" => $password,
			      "email" => $email
			  );
			  array_push($admins, $admin_item);*/
			}
			/*$row = $stm->fetch(PDO::FETCH_ASSOC);
			extract($row);*/
			
		}
		//$respuesta["code"] = 200;
	} catch (Exception $e) {
		$respuesta["code"] = 500;
		$respuesta["msg"] = "Error en el servidor: \n".$e->getMessage();
	}
	return $respuesta;
	
}

function get_detalle_pedido(){
	$detalles = array();
	if(isset($_POST["id"])){
		$id = $_POST["id"];
		try {
			$conn = PDOConnection::getConnection();
			$sql = "SELECT DVM.*,CA.Producto, Inventario.ID as idInventario FROM Ventas_menudeo as VM
			INNER JOIN Detalle_venta_menudeo AS DVM ON VM.ID = DVM.idVenta_menudeo
			INNER JOIN Catalogo AS CA ON DVM.idCatalogo = CA.ID
			INNER JOIN Inventario ON CA.ID = Inventario.idCatalogo 
			WHERE VM.ID = ".$id." UNION " ."
			  SELECT DVM.*, O.Nombre AS Producto, NULL FROM Ventas_menudeo as VM 
			  INNER JOIN Detalle_venta_menudeo AS DVM ON VM.ID = DVM.idVenta_menudeo
			  INNER JOIN Ofertas AS O ON DVM.idOferta = O.ID WHERE VM.ID = ".$id." UNION " ."
			  SELECT DVM.*,PO.Promocion AS Producto, NUll FROM Ventas_menudeo as VM 
			  INNER JOIN Detalle_venta_menudeo AS DVM ON VM.ID = DVM.idVenta_menudeo
			  INNER JOIN Promociones AS PO ON DVM.idPromocion = PO.ID WHERE VM.ID = ".$id;
			$stm = $conn->prepare($sql);
			$stm->execute();
			$num = $stm->rowCount();
			if($num > 0){
				$detalles = $stm->fetchAll();
				$respuesta["code"] = 200;
				$respuesta["detalles"] = $detalles;
			}
		} catch (Exception $e) {
			$respuesta["code"] = 500;
			$respuesta["msg"] = $e->getMessage();
		}
	}else{
		$respuesta["code"] = 400;
		$respuesta["msg"] = "No se recibió parámetro";
	}
	responder($respuesta);
}

function get_datos_usuario(){
	global $loginExp;
	if(isset($_COOKIE["ech_lg"])){
		$tmp = json_decode( $_COOKIE["ech_lg"], true );
		$email = $tmp["u"];
		$id = $tmp["id"];
		$response["code"]=200;
		$response["usuario"] = info_usuario($email);
		//$response["eliminados"] = borrar_pedidos_pendientes($id);
		$response["pedidos"] = get_pedidos($id);

	}else{
		$response["code"]=400;
		$response["msg"]="no está logueado";
	}
	responder($response);
}

function actualizar_datos_usuario(){
	try {
		global $idCliente;
		global $secreto;
		global $loginExp;
		$descuento = 0;
		$conn = PDOConnection::getConnection();
		$sql = "UPDATE Clientes_menudeo SET Nombre = :nombre, Tel1 = :tel, Tel2 = '', Email = :email, Nacimiento = :nacimiento, Password = :pass
		WHERE ID = :id";
		$stm = $conn->prepare($sql);
		$stm->bindParam(":nombre",$_POST["nombre"]);
		$stm->bindParam(":tel",$_POST["telefono"]);
		$stm->bindParam(":email",$_POST["email"]);
		//$stm->bindParam(":idCliente",$idCliente);
		$nac = $_POST["anio"]."-".$_POST["mes"]."-".$_POST["dia"];
		$stm->bindParam(":nacimiento",$nac);
		$stm->bindParam(":id",$_POST["id"]);
		$pass = $_POST["cur_pass"];
		if($_POST["new_pass"] != "")
			$pass = $_POST["new_pass"];
		$passEnc = openssl_encrypt($pass, "AES-128-ECB", $secreto);
		$stm->bindParam(":pass",$passEnc);
		$respuesta["sql"] = $stm;
		$result = false;
		$result = $stm->execute();
		if($result){

			$idClienteMen = $conn->lastInsertId();
			$tmp = array(
				"u"=>$_POST["email"],
				"id"=>$_POST["id"]
			);
			setcookie("ech_lg",json_encode($tmp),$loginExp,"/");
			setcookie("ech_lg_tmp","",$loginExp * -1,"/");
		  	unset($_COOKIE["ech_lg_tmp"]);
			$respuesta["usuario"] = info_usuario($_POST["email"]);
			$respuesta["code"] = 200;

		}else{
			$respuesta["code"] = 400;			
			$respuesta["msg"] = "Error al guardar al cliente";
		}
	} catch (Exception $e) {
		$respuesta["code"] = 500;
		$respuesta["msg"] = "Error en el servidor: \n".$e->getMessage();
	}
	$conn = null;
	responder($respuesta);
}

function actualizar_domicilio(){
	try {
		global $idCliente;
		global $secreto;
		global $loginExp;
		$descuento = 0;
		$conn = PDOConnection::getConnection();
		$sql = "UPDATE Clientes_menudeo SET Calle_numero = :calle, Colonia = :colonia, Ciudad = :municipio, Municipio = :municipio, Estado = :estado, CP = :cp, Tel1 = :tel, Pais = 'Mexico'
		WHERE ID = :id";
		$stm = $conn->prepare($sql);
		$calle_num = $_POST["calle"]." #".$_POST["exterior"];
		$stm->bindParam(":calle",$calle_num);
		$stm->bindParam(":colonia",$_POST["colonia"]);
		$stm->bindParam(":municipio",$_POST["municipio"]);
		$stm->bindParam(":estado",$_POST["estado"]);
		$stm->bindParam(":cp",$_POST["cp"]);
		$stm->bindParam(":tel",$_POST["tel"]);
		$stm->bindParam(":id",$_POST["id"]);
		$respuesta["sql"] = $stm;
		$result = false;
		$result = $stm->execute();
		if($result){
			
			$respuesta["usuario"] = info_usuario($_POST["email"]);
			$respuesta["code"] = 200;

		}else{
			$respuesta["code"] = 400;			
			$respuesta["msg"] = "Error al guardar al cliente";
		}
	} catch (Exception $e) {
		$respuesta["code"] = 500;
		$respuesta["msg"] = "Error en el servidor: \n".$e->getMessage();
	}
	$conn = null;
	responder($respuesta);
}

function salir(){
	global $loginExp;
	global $cookieExp;
	setcookie("ech_lg","",$loginExp * -1,"/");
  	unset($_COOKIE["ech_lg"]);
  	setcookie("ech_cart","",$cookieExp * -1,"/");
  	unset($_COOKIE["ech_cart"]);
  	$respuesta["code"] = 200;
  	responder($respuesta);
}
?>