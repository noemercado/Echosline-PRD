<?php
date_default_timezone_set('America/Mexico_City');
//require "conexion_test.php";
require "conexion.php";
//require "conexion_2.php";
/*require "conexiones_dev/conexion_avynacosMX.php";
require "conexiones_dev/conexion_prosalonDev.php";*/
//$idSucursal = 5;
$idSucursal = 1;
$cookieExp = time()+3600 * 24 * 365;
$loginExp = time() + 3600;

$conexion_prosalon = PDOConnection::getConnection();
//$conexion_prosalon = PDOConnection2::getConnection();
//$conexion_test = PDOConnection3::getConnection();

if(isset($_POST["opcion"])){
	switch ($_POST["opcion"]) {
		case "getCupones":
			getCuponesByIdCliente();
			break;
		case "aplicarCupon":
			aplicarCupon();
			break;
		case "quitarCupon":
			quitarCupon();
			break;
		case "validarCuponGeneral":
			validarCuponGeneral();
			break;

	}
}else{
	$respuesta["code"] = 400;
	$respuesta["msg"] = "No se recibió opción";
	//responder($respuesta);
}

function responder($respuesta){
	global $conexion_avynacos;
	global $conexion_prosalon;
	$conexion_avynacos = null;
	$conexion_prosalon = null;
	if(isset($_COOKIE["ech_lg"]))
		setcookie("ech_lg",$_COOKIE["ech_lg"],time() + 60 * 60,"/");
	header('Content-type: application/json');
	echo json_encode($respuesta);
}

function getCuponesByIdCliente(){
	global $cookieExp;
	global $conexion_prosalon;
	if(isset($_COOKIE["ech_lg"])){
	  $tmp = json_decode( $_COOKIE["ech_lg"], true );
	   $email = $tmp["u"];
	  try {
	    //$conn = PDOConnection3::getConnection();
	    $sql = "SELECT * FROM cupones_full
	            WHERE
	            idCliente_menudeo = :idCliente
	            AND Fecha_venc >= NOW()
	            AND Fecha_inicio <= NOW()
	            AND STATUS = 'Activo'";
	    //global $secreto;
	    $stm = $conexion_prosalon->prepare($sql);
	    $stm->bindParam(":idCliente",$tmp["id"]);
	    $stm->execute();
	    $num = $stm->rowCount();
	    if($num > 0){
	      $cupones = $stm->fetchAll(PDO::FETCH_ASSOC);
	      $response["code"]=200;
	      $response["cupones"] = $cupones;
	      if(isset($_COOKIE["cupon"])){
	      	$cuponCookie = json_decode($_COOKIE["cupon"],true);
	      	$cupon = infoCupon($cuponCookie["id"]);
	      	$d = time();
	      	$fecha = date("Y-m-d", $d);
	      	if($cupon["Fecha_venc"] >= $fecha){
	      		$response["enUso"] = $cupon["ID"];
	      	}else{
	      		/*eliminar cookie de cupon inválido*/
	      		$response["eliminado"] = $_COOKIE["cupon"];
      			setcookie("cupon","",$cookieExp * -1,"/");
      		  	unset($_COOKIE["cupon"]);
	      	}
	      }
	    }else{
	    	$response["code"]=400;
	    	$response["msg"] = "No hay cupones disponibles";
	    }
	  } catch (Exception $e) {
	    $response["code"]=500;
	    $response["msg"]="Error en el servidor: ".$e->getMessage();
	  }
	}else{
	  $response["code"]=400;
	  $response["msg"]="No se enviaron datos";
	}
	responder($response);
}

function aplicarCupon(){
	$idCupon = $_POST["cupon"];
	$cupon = infoCupon($idCupon);
	global $cookieExp;
	if($cupon){
		$carrito = get_cart();
		$monto = $cupon["Monto"];
		$modo = $cupon["Modo"];
		$vencimiento = $cupon["Fecha_venc"];
		if($carrito["code"] == 400){
			$response["code"] = 400;
			$response["msg"] = "No hay productos en tu carrito";
		}else{
			$productos = $carrito["itemsCart"];
			$totalPromocion = 0;
			$contadorPromocioanles = 0;
			$algunoSinPromocion = false;
			/*foreach ($productos as $key => $value) {
				if($value["promocional"] == 1){
					$contadorPromocioanles++;
					$totalPromocion += $value["total"];
				}else{
					$algunoSinPromocion = true;
				}
			}*/
			/*revisar si el cupon es tipo Promocion, si sí, entonces aplicar las restricciones*/
			/*if($cupon["Tipo"] == "Promocion"){
				if($algunoSinPromocion){
					if($modo == "Porcentaje"){
						$monto = ($monto * $totalPromocion) / 100;
					}
					if($totalPromocion >= $monto){
						$response["code"] = 200;
						$cupon = array("id"=>$idCupon,"monto"=>$monto,"modo"=>$modo,"vencimiento"=>$vencimiento);
						setcookie("cupon",json_encode($cupon),$cookieExp,"/");
					}else{
						$response["code"] = 400;
						$dif = $monto - $totalPromocion;
						$response["msg"] = "Necesitas un mínimo de compra (en productos promocionales) de: $".$monto." para aplicar este cupón";
					}
					
				}else if($contadorPromocioanles > 0){
					$response["code"] = 200;
					$cupon = array("id"=>$idCupon,"monto"=>$monto,"modo"=>$modo,"vencimiento"=>$vencimiento);
					setcookie("cupon",json_encode($cupon),$cookieExp,"/");
				}else{
					$response["code"] = 400;
					$response["msg"] = "No hay productos promocionales en tu carrito";
				}
			//}else{*/
				$response["code"] = 200;
				$cupon = array("id"=>$idCupon,"monto"=>$monto,"modo"=>$modo,"vencimiento"=>$vencimiento, "gral" => false);
				setcookie("cupon",json_encode($cupon),$cookieExp,"/");
			//}
			
		}
	}else{
		$response["code"] = 400;
		$response["msg"] = "Cupón inválido";
		eliminarCuponesCookie();
	}
	
	responder($response);

}

function quitarCupon(){
	if(isset($_COOKIE["cupon"])){
		global $cookieExp;
		setcookie("cupon","",$cookieExp * -1,"/");
	  	unset($_COOKIE["cupon"]);
	  	$respuesta["code"] = 200;
	  	$respuesta["msg"] = "Se quitó el cupón del carrito!";
	}else{
		$respuesta["code"] = 400;
		$respuesta["msg"] = "No hay cupón aplicado";
	}
	responder($respuesta);
}

function eliminarCuponesCookie(){
	if(isset($_COOKIE["cupon"])){
		global $cookieExp;
		setcookie("cupon","",$cookieExp * -1,"/");
	  	unset($_COOKIE["cupon"]);
	}
}

function infoCupon($id){
	try {
		global $conexion_prosalon;
		//$conn =  PDOConnection2::getConnection();
		$sql = "SELECT * FROM cupones_full WHERE ID = :id AND Fecha_venc >= NOW()
	            AND Fecha_inicio <= NOW()
	            AND STATUS = 'Activo'";
		$stm = $conexion_prosalon->prepare($sql);
		$stm->bindParam(":id",$id);
	    $stm->execute();
	    $num = $stm->rowCount();
	    if($num > 0){
	    	$row = $stm->fetch(PDO::FETCH_ASSOC);
	    	return $row;
	    	//extract($row);
	    	/*if($Tipo == "Promocion")
	    		return $row;*/
	    }
	} catch (Exception $e) {
		return false;
	}

	return false;
}

function get_cart(){
	$respuesta = array();
	$productos = array();
	$subtotal = 0.0;
	$avyna_cart = isset($_COOKIE["ech_cart"]);
	global $conexion_prosalon;
	//$avyna_cart_oferta = isset($_COOKIE["avyna_cart_oferta"]);
	$avyna_cart_oferta = isset($_COOKIE["oferta"]);
	if($avyna_cart){
		$carrito = json_decode( $_COOKIE["ech_cart"], true );
		//$respuesta["carriro"] = $carrito;
		foreach ($carrito as $k => $v) {
			$resp = findProd($v["p"],$v["inv"],null);
			$respuesta[$k] = $resp;
			if($resp){
				$resp["qty"] = $v["qty"];
				//$resp["total"] = $v["qty"] * $resp["precio"];
				//$resp["promocion"] = $v["promocion"];
				//$subtotal += floatval($v["qty"] * $resp["precio"]);
				/*try {
					//$conn =  PDOConnection::getConnection();
					$sql = "SELECT id,url_imagen FROM productos WHERE id = :id";
					$stm = $conexion_avynacos->prepare($sql);
					$stm->bindParam(":id",$v["p"]);
					//$stm->bindParam("idInv",$idInv);
					$stm->execute();
					$num = $stm->rowCount();
					if($num > 0){
						$row = $stm->fetch(PDO::FETCH_ASSOC);
						extract($row);
						$resp["idProducto"] = $row["id"];
						$resp["img"] = $row["url_imagen"];
					}
				} catch (Exception $e) {
					
				}*/
				array_push($productos, $resp);
			}else{
				$respuesta["no_p"] = true;
			}
		}
		$respuesta["code"] = 200;
		$respuesta["itemsCart"] = $productos;
	}
	$productos = array();
	$respuesta["limpios"] = $productos;
	$resp = null;
	if($avyna_cart_oferta){
		$carrito = json_decode( $_COOKIE["oferta"], true );
		//foreach ($carrito as $k => $v) {
			//$resp = findProd_oferta($v["p"],null);
			//if($resp){
				$resp["qty"] = $carrito["qty"];
				$resp["nombre"] = $carrito["nombre"];
				$resp["total"] = 0;
				$resp["id"] = $carrito["p"]; 
				$subtotal += floatval(0);
				array_push($productos, $resp);
			//}
		//}
		$respuesta["code"] = 200;
		$respuesta["itemsCart_oferta"] = $productos;
	}
	if($avyna_cart /*|| $avyna_cart_oferta*/){
		$respuesta["code"] = 200;
		$respuesta["subtotal"] = /*number_format($subtotal,2,'.',',')*/(float)$subtotal;
	}
	else
		$respuesta["code"] = 400;
	 
	return $respuesta;
}

function findProd($idP,$idInv, $conn){
	$data = false;
	try {
		if($conn == null)
			$conn = PDOConnection::getConnection();
		$sql = "SELECT Catalogo.ID, Catalogo.Producto, Catalogo.Descripcion, Catalogo.Url_imagen, /*ROUND(*/Inventario.Precio_publico/* / 2.0,2)*/ AS Precio_publico, Inventario.Min, Inventario.ID as ID_inventario, Inventario.Minimo_compra
		FROM Catalogo
		INNER JOIN Inventario
		ON Inventario.idCatalogo = Catalogo.ID
		WHERE Catalogo.ID = :id
		AND Inventario.ID = :idInv";
		$stm = $conn->prepare($sql);
		$stm->bindParam("id",$idP);
		$stm->bindParam("idInv",$idInv);
		$stm->execute();
		$data = $stm->fetch(PDO::FETCH_ASSOC);
	} catch (Exception $e) {
		$data = false;
	}
	if($conn != null)
		$conn = null;
	return $data;

}

function conteoCuponGeneral($id){
	try {
		global $conexion_prosalon;
		//$conn =  PDOConnection2::getConnection();
		$sql = "SELECT COUNT(*) AS conteo FROM Ventas WHERE idCupon = :id";
		$stm = $conexion_prosalon->prepare($sql);
		$stm->bindParam(":id",$id);
	    $stm->execute();
	    $num = $stm->rowCount();
	    if($num > 0){
	    	$row = $stm->fetch(PDO::FETCH_ASSOC);
	    	//return $row;
	    	extract($row);
	    	return $conteo;
	    	/*if($Tipo == "Promocion")
	    		return $row;*/
	    }
	} catch (Exception $e) {
		return 0;
	}

	return 0;
} 
function infoCuponGeneral($id){
	try {
		global $conexion_prosalon;
		//$conn =  PDOConnection2::getConnection();
		$sql = "SELECT * FROM cupones_full WHERE Cupon = :id AND Fecha_venc >= :fecha
	            AND Fecha_inicio <= :fecha
	            AND STATUS = 'Activo'
	            AND idCliente_menudeo IS NULL";
		$stm = $conexion_prosalon->prepare($sql);
		$d = time();
		$fecha = date("Y-m-d", $d);
		$stm->bindParam(":id",$id);
		$stm->bindParam(":fecha",$fecha);
	    $stm->execute();
	    $num = $stm->rowCount();
	    if($num > 0){
	    	$row = $stm->fetch(PDO::FETCH_ASSOC);
	    	return $row;
	    	//extract($row);
	    	/*if($Tipo == "Promocion")
	    		return $row;*/
	    }
	} catch (Exception $e) {
		return false;
	}

	return false;
}

function validarCuponGeneral(){
	$cupon = $_POST["cupon"];
	/*condicion 1: verificar la fecha de vigencia del cupon*/
	$info = infoCuponGeneral($cupon); // contiene todo lo del cupon, la cantidad por ejemplo, para el siguiente paso
	if($info){
		/*condicion 2: contar las veces que se ha aplicado para saber si aún puede aplicarlo*/
		$conteo = conteoCuponGeneral($info["ID"]);
		$respuesta["conteo"] = $conteo;
		$respuesta["cantidadDisponible"] = $info["Cantidad"];
		if($conteo < $info["Cantidad"]){
			if($cupon){
				$carrito = get_cart();
				if($carrito["code"] == 400){
					$respuesta["code"] = 400;
					$respuesta["msg"] = "No hay productos en tu carrito";
				}else{
					$respuesta["code"] = 200;
					global $cookieExp;
					$cupon = array("id"=>$info["ID"],"monto"=>$info["Monto"],"modo"=>$info["Modo"],"vencimiento"=>$info["Fecha_venc"], "gral"=>true);
					setcookie("cupon",json_encode($cupon),$cookieExp,"/");
				}
			}else{
				$respuesta["code"] = 400;
				$respuesta["msg"] = "Cupón inválido";
				eliminarCuponesCookie();
			}
		}else{
			$respuesta["code"] = 400;
			$respuesta["msg"] = "No se puede aplicar el cupón.";
		}
	}else{
		$respuesta["code"] = 400;
		$respuesta["msg"] = "Cupón invalido.";
	}
	responder($respuesta);
}

?>