<div class="modal fade" id="modal_ticket" tabindex="-1" role="dialog" aria-labelledby="label_modal" aria-hidden="true" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
              <input type="hidden" id="idTicket">
              <input type="hidden" id="urlTicket">
              <h5 class="modal-title" id="label_modal">Ticket de venta</h5>
              <button type="button" class="close" onclick="closeModalTicket()" aria-label="Close">
                <h1><span aria-hidden="true">&times;</span></h1>
              </button>
            </div>
            <div class="modal-body">
                <div class="" id="ticket" style="height: 500px"></div>
            </div>
            <div class="modal-footer">
                <div class="form-row">
                    <div class="form__group col-6">
                        <input type="email" class="form__input form__input--2" placeholder="Correo electrónico"  id="emailTicket" ref="emailTicket">
                    </div>
                    <div class="form__group col-6">
                        <button type="button" class="btn btn-success" onclick="sendEmailTicket()"> Enviar correo electrónico </button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>