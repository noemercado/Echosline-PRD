<?php

if(!isset($_COOKIE["ech_lg"]) && !isset($_COOKIE["ech_lg_tmp"])){
    header("Location: login.html");
}
date_default_timezone_set('America/Mexico_City');
//require "server/conexion.php";
require "server/api.php";

require __DIR__ . '/vendor/autoload.php';

$res = guardar_venta();
if($res["code"] != 200){
    echo $res["msg"];
}else{
    $eleccion = '';
    $sql = "UPDATE Ventas SET Status = :status, Adeudo = 0.00, idPago = :idPago, idOrden = :idOrden WHERE ID = :idVenta";
    $cupon = false;
    if(isset($_COOKIE["cupon"])){
        $cupon = json_decode($_COOKIE["cupon"],true);
        if($cupon["gral"]){
            $sql = "UPDATE Ventas SET Status = :status, Adeudo = 0.00, idPago = :idPago, idOrden = :idOrden WHERE ID = :idVenta, idCupon = '".$cupon['id']."'"; 
        }
    }
    //$sql = "UPDATE Ventas SET Status = :status, Adeudo = 0.00, idPago = :idPago, idOrden = :idOrden WHERE ID = :idVenta";

    if (isset($_POST["tipo"])) {
        $id_pago = $_POST['payment_id'];
        $status_pago = $_POST['payment_status'];
        $id_orden = $_POST['merchant_order_id'];
        $status = 'Aprovado';
        $eleccion = 'PayPal';
    }
    else{
        //$id = $_POST["payment_id"];
        $id = $_GET["collection_id"];
        //$status = $_POST["payment_status"];
        $status = $_GET["collection_status"];

        if($status == 'success'){
            $status = 'Aprobado';
            if($cupon && $cupon["gral"])
                $sql = "UPDATE Ventas SET Status = :status, Adeudo = 0.00, idPago = :idPago, idOrden = :idOrden, idCupon = ".$cupon['id']." WHERE ID = :idVenta";
            else
                $sql = "UPDATE Ventas SET Status = :status, Adeudo = 0.00, idPago = :idPago, idOrden = :idOrden WHERE ID = :idVenta";
        }else
        if($status == 'failure'){
            $status = 'Fallido';

        }else
        if($status == 'pending'){
            $status = 'Adeudo';
            if($cupon && $cupon["gral"])
                $sql = "UPDATE Ventas SET Status = :status, idPago = :idPago, idOrden = :idOrden, idCupon = ".$cupon['id']." WHERE ID = :idVenta";
            else
                $sql = "UPDATE Ventas SET Status = :status, idPago = :idPago, idOrden = :idOrden WHERE ID = :idVenta";
        }else
        if($status == 'approved'){
            $status = 'Aprobado';
            if($cupon && $cupon["gral"])
                $sql = "UPDATE Ventas SET Status = :status, Adeudo = 0.00, idPago = :idPago, idOrden = :idOrden, idCupon = ".$cupon['id']." WHERE ID = :idVenta";
            else
                $sql = "UPDATE Ventas SET Status = :status, Adeudo = 0.00, idPago = :idPago, idOrden = :idOrden WHERE ID = :idVenta";
        }else
        if($status == 'in_process'){
            $status = 'Adeudo';
            if($cupon && $cupon["gral"])
                $sql = "UPDATE Ventas SET Status = :status, idPago = :idPago, idOrden = :idOrden, idCupon = ".$cupon['id']." WHERE ID = :idVenta";
            else
                $sql = "UPDATE Ventas SET Status = :status, idPago = :idPago, idOrden = :idOrden WHERE ID = :idVenta";
        }

        $id_pago = $_GET['collection_id'];
        $status_pago = $_GET['collection_status'];
        $id_orden = $_GET['merchant_order_id'];   
        $eleccion = 'MercadoPago';
        //echo $id_pago;
    }



    if(isset($res["idVenta"])){
        $idV = $res["idVenta"];
        try {
            $conn = PDOConnection::getConnection();
            $stm = $conn->prepare($sql);
            $stm->bindParam(":idVenta",$idV);
            $stm->bindParam(":status",$status);
            $stm->bindParam(":idPago",$id_pago);
            $stm->bindParam(":idOrden",$id_orden);
            if($stm->execute()){
                if(isset($_COOKIE["cupon"])){
                    $cupon = json_decode($_COOKIE["cupon"],true);
                    if(!$cupon["gral"]){
                        $sql = "UPDATE cupones_full SET Status = 'Asignado', idVenta = :idVenta WHERE ID = :idCupon";
                        $stm = $conn->prepare($sql);
                        $stm->bindParam(":idCupon",$cupon["id"]);
                        $stm->bindParam(":idVenta",$idV);
                        $stm->execute();
                    }
                    
                    global $cookieExp;
                    setcookie("cupon","",$cookieExp * -1,"/");
                    unset($_COOKIE["cupon"]);
                }
                limpiar_carrito_return();
            }
        } catch (Exception $e) {
            $conn = null;
            var_dump($e->getMessage());
        }
    }

    if(isset($res["idVentaMen"])){
        $idVM = $res["idVentaMen"];
    }else{
        $idV = "";
    }

    $resp = get_cart();
    $lista = array();
    $subt = 0;
    if(isset($resp["itemsCart"])){
        $cart = $resp["itemsCart"];
        foreach ($cart as $key => $value) {
            array_push($lista, $key);
            $subt += $value["qty"] * $value["Precio_publico"];
        }
    }

    if(isset($resp["itemsCart_oferta"])){
        $cart_oferta = $resp["itemsCart_oferta"];
        foreach ($cart_oferta as $key => $value) {
            array_push($lista, $key);
            $subt += $value["qty"] * $value["Precio_publico"];
        }
    }

    $datos = get_datos_usuario2();
    $usuario = $datos["usuario"];
    $descuentoCliente = $datos["usuario"]["Descuento_%"];

    MercadoPago\SDK::setAccessToken('APP_USR-4316132444735395-031822-161d41833e9a2aef5249acbfc39cc3d1-535572779');

    //MercadoPago\SDK::setAccessToken('TEST-4316132444735395-031822-e0f7eaa20611305e499b0eda039ee661-535572779');
    //$subt = $resp["subtotal"];
    if($subt >= 1000)
      $envio = 0;
    else
      $envio = 100;
    $descuentoProf = number_format(0,2,'.',',');

    if(isset($_COOKIE["cupon"])){
      $cupon = json_decode($_COOKIE["cupon"],true);
      if($cupon["modo"] == "Porcentaje"){
        $descuentoCupon = floatval(($subt-$descuentoCliente)*(floatval($cupon["monto"])/100));
      }else{
        $descuentoCupon = floatval($cupon["monto"]);
      }
    }else{
      $descuentoCupon = 0;
    }

    //echo $subt."\n";



    $total = floatval(($subt) - ($descuentoCliente + $descuentoCupon));
    //echo $total."\n";

    $subtotal = floatval($total / 1.16);
    //echo $subtotal."\n";
    if($subtotal < 0)
      $subtotal = 0;
    //echo $total;


    //echo $subtotal;
    $impuestos = floatval($total - $subtotal);
    if($impuestos < 0)
      $impuestos = 0;


    //echo $impuestos;
    if($total < 0)
      $total = 0;
    //echo $envio;
    $total += $envio;
    //echo $total."\n";
    $totalPaypal = $total;
    $total = number_format($total,2,'.',',');
    $subtotal = number_format($subtotal,2,'.',',');
    $impuestos = number_format($impuestos,2,'.',',');
    $descuentoCliente = number_format($descuentoCliente,2,'.',',');
    $descuentoCupon = number_format($descuentoCupon,2,'.',',');
    //echo $envio;
    //$envio = 0;



	

?>
<!doctype html>
<html class="no-js" lang="zxx">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="meta description">
    <!-- Favicons -->
    <link rel="shortcut icon" href="assets/img/favicon.jpg" type="image/x-icon">
    <link rel="apple-touch-icon" href="assets/img/icon.png">

    <!-- Title -->
    <title>ECHOSLINE</title>

    <!-- ************************* CSS Files ************************* -->

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="assets/css/bootstrap.min.css">

    <!-- Font Awesome CSS -->
    <link rel="stylesheet" href="assets/css/font-awesome.min.css">

    <!-- dl Icon CSS -->
    <link rel="stylesheet" href="assets/css/dl-icon.css">

    <!-- All Plugins CSS -->
    <link rel="stylesheet" href="assets/css/plugins.css">

    <!-- Revoulation Slider CSS  -->
    <link rel="stylesheet" href="assets/css/revoulation.css">

    <!-- style CSS -->
    <link rel="stylesheet" href="assets/css/style.css">
    <link rel="stylesheet" href="assets/css/main.css">
    

    <!-- modernizr JS
    ============================================ -->
    <script src="assets/js/vendor/modernizr-2.8.3.min.js"></script>
    <!--[if lt IE 9]>
    <script src="//oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="//oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    
    <!--FONT AWESOME-->
    <script src="https://kit.fontawesome.com/816afb7ec8.js"></script>
    <script src="server/controllers/jquery/dist/jquery.js"></script>
    <script src="server/controllers/header.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@9.3.16/dist/sweetalert2.all.min.js"></script>

    

    <style>
        .swal2-popup{
            font-size: 1.4rem;
        }
        #agregado{
            display: flex;
            justify-content: center;
            align-items: center;
            flex-flow: column;
        }
        #agregado img{
            max-width: 150px;
        }
    </style>
</head>

<body>


    <div class="ai-preloader active">
        <div class="ai-preloader-inner h-100 d-flex align-items-center justify-content-center">
            <div class="ai-child ai-bounce1"></div>
            <div class="ai-child ai-bounce2"></div>
            <div class="ai-child ai-bounce3"></div>
        </div>
    </div>
  
    <!-- Main Wrapper Start -->
    <div class="wrapper">
        <!-- Header Area Start -->
        <header class="header header-fullwidth header-style-4">
            <div class="header-inner fixed-header">
                <div class="container-fluid">
                    <div class="row align-items-center">
                        <div class="col-lg-2 col-md-3 col-4 order-1">
                            <div class="header-left d-flex">
                                <!-- Logo Start Here -->
                                <a href="index.html" class="logo-box">
                                    <figure class="logo--normal"> 
                                        <img src="assets/img/logo/logo.png" alt="Logo"/>   
                                    </figure>
                                    <figure class="logo--transparency">
                                        <img src="assets/img/logo/logo.png" alt="Logo"/>  
                                    </figure>
                                </a>
                                <!-- Logo End Here -->
                            </div>
                        </div>

                        <div class="col-lg-8 order-3 order-lg-2">
                            <!-- Main Navigation Start Here -->
                            <nav class="main-navigation">
                                <ul class="mainmenu mainmenu--centered">
                                    <li class="mainmenu__item">
                                        <a href="index.html" class="mainmenu__link">
                                            <span class="mm-text">INICIO</span>
                                        </a>
                                    </li>
                                    <li class="mainmenu__item menu-item-has-children megamenu-holder">
                                        <a href="linea.html" class="mainmenu__link">
                                            <span class="mm-text">LÍNEAS</span>
                                        </a>
                                        <ul class="megamenu four-column">
                                            <li>
                                                <a class="megamenu-title" href="#">
                                                    <span class="mm-text">LÍNEAS DE PRODUCTO</span>
                                                </a>
                                                <ul>
                                                    <li>
                                                        <a href="linea.html">
                                                            <span class="mm-text">Karbon 9</span>
                                                        </a>
                                                    </li>
                                                    <li>
                                                        <a href="linea-kipower.html">
                                                            <span class="mm-text">Ki-Power</span>
                                                        </a>
                                                    </li>
                                                    <li>
                                                        <a href="linea-keratin.html">
                                                            <span class="mm-text">Seliar Keratin</span>
                                                        </a>
                                                    </li>
                                                    <li>
                                                        <a href="linea-luxury.html">
                                                            <span class="mm-text">Seliar Luxury</span>
                                                        </a>
                                                    </li>
                                                </ul>
                                            </li>
                                            <li>
                                                <a class="megamenu-title" href="#">
                                                    <span class="mm-text">INGREDIENTES</span>
                                                </a>
                                                <ul>
                                                    <li>
                                                        <a href="linea.html">
                                                            <span class="mm-text">Argan</span>
                                                        </a>
                                                    </li>
                                                    <li>
                                                        <a href="linea.html">
                                                            <span class="mm-text">Ácido Hialurónico</span>
                                                        </a>
                                                    </li>
                                                    <li>
                                                        <a href="linea.html">
                                                            <span class="mm-text">Aceites Botánicos</span>
                                                        </a>
                                                    </li>
                                                    <li>
                                                        <a href="linea.html">
                                                            <span class="mm-text">Carbón Activado</span>
                                                        </a>
                                                    </li>
                                                    <li>
                                                        <a href="linea.html">
                                                            <span class="mm-text">Keratina</span>
                                                        </a>
                                                    </li>
                                                </ul>
                                            </li>
                                            <li>
                                                <a class="megamenu-title" href="#">
                                                    <span class="mm-text">TIPO DE PRODUCTO</span>
                                                </a>
                                                <ul>
                                                    <li>
                                                        <a href="linea.html">
                                                            <span class="mm-text">Fluid</span>
                                                        </a>
                                                    </li>
                                                    <li>
                                                        <a href="linea.html">
                                                            <span class="mm-text">Kits</span>
                                                        </a>
                                                    </li>
                                                    <li>
                                                        <a href="linea.html">
                                                            <span class="mm-text">Lotion</span>
                                                        </a>
                                                    </li>
                                                    <li>
                                                        <a href="linea.html">
                                                            <span class="mm-text">Mask</span>
                                                        </a>
                                                    </li>
                                                    <li>
                                                        <a href="linea.html">
                                                            <span class="mm-text">Oil</span>
                                                        </a>
                                                    </li>
                                                    <li>
                                                        <a href="linea.html">
                                                            <span class="mm-text">Serum</span>
                                                        </a>
                                                    </li>
                                                    <li>
                                                        <a href="linea.html">
                                                            <span class="mm-text">Shampoo</span>
                                                        </a>
                                                    </li>
                                                    <li>
                                                        <a href="linea.html">
                                                            <span class="mm-text">Treatment</span>
                                                        </a>
                                                    </li>
                                                </ul>
                                            </li>
                                            <li class="d-none d-lg-block banner-holder">
                                                <div class="megamenu-banner">
                                                    <div class="megamenu-banner-image"></div>
                                                    <div class="megamenu-banner-info" id="contenedorBannerM">
                                                           <h3>Suscríbete<br> y recibe un<br> regalo en tus<br> pedidos<br> mensuales</h3>
                                                        <a class="banner-btn-2" href="shop-sidebar.html">
                                                            <span class="normal-view">SABER MÁS</span>
                                                        </a>
                                                    </div>
                                                    <a href="shop-sidebar.html" class="megamenu-banner-link"></a>
                                                </div>
                                            </li>   
                                        </ul>
                                    </li>
                                    <li class="mainmenu__item">
                                        <a href="catalogo.html" class="mainmenu__link">
                                            <span class="mm-text">TIENDA</span>
                                        </a>
                                    </li>
                                    <li class="mainmenu__item">
                                        <a href="nosotros.html" class="mainmenu__link">
                                            <span class="mm-text">ECHOSLINE</span>
                                        </a>
                                    </li>
                                    <li class="mainmenu__item">
                                        <a href="blog.html" class="menuActivo mainmenu__link">
                                            <span class="mm-text">BLOG</span>
                                        </a>
                                    </li>
                                    <li class="mainmenu__item">
                                        <a href="contacto.html" class="mainmenu__link">
                                            <span class="mm-text">CONTACTO</span>
                                        </a>
                                    </li>
                                </ul>
                            </nav>
                            <!-- Main Navigation End Here -->
                        </div>

                        <div class="col-lg-2 col-sm-4 col-md-9 col-8 order-2 order-lg-3 p-0">
                            <ul class="header-toolbar text-right">
                                <li class="header-toolbar__item">
                                    <div class="col-lg-12 col-md-12 col-12 order-12 p-0">
                                        <ul class="contenedorIdioma">
                                            <li class="es langAct"><a>ES</a></li>
                                            <li class="en"><a>EN</a></li>
                                        </ul>
                                    </div>
                                </li>
                                <li class="header-toolbar__item user-info-menu-btn">
                                    <a href="#">
                                        <i class="fas fa-user"></i>
                                    </a>
                                    <ul class="user-info-menu">
                                        <li>
                                            <a href="my-account.html">Mi cuenta</a>
                                        </li>
                                        <li>
                                            <a href="cart.html">Carrito de compras</a>
                                        </li>
                                        <li>
                                            <a href="checkout.html">Check Out</a>
                                        </li>
                                        <li>
                                            <a href="wishlist.html">Lista de deseos</a>
                                        </li>
                                        <li>
                                            <a href="order-tracking.html">Número de orden</a>
                                        </li>
                                        <li>
                                            <a href="#" id="cerrarSesion" style="display: none;">Cerrar sesión</a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="header-toolbar__item">
                                    <a href="#miniCart" class="mini-cart-btn toolbar-btn">
                                        <i class="fa fa-shopping-cart" aria-hidden="true"></i>
                                        <sup class="mini-cart-count">2</sup>
                                    </a>
                                </li>
                                <li class="header-toolbar__item">
                                    <a href="#searchForm" class="search-btn toolbar-btn">
                                        <i class="dl-icon-search1"></i>
                                    </a>
                                </li>
                                <li class="header-toolbar__item d-lg-none">
                                    <a href="#" class="menu-btn"></a>                 
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </header>
        <!-- Header Area End -->

        <!-- Mobile Header area Start -->
        <header class="header-mobile">
            <div class="container-fluid">
                <div class="row align-items-center">
                    <div class="col-4">
                        <a href="index.html" class="logo-box">
                            <figure class="logo--normal">
                                <img src="assets/img/logo/logo.png" alt="Logo">
                            </figure>
                        </a>
                    </div>
                    <div class="col-8">
                        <ul class="header-toolbar text-right">
                            <li class="header-toolbar__item user-info-menu-btn">
                                <a href="#">
                                    <i class="fas fa-user"></i>
                                </a>
                                <ul class="user-info-menu">
                                    <li>
                                        <a href="my-account.html">Mi cuenta</a>
                                    </li>
                                    <li>
                                        <a href="cart.html">Carrito de compras</a>
                                    </li>
                                    <li>
                                        <a href="checkout.html">Check Out</a>
                                    </li>
                                    <li>
                                        <a href="wishlist.html">Lista de deseos</a>
                                    </li>
                                    <li>
                                        <a href="order-tracking.html">Número de orden</a>
                                    </li>
                                    <li>
                                        <a href="#" id="cerrarSesionMovil" style="display: none;">Cerrar sesión</a>
                                    </li>
                                </ul>
                            </li>
                            <li class="header-toolbar__item">
                                <a href="#miniCart" class="mini-cart-btn toolbar-btn">
                                    <i class="fa fa-shopping-cart" aria-hidden="true"></i>
                                    <sup class="mini-cart-count">2</sup>
                                </a>
                            </li>
                            <li class="header-toolbar__item">
                                <a href="#searchForm" class="search-btn toolbar-btn">
                                    <i class="dl-icon-search1"></i>
                                </a>
                            </li>
                            <li class="header-toolbar__item d-lg-none">
                                <a href="#" class="menu-btn"></a>                 
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="row">
                    <div class="col-12">
                        <!-- Mobile Navigation Start Here -->
                        <div class="mobile-navigation dl-menuwrapper" id="dl-menu">
                            <button class="dl-trigger">Abrir Menú</button>
                            <ul class="dl-menu">
                                <li>
                                    <a href="index.html">
                                        INICIO
                                    </a>
                                </li>
                                <li>
                                    <a href="linea.html">
                                        LÍNEAS
                                    </a>
                                    <ul class="dl-submenu">
                                        <li>
                                            <a class="megamenu-title" href="#">
                                                LÍNEAS DE PRODUCTO
                                            </a>
                                            <ul class="dl-submenu">
                                                <li>
                                                    <a href="linea.html">
                                                        Karbon 9
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="linea-kipower.html">
                                                        KI-Power
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="linea-keratin.html">
                                                        Seliar Keratin
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="linea-luxury.html">
                                                        Seliar Luxury
                                                    </a>
                                                </li>
                                            </ul>
                                        </li>
                                        <li>
                                            <a class="megamenu-title" href="#">
                                                Ingredientes
                                            </a>
                                            <ul class="dl-submenu">
                                                <li>
                                                    <a href="linea.html">
                                                        Argán
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="linea.html">
                                                        Ácido Hialurónico
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="linea.html">
                                                        Aceites Botánicos
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="linea.html">
                                                        Carbón Activado
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="linea.html">
                                                        Keratina
                                                    </a>
                                                </li>
                                            </ul>
                                        </li>
                                        <li>
                                            <a class="megamenu-title" href="#">
                                                TIPO DE PRODUCTO
                                            </a>
                                            <ul class="dl-submenu">
                                                <li>
                                                    <a href="linea.html">
                                                        Fluid
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="linea.html">
                                                        Kits
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="linea.html">
                                                        Lotion
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="linea.html">
                                                        Mask
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="linea.html">
                                                        Oil
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="linea.html">
                                                        Serum
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="linea.html">
                                                        Shampoo
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="linea.html">
                                                        Treatment
                                                    </a>
                                                </li>
                                            </ul>
                                        </li>
                                    </ul>
                                </li>
                                <li>
                                    <a href="catalogo.html">
                                        TIENDA
                                    </a>
                                </li>
                                <li>
                                    <a href="nosotros.html">
                                        ECHOSLINE
                                    </a>
                                </li>
                                <li>
                                    <a href="blog.html">
                                        BLOG
                                    </a>
                                </li>
                                <li>
                                    <a href="contacto.html">
                                        CONTACTO
                                    </a>
                                </li>
                            </ul>
                        </div>
                        <!-- Mobile Navigation End Here -->
                    </div>
                </div>
            </div>
        </header>
        <!-- Mobile Header area End -->

        <!-- Main Content Wrapper Start -->
        <div id="content" class="main-content-wrapper">
            <div class="page-content-inner">
                <div class="container">
                    <div class="row pt--80 pt-md--60 pt-sm--40">
                        <div class="col-12">
                        	<h1 class="heading-tertiary heading-color mb--15 averta-bold">Pedido generado con éxito</h1>
                        </div>
                    </div> 
                    <div class="row pb--80 pb-md--60 pb-sm--40">
                        <!-- Checkout Area Start -->  
                        <div class="col-lg-6">
                            <div class="col-lg-12 mb-md--30 pl-0">
                                <h3 class="heading-color mb--15 averta-bold">Datos de envío</h3>
                            </div>
                            <div class="checkout-form">
                                <form action="javascript:void(0);" class="form form--checkout" id="formDatos">
                                    <div class="form-row mb--30">
                                        <div class="form__group col-md-12 mb-sm--30">
                                            <label for="billing_fname" class="form__label form__label--2">Nombre completo <span class="required">*</span></label>
                                            <input type="text" name="billing_fname" id="billing_fname" class="form__input form__input--2" required disabled="true">
                                        </div>
                                    </div>
                                    <div class="form-row mb--30">
                                        <div class="form__group col-12">
                                            <label for="billing_company" class="form__label form__label--2">Calle <span class="required">*</span></label>
                                            <input type="text" name="billing_calle" id="billing_calle" class="form__input form__input--2" required disabled="true">
                                        </div>
                                    </div>
                                    <div class="form-row mb--30">
                                        <div class="form__group col-6">
                                            <label for="billing_company" class="form__label form__label--2">Número exterior <span class="required">*</span></label>
                                            <input type="number" name="billing_exterior" id="billing_exterior" class="form__input form__input--2" required disabled="true">
                                        </div>
                                        <div class="form__group col-6">
                                            <label for="billing_company" class="form__label form__label--2">Número interior</label>
                                            <input type="number" name="billing_interior" id="billing_interior" class="form__input form__input--2" disabled="true">
                                        </div>
                                    </div>
                                    <div class="form-row mb--30">
                                        <div class="form__group col-6">
                                            <label for="billing_company" class="form__label form__label--2">Código Postal <span class="required">*</span></label>
                                            <input type="number" name="billing_cp" id="billing_cp" class="form__input form__input--2" placeholder="" required disabled="true">
                                        </div>
                                        <div class="form__group col-6">
                                            <label for="billing_estado" class="form__label form__label--2">Estado <span class="required">*</span></label>
                                            <input type="text" name="billing_estado" id="billing_estado" class="form__input form__input--2" placeholder="" required disabled="true">
                                            <!--<select id="billing_estado" name="billing_estado" class="form__input form__input--2 nice-select" required>
                                                <option value="">Selecciona un estado…</option>
                                                <option value="AF">Aguascalientes</option>
                                                <option value="AL">Colima</option>
                                                <option value="DZ">Durango</option>
                                                <option value="BD" selected="selected">Jalisco</option>
                                                <option value="BD">Edo. de México</option>
                                                <option value="NOE">PRUEBA</option>
                                            </select>-->
                                        </div>
                                    </div>
                                    <div class="form-row mb--30">
                                        <div class="form__group col-6">
                                            <label for="billing_municipio" class="form__label form__label--2">Municipio <span class="required">*</span></label>
                                            <input type="text" name="billing_municipio" id="billing_municipio" class="form__input form__input--2" placeholder="" required disabled="true">
                                            <!--<select id="billing_municipio" name="billing_municipio" class="form__input form__input--2 nice-select" required>
                                                <option value="">Selecciona un municipio…</option>
                                                <option value="AF">Tlaquepaque</option>
                                                <option value="AL">Zapopan</option>
                                                <option value="DZ">Tonalá</option>
                                                <option value="BD" selected="selected">Tlajomulco</option>
                                                <option value="BD">Guadalajara</option>
                                            </select>-->
                                        </div>
                                        <div class="form__group col-6">
                                            <label for="billing_colonia" class="form__label form__label--2">Colonia <span class="required">*</span></label>
                                            <input type="text" name="billing_colonia" id="billing_colonia" class="form__input form__input--2" placeholder="" required disabled="true">
                                        </div>
                                    </div>
                                    <div class="form-row">
                                        <div class="form__group col-12">
                                            <label for="billing_streetAddress" class="form__label form__label--2">Ingresa tu teléfono <span class="required">*</span></label>

                                            <input type="text" name="tel" class="form__input form__input--2 mb--30" placeholder="Teléfono celular" id="telefono" required disabled="true">

                                        </div>
                                    </div>
                                    <div class="form-row ">
                                        <div class="form__group col-12">
                                            <h4 class="nav-links__title averta-bold mt--20 mb--20">Información personal</h4>
                                        </div>
                                    </div>
                                    
                                    <div class="form-row mb--30">
                                        <div class="form__group col-12">
                                            <label for="billing_email" class="form__label form__label--2">Ingresa tu correo <span class="required">*</span></label>
                                            <input type="email" name="billing_email" id="billing_email" class="form__input form__input--2" required disabled="true">
                                        </div>
                                    </div>
                                    <div class="form-row mb--30">
                                        <div class="form__group col-12">
                                            <label for="billing_email" class="form__label form__label--2">Fecha de nacimiento <span class="required">*</span></label>
                                        </div>
                                        <div class="form__group col-4">
                                            <select id="billing_dia" name="billing_dia" class="form__input form__input--2" required disabled="true">
                                                <option value="" selected>Día</option>
                                            </select>
                                        </div>
                                        
                                        <div class="form__group col-4">
                                            <select id="billing_mes" name="billing_mes" class="form__input form__input--2" required disabled="true">
                                                <option value="" selected>Mes</option>
                                            </select>
                                        </div>
                                        
                                        <div class="form__group col-4">
                                            <select id="billing_year" name="billing_year" class="form__input form__input--2" required disabled="true">
                                                <option value="" selected>Año</option>
                                            </select>
                                        </div>
                                    </div>
                                    <!--<div class="form-row mb--30">
                                        <div class="form__group col-12">
                                            <div class="custom-checkbox ">
                                                
                                                <label class="contenedor">
                                                    <input type="checkbox" name="requiere_factura" id="requiere_factura"  class="form__checkbox">
                                                    <span class="checkmark"></span>
                                                </label>
                                                
                                                
                                                <label for="" class="pt--5 pl-0 averta-bold form__label form__label--2 shipping-label">Requiero facturación electrónica</label>
                                            </div>
                                        </div>
                                        
                                    </div>-->
                                </form>
                            </div>
                        </div>
                        <div class="col-xl-5 offset-xl-1 col-lg-6 mt-md--40">
                            <div class="order-details">
                                <div class="checkout-title mt--10">
                                    <h2>Resumen de tu pedido</h2>
                                </div>
                                <div class="table-content table-responsive mb--30">
                                    <table class="tabla-pedido table order-table order-table-2">
                                        <thead>
                                            <tr>
                                                <?php
                                                $countProd = 0;
                                                //$envio = number_format(0,2,'.',',');
                                                if($resp["code"] == 200){
                                                    if(isset($resp["itemsCart"])){
                                                        foreach ($cart as $key => $value) {
                                                            $countProd += $value["qty"];
                                                        }
                                                    }
                                                    if(isset($resp["itemsCart_oferta"])){
                                                        foreach ($cart_oferta as $key => $value) {
                                                            $countProd += $value["qty"];
                                                        }
                                                    }
                                                    
                                                }
                                                ?>
                                                <th class="averta-bold p-0"><?php echo $countProd;?> Productos</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php
                                                if($resp["code"] == 200){
                                                    if(isset($resp["itemsCart_oferta"])){
                                                        foreach ($cart_oferta as $key => $value) {
                                                        //$countProd += $value["qty"];
                                            ?>
                                            <tr>
                                                <th><?php echo $value["Promocion"];?></th>
                                                <!--<th>$<?php //echo $value["Precio_publico"];?></th>-->
                                                <th><strong style="color:#000">x</strong> <?php echo $value["qty"];?></th>
                                            </tr>
                                            <?php
                                                        }
                                                    }
                                                }
                                            ?>
                                            <?php
                                                if($resp["code"] == 200){
                                                    if(isset($resp["itemsCart"])){
                                                        foreach ($cart as $key => $value) {
                                                        //$countProd += $value["qty"];
                                            ?>
                                            <tr>
                                                <th><?php echo $value["Producto"];?></th>
                                                <!--<th>$<?php //echo $value["Precio_publico"];?></th>-->
                                                <th><strong style="color:#000">x</strong> <?php echo $value["qty"];?></th>
                                            </tr>
                                            <?php
                                                        }
                                                    }
                                                }
                                            ?>
                                        </tbody>
                                        <tfoot class="tfootResumen">
                                            <?php
                                            if(isset($cupon)){
                                              ?>
                                              <tr class="shipping">
                                                  <th class="averta-bold pb--20">Cupón</th>
                                                  <td class="text-right p-0 pb--20">
                                                      <span class="order-total-ammount">   -$<?php 
                                                        if($resp["code"] == 200 && count($lista) > 0){
                                                          echo $descuentoCupon;
                                                        }else{
                                                            echo "0.00";
                                                        }
                                                      ?>           
                                                      </span>
                                                  </td>
                                              </tr>
                                              <?php
                                            }
                                            ?>
                                            <tr class="cart-subtotal">
                                                <th class="averta-bold">Subtotal</th>
                                                <td class="text-right p-0">
                                                    <span class="order-total-ammount">$<?php
                                                        if($resp["code"] == 200 && count($lista) > 0){
                                                            echo $subtotal;
                                                        }
                                                        else
                                                            echo "0.00";
                                                    ?>
                                                        
                                                    </span>
                                                </td>
                                            </tr>
                                            <tr class="shipping">
                                                <th class="averta-bold pb--20">Envío</th>
                                                <td class="text-right p-0 pb--20">
                                                    <span class="order-total-ammount">$<?php 
                                                        if($resp["code"] == 200 && count($lista) > 0){
                                                            
                                                              echo $envio;
                                                            
                                                            
                                                        }else
                                                          echo "0.00";
                                                    ?>
                                                        
                                                    </span>
                                                </td>
                                            </tr>
                                            <tr class="shipping">
                                                <th class="averta-bold pb--20">Descuento</th>
                                                <td class="text-right p-0 pb--20">
                                                    <span class="order-total-ammount">   $<?php
                                                    if($resp["code"] == 200 && count($lista) > 0){
                                                      echo $descuentoCliente;
                                                    }else{
                                                        echo "0.00";
                                                    }
                                                    ?>             
                                                    </span>
                                                </td>
                                            </tr>
                                            <tr class="cart-subtotal">
                                                <th class="averta-bold">Impuestos</th>
                                                <td class="text-right p-0">
                                                    <span class="order-total-ammount">$<?php
                                                        if($resp["code"] == 200 && count($lista) > 0){
                                                          echo $impuestos;
                                                        }else{
                                                            echo "0.00";
                                                        }
                                                    ?>
                                                        
                                                    </span>
                                                </td>
                                            </tr>
                                            <tr class="order-total b-3">
                                                <th class="averta-bold pt--20 pb--20">Total</th>
                                                <td class="text-right pt--20 pb--20 pl-0 pr-0">
                                                    <span class="order-total-ammount">$<?php 
                                                    if($resp["code"] == 200){
                                                        //$total = floatval((($resp["subtotal"] + $envio)-$descuentoProf)-$desc); 
                                                        //echo number_format($total,2,'.',',');
                                                        echo $total;
                                                    }else{
                                                        echo "0.00";
                                                    }
                                                    ?>
                                                    </span>
                                                </td>
                                            </tr>
                                        </tfoot>
                                    </table>
                                </div>
                                <button class="btn btn-fullwidth btn-style-1" id=""><a href="catalogo.html">SEGUIR COMPRANDO</a></button>
                            </div>
                        </div>
                        <!-- Checkout Area End -->
                    </div>
                </div>
            </div>
        </div>
        <!-- Main Content Wrapper Start -->


        <!-- Footer Start -->
        <footer class="footer footer-3 bg--gris border-top">
            <div class="container">
                <div class="row pt--40 pt-md--30 mb--40 mb-sm--30">
                    <div class="col-12 text-md-center">
                        <div class="footer-widget">
                            <div class="textwidget">
                                <a href="index.html" class="footer-logo">
                                    <img src="assets/img/logo/logo-footer.png" alt="Logo">
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row mb--15 mb-sm--20">
                    <div class="col-xl-3 col-md-3 mb-lg--30">
                        <div class="footer-widget">
                            <h3 class="widget-title widget-title--2">Atención al cliente</h3>
                            <ul class="widget-menu widget-menu--2">
                                <li>Contáctanos</li>
                                <li><a href="tel:+523333333333">333-333-3333</a></li>
                                <li><a href="faq.html">Preguntas frecuentes</a></li>
                                <li><a href="politicas.html">Políticas de Devoluciones</a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-xl-3 col-md-3 mb-lg--30">
                        <div class="footer-widget">
                            <h3 class="averta-bold widget-title widget-title--2" id="tituloCuenta">Tu cuenta</h3>
                            <ul class="widget-menu widget-menu--2">
                                <li><a href="mi-cuenta.html" id="footer_registro">Registrarme</a></li>
                                <li><a href="clientes.html" id="footer_clientes">Clientes</a></li>
                                <li><a href="pedidos.html" id="footer_pedidos">Mis pedidos</a></li>
                                <li><a href="favoritos.html" id="footer_favoritos">Mis favoritos</a></li>
                                <li><a href="salones.html" id="footer_salones">Salones y mayoristas</a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-xl-3 col-md-3 mb-lg--30">
                        <div class="footer-widget">
                            <h3 class="widget-title widget-title--2">Mapa de sitio</h3>
                            <ul class="widget-menu widget-menu--2">
                                <li><a href="index.html">Inicio</a></li>
                                <li><a href="catalogo.html">Tienda</a></li>
                                
                                <li><a href="nosotros.html">Echosline</a></li>
                                <li><a href="contacto.html">Contacto</a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-xl-3 col-md-3 mb-lg--30">
                        <div class="row">
                            <div class="col-xl-12 col-md-12 mb-lg--30">
                                <div class="footer-widget">
                                    <h3 class="widget-title widget-title--2">Síguenos</h3>
                                    <ul class="widget-menu widget-menu--2">
                                        <li>
                                            <ul class="contenedorSocials">
                                                <li><a href="https://www.facebook.com/echosline.official/"><i class="fab fa-facebook-f"></i></a></li>
                                                <li><a href="https://www.instagram.com/echosline.official/?hl=es-la"><i class="fab fa-instagram"></i></a></li>
                                            </ul>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                            <div class="col-xl-12 col-md-12 mb-lg--30">
                                <div class="footer-widget">
                                    <h3 class="widget-title widget-title--2">Métodos de pago</h3>
                                    <ul class="widget-menu widget-menu--2">
                                        <li>
                                            <ul class="contenedorPagos">
                                                <li><a href="#"><img class="meotodoPago" src="assets/img/icons/mercado-pago.png"></a></li>
                                                <li><a href="#"><img class="paypal" src="assets/img/icons/paypal.png"></a></li>
                                            </ul>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row align-items-center pt--10 pb--30">
                    <div class="col-md-4">
                    </div>
                    <div class="col-md-4 text-md-center">
                        <p class="copyright-text">© Copyright Echosline 2019</p>
                    </div>
                </div>
            </div>
        </footer>
        <!-- Footer End -->


        <!-- Search from Start --> 
        <div class="searchform__popup" id="searchForm">
            <a href="#" class="btn-close"><i class="dl-icon-close"></i></a>
            <div class="searchform__body">
                <p>Start typing and press Enter to search</p>
                <form class="searchform">
                    <input type="text" name="search" id="search" class="searchform__input" placeholder="Search Entire Store...">
                    <button type="submit" class="searchform__submit"><i class="dl-icon-search10"></i></button>
                </form>
            </div>
        </div>
        <!-- Search from End --> 
        
        <!-- Side Navigation Start -->
        <aside class="side-navigation" id="sideNav">
            <div class="side-navigation-wrapper">
                <a href="" class="btn-close"><i class="dl-icon-close"></i></a>
                <div class="side-navigation-inner">
                    <div class="widget">
                        <ul class="sidenav-menu">
                            <li><a href="about-us.html">About Airi Shop</a></li>
                            <li><a href="about-us.html">Help Center</a></li>
                            <li><a href="catalogo.html">Portfolio</a></li>
                            <li><a href="blog.html">Blog</a></li>
                            <li><a href="unete.html">New Look</a></li>
                        </ul>
                    </div>
                    <div class="widget pt--30 pr--20">
                        <div class="text-widget">
                            <p>
                                <img src="assets/img/others/payments.png" alt="payment">
                            </p>
                            <p>Pellentesque mollis nec orci id tincidunt. Sed mollis risus eu nisi aliquet, sit amet fermentum justo dapibus.</p>
                        </div>
                    </div>
                    <div class="widget">
                        <div class="text-widget">
                            <p>
                                <a href="#">(+612) 2531 5600</a>
                                <a href="mailto:info@la-studioweb.com">info@la-studioweb.com</a>
                                PO Box 1622 Colins Street West
                            </p>
                        </div>
                    </div>
                    <div class="widget">
                        <div class="text-widget google-map-link">
                            <p>
                                <a href="https://www.google.com/maps" target="_blank">Google Maps</a>
                            </p>
                        </div>
                    </div>
                    <div class="widget">
                        <div class="text-widget">
                            <!-- Social Icons Start Here -->
                            <ul class="social social-small">
                                <li class="social__item">
                                    <a href="https://twitter.com" class="social__link">
                                        <i class="fa fa-twitter"></i>
                                    </a>
                                </li>
                                <li class="social__item">
                                    <a href="https://plus.google.com" class="social__link">
                                        <i class="fa fa-google-plus"></i>
                                    </a>
                                </li>
                                <li class="social__item">
                                    <a href="https://facebook.com" class="social__link">
                                        <i class="fa fa-facebook"></i>
                                    </a>
                                </li>
                                <li class="social__item">
                                    <a href="https://youtube.com" class="social__link">
                                        <i class="fa fa-youtube"></i>
                                    </a>
                                </li>
                                <li class="social__item">
                                    <a href="https://instagram.com" class="social__link">
                                        <i class="fa fa-instagram"></i>
                                    </a>
                                </li>
                            </ul>
                            <!-- Social Icons End Here -->
                        </div>
                    </div>
                    <div class="widget">
                        <div class="text-widget">
                            <p class="copyright-text">&copy; 2018 Airi All rights reserved</p>
                        </div>
                    </div>
                </div>
            </div>
        </aside>
        <!-- Side Navigation End -->

        <!-- Mini Cart Start -->
        <aside class="mini-cart" id="miniCart">
            <div class="mini-cart-wrapper">
                <a href="" class="btn-close"><i class="dl-icon-close"></i></a>
                <div class="mini-cart-inner">
                    <h5 class="mini-cart__heading mb--40 mb-lg--30">Shopping Cart</h5>
                    <div class="mini-cart__content">
                        <ul class="mini-cart__list">
                            <li class="mini-cart__product">
                                <a href="#" class="remove-from-cart remove">
                                    <i class="dl-icon-close"></i>
                                </a>
                                <div class="mini-cart__product__image">
                                    <img src="assets/img/products/prod-17-1-70x91.jpg" alt="products">
                                </div>
                                <div class="mini-cart__product__content">
                                    <a class="mini-cart__product__title" href="detalle-producto.html">Chain print bermuda shorts  </a>
                                    <span class="mini-cart__product__quantity">1 x $49.00</span>
                                </div>
                            </li>
                            <li class="mini-cart__product">
                                <a href="#" class="remove-from-cart remove">
                                    <i class="dl-icon-close"></i>
                                </a>
                                <div class="mini-cart__product__image">
                                    <img src="assets/img/products/prod-18-1-70x91.jpg" alt="products">
                                </div>
                                <div class="mini-cart__product__content">
                                    <a class="mini-cart__product__title" href="detalle-producto.html">Waxed-effect pleated skirt</a>
                                    <span class="mini-cart__product__quantity">1 x $49.00</span>
                                </div>
                            </li>
                            <li class="mini-cart__product">
                                <a href="#" class="remove-from-cart remove">
                                    <i class="dl-icon-close"></i>
                                </a>
                                <div class="mini-cart__product__image">
                                    <img src="assets/img/products/prod-19-1-70x91.jpg" alt="products">
                                </div>
                                <div class="mini-cart__product__content">
                                    <a class="mini-cart__product__title" href="detalle-producto.html">Waxed-effect pleated skirt</a>
                                    <span class="mini-cart__product__quantity">1 x $49.00</span>
                                </div>
                            </li>
                            <li class="mini-cart__product">
                                <a href="#" class="remove-from-cart remove">
                                    <i class="dl-icon-close"></i>
                                </a>
                                <div class="mini-cart__product__image">
                                    <img src="assets/img/products/prod-2-1-70x91.jpg" alt="products">
                                </div>
                                <div class="mini-cart__product__content">
                                    <a class="mini-cart__product__title" href="detalle-producto.html">Waxed-effect pleated skirt</a>
                                    <span class="mini-cart__product__quantity">1 x $49.00</span>
                                </div>
                            </li>
                        </ul>
                        <div class="mini-cart__total">
                            <span>Subtotal</span>
                            <span class="ammount">$98.00</span>
                        </div>
                        <div class="mini-cart__buttons">
                            <a href="cart.html" class="btn btn-fullwidth btn-style-1">Ver Carrito</a>
                            <a href="checkout.html" class="btn btn-fullwidth btn-style-1">Checkout</a>
                        </div>
                    </div>
                </div>
            </div>
        </aside>
        <!-- Mini Cart End -->



    </div>
    <!-- Main Wrapper End -->


    <!-- ************************* JS Files ************************* -->

    <!-- jQuery JS -->
    <script src="assets/js/vendor/jquery.min.js"></script>

    <!-- Bootstrap and Popper Bundle JS -->
    <script src="assets/js/bootstrap.bundle.min.js"></script>

    <!-- All Plugins Js -->
    <script src="assets/js/plugins.js"></script>

    <!-- Ajax Mail Js -->
    <script src="assets/js/ajax-mail.js"></script>

    <!-- Main JS -->
    <script src="assets/js/main.js"></script>

    <!-- REVOLUTION JS FILES -->
    <script src="assets/js/revoulation/jquery.themepunch.tools.min.js"></script>
    <script src="assets/js/revoulation/jquery.themepunch.revolution.min.js"></script>    

    <!-- SLIDER REVOLUTION 5.0 EXTENSIONS  (Load Extensions only on Local File Systems !  The following part can be removed on Server for On Demand Loading) -->
    <script src="assets/js/revoulation/extensions/revolution.extension.actions.min.js"></script>
    <script src="assets/js/revoulation/extensions/revolution.extension.carousel.min.js"></script>
    <script src="assets/js/revoulation/extensions/revolution.extension.kenburn.min.js"></script>
    <script src="assets/js/revoulation/extensions/revolution.extension.layeranimation.min.js"></script>
    <script src="assets/js/revoulation/extensions/revolution.extension.migration.min.js"></script>
    <script src="assets/js/revoulation/extensions/revolution.extension.navigation.min.js"></script>
    <script src="assets/js/revoulation/extensions/revolution.extension.parallax.min.js"></script>
    <script src="assets/js/revoulation/extensions/revolution.extension.slideanims.min.js"></script>
    <script src="assets/js/revoulation/extensions/revolution.extension.video.min.js"></script>

    <!-- REVOLUTION ACTIVE JS FILES -->
    <script src="assets/js/revoulation.js"></script>
    <script src="server/controllers/ajax_cart.js"></script>
    <script src="https://secure.mlstatic.com/sdk/javascript/v1/mercadopago.js"></script>
    <script src="server/controllers/checkout.js"></script>

    <script>

    	$(document).ready(function(){
            var statusPago = "<?php echo $status; ?>";
            var idPago = "<?php echo $id_pago; ?>";
            var $idOrden = "<?php echo $id_orden; ?>";
            var idVM = "<?php echo $idVM; ?>";
            var idV = "<?php echo $idV; ?>";
            limpiar_carrito();

            actualizar_status_venta2(statusPago,idV,idVM);

            function actualizar_status_venta2(status,idVenta,idVentaMenudeo){
              if(idVenta != "" && idVentaMenudeo != ""){
                openModalTicket(idVentaMenudeo);
                //enviar_correo_pedido(idVenta);
                limpiar_carrito();
              }
              
            }
        })
    </script>
    <!--Start of Tawk.to Script-->
    <script type="text/javascript">
    var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
    (function(){
    var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
    s1.async=true;
    s1.src='https://embed.tawk.to/5e7cc9c469e9320caabd4dcf/default';
    s1.charset='UTF-8';
    s1.setAttribute('crossorigin','*');
    s0.parentNode.insertBefore(s1,s0);
    })();
    </script>
    <!--End of Tawk.to Script-->
</body>

</html>
<?php
    require_once("ticket.php");
}
?>