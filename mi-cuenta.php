<?php
if(!isset($_COOKIE["ech_lg"]) && !isset($_COOKIE["ech_lg_tmp"])){
    header("Location: login.html?r=acc");
}
?>
<!doctype html>
<html class="no-js" lang="zxx">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="meta description">
    <meta name="keywords" content="cabello, decoloracion, alaciado, balayage, nanoplastia,, queratina, hialuronico, frizz, lacio, peinado, maquillaje, piel, seca, piel grasa, tratamiento cabello, cabello maltratado, piel antiedad, matizador, tinte cabello, corte cabello moderno, estilista">
    <!-- Favicons -->
    <link rel="shortcut icon" href="assets/img/favicon.jpg" type="image/x-icon">
    <link rel="apple-touch-icon" href="assets/img/icon.png">

    <!-- Title -->
    <title>ECHOSLINE</title>

    <!-- ************************* CSS Files ************************* -->

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="assets/css/bootstrap.min.css">

    <!-- Font Awesome CSS -->
    <link rel="stylesheet" href="assets/css/font-awesome.min.css">

    <!-- dl Icon CSS -->
    <link rel="stylesheet" href="assets/css/dl-icon.css">

    <!-- All Plugins CSS -->
    <link rel="stylesheet" href="assets/css/plugins.css">

    <!-- Revoulation Slider CSS  -->
    <link rel="stylesheet" href="assets/css/revoulation.css">

    <!-- style CSS -->
    <link rel="stylesheet" href="assets/css/style.css">
    <link rel="stylesheet" href="assets/css/main.css">

    <!-- modernizr JS
    ============================================ -->
    <script src="assets/js/vendor/modernizr-2.8.3.min.js"></script>
    <!--[if lt IE 9]>
    <script src="//oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="//oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    
    <!--FONT AWESOME-->
    <script src="https://kit.fontawesome.com/816afb7ec8.js"></script>
    <script src="server/controllers/jquery/dist/jquery.js"></script>
    <script src="server/controllers/header.js"></script>
    <script src="server/controllers/get.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@9.3.16/dist/sweetalert2.all.min.js"></script>

    

    <style>
        .swal2-popup{
            font-size: 1.4rem;
        }
        #agregado{
            display: flex;
            justify-content: center;
            align-items: center;
            flex-flow: column;
        }
        #agregado img{
            max-width: 150px;
        }
    </style>
</head>

<body>


    <div class="ai-preloader active">
        <div class="ai-preloader-inner h-100 d-flex align-items-center justify-content-center">
            <div class="ai-child ai-bounce1"></div>
            <div class="ai-child ai-bounce2"></div>
            <div class="ai-child ai-bounce3"></div>
        </div>
    </div>
  
    <!-- Main Wrapper Start -->
    <div class="wrapper">
        <!-- Header Area Start -->
        <header class="header header-fullwidth header-style-4">
            <div class="header-inner fixed-header">
                <div class="container-fluid">
                    <div class="row align-items-center">
                        <div class="col-lg-2 col-md-3 col-4 order-1">
                            <div class="header-left d-flex">
                                <!-- Logo Start Here -->
                                <a href="index.html" class="logo-box">
                                    <figure class="logo--normal"> 
                                        <img src="assets/img/logo/logo.png" alt="Logo"/>   
                                    </figure>
                                    <figure class="logo--transparency">
                                        <img src="assets/img/logo/logo.png" alt="Logo"/>  
                                    </figure>
                                </a>
                                <!-- Logo End Here -->
                            </div>
                        </div>

                        <div class="col-lg-8 order-3 order-lg-2">
                            <!-- Main Navigation Start Here -->
                            <nav class="main-navigation">
                                <ul class="mainmenu mainmenu--centered">
                                    <li class="mainmenu__item">
                                        <a href="index.html" class="mainmenu__link">
                                            <span class="mm-text">INICIO</span>
                                        </a>
                                    </li>
                                    <li class="mainmenu__item menu-item-has-children megamenu-holder">
                                        <a href="linea.html" class="mainmenu__link">
                                            <span class="mm-text">LÍNEAS</span>
                                        </a>
                                        <ul class="megamenu four-column">
                                            <li>
                                                <a class="megamenu-title" href="#">
                                                    <span class="mm-text">LÍNEAS DE PRODUCTO</span>
                                                </a>
                                                <ul>
                                                    <li>
                                                        <a href="linea.html">
                                                            <span class="mm-text">Karbon 9</span>
                                                        </a>
                                                    </li>
                                                    <li>
                                                        <a href="linea-kipower.html">
                                                            <span class="mm-text">Ki-Power</span>
                                                        </a>
                                                    </li>
                                                    <li>
                                                        <a href="linea-keratin.html">
                                                            <span class="mm-text">Seliar Keratin</span>
                                                        </a>
                                                    </li>
                                                    <li>
                                                        <a href="linea-luxury.html">
                                                            <span class="mm-text">Seliar Luxury</span>
                                                        </a>
                                                    </li>
                                                </ul>
                                            </li>
                                            <li>
                                                <a class="megamenu-title" href="#">
                                                    <span class="mm-text">INGREDIENTES</span>
                                                </a>
                                                <ul>
                                                    <li>
                                                        <a href="linea.html">
                                                            <span class="mm-text">Argan</span>
                                                        </a>
                                                    </li>
                                                    <li>
                                                        <a href="linea.html">
                                                            <span class="mm-text">Ácido Hialurónico</span>
                                                        </a>
                                                    </li>
                                                    <li>
                                                        <a href="linea.html">
                                                            <span class="mm-text">Aceites Botánicos</span>
                                                        </a>
                                                    </li>
                                                    <li>
                                                        <a href="linea.html">
                                                            <span class="mm-text">Carbón Activado</span>
                                                        </a>
                                                    </li>
                                                    <li>
                                                        <a href="linea.html">
                                                            <span class="mm-text">Keratina</span>
                                                        </a>
                                                    </li>
                                                </ul>
                                            </li>
                                            <li>
                                                <a class="megamenu-title" href="#">
                                                    <span class="mm-text">TIPO DE PRODUCTO</span>
                                                </a>
                                                <ul>
                                                    <li>
                                                        <a href="linea.html">
                                                            <span class="mm-text">Fluid</span>
                                                        </a>
                                                    </li>
                                                    <li>
                                                        <a href="linea.html">
                                                            <span class="mm-text">Kits</span>
                                                        </a>
                                                    </li>
                                                    <li>
                                                        <a href="linea.html">
                                                            <span class="mm-text">Lotion</span>
                                                        </a>
                                                    </li>
                                                    <li>
                                                        <a href="linea.html">
                                                            <span class="mm-text">Mask</span>
                                                        </a>
                                                    </li>
                                                    <li>
                                                        <a href="linea.html">
                                                            <span class="mm-text">Oil</span>
                                                        </a>
                                                    </li>
                                                    <li>
                                                        <a href="linea.html">
                                                            <span class="mm-text">Serum</span>
                                                        </a>
                                                    </li>
                                                    <li>
                                                        <a href="linea.html">
                                                            <span class="mm-text">Shampoo</span>
                                                        </a>
                                                    </li>
                                                    <li>
                                                        <a href="linea.html">
                                                            <span class="mm-text">Treatment</span>
                                                        </a>
                                                    </li>
                                                </ul>
                                            </li>
                                            <li class="d-none d-lg-block banner-holder">
                                                <div class="megamenu-banner">
                                                    <div class="megamenu-banner-image"></div>
                                                    <div class="megamenu-banner-info" id="contenedorBannerM">
                                                           <h3>Suscríbete<br> y recibe un<br> regalo en tus<br> pedidos<br> mensuales</h3>
                                                        <a class="banner-btn-2" href="shop-sidebar.html">
                                                            <span class="normal-view">SABER MÁS</span>
                                                        </a>
                                                    </div>
                                                    <a href="shop-sidebar.html" class="megamenu-banner-link"></a>
                                                </div>
                                            </li>   
                                        </ul>
                                    </li>
                                    <li class="mainmenu__item">
                                        <a href="catalogo.html" class="mainmenu__link">
                                            <span class="mm-text">TIENDA</span>
                                        </a>
                                    </li>
                                    <li class="mainmenu__item">
                                        <a href="nosotros.html" class="mainmenu__link">
                                            <span class="mm-text">ECHOSLINE</span>
                                        </a>
                                    </li>
                                    <li class="mainmenu__item">
                                        <a href="blog.html" class="menuActivo mainmenu__link">
                                            <span class="mm-text">BLOG</span>
                                        </a>
                                    </li>
                                    <li class="mainmenu__item">
                                        <a href="contacto.html" class="mainmenu__link">
                                            <span class="mm-text">CONTACTO</span>
                                        </a>
                                    </li>
                                </ul>
                            </nav>
                            <!-- Main Navigation End Here -->
                        </div>

                        <div class="col-lg-2 col-sm-4 col-md-9 col-8 order-2 order-lg-3 p-0">
                            <ul class="header-toolbar text-right">
                                <li class="header-toolbar__item">
                                    <div class="col-lg-12 col-md-12 col-12 order-12 p-0">
                                        <ul class="contenedorIdioma">
                                            <li class="es langAct"><a>ES</a></li>
                                            <li class="en"><a>EN</a></li>
                                        </ul>
                                    </div>
                                </li>
                                <li class="header-toolbar__item user-info-menu-btn">
                                    <a href="#">
                                        <i class="fas fa-user"></i>
                                    </a>
                                    <ul class="user-info-menu">
                                        <li>
                                            <a href="my-account.html">Mi cuenta</a>
                                        </li>
                                        <li>
                                            <a href="cart.html">Carrito de compras</a>
                                        </li>
                                        <li>
                                            <a href="checkout.html">Check Out</a>
                                        </li>
                                        <li>
                                            <a href="wishlist.html">Lista de deseos</a>
                                        </li>
                                        <li>
                                            <a href="order-tracking.html">Número de orden</a>
                                        </li>
                                        <li>
                                            <a href="#" id="cerrarSesion" style="display: none;">Cerrar sesión</a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="header-toolbar__item">
                                    <a href="#miniCart" class="mini-cart-btn toolbar-btn">
                                        <i class="fa fa-shopping-cart" aria-hidden="true"></i>
                                        <sup class="mini-cart-count">2</sup>
                                    </a>
                                </li>
                                <li class="header-toolbar__item">
                                    <a href="#searchForm" class="search-btn toolbar-btn">
                                        <i class="dl-icon-search1"></i>
                                    </a>
                                </li>
                                <li class="header-toolbar__item d-lg-none">
                                    <a href="#" class="menu-btn"></a>                 
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </header>
        <!-- Header Area End -->

        <!-- Mobile Header area Start -->
        <header class="header-mobile">
            <div class="container-fluid">
                <div class="row align-items-center">
                    <div class="col-4">
                        <a href="index.html" class="logo-box">
                            <figure class="logo--normal">
                                <img src="assets/img/logo/logo.png" alt="Logo">
                            </figure>
                        </a>
                    </div>
                    <div class="col-8">
                        <ul class="header-toolbar text-right">
                            <li class="header-toolbar__item user-info-menu-btn">
                                <a href="#">
                                    <i class="fas fa-user"></i>
                                </a>
                                <ul class="user-info-menu">
                                    <li>
                                        <a href="my-account.html">Mi cuenta</a>
                                    </li>
                                    <li>
                                        <a href="cart.html">Carrito de compras</a>
                                    </li>
                                    <li>
                                        <a href="checkout.html">Check Out</a>
                                    </li>
                                    <li>
                                        <a href="wishlist.html">Lista de deseos</a>
                                    </li>
                                    <li>
                                        <a href="order-tracking.html">Número de orden</a>
                                    </li>
                                    <li>
                                        <a href="#" id="cerrarSesionMovil" style="display: none;">Cerrar sesión</a>
                                    </li>
                                </ul>
                            </li>
                            <li class="header-toolbar__item">
                                <a href="#miniCart" class="mini-cart-btn toolbar-btn">
                                    <i class="fa fa-shopping-cart" aria-hidden="true"></i>
                                    <sup class="mini-cart-count">2</sup>
                                </a>
                            </li>
                            <li class="header-toolbar__item">
                                <a href="#searchForm" class="search-btn toolbar-btn">
                                    <i class="dl-icon-search1"></i>
                                </a>
                            </li>
                            <li class="header-toolbar__item d-lg-none">
                                <a href="#" class="menu-btn"></a>                 
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="row">
                    <div class="col-12">
                        <!-- Mobile Navigation Start Here -->
                        <div class="mobile-navigation dl-menuwrapper" id="dl-menu">
                            <button class="dl-trigger">Abrir Menú</button>
                            <ul class="dl-menu">
                                <li>
                                    <a href="index.html">
                                        INICIO
                                    </a>
                                </li>
                                <li>
                                    <a href="linea.html">
                                        LÍNEAS
                                    </a>
                                    <ul class="dl-submenu">
                                        <li>
                                            <a class="megamenu-title" href="#">
                                                LÍNEAS DE PRODUCTO
                                            </a>
                                            <ul class="dl-submenu">
                                                <li>
                                                    <a href="linea.html">
                                                        Karbon 9
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="linea-kipower.html">
                                                        KI-Power
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="linea-keratin.html">
                                                        Seliar Keratin
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="linea-luxury.html">
                                                        Seliar Luxury
                                                    </a>
                                                </li>
                                            </ul>
                                        </li>
                                        <li>
                                            <a class="megamenu-title" href="#">
                                                Ingredientes
                                            </a>
                                            <ul class="dl-submenu">
                                                <li>
                                                    <a href="linea.html">
                                                        Argán
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="linea.html">
                                                        Ácido Hialurónico
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="linea.html">
                                                        Aceites Botánicos
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="linea.html">
                                                        Carbón Activado
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="linea.html">
                                                        Keratina
                                                    </a>
                                                </li>
                                            </ul>
                                        </li>
                                        <li>
                                            <a class="megamenu-title" href="#">
                                                TIPO DE PRODUCTO
                                            </a>
                                            <ul class="dl-submenu">
                                                <li>
                                                    <a href="linea.html">
                                                        Fluid
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="linea.html">
                                                        Kits
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="linea.html">
                                                        Lotion
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="linea.html">
                                                        Mask
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="linea.html">
                                                        Oil
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="linea.html">
                                                        Serum
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="linea.html">
                                                        Shampoo
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="linea.html">
                                                        Treatment
                                                    </a>
                                                </li>
                                            </ul>
                                        </li>
                                    </ul>
                                </li>
                                <li>
                                    <a href="catalogo.html">
                                        TIENDA
                                    </a>
                                </li>
                                <li>
                                    <a href="nosotros.html">
                                        ECHOSLINE
                                    </a>
                                </li>
                                <li>
                                    <a href="blog.html">
                                        BLOG
                                    </a>
                                </li>
                                <li>
                                    <a href="contacto.html">
                                        CONTACTO
                                    </a>
                                </li>
                            </ul>
                        </div>
                        <!-- Mobile Navigation End Here -->
                    </div>
                </div>
            </div>
        </header>
        <!-- Mobile Header area End -->


         <!-- Main Content Wrapper Start -->
        <div id="content" class="main-content-wrapper">
            <div class="page-content-inner">
                <div class="container">
                    <div class="row pt--80 pt-md--60 pt-sm--40 pb--80 pb-md--60 pb-sm--40">
                        <div class="col-12">
                            <div class="user-dashboard-tab flex-column flex-md-row">
                                <div class="user-dashboard-tab__head nav flex-md-column" role="tablist" aria-orientation="vertical">
                                    <a class="nav-link active" data-toggle="pill" role="tab" href="#dashboard">Mi panel de usuario</a>
                                    <a class="nav-link" data-toggle="pill" role="tab" href="#orders" aria-controls="orders" aria-selected="true" id="tabPedidos">pedidos</a>
                                    <!--<a class="nav-link" data-toggle="pill" role="tab" href="#downloads" aria-controls="downloads" aria-selected="true">suscripciones</a>-->
                                    <a class="nav-link" data-toggle="pill" role="tab" href="#addresses" aria-controls="addresses" aria-selected="true">domicilios</a>
                                    <a class="nav-link" data-toggle="pill" role="tab" href="#accountdetails" aria-controls="accountdetails" aria-selected="true">detalles de la cuenta</a>
                                    <a class="nav-link" href="login-register.html">salir</a>
                                </div>
                                <div class="user-dashboard-tab__content tab-content">
                                    <div class="tab-pane fade show active" id="dashboard">
                                        <p class="averta-bold font-20" id="nombre-usuario"></p>
                                        <p>En tu panel de usuario puedes <a data-toggle="pill" href="#orders">rastrear pedidos</a>,<a href="">administrar tus domicilios</a> y <a href="">cambiar tu contraseña</a><br> así como otros detalles de tu cuenta.</p>
                                    </div>
                                    <div class="tab-pane fade" id="orders">
                                        <div class="message-box mb--30 d-none">
                                            <p><i class="fa fa-check-circle"></i>Aún no hay ordenes realizadas.</p>
                                            <a href="catalogo.html">Ir a la tienda</a>
                                        </div>
                                        <p class="averta-bold font-20">Mis pedidos</p>
                                        <div class="table-content table-responsive text-black">
                                            <table class="table text-center" id="tabla-pedidos">
                                                <thead>
                                                    <tr class="bb3">
                                                        <th class="averta-bold">Orden</th>
                                                        <th class="averta-bold">Fecha</th>
                                                        <th class="averta-bold">Estatus</th>
                                                        <th class="averta-bold">Total</th>
                                                        <th class="averta-bold">Recibo</th>
                                                    </tr>
                                                </thead>
                                                <tbody >
                                                    <tr class="">
                                                        <td>1</td>
                                                        <td class="wide-column">15/10/19</td>
                                                        <td>En proceso</td>
                                                        <td class="wide-column">$49.00</td>
                                                        <td>
                                                            <a href="product-details.html" class="btn-recibo">
                                                                <i class="fas fa-receipt"></i>
                                                            </a>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>2</td>
                                                        <td class="wide-column">15/10/19</td>
                                                        <td>En proceso</td>
                                                        <td class="wide-column">$49.00</td>
                                                        <td>
                                                            <a href="product-details.html" class="btn-recibo">
                                                                <i class="fas fa-receipt"></i>
                                                            </a>
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                    <div class="tab-pane fade" id="downloads">
                                        <p class="averta-bold font-20 pb--0">Contenidos suscripción ####</p>
                                        <div class="table-content table-responsive text-black">
                                            <div class="col-lg-12 mb-md--30 p-0">
                                                <div class="row no-gutters">
                                                    <div class="col-12">
                                                        <div class="table-content table-responsive">
                                                            <table class="table text-center text-black">
                                                                <thead>
                                                                    <tr class="bb3">
                                                                        <th>&nbsp;</th>
                                                                        <th>&nbsp;</th>
                                                                        <th class="text-left averta-bold">Producto</th>
                                                                        <th class="averta-bold">Precio</th>
                                                                        <th class="averta-bold">cantidad</th>
                                                                        <th class="averta-bold">total</th>
                                                                    </tr>
                                                                </thead>
                                                                <tbody>
                                                                    <tr>
                                                                        <td class="product-remove text-left"><a href=""><i class="dl-icon-close"></i></a></td>
                                                                        <td class="product-thumbnail text-left">
                                                                            <img src="assets/img/products/prod-14-2-70x81.jpg" alt="Product Thumnail">
                                                                        </td>
                                                                        <td class="product-name text-left wide-column">
                                                                            <h3>
                                                                                <a class="text-black" href="product-details.html">Super skinny blazer</a>
                                                                            </h3>
                                                                        </td>
                                                                        <td class="product-price">
                                                                            <span class="product-price-wrapper">
                                                                                <span class="money">$49.00</span>
                                                                            </span>
                                                                        </td>
                                                                        <td class="product-quantity">
                                                                            <div class="quantity">
                                                                                <input type="number" class="quantity-input" name="qty" id="qty-1" value="1" min="1">
                                                                            </div>
                                                                        </td>
                                                                        <td class="product-total-price">
                                                                            <span class="product-price-wrapper">
                                                                                <span class="money"><strong>$49.00</strong></span>
                                                                            </span>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td class="product-remove text-left"><a href=""><i class="dl-icon-close"></i></a></td>
                                                                        <td class="product-thumbnail text-left">
                                                                            <img src="assets/img/products/prod-9-1-70x81.jpg" alt="Product Thumnail">
                                                                        </td>
                                                                        <td class="product-name text-left wide-column">
                                                                            <h3>
                                                                                <a class="text-black" href="product-details.html"> Jogging trousers</a>
                                                                            </h3>
                                                                        </td>
                                                                        <td class="product-price">
                                                                            <span class="product-price-wrapper">
                                                                                <span class="money">$49.00</span>
                                                                            </span>
                                                                        </td>
                                                                        <td class="product-quantity">
                                                                            <div class="quantity">
                                                                                <input type="number" class="quantity-input" name="qty" id="qty-2" value="1" min="1">
                                                                            </div>
                                                                        </td>
                                                                        <td class="product-total-price">
                                                                            <span class="product-price-wrapper">
                                                                                <span class="money"><strong>$49.00</strong></span>
                                                                            </span>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td class="product-remove text-left"><a href=""><i class="dl-icon-close"></i></a></td>
                                                                        <td class="product-thumbnail text-left">
                                                                            <img src="assets/img/products/prod-10-1-70x81.jpg" alt="Product Thumnail">
                                                                        </td>
                                                                        <td class="product-name text-left wide-column">
                                                                            <h3>
                                                                                <a class="text-black" href="product-details.html"> Grey blue leather backpack</a>
                                                                            </h3>
                                                                        </td>
                                                                        <td class="product-price">
                                                                            <span class="product-price-wrapper">
                                                                                <span class="money">$49.00</span>
                                                                            </span>
                                                                        </td>
                                                                        <td class="product-quantity">
                                                                            <div class="quantity">
                                                                                <input type="number" class="quantity-input" name="qty" id="qty-3" value="1" min="1">
                                                                            </div>
                                                                        </td>
                                                                        <td class="product-total-price">
                                                                            <span class="product-price-wrapper">
                                                                                <span class="money"><strong>$49.00</strong></span>
                                                                            </span>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td class="product-remove text-left"><a href=""><i class="dl-icon-close"></i></a></td>
                                                                        <td class="product-thumbnail text-left">
                                                                            <img src="assets/img/products/prod-11-1-70x81.jpg" alt="Product Thumnail">
                                                                        </td>
                                                                        <td class="product-name text-left wide-column">
                                                                            <h3>
                                                                                <a class="text-black" href="product-details.html">Dress with belt</a>
                                                                            </h3>
                                                                        </td>
                                                                        <td class="product-price">
                                                                            <span class="product-price-wrapper">
                                                                                <span class="money">$49.00</span>
                                                                            </span>
                                                                        </td>
                                                                        <td class="product-quantity">
                                                                            <div class="quantity">
                                                                                <input type="number" class="quantity-input" name="qty" id="qty-4" value="1" min="1">
                                                                            </div>
                                                                        </td>
                                                                        <td class="product-total-price">
                                                                            <span class="product-price-wrapper">
                                                                                <span class="money"><strong>$49.00</strong></span>
                                                                            </span>
                                                                        </td>
                                                                    </tr>
                                                                </tbody>
                                                            </table>
                                                        </div>  
                                                    </div>
                                                </div>
                                                <div class="row no-gutters bt-3 pt--20 mt--20">
                                                    <div class="col-sm-4">
                                                        <div class="payment-group">
                                                            <button type="button" class="btn btn-fullwidth btn-style-1">AGREGAR PRODUCTO</button>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-4 offset-sm-4">
                                                        <div class="payment-group">
                                                            <button type="button" class="btn btn-fullwidth btn-style-6">GUARDAR CAMBIOS</button>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row no-gutters pt--20 ">
                                                    <div class="col-sm-12">
                                                        <p class="alertaMinimo"><i class="fas fa-exclamation-triangle"></i> Tu paquete de suscripción debe contener más de $1,000 en producto</p>
                                                    </div>
                                                </div>
                                                <div class="row">
                                            <div class="col-md-6 mb-sm--20 text-black">
                                                <p class="mb--20 averta-bold">Suscripción ###</p>
                                                <p class="m-0 ln-0 averta-bold">Fecha de alta <span class="averta-regular">10/12/19</span></p> 
                                                <p class="m-0 ln-0 averta-bold">Fecha de corte <span class="averta-regular">10/12/19</span></p> 
                                                <p class="m-0 ln-0 averta-bold">Estatus <span class="averta-regular">Pausada</span></p> 
                                                <p class="m-0 ln-0 averta-bold">Monto <span class="averta-regular">$1,830.75</span></p>
                                                <p class="m-0 ln-0 averta-bold">Contenidos: <span class="averta-regular">Paquete diciembre 1(1 charcoal shampoo 1L, 1 keratin Mask 300 mL.)</span></p>
                                                <p class="m-0 ln-0 averta-bold">Método de pago: <span class="averta-regular">Paypal</span></p>
                                                
                                                <div class="text-block mt--20 mb--10">
                                                    <a class="averta-bold decoration-none" href="">Editar contenidos</a> | <a class="averta-bold decoration-none" href="">Eliminar suscripción</a>
                                                </div>
                                                
                                                <div class="custom-checkbox bt2 mb--10 pb--10">
                                                    <label class="contenedor">
                                                        <input checked type="checkbox" class="form__checkbox">
                                                        <span class="checkmark"></span>
                                                    </label>

                                                    <label for="" class="inline-block pl-0 form__label form__label--2 shipping-label mb-0 ">Suscripción activa (desmarca para usar)</label>
                                                </div>
                                            </div>
                                            <div class="col-md-6 mb-sm--20 text-black">
                                                <p class="mb--20 averta-bold">Suscripción ###</p>
                                                <p class="m-0 ln-0 averta-bold">Fecha de alta <span class="averta-regular">10/12/19</span></p> 
                                                <p class="m-0 ln-0 averta-bold">Fecha de corte <span class="averta-regular">10/12/19</span></p> 
                                                <p class="m-0 ln-0 averta-bold">Estatus <span class="averta-regular">Pausada</span></p> 
                                                <p class="m-0 ln-0 averta-bold">Monto <span class="averta-regular">$1,830.75</span></p>
                                                <p class="m-0 ln-0 averta-bold">Contenidos: <span class="averta-regular">Paquete diciembre 1(1 charcoal shampoo 1L, 1 keratin Mask 300 mL.)</span></p>
                                                <p class="m-0 ln-0 averta-bold">Método de pago: <span class="averta-regular">Paypal</span></p>
                                                
                                                <div class="text-block mt--20 mb--10">
                                                    <a class="averta-bold decoration-none" href="">Editar contenidos</a> | <a class="averta-bold decoration-none" href="">Eliminar suscripción</a>
                                                </div>
                                                
                                                <div class="custom-checkbox bt2 mb--10 pb--10">
                                                    <label class="contenedor">
                                                        <input type="checkbox" class="form__checkbox">
                                                        <span class="checkmark"></span>
                                                    </label>

                                                    <label for="" class="inline-block pl-0 form__label form__label--2 shipping-label mb-0 ">Suscripción activa (desmarca para usar)</label>
                                                </div>
                                            </div>
                                        </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="tab-pane fade" id="addresses">
                                        <p class="averta-bold font-20 bb3 pb--30">Mis domicilios</p>
                                        <div class="row" id="lista-domicilios">
                                            <!--<div class="col-md-6 mb-sm--20 text-black">
                                                <p class="mb--20">Oficina</p>
                                                <p class="m-0 ln-0">Privada circunvalación poniente 18</p> 
                                                <p class="m-0 ln-0">Ciudad Granja 45050</p> 
                                                <p class="m-0 ln-0">Guadalajara, Jalisco, México</p>
                                                <p class="m-0 ln-0 averta-bold">Teléfono: <span class="averta-regular">333-333-3333</span></p>
                                                
                                                <div class="text-block mt--20 mb--20">
                                                    <a class="averta-bold" href="">Editar dirección</a> | <a class="averta-bold" href="">Eliminar dirección</a>
                                                </div>
                                                
                                                <div class="custom-checkbox mb--10 bb2 pb--20">
                                                    <label class="contenedor">
                                                        <input type="checkbox" class="form__checkbox">
                                                        <span class="checkmark"></span>
                                                    </label>

                                                    <label for="" class="inline-block pl-0 form__label form__label--2 shipping-label mb-0 ">Dirección principal</label>
                                                </div>
                                            </div>
                                            <div class="col-md-6 mb-sm--20 text-black">
                                                <p class="mb--20">Casa</p>
                                                <p class="m-0 ln-0">Privada circunvalación poniente 18</p> 
                                                <p class="m-0 ln-0">Ciudad Granja 45050</p> 
                                                <p class="m-0 ln-0">Guadalajara, Jalisco, México</p>
                                                <p class="m-0 ln-0 averta-bold">Teléfono: <span class="averta-regular">333-333-3333</span></p>
                                                
                                                <div class="text-block mt--20 mb--20">
                                                    <a class="averta-bold" href="">Editar dirección</a> | <a class="averta-bold" href="">Eliminar dirección</a>
                                                </div>
                                                
                                                <div class="custom-checkbox mb--10 bb2 pb--20">
                                                    <label class="contenedor">
                                                        <input type="checkbox" class="form__checkbox">
                                                        <span class="checkmark"></span>
                                                    </label>

                                                    <label for="" class="inline-block pl-0 form__label form__label--2 shipping-label mb-0">Dirección principal</label>
                                                </div>
                                            </div>-->
                                        </div>
                                        <!--<a class="btn-add">Añadir domicilio</a>-->
                                    </div>
                                    <div class="tab-pane fade text-black" id="accountdetails">
                                        <p class="averta-bold font-20 bb3 pb--30">Detalles de la cuenta</p>
                                        <form action="javascript:void(0);" class="form form--account" id="form-cuenta">
                                            <div class="row grid-space-30 mb--20 mt--20">
                                                <div class="col-md-12 mb-sm--20">
                                                    <div class="form__group">
                                                        <label class="form__label" for="f_name">Nombre <span class="required">*</span></label>
                                                        <input type="text" name="nombre" id="nombre" class="form__input" required>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row mb--20">
                                                <div class="col-md-12">
                                                    <div class="form__group">
                                                        <label class="form__label" for="l_name">Teléfono <span class="required">*</span></label>
                                                        <input type="text" name="telefono" id="telefono" class="form__input" required>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row mb--20">
                                                <div class="col-12">
                                                    <div class="form__group">
                                                        <label class="form__label" for="email">Correo <span class="required">*</span></label>
                                                        <input type="email" name="email" id="email" class="form__input" required>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row mb--30">
                                                <div class="form__group col-12">
                                                    <label for="billing_email" class="form__label form__label--2">Fecha de nacimiento <span class="required">*</span></label>
                                                </div>
                                                <div class="form__group col-4">
                                                    <select id="dia" name="dia" class="form__input form__input--2">
                                                        <option value="" selected>Día</option>
                                                    </select>
                                                </div>

                                                <div class="form__group col-4">
                                                    <select id="mes" name="mes" class="form__input form__input--2">
                                                        <option value="" selected>Mes</option>
                                                    </select>
                                                </div>

                                                <div class="form__group col-4">
                                                    <select id="anio" name="anio" class="form__input form__input--2">
                                                        <option value="" selected>Año</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <p class="averta-bold font-20 bb3 pb--30">Cambiar mi contraseña</p>
                                            <fieldset class="form__fieldset mb--20 mt--30 bn p-0">
                                                <div class="row mb--20">
                                                    <div class="col-12">
                                                        <div class="form__group">
                                                            <label class="form__label averta-bold" for="cur_pass">Contraseña actual</label>
                                                            <input type="password" name="cur_pass" id="cur_pass" class="form__input" placeholder="" required disabled>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row mb--20">
                                                    <div class="col-12">
                                                        <div class="form__group">
                                                            <label class="form__label averta-bold" for="new_pass">Nueva contraseña</label>
                                                            <input type="password" name="new_pass" id="new_pass" class="form__input" placeholder="Llena este campo si quieres cambiar tu contraseña">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-12">
                                                        <div class="form__group">
                                                            <label class="form__label averta-bold" for="conf_new_pass">Confirmar contraseña</label>
                                                            <input type="password" name="conf_new_pass" id="conf_new_pass" class="form__input" placeholder="Llena este campo si quieres cambiar tu contraseña">
                                                        </div>
                                                    </div>
                                                </div>
                                            </fieldset>
                                            <div class="row">
                                                <div class="col-12">
                                                    <div class="form__group">
                                                        <input type="submit" value="ACTUALIZAR CAMBIOS" class="btn btn-style-1 btn-submit f-right">
                                                    </div>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Main Content Wrapper Start -->

        <!-- Footer Start -->
        <footer class="footer footer-3 bg--gris border-top">
            <div class="container">
                <div class="row pt--40 pt-md--30 mb--40 mb-sm--30">
                    <div class="col-12 text-md-center">
                        <div class="footer-widget">
                            <div class="textwidget">
                                <a href="index.html" class="footer-logo">
                                    <img src="assets/img/logo/logo-footer.png" alt="Logo">
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row mb--15 mb-sm--20">
                    <div class="col-xl-3 col-md-3 mb-lg--30">
                        <div class="footer-widget">
                            <h3 class="widget-title widget-title--2">Atención al cliente</h3>
                            <ul class="widget-menu widget-menu--2">
                                <li>Contáctanos</li>
                                <li><a href="tel:+523316706832">+52 33 1670 6832</a></li>
                                <li><a href="faqs.html">Preguntas frecuentes</a></li>
                                <li><a href="politica-devoluciones.html">Políticas de Devoluciones</a></li>
                                <li><a href="terminos-y-condiciones.html">Términos y condiciones</a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-xl-3 col-md-3 mb-lg--30">
                        <div class="footer-widget">
                            <h3 class="averta-bold widget-title widget-title--2" id="tituloCuenta">Tu cuenta</h3>
                            <ul class="widget-menu widget-menu--2">
                                <li><a href="mi-cuenta.html" id="footer_registro">Registrarme</a></li>
                                <li><a href="clientes.html" id="footer_clientes">Clientes</a></li>
                                <li><a href="pedidos.html" id="footer_pedidos">Mis pedidos</a></li>
                                <li><a href="favoritos.html" id="footer_favoritos">Mis favoritos</a></li>
                                <li><a href="salones.html" id="footer_salones">Salones y mayoristas</a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-xl-3 col-md-3 mb-lg--30">
                        <div class="footer-widget">
                            <h3 class="widget-title widget-title--2">Mapa de sitio</h3>
                            <ul class="widget-menu widget-menu--2">
                                <li><a href="index.html">Inicio</a></li>
                                <li><a href="catalogo.html">Tienda</a></li>
                                
                                <li><a href="nosotros.html">Echosline</a></li>
                                <li><a href="contacto.html">Contacto</a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-xl-3 col-md-3 mb-lg--30">
                        <div class="row">
                            <div class="col-xl-12 col-md-12 mb-lg--30">
                                <div class="footer-widget">
                                    <h3 class="widget-title widget-title--2">Síguenos</h3>
                                    <ul class="widget-menu widget-menu--2">
                                        <li>
                                            <ul class="contenedorSocials">
                                                <li><a href="https://facebook.com/echoslinemex"><i class="fab fa-facebook-f"></i></a></li>
                                                <li><a href="https://instagram.com/echoslinemex/?hl=es-la"><i class="fab fa-instagram"></i></a></li>
                                            </ul>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                            <div class="col-xl-12 col-md-12 mb-lg--30">
                                <div class="footer-widget">
                                    <h3 class="widget-title widget-title--2">Métodos de pago</h3>
                                    <ul class="widget-menu widget-menu--2">
                                        <li>
                                            <ul class="contenedorPagos">
                                                <li><a href="#"><img class="meotodoPago" src="assets/img/icons/mercado-pago.png"></a></li>
                                                <li><a href="#"><img class="paypal" src="assets/img/icons/paypal.png"></a></li>
                                            </ul>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row align-items-center pt--10 pb--30">
                    <div class="col-md-4">
                    </div>
                    <div class="col-md-4 text-md-center">
                        <p class="copyright-text">© Copyright Echosline 2019</p>
                    </div>
                </div>
            </div>
        </footer>
        <!-- Footer End -->


        <!-- Search from Start --> 
        <div class="searchform__popup" id="searchForm">
            <a href="#" class="btn-close"><i class="dl-icon-close"></i></a>
            <div class="searchform__body">
                <p>Comience a escribir y presione Enter para buscar</p>
                <form class="searchform">
                    <input type="text" name="search" id="search" class="searchform__input" placeholder="Buscar en toda la tienda...">
                    <button type="submit" class="searchform__submit"><i class="dl-icon-search10"></i></button>
                </form>
            </div>
        </div>
        <!-- Search from End --> 

        <!-- Mini Cart Start -->
        <aside class="mini-cart" id="miniCart">
            <div class="mini-cart-wrapper">
                <a href="" class="btn-close"><i class="dl-icon-close"></i></a>
                <div class="mini-cart-inner">
                    <h5 class="mini-cart__heading mb--40 mb-lg--30">Carrito de compras</h5>
                    <div class="mini-cart__content">
                        <ul class="mini-cart__list">
                            <li class="mini-cart__product">
                                <a href="#" class="remove-from-cart remove">
                                    <i class="dl-icon-close"></i>
                                </a>
                                <div class="mini-cart__product__image">
                                    <img src="assets/img/products/prod-17-1-70x91.jpg" alt="products">
                                </div>
                                <div class="mini-cart__product__content">
                                    <a class="mini-cart__product__title" href="detalle-producto.html">Chain print bermuda shorts  </a>
                                    <span class="mini-cart__product__quantity">1 x $49.00</span>
                                </div>
                            </li>
                        </ul>
                        <div class="mini-cart__total">
                            <span>Subtotal</span>
                            <span class="ammount">$98.00</span>
                        </div>
                        <div class="mini-cart__buttons">
                            <a href="cart.html" class="btn btn-fullwidth btn-style-1">Ver Carrito</a>
                            <a href="checkout.html" class="btn btn-fullwidth btn-style-1">Checkout</a>
                        </div>
                    </div>
                </div>
            </div>
        </aside>
        <!-- Mini Cart End -->

        <!-- Global Overlay Start -->
        <div class="ai-global-overlay"></div>
        <!-- Global Overlay End -->
        
        <!--
        <div class="modal modalEditProds" id="" tabindex="-1" role="dialog" aria-hidden="true">
          <div class="modal-dialog w100 " role="document">
            <div class="modal-content ">
              <div class="modal-body">
                <button type="button" class="close custom-close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true"><i class="dl-icon-close"></i></span>
                </button>
                <div class="row popProds">
                    <div class="col-lg-12 col-sm-12 mb--10 mb-md--30">
                        <p class="averta-bold font-25">Selecciona tus productos</p>
                    </div>
                    <div class="col-lg-3 col-sm-4 mb--40 mb-md--30">
                       <div class="airi-product">
                            <div class="product-inner">
                                <figure class="product-image">
                                    <div class="product-image--holder">
                                        <a href="detalle-producto.html">
                                            <img src="assets/img/products/producto3.jpg" alt="Product Image" class="primary-image">
                                            <img src="assets/img/products/producto3.jpg" alt="Product Image" class="secondary-image">
                                        </a>
                                    </div>
                                    <div class="airi-product-action">
                                        <div class="product-action">
                                            <a class="add_to_cart_btn action-btn" href="cart.html" data-toggle="tooltip" data-placement="top" title="Añadir al carrito">
                                                <i class="dl-icon-cart29"></i>
                                            </a>
                                        </div>
                                    </div>
                                </figure>
                                <div class="product-info text-center">
                                    <h3 class="product-title">
                                        <a href="detalle-producto.html">Ki-Power Lotion</a>
                                    </h3>
                                    <span class="product-price-wrapper">
                                        <span class="money">$706.81</span>
                                    </span>
                                </div>
                                <div class="product-overlay"></div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-sm-4 mb--40 mb-md--30">
                       <div class="airi-product">
                            <div class="product-inner">
                                <figure class="product-image">
                                    <div class="product-image--holder">
                                        <a href="detalle-producto.html">
                                            <img src="assets/img/products/producto3.jpg" alt="Product Image" class="primary-image">
                                            <img src="assets/img/products/producto3.jpg" alt="Product Image" class="secondary-image">
                                        </a>
                                    </div>
                                    <div class="airi-product-action">
                                        <div class="product-action">
                                            <a class="add_to_cart_btn action-btn" href="cart.html" data-toggle="tooltip" data-placement="top" title="Añadir al carrito">
                                                <i class="dl-icon-cart29"></i>
                                            </a>
                                        </div>
                                    </div>
                                </figure>
                                <div class="product-info text-center">
                                    <h3 class="product-title">
                                        <a href="detalle-producto.html">Ki-Power Lotion</a>
                                    </h3>
                                    <span class="product-price-wrapper">
                                        <span class="money">$706.81</span>
                                    </span>
                                </div>
                                <div class="product-overlay"></div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-sm-4 mb--40 mb-md--30">
                       <div class="airi-product">
                            <div class="product-inner">
                                <figure class="product-image">
                                    <div class="product-image--holder">
                                        <a href="detalle-producto.html">
                                            <img src="assets/img/products/producto3.jpg" alt="Product Image" class="primary-image">
                                            <img src="assets/img/products/producto3.jpg" alt="Product Image" class="secondary-image">
                                        </a>
                                    </div>
                                    <div class="airi-product-action">
                                        <div class="product-action">
                                            <a class="add_to_cart_btn action-btn" href="cart.html" data-toggle="tooltip" data-placement="top" title="Añadir al carrito">
                                                <i class="dl-icon-cart29"></i>
                                            </a>
                                        </div>
                                    </div>
                                </figure>
                                <div class="product-info text-center">
                                    <h3 class="product-title">
                                        <a href="detalle-producto.html">Ki-Power Lotion</a>
                                    </h3>
                                    <span class="product-price-wrapper">
                                        <span class="money">$706.81</span>
                                    </span>
                                </div>
                                <div class="product-overlay"></div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-sm-4 mb--40 mb-md--30">
                       <div class="airi-product">
                            <div class="product-inner">
                                <figure class="product-image">
                                    <div class="product-image--holder">
                                        <a href="detalle-producto.html">
                                            <img src="assets/img/products/producto3.jpg" alt="Product Image" class="primary-image">
                                            <img src="assets/img/products/producto3.jpg" alt="Product Image" class="secondary-image">
                                        </a>
                                    </div>
                                    <div class="airi-product-action">
                                        <div class="product-action">
                                            
                                            <a class="add_to_cart_btn action-btn" href="cart.html" data-toggle="tooltip" data-placement="top" title="Añadir al carrito">
                                                <i class="dl-icon-cart29"></i>
                                            </a>
                                        </div>
                                    </div>
                                </figure>
                                <div class="product-info text-center">
                                    <h3 class="product-title">
                                        <a href="detalle-producto.html">Ki-Power Lotion</a>
                                    </h3>
                                    <span class="product-price-wrapper">
                                        <span class="money">$706.81</span>
                                    </span>
                                </div>
                                <div class="product-overlay"></div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-sm-4 mb--40 mb-md--30">
                       <div class="airi-product">
                            <div class="product-inner">
                                <figure class="product-image">
                                    <div class="product-image--holder">
                                        <a href="detalle-producto.html">
                                            <img src="assets/img/products/producto3.jpg" alt="Product Image" class="primary-image">
                                            <img src="assets/img/products/producto3.jpg" alt="Product Image" class="secondary-image">
                                        </a>
                                    </div>
                                    <div class="airi-product-action">
                                        <div class="product-action">
                                            <a class="add_to_cart_btn action-btn" href="cart.html" data-toggle="tooltip" data-placement="top" title="Añadir al carrito">
                                                <i class="dl-icon-cart29"></i>
                                            </a>
                                        </div>
                                    </div>
                                </figure>
                                <div class="product-info text-center">
                                    <h3 class="product-title">
                                        <a href="detalle-producto.html">Ki-Power Lotion</a>
                                    </h3>
                                    <span class="product-price-wrapper">
                                        <span class="money">$706.81</span>
                                    </span>
                                </div>
                                <div class="product-overlay"></div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-sm-4 mb--40 mb-md--30">
                       <div class="airi-product">
                            <div class="product-inner">
                                <figure class="product-image">
                                    <div class="product-image--holder">
                                        <a href="detalle-producto.html">
                                            <img src="assets/img/products/producto3.jpg" alt="Product Image" class="primary-image">
                                            <img src="assets/img/products/producto3.jpg" alt="Product Image" class="secondary-image">
                                        </a>
                                    </div>
                                    <div class="airi-product-action">
                                        <div class="product-action">
                                            <a class="add_to_cart_btn action-btn" href="cart.html" data-toggle="tooltip" data-placement="top" title="Añadir al carrito">
                                                <i class="dl-icon-cart29"></i>
                                            </a>
                                        </div>
                                    </div>
                                </figure>
                                <div class="product-info text-center">
                                    <h3 class="product-title">
                                        <a href="detalle-producto.html">Ki-Power Lotion</a>
                                    </h3>
                                    <span class="product-price-wrapper">
                                        <span class="money">$706.81</span>
                                    </span>
                                </div>
                                <div class="product-overlay"></div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-sm-4 mb--40 mb-md--30">
                       <div class="airi-product">
                            <div class="product-inner">
                                <figure class="product-image">
                                    <div class="product-image--holder">
                                        <a href="detalle-producto.html">
                                            <img src="assets/img/products/producto3.jpg" alt="Product Image" class="primary-image">
                                            <img src="assets/img/products/producto3.jpg" alt="Product Image" class="secondary-image">
                                        </a>
                                    </div>
                                    <div class="airi-product-action">
                                        <div class="product-action">
                                            <a class="add_to_cart_btn action-btn" href="cart.html" data-toggle="tooltip" data-placement="top" title="Añadir al carrito">
                                                <i class="dl-icon-cart29"></i>
                                            </a>
                                        </div>
                                    </div>
                                </figure>
                                <div class="product-info text-center">
                                    <h3 class="product-title">
                                        <a href="detalle-producto.html">Ki-Power Lotion</a>
                                    </h3>
                                    <span class="product-price-wrapper">
                                        <span class="money">$706.81</span>
                                    </span>
                                </div>
                                <div class="product-overlay"></div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-sm-4 mb--40 mb-md--30">
                       <div class="airi-product">
                            <div class="product-inner">
                                <figure class="product-image">
                                    <div class="product-image--holder">
                                        <a href="detalle-producto.html">
                                            <img src="assets/img/products/producto3.jpg" alt="Product Image" class="primary-image">
                                            <img src="assets/img/products/producto3.jpg" alt="Product Image" class="secondary-image">
                                        </a>
                                    </div>
                                    <div class="airi-product-action">
                                        <div class="product-action">
                                            <a class="add_to_cart_btn action-btn" href="cart.html" data-toggle="tooltip" data-placement="top" title="Añadir al carrito">
                                                <i class="dl-icon-cart29"></i>
                                            </a>
                                        </div>
                                    </div>
                                </figure>
                                <div class="product-info text-center">
                                    <h3 class="product-title">
                                        <a href="detalle-producto.html">Ki-Power Lotion</a>
                                    </h3>
                                    <span class="product-price-wrapper">
                                        <span class="money">$706.81</span>
                                    </span>
                                </div>
                                <div class="product-overlay"></div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-sm-4 mb--40 mb-md--30">
                       <div class="airi-product">
                            <div class="product-inner">
                                <figure class="product-image">
                                    <div class="product-image--holder">
                                        <a href="detalle-producto.html">
                                            <img src="assets/img/products/producto3.jpg" alt="Product Image" class="primary-image">
                                            <img src="assets/img/products/producto3.jpg" alt="Product Image" class="secondary-image">
                                        </a>
                                    </div>
                                    <div class="airi-product-action">
                                        <div class="product-action">
                                            <a class="add_to_cart_btn action-btn" href="cart.html" data-toggle="tooltip" data-placement="top" title="Añadir al carrito">
                                                <i class="dl-icon-cart29"></i>
                                            </a>
                                        </div>
                                    </div>
                                </figure>
                                <div class="product-info text-center">
                                    <h3 class="product-title">
                                        <a href="detalle-producto.html">Ki-Power Lotion</a>
                                    </h3>
                                    <span class="product-price-wrapper">
                                        <span class="money">$706.81</span>
                                    </span>
                                </div>
                                <div class="product-overlay"></div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-sm-4 mb--40 mb-md--30">
                       <div class="airi-product">
                            <div class="product-inner">
                                <figure class="product-image">
                                    <div class="product-image--holder">
                                        <a href="detalle-producto.html">
                                            <img src="assets/img/products/producto3.jpg" alt="Product Image" class="primary-image">
                                            <img src="assets/img/products/producto3.jpg" alt="Product Image" class="secondary-image">
                                        </a>
                                    </div>
                                    <div class="airi-product-action">
                                        <div class="product-action">
                                            <a class="add_to_cart_btn action-btn" href="cart.html" data-toggle="tooltip" data-placement="top" title="Añadir al carrito">
                                                <i class="dl-icon-cart29"></i>
                                            </a>
                                        </div>
                                    </div>
                                </figure>
                                <div class="product-info text-center">
                                    <h3 class="product-title">
                                        <a href="detalle-producto.html">Ki-Power Lotion</a>
                                    </h3>
                                    <span class="product-price-wrapper">
                                        <span class="money">$706.81</span>
                                    </span>
                                </div>
                                <div class="product-overlay"></div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-sm-4 mb--40 mb-md--30">
                       <div class="airi-product">
                            <div class="product-inner">
                                <figure class="product-image">
                                    <div class="product-image--holder">
                                        <a href="detalle-producto.html">
                                            <img src="assets/img/products/producto3.jpg" alt="Product Image" class="primary-image">
                                            <img src="assets/img/products/producto3.jpg" alt="Product Image" class="secondary-image">
                                        </a>
                                    </div>
                                    <div class="airi-product-action">
                                        <div class="product-action">
                                            <a class="add_to_cart_btn action-btn" href="cart.html" data-toggle="tooltip" data-placement="top" title="Añadir al carrito">
                                                <i class="dl-icon-cart29"></i>
                                            </a>
                                        </div>
                                    </div>
                                </figure>
                                <div class="product-info text-center">
                                    <h3 class="product-title">
                                        <a href="detalle-producto.html">Ki-Power Lotion</a>
                                    </h3>
                                    <span class="product-price-wrapper">
                                        <span class="money">$706.81</span>
                                    </span>
                                </div>
                                <div class="product-overlay"></div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-sm-4 mb--40 mb-md--30">
                       <div class="airi-product">
                            <div class="product-inner">
                                <figure class="product-image">
                                    <div class="product-image--holder">
                                        <a href="detalle-producto.html">
                                            <img src="assets/img/products/producto3.jpg" alt="Product Image" class="primary-image">
                                            <img src="assets/img/products/producto3.jpg" alt="Product Image" class="secondary-image">
                                        </a>
                                    </div>
                                    <div class="airi-product-action">
                                        <div class="product-action">
                                            <a class="add_to_cart_btn action-btn" href="cart.html" data-toggle="tooltip" data-placement="top" title="Añadir al carrito">
                                                <i class="dl-icon-cart29"></i>
                                            </a>
                                        </div>
                                    </div>
                                </figure>
                                <div class="product-info text-center">
                                    <h3 class="product-title">
                                        <a href="detalle-producto.html">Ki-Power Lotion</a>
                                    </h3>
                                    <span class="product-price-wrapper">
                                        <span class="money">$706.81</span>
                                    </span>
                                </div>
                                <div class="product-overlay"></div>
                            </div>
                        </div>
                    </div>
                    
                </div>
              </div>
            </div>
          </div>
        </div>
        -->



    </div>
    <!-- Main Wrapper End -->


    <!-- ************************* JS Files ************************* -->

    <!-- jQuery JS -->
    <script src="assets/js/vendor/jquery.min.js"></script>

    <!-- Bootstrap and Popper Bundle JS -->
    <script src="assets/js/bootstrap.bundle.min.js"></script>

    <!-- All Plugins Js -->
    <script src="assets/js/plugins.js"></script>

    <!-- Ajax Mail Js -->
    <script src="assets/js/ajax-mail.js"></script>

    <!-- Main JS -->
    <script src="assets/js/main.js"></script>

    <!-- REVOLUTION JS FILES -->
    <script src="assets/js/revoulation/jquery.themepunch.tools.min.js"></script>
    <script src="assets/js/revoulation/jquery.themepunch.revolution.min.js"></script>    

    <!-- SLIDER REVOLUTION 5.0 EXTENSIONS  (Load Extensions only on Local File Systems !  The following part can be removed on Server for On Demand Loading) -->
    <script src="assets/js/revoulation/extensions/revolution.extension.actions.min.js"></script>
    <script src="assets/js/revoulation/extensions/revolution.extension.carousel.min.js"></script>
    <script src="assets/js/revoulation/extensions/revolution.extension.kenburn.min.js"></script>
    <script src="assets/js/revoulation/extensions/revolution.extension.layeranimation.min.js"></script>
    <script src="assets/js/revoulation/extensions/revolution.extension.migration.min.js"></script>
    <script src="assets/js/revoulation/extensions/revolution.extension.navigation.min.js"></script>
    <script src="assets/js/revoulation/extensions/revolution.extension.parallax.min.js"></script>
    <script src="assets/js/revoulation/extensions/revolution.extension.slideanims.min.js"></script>
    <script src="assets/js/revoulation/extensions/revolution.extension.video.min.js"></script>

    <!-- REVOLUTION ACTIVE JS FILES -->
    <script src="assets/js/revoulation.js"></script>
    <script src="server/controllers/ajax_cart.js"></script>
    


    <div class="modal fade" id="editar-direccion-modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">Editar dirección</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            <form action="javascript:void(0);" class="form form--account" id="form-editar-direccion">
                <div class="form-row mb--30">
                    <div class="form__group col-12">
                        <label for="billing_company" class="form__label form__label--2">Calle <span class="required">*</span></label>
                        <input type="text" name="calle" id="calle" class="form__input form__input--2" required>
                    </div>
                </div>
                <div class="form-row mb--30">
                    <div class="form__group col-6">
                        <label for="billing_company" class="form__label form__label--2">Número exterior <span class="required">*</span></label>
                        <input type="number" name="exterior" id="exterior_dom" class="form__input form__input--2" required>
                    </div>
                    <div class="form__group col-6">
                        <label for="billing_company" class="form__label form__label--2">Número interior</label>
                        <input type="number" name="interior" id="interior" class="form__input form__input--2">
                    </div>
                </div>
                <div class="form-row mb--30">
                    <div class="form__group col-6">
                        <label for="billing_company" class="form__label form__label--2">Código Postal <span class="required">*</span></label>
                        <input type="number" name="cp" id="cp" class="form__input form__input--2" placeholder="" required>
                    </div>
                    <div class="form__group col-6">
                        <label for="billing_estado" class="form__label form__label--2">Estado <span class="required">*</span></label>
                        <input type="text" name="estado" class="form__input form__input--2 mb--30" placeholder="Estado" id="estado" required>
                    </div>
                </div>
                <div class="form-row mb--30">
                    <div class="form__group col-6">
                        <label for="billing_municipio" class="form__label form__label--2">Municipio <span class="required">*</span></label>
                        <input type="text" name="municipio" class="form__input form__input--2 mb--30" placeholder="" id="municipio" required>
                    </div>
                    <div class="form__group col-6">
                        <label for="billing_colonia" class="form__label form__label--2">Colonia <span class="required">*</span></label>
                        <input type="text" name="colonia" class="form__input form__input--2 mb--30" placeholder="" id="colonia" required>
                    </div>
                </div>
                <div class="form-row">
                    <div class="form__group col-12">
                        <label for="billing_streetAddress" class="form__label form__label--2">Ingresa tu teléfono <span class="required">*</span></label>

                        <input type="text" name="tel" class="form__input form__input--2 mb--30" placeholder="Teléfono celular" id="telefono_dom" required>

                    </div>
                </div>
                <input type="submit" hidden id="enviar-form">
            </form>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
            <button type="button" class="btn btn-primary" id="guardar-domicilio">Guardar</button>
          </div>
        </div>
      </div>
    </div>

    <div class="modal fade" id="detalle-pedido-modal" tabindex="-1" role="dialog" aria-labelledby="titulo-pedido-modal" aria-hidden="true">
      <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="titulo-pedido-modal">Modal title</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            <table class="table text-center" id="tabla-detalle-pedido">
                <thead>
                    <tr class="bb3">
                        <th class="averta-bold">Producto</th>
                        <th class="averta-bold">Cantidad</th>
                        <th class="averta-bold">Precio unitario</th>
                        <th class="averta-bold">Importe</th>
                    </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
            <!--<button type="button" class="btn btn-primary">Save changes</button>-->
          </div>
        </div>
      </div>
    </div>

    <style>
        #form-editar-direccion .form__label, #tabla-detalle-pedido *{
            color:#000;
        }
    </style>
<script type="text/javascript" src="assets/get.js"></script>
<script src="server/controllers/mi-cuenta.js"></script>
<!--Start of Tawk.to Script-->
<script type="text/javascript">
var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
(function(){
var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
s1.async=true;
s1.src='https://embed.tawk.to/5e7cc9c469e9320caabd4dcf/default';
s1.charset='UTF-8';
s1.setAttribute('crossorigin','*');
s0.parentNode.insertBefore(s1,s0);
})();
</script>
<!--End of Tawk.to Script-->
</body>

</html>
<?php
require_once("ticket.php");
?>