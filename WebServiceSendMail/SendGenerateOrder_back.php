<?php 

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

// Import PHPMailer classes into the global namespace
// These must be at the top of your script, not inside a function
require 'PHPMailer/Exception.php';
require 'PHPMailer/PHPMailer.php';
require 'PHPMailer/SMTP.php';


require('Conexion.php');
$Conexion = new Conexion();
$connect = $Conexion->Connect();

session_start();

mysqli_set_charset($connect,"utf8");

$idVenta  = $_GET['idVenta'];
$Cliente  = "";
$Email    = "";
$Folio    = "";
$Fecha    = "";
$Total    = "";
$tbody    = "";

/*transferencia*/
$transferencia = $_GET["transferencia"];


$sql = "SELECT CONCAT(CL.Nombre,' ',CL.Apellidos) AS Cliente, CL.Email, VM.ID AS Folio, V.Fecha_venta AS Fecha, V.Total FROM Ventas AS V
INNER JOIN Ventas_menudeo AS VM ON V.ID = VM.idVenta
INNER JOIN Clientes_menudeo AS CL ON VM.idCliente_menudeo = CL.ID
WHERE V.ID = ". $idVenta; 

$result = $Conexion->Query($sql);

$Cliente  = $result[0]['Cliente'];
$Email    = trim($result[0]['Email']);
$Folio    = $result[0]['Folio'];
$date     = explode(" ", $result[0]['Fecha']);
$Total    = $result[0]['Total'];

$Fecha = obtenerFechaEnLetra($date[0]);


$movimientos = "SELECT DVM.Cantidad, CA.Producto, DVM.Precio_unitario, DVM.Importe FROM Ventas AS V
INNER JOIN Ventas_menudeo AS VM ON V.ID = VM.idVenta
INNER JOIN detalle_venta_menudeo AS DVM ON VM.ID = DVM.idVenta_menudeo
INNER JOIN catalogo AS CA on DVM.idCatalogo = CA.ID
WHERE V.ID = ". $idVenta."

    UNION

        SELECT DVM.Cantidad, PO.Promocion AS Producto, DVM.Precio_unitario, DVM.Importe FROM Ventas AS V
INNER JOIN Ventas_menudeo AS VM ON V.ID = VM.idVenta
INNER JOIN detalle_venta_menudeo AS DVM ON VM.ID = DVM.idVenta_menudeo
INNER JOIN promociones AS PO on DVM.idPromocion = PO.ID
WHERE V.ID = ". $idVenta."

UNION

      SELECT DVM.Cantidad, OF.Nombre AS Producto, DVM.Precio_unitario, DVM.Importe FROM Ventas AS V
INNER JOIN Ventas_menudeo AS VM ON V.ID = VM.idVenta
INNER JOIN detalle_venta_menudeo AS DVM ON VM.ID = DVM.idVenta_menudeo
INNER JOIN ofertas AS OF on DVM.idOferta = OF.ID
WHERE V.ID = ". $idVenta.""; 

$response = $Conexion->Query($movimientos);

foreach ($response as $key => $value) {
    $tbody .= "<tr>
                <td style='border: 1px solid #dddddd; text-align: left; padding: 8px;'>".$value['Cantidad']."</td>
                <td style='border: 1px solid #dddddd; text-align: left; padding: 8px;'>".$value['Producto']."</td>
                <td style='border: 1px solid #dddddd; text-align: left; padding: 8px;'>".$value['Precio_unitario']."</td>
                <td style='border: 1px solid #dddddd; text-align: left; padding: 8px;'>".$value['Importe']."</td>
            </tr>";
}

if ($Email != "") {

    // Instantiation and passing `true` enables exceptions
    $mail = new PHPMailer(true);

    try {
        //Server settings
        $mail->SMTPDebug = 0;                      // Enable verbose debug output
        $mail->isSMTP();                                            // Send using SMTP
        $mail->Host       = 'localhost';                    // Set the SMTP server to send through
        //$mail->Host       = 'avynacos.mx'; 
        //$mail->Host       = 'smtp.gmail.com';
        //$mail->Host       = 'smtpout.secureserver.net';
        $mail->SMTPAuth   = false;                                   // Enable SMTP authentication
        //$mail->SMTPAuth   = true; 
        //$mail->Username   = 'ventas@avynacos.mx';                     // SMTP username
        //$mail->Username   = 'avyna.develop@gmail.com'; 
        //$mail->Password   = 'ventasAdmin1';                               // SMTP password
        //$mail->Password   = '30deagosto95';
        //$mail->SMTPSecure = 'TLS';         // Enable TLS encryption; `PHPMailer::ENCRYPTION_SMTPS` encouraged
        $mail->SMTPAutoTLS = false; 
        $mail->Port       = 25;                                    // TCP port to connect to, use 465 for `PHPMailer::ENCRYPTION_SMTPS` above

        //Recipients
        $mail->setFrom('ventas@avynacos.mx', 'Avyna Cosmeticos');
        $mail->addAddress($Email,$Cliente);     // Add a recipient

        // Content
        $mail->isHTML(true);                                  // Set email format to HTML
        $mail->Subject = 'TU PEDIDO AVYNA HA SIDO GENERADO!';
        $fecha = date('d-m-Y');
        $nuevafecha = strtotime ( '+18 day' , strtotime ( $fecha ) ) ;
        $nuevafecha = date( 'd-m-Y' , $nuevafecha );
        $fechaEsp = fechaEsp($nuevafecha);
        $fechaRango1 = strtotime ( '+10 day' , strtotime ( $fecha ) ) ;
        $fechaRango1 = date( 'd-m-Y' , $fechaRango1 );
        $fechaEsp1 = fechaEsp($fechaRango1);
        $mail->Body    = '<h2>Hola '.$Cliente.' !</h2>
                        <h4>Hemos registrado tu pedido N°'.$Folio.'.</h4>
                        <h4>Fecha de pedido: '.$Fecha.'</h4>
                        <h4>Total del pedido: $'.$Total.'</h4>

                        <h4>Productos del pedido:</h4><br>
                        <table style="font-family: arial, sans-serif; border-collapse: collapse; width: 100%;">
                            <thead>
                                <tr>
                                    <th style="border: 1px solid #dddddd; text-align: left; padding: 8px;">Cantidad</th>
                                    <th style="border: 1px solid #dddddd; text-align: left; padding: 8px;">Producto</th>
                                    <th style="border: 1px solid #dddddd; text-align: left; padding: 8px;">Precio Unitario</th>
                                    <th style="border: 1px solid #dddddd; text-align: left; padding: 8px;">Importe</th>
                                </tr>
                            </thead>
                            <tbody>'.
                                $tbody
                            .'</tbody>
                        </table><br>
                        <h4>Pronto recibirás más noticias de tu pedido.</h4><br>
                        <p>Fecha estimada de entrega: del '.$fechaEsp1.' al '.$fechaEsp.'</p>
                        <p>Envío realizado por Estafeta</p>
                        <p><img src="https://avynacos.mx/img/status/logo_estafeta.png" style="max-width:250px;"></p>
                        <img src="http://integrattodev.cloudapp.net/WebServiceSendMail/1.png" width="400" height="50%"/><br>
                        <h4>Si tienes alguna duda comunícate con nosotros...</h4><br>
                        <a href="https://www.facebook.com/avynabeauty">https://www.facebook.com/avynabeauty</a>';

        if($transferencia)
            $mail->Body .= '<h2>Realiza tu depósito o transferencia a la siguiente cuenta:</h2>'.'<p><strong>Banco : </strong>Banorte</p>'.'<p><strong>Cuenta : </strong>0248840776</p>'.'<p><strong>Clabe : </strong>072320002488407766</p>'.'<p><strong>A nombre de : </strong>AVYNA COSMETICOS SA DE CV</p>'.'<p><strong>Referencia : </strong>'.$idVenta.'</p>'.'<p><strong>RFC  : </strong>ACO140605RN0   ( el primer "O" es letra, los demás "0" son número cero )</p>'.'<br/>'.'<p>Puedes realizar transferencia electrónica a la cuenta anterior, o realizar depósito a esa cuenta en los siguientes establecimientos...</p>'.'<p><img src="https://avynacos.mx/img/lugaresPagos.jpeg"></p>'.'<p>Posteriormente envía tu comprobante de pago por whatsapp al número: +52 1 33 1862 8933</p>'.'<br/>'.'<p>Después de enviar tu comprobante tu pedido será procesado y enviado, este proceso puede tardar hasta <span style="color: red;">2 días hábiles</span>...</p>';
        //$mail->AddEmbeddedImage('2.png','2');
        //$mail->AltBody = 'This is the body in plain text for non-HTML mail clients';

        $mail->send();
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <link href="https://fonts.googleapis.com/css?family=Dancing+Script&display=swap" rel="stylesheet">
    <title>Avyna Cosmeticos</title>
</head>
<body style="font-family: 'Dancing Script', cursive;">
    <h1 class="text-center" style="margin-top: 40px; color: #1e8587;">El correo fue enviado con exito</h1>

    <img src="Email.png" alt="" class="rounded mx-auto d-block" width="50%" height="50%">

    <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
</body>
</html>

<?php
    } catch (Exception $e) { ?>
        <!DOCTYPE html>
        <html lang="en">
        <head>
            <meta charset="UTF-8">
            <meta name="viewport" content="width=device-width, initial-scale=1.0">
            <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
            <link href="https://fonts.googleapis.com/css?family=Dancing+Script&display=swap" rel="stylesheet">
            <title>Avyna Cosmeticos</title>
        </head>
        <body style="font-family: 'Dancing Script', cursive;">
            <h1 class="text-center" style="margin-top: 40px; color: #e3a723;">Ocurrio un error al tratar enviar el correo</h1>
            <p class="text-center"><?php echo $mail->ErrorInfo ?></p>

            <img src="Error.png" alt="" class="rounded mx-auto d-block" width="50%" height="50%">

            <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
            <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
            <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
        </body>
        </html>
    <?php }

}else{
    echo 'El cliente no tiene ningún correo asignado.';
}


function obtenerFechaEnLetra($fecha){
    $dia= conocerDiaSemanaFecha($fecha);
    $num = date("j", strtotime($fecha));
    $anno = date("Y", strtotime($fecha));
    $mes = array('enero', 'febrero', 'marzo', 'abril', 'mayo', 'junio', 'julio', 'agosto', 'septiembre', 'octubre', 'noviembre', 'diciembre');
    $mes = $mes[(date('m', strtotime($fecha))*1)-1];
    return $dia.', '.$num.' de '.$mes.' del '.$anno;
}
 
function conocerDiaSemanaFecha($fecha) {
    $dias = array('Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado');
    $dia = $dias[date('w', strtotime($fecha))];
    return $dia;
}

function fechaEsp ($fecha) {
  $fecha = substr($fecha, 0, 10);
  $numeroDia = date('d', strtotime($fecha));
  $dia = date('l', strtotime($fecha));
  $mes = date('F', strtotime($fecha));
  $anio = date('Y', strtotime($fecha));
  //$dias_ES = array("Lunes", "Martes", "Miércoles", "Jueves", "Viernes", "Sábado", "Domingo");
  //$dias_EN = array("Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday");
  //$nombredia = str_replace($dias_EN, $dias_ES, $dia);
  $meses_ES = array("Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre");
  $meses_EN = array("January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December");
  $nombreMes = str_replace($meses_EN, $meses_ES, $mes);
  return $numeroDia."-".$nombreMes."-".$anio;
}

?>