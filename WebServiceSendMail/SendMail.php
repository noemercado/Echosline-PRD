<?php 

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

// Import PHPMailer classes into the global namespace
// These must be at the top of your script, not inside a function
require 'PHPMailer/Exception.php';
require 'PHPMailer/PHPMailer.php';
require 'PHPMailer/SMTP.php';


require('Conexion.php');
$Conexion = new Conexion();
$connect = $Conexion->Connect();

session_start();

mysqli_set_charset($connect,"utf8");

$idVenta  = $_GET['idVenta'];
$Cliente  = "";
$Email    = "";
$Folio    = "";


$sql = "SELECT CONCAT(CL.Nombre,' ',CL.Apellidos) AS Cliente, CL.Email, VM.ID AS Folio FROM Ventas AS V
INNER JOIN Ventas_menudeo AS VM ON V.ID = VM.idVenta
INNER JOIN Clientes_menudeo AS CL ON VM.idCliente_menudeo = CL.ID
WHERE V.ID = ". $idVenta; 

$result = $Conexion->Query($sql);

$Cliente  = $result[0]['Cliente'];
$Email    = trim($result[0]['Email']);
$Folio    = $result[0]['Folio'];
$ruta     = 'http://'.$_SERVER['HTTP_HOST'].'/WebServiceSendMail/2.png';

if ($Email != "") {

    // Instantiation and passing `true` enables exceptions
    $mail = new PHPMailer(true);

    try {
        //Server settings
        $mail->SMTPDebug = 0;                      // Enable verbose debug output
        $mail->isSMTP();                                            // Send using SMTP
        //$mail->Host       = 'smtp.gmail.com';                    // Set the SMTP server to send through
        $mail->Host       = 'smtp.gmail.com';                    // Set the SMTP server to send through
        //$mail->Host       = 'smtpout.secureserver.net';
        $mail->SMTPAuth   = true;                                   // Enable SMTP authentication
        $mail->Username   = 'avyna.develop@gmail.com';                     // SMTP username
        $mail->Password   = '30deagosto95';                               // SMTP password
        $mail->SMTPSecure = 'TLS';         // Enable TLS encryption; `PHPMailer::ENCRYPTION_SMTPS` encouraged
        $mail->Port       = 587;                                   // TCP port to connect to, use 465 for `PHPMailer::ENCRYPTION_SMTPS` above                                  // TCP port to connect to, use 465 for `PHPMailer::ENCRYPTION_SMTPS` above

        //Recipients
        $mail->setFrom('whs.mx@avyna.info', 'Avyna Cosmeticos');
        $mail->addAddress($Email,$Cliente);     // Add a recipient

        // Content
        $mail->isHTML(true);                                  // Set email format to HTML
        $mail->Subject = 'TU PEDIDO AVYNA ESTA EN PROCESO';
        $mail->Body    = '<h2>Hola '.$Cliente.'!</h2>
                        <h4>Estamos surtiendo tu pedido N°'.$Folio.' muy pronto estaremos enviándolo a tu domicilio.</h4>
                        <img src="http://integrattodev.cloudapp.net/WebServiceSendMail/2.png" width="400" height="50%"/><br>
                        <h4>Si tienes alguna duda comunícate con nosotros...</h4><br>
                        <a href="https://www.facebook.com/avynabeauty">https://www.facebook.com/avynabeauty</a>';

        //$mail->AddEmbeddedImage('2.png','2');
        //$mail->AltBody = 'This is the body in plain text for non-HTML mail clients';

        $mail->send();
        ?>

            <!DOCTYPE html>
            <html lang="en">
            <head>
                <meta charset="UTF-8">
                <meta name="viewport" content="width=device-width, initial-scale=1.0">
                <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
                <link href="https://fonts.googleapis.com/css?family=Dancing+Script&display=swap" rel="stylesheet">
                <title>Avyna Cosmeticos</title>
            </head>
            <body style="font-family: 'Dancing Script', cursive;">
                <h1 class="text-center" style="margin-top: 40px; color: #1e8587;">El correo fue enviado con exito</h1>

                <img src="Email.png" alt="" class="rounded mx-auto d-block" width="50%" height="50%">

                <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
                <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
                <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
            </body>
            </html>

<?php
    } catch (Exception $e) { ?>
        <!DOCTYPE html>
        <html lang="en">
        <head>
            <meta charset="UTF-8">
            <meta name="viewport" content="width=device-width, initial-scale=1.0">
            <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
            <link href="https://fonts.googleapis.com/css?family=Dancing+Script&display=swap" rel="stylesheet">
            <title>Avyna Cosmeticos</title>
        </head>
        <body style="font-family: 'Dancing Script', cursive;">
            <h1 class="text-center" style="margin-top: 40px; color: #e3a723;">Ocurrio un error al tratar enviar el correo</h1>
            <p class="text-center"><?php echo $mail->ErrorInfo ?></p>

            <img src="Error.png" alt="" class="rounded mx-auto d-block" width="50%" height="50%">

            <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
            <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
            <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
        </body>
        </html>
    <?php }

}else{
    echo 'El cliente no tiene ningún correo asignado.';
}

?>